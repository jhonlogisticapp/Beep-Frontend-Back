import { AxpressPage } from './app.po';

describe('axpress App', () => {
  let page: AxpressPage;

  beforeEach(() => {
    page = new AxpressPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
