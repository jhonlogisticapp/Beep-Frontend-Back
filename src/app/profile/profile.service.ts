import { Injectable }              from '@angular/core';
import { Http, Response }          from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable }              from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { ConstantUbeep } from './../constantsUbeep';

@Injectable()
export class HttpProfile {

    private _urlLogin:string = "assets/services/profile/profile.service.php";
    private _urlLoginComplete:string = "assets/services/profile/profile.service.complete.php";

    constructor(private _http: Http, private _ConstantUbeep: ConstantUbeep){

    }

  updateServiceComplete(client_id:string, name:string, email:string, local_phone:string, movil_phone:string, identify:string){

    let data = JSON.stringify({
      key:"3edcdb20e0030daab21d0ba9af4c0dc2",
      client_id: client_id,
      name: name,
      email: email,
      local_phone: local_phone,
      movil_phone: movil_phone,
      identify: identify,
      url: this._ConstantUbeep.urlServer
    });

    // console.log(data);

    let headers = new Headers({ "Access-Control-Allow-Origin": "*" });
    let options = new RequestOptions({headers: headers, method: "POST" });

    return this._http.post(this._urlLoginComplete, data, options)
        .map(res => res.json())
        .catch(this.handleError);
  }

    updateService(client_id:string, name:string, email:string, local_phone:string, movil_phone:string, identify:string, ){

      let data = JSON.stringify({
        key:"3edcdb20e0030daab21d0ba9af4c0dc2",
        client_id: client_id,
        name: name,
        email: email,
        local_phone: local_phone,
        movil_phone: movil_phone,
        identify: identify,
        url: this._ConstantUbeep.urlServer
      });

      // console.log(data);

      let headers = new Headers({ "Access-Control-Allow-Origin": "*" });
      let options = new RequestOptions({headers: headers, method: "POST" });

      return this._http.post(this._urlLogin, data, options)
          .map(res => res.json())
          .catch(this.handleError);
    }

    private handleError (error: Response) {
        // console.error(error);
        return Observable.throw(error.json().error || ' error');
    }
}
