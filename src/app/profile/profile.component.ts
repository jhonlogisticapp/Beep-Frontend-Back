import { Component, ViewChild, Injectable, Compiler, ViewContainerRef, OnInit } from '@angular/core';

import { Ng2BootstrapModule, ModalDirective }                                                                      from 'ng2-bootstrap';
import { CookieService }                                                                                           from 'angular2-cookie/services/cookies.service';
import { Profile } 																								   from './profile.form'
import { HttpProfile }                                                                                             from "./profile.service";
import { AppComponent }                                                                                            from './../app.component';

@Component({
	templateUrl: './profile.component.html',
	styleUrls: ['./profile.component.css'],
	providers: [HttpProfile]
})
export class ProfileComponent implements OnInit {


	public constructor(private _viewContainerRef: ViewContainerRef, private _compiler: Compiler, private _httpService : HttpProfile, private _cookieService:CookieService, private _AppComponent: AppComponent) {
		this.viewContainerRef = _viewContainerRef;
		this._compiler.clearCache();
	}

	ngOnInit() {
        var myDiv = document.getElementById('containerProfile');
        // console.log(myDiv);
        scrollTo(document.body, myDiv.offsetTop, 10);

        function scrollTo(element, to, duration) {
          if (duration < 0) return;
          var difference = to - element.scrollTop;
          var perTick = difference / duration * 2;

          setTimeout(function() {
            element.scrollTop = element.scrollTop + perTick;
            scrollTo(element, to, duration - 2);
          }, 10);
        }
    }


	private viewContainerRef: ViewContainerRef;
	public dataProfile = new Profile(this._cookieService.get('name'), this._cookieService.get('identify'), this._cookieService.get('email'), this._cookieService.get('movil_phone'), this._cookieService.get('id'));
	public loadingProcess = false;
	public responseMessage = "";
	public postService : string;

	submitProfile(data: any){
		// console.log(data.value);
		this.loadingProcess = true;
		this.responseMessage = "";
		// console.log(data);
		this._httpService.updateService(data.value.client_id, data.value.name, data.value.email, "", data.value.movil_phone, data.value.identify).subscribe(
			data => this.postService = JSON.stringify(data),
			error => this.responseService(this.postService, data),
			() => this.responseService(this.postService, data)
		);
	}

	responseService(data:any, f:any){
	  	let dataResponse = JSON.parse(data);
		// console.log(dataResponse);
		var thisClass = this;
		this.loadingProcess = false;
		if(dataResponse.return){
			let nameUser = (f.value.name).split(" ");
			this._AppComponent.stringNavLogin = nameUser[0];
			this._cookieService.put('name', f.value.name);
			this._cookieService.put('email', f.value.email);
			this._cookieService.put('movil_phone', f.value.movil_phone);
			this._cookieService.put('local_phone', f.value.local_phone);
			this._cookieService.put('identify', f.value.identify);
			this.responseMessage = "Los datos se han actualizado correctamente";
			//f.reset();
			setTimeout(function() {
				// console.log(thisClass);
	       		thisClass.responseMessage="";
	    	}, 4000);
		}else{
			// console.log(thisClass);
	       	thisClass.responseMessage = "Verifique los campos e intente nuevamente";
		}
	}

}
