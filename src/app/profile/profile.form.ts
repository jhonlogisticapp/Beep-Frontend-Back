export class Profile {
    constructor(
        public name: string,
        public identify: string,
		public email: string,
		public movil_phone: string,
		public client_id: string
    ) {  }
}