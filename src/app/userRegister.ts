export class RegisterUser {
    constructor(public email = null,
                public pass = null,
                public name = null,
                public localphone = null,
                public cellphone = null,
                public identify = null,
                public id_agreement = null,
                public code_agreement = null){

    }
}