import { Component, ViewChild, Injectable, Compiler, ViewContainerRef, OnInit } from '@angular/core';

import { Ng2BootstrapModule, ModalDirective }                        		    from 'ng2-bootstrap';
import { NgModel }                                                   		    from '@angular/forms';
import { Router }                                                    		    from '@angular/router';
import { CookieService }                                             		    from 'angular2-cookie/services/cookies.service';
import { HttpHistoryCorporative }                                               from "./corporative.service";
import { HttpSolicitarService }                                                 from "./../services/services.service";

@Component({
  templateUrl: './corporative.component.html',
  styleUrls: ['./corporative.component.css'],
  providers: [ HttpHistoryCorporative, HttpSolicitarService]
})
export class CorporativeComponent implements OnInit {

	private viewContainerRef: ViewContainerRef;
	postServiceHistorys : string;
	postServiceHistory : string;
	public objHistory : any = [];
	public dataService = {};
	public activeHistory = null;

	public load = true;

	responseGetCreditString: string;

	constructor(private _viewContainerRef: ViewContainerRef, private _compiler: Compiler, private _routerModule: Router, private _cookieService:CookieService, public _httpHistory : HttpHistoryCorporative, public _httpService:HttpSolicitarService) {
    	this.viewContainerRef = _viewContainerRef;
    	this._compiler.clearCache();
    }

	ngOnInit() {
		var myDiv = document.getElementById('container-history');
		// console.log(myDiv);
		scrollTo(document.body, myDiv.offsetTop, 10);

		function scrollTo(element, to, duration) {
			if (duration < 0) return;
			var difference = to - element.scrollTop;
			var perTick = difference / duration * 2;

			setTimeout(function() {
			  element.scrollTop = element.scrollTop + perTick;
			  scrollTo(element, to, duration - 2);
			}, 10);
		}

		this._httpService.getCredit(this._cookieService.get("id")).subscribe(
          data => this.responseGetCreditString = JSON.stringify(data),
          error => this.responseGetCredit(this.responseGetCreditString),
          () => this.responseGetCredit(this.responseGetCreditString)
        );

	}

	getHistorory(){
		// console.log("getHistorory");
	    this._httpHistory.getHistorys(this._cookieService.get('id'), "2").subscribe(
	      data => this.postServiceHistorys = JSON.stringify(data),
	      error => this.responseHistorys(this.postServiceHistorys),
	      () => this.responseHistorys(this.postServiceHistorys)
	    );
	}

	responseHistorys(data){
		// console.log(data);
		let dataResponse = JSON.parse(data);
		// console.log(dataResponse);
		var cont = 0;
		var thisClass = this;
		this.load = false;
		if(dataResponse.return){
			if(dataResponse.dataToday.length>0){
				dataResponse.dataToday.forEach(function(d,i){
					if(i==0){
						thisClass.getHistoryData(d.id, 0);
					}
					thisClass.objHistory.push(d);
					if(cont==i){
						if(dataResponse.dataPrevious.length>0){
							dataResponse.dataPrevious.forEach(function(dp,ip){
								thisClass.objHistory.push(dp);
							});
						}
					}
				});
			} else{
				if(dataResponse.dataPrevious.length>0){
					dataResponse.dataPrevious.forEach(function(dp,ip){
						if(ip==0){
							thisClass.getHistoryData(dp.id, 0);
						}
						thisClass.objHistory.push(dp);
					});
				}
			}
			// console.log("ok");
			//this.objHistory = dataResponse.dataPrevious;
		} else{
			// console.log("no ok");
		}
	}

	private getHistoryData(id, index){
		this.activeHistory = index;
		// console.log(event);
		// console.log(id);
		this._httpHistory.getHistory(id).subscribe(
	      data => this.postServiceHistory = JSON.stringify(data),
	      error => this.responseHistory(this.postServiceHistory),
	      () => this.responseHistory(this.postServiceHistory)
	    );
	}

	responseHistory(data){
		// console.log(data);
		let dataResponse = JSON.parse(data);
		// console.log(dataResponse);
		if(dataResponse.return){
			// console.log("ok");
			var rating = parseInt(dataResponse.data.rating);
			dataResponse.data.ratingImg = rating;
			this.dataService = dataResponse.data;
		} else{
			// console.log("no ok");
			this.dataService = {};
		}
	}

	public valueCredit = null;

	responseGetCredit(data: any){
		this.getHistorory();
		// console.log(data);
		var dataResponseGetCredit = JSON.parse(data);
		// console.log(dataResponseGetCredit);
		if(dataResponseGetCredit.return)
		{
			let creditValue = dataResponseGetCredit.data.credit;
			this.valueCredit = creditValue;
			this._cookieService.put("credit", creditValue);
			// console.log(this._cookieService.get("credit"));
		}else
		{
			// console.log("problemas al actualizar el credito");
			let creditValue = this._cookieService.get("credit");
			this.valueCredit = creditValue;
		}
	}

}
