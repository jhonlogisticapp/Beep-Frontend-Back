import { Component, ViewChild, NgZone, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { NgModel }                                               from '@angular/forms';
import { Http, Response, Headers, RequestOptions }               from '@angular/http'
import { HttpContactar }                                         from "./contact-admin.service";
import { Contact }                                               from "./contact-admin.form";

@Component({
  templateUrl: './contact-admin.component.html',
  styleUrls: ['./contact-admin.component.css'],
  providers: [HttpContactar]
})
export class ContactAdminComponent implements OnInit {

	public dataContact = new Contact("", "", "", "", "");
	public strResponseService : string;
	public loadingProcess = false;
	public responseMessage = "";
    public statusForm = false;

    constructor( private _httpService:HttpContactar ) {
    }

	ngOnInit(){
		var myDiv = document.getElementById('container-contact-admin');
		// console.log(myDiv);
		scrollTo(document.body, myDiv.offsetTop, 10);
      
		function scrollTo(element, to, duration) {
			if (duration < 0) return;
			var difference = to - element.scrollTop;
			var perTick = difference / duration * 2;
			setTimeout(function() {
				element.scrollTop = element.scrollTop + perTick;
				scrollTo(element, to, duration - 2);
			}, 10);
		}
	}

	submitEmailContact(data:any){
		let validName = false;
		let validLastname = false;
		let validEmail = false;
		let validPhone = false;
		let validReason = false;

        if(data.value.name.toString().trim().length > 0)
        {
            validName = true;
            document.getElementById("status1").setAttribute("style","display: none !important");
            document.getElementById("name").className = "form-control";
        }
        else
        {
            validName = false;
            document.getElementById("status1").setAttribute("style","display: block !important");
            var d = document.getElementById("name");
            d.className += " inputImportant";
        }

        if(data.value.lastname.toString().trim().length > 0)
        {
            validLastname = true;
            document.getElementById("status2").setAttribute("style","display: none !important");
            document.getElementById("lastname").className = "form-control";
        }
        else
        {
            validLastname = false;
            document.getElementById("status2").setAttribute("style","display: block !important");
            var d = document.getElementById("lastname");
            d.className += " inputImportant";
        }

        if(data.value.email.toString().trim().length > 0)
        {
            validEmail = true;
            document.getElementById("status3").setAttribute("style","display: none !important");
            document.getElementById("email").className = "form-control";
        }
        else
        {
            validEmail = false;
            document.getElementById("status3").setAttribute("style","display: block !important");
            var d = document.getElementById("email");
            d.className += " inputImportant";
        }

        if(data.value.phone.toString().trim().length > 0)
        {
            validPhone = true;
            document.getElementById("status4").setAttribute("style","display: none !important");
            document.getElementById("phone").className = "form-control";
        }
        else
        {
            validPhone = false;
            document.getElementById("status4").setAttribute("style","display: block !important");
            var d = document.getElementById("phone");
            d.className += " inputImportant";
        }

        if(data.value.reason.toString().trim().length > 0)
        {
            validReason = true;
            document.getElementById("status5").setAttribute("style","display: none !important");
            document.getElementById("reason").className = "form-control";
        }
        else
        {
            validReason = false;
            document.getElementById("status5").setAttribute("style","display: block !important");
            var d = document.getElementById("reason");
            d.className += " inputImportant";
        }

        if( validName && validLastname && validEmail && validPhone && validReason )
        {
            this.statusForm = false;
			this.loadingProcess = true;
			this.responseMessage="";
			// console.log(data.value);
			this._httpService.contact(data.value.name, data.value.lastname, data.value.email, data.value.phone, data.value.reason).subscribe(
	          data => this.strResponseService = JSON.stringify(data),
	          error => this.responseService(this.strResponseService, data),
	          () => this.responseService(this.strResponseService, data)
	        );
	    }
        else
        {
            this.statusForm = true;
        }
	}

	responseService(data:any, f:any){
		let dataResponse = JSON.parse(data);
		// console.log(dataResponse);
		var thisClass = this;
		this.loadingProcess = false;
		if(dataResponse.return){
			this.responseMessage=dataResponse.message;
			f.reset();
			this.dataContact = new Contact("", "", "", "", "");
			setTimeout(function() {
				// console.log(thisClass);
           		thisClass.responseMessage="";
        	}, 4000);
		}else{
			// console.log(thisClass);
           	thisClass.responseMessage="Verifique los campos e intente nuevamente";
		}
	}

    changeValidateInput(type : string, data: any){
        // console.log(this.statusForm);
        // console.log(data.value);
        if(this.statusForm){
            switch (type) {
                case "name":
                    if(data.value.name.toString().trim().length > 0)
                    {
                        document.getElementById("status1").setAttribute("style","display: none !important");
                        document.getElementById("name").className = "form-control";
                    }
                    else
                    {
                        document.getElementById("status1").setAttribute("style","display: block !important");
                        var d = document.getElementById("name");
                        d.className += " inputImportant";
                    }
                break;

                case "lastname":
                    // console.log(type);
                    if(data.value.lastname.toString().trim().length > 0)
                    {
                        document.getElementById("status2").setAttribute("style","display: none !important");
                        document.getElementById("lastname").className = "form-control";
                    }
                    else
                    {
                        document.getElementById("status2").setAttribute("style","display: block !important");
                        var d = document.getElementById("lastname");
                        d.className += " inputImportant";
                    }
                break;

                case "email":
                    if(data.value.email.toString().trim().length > 0)
                    {
                        document.getElementById("status3").setAttribute("style","display: none !important");
                        document.getElementById("email").className = "form-control";
                    }
                    else
                    {
                        document.getElementById("status3").setAttribute("style","display: block !important");
                        var d = document.getElementById("email");
                        d.className += " inputImportant";
                    }
                break;

                case "reason":
                    if(data.value.reason.toString().trim().length > 0)
                    {
                        document.getElementById("status5").setAttribute("style","display: none !important");
                        document.getElementById("reason").className = "form-control";
                    }
                    else
                    {
                        document.getElementById("status5").setAttribute("style","display: block !important");
                        var d = document.getElementById("reason");
                        d.className += " inputImportant";
                    }
                break;

                case "phone":
                    if(data.value.phone.toString().trim().length > 0)
                    {
                        document.getElementById("status4").setAttribute("style","display: none !important");
                        document.getElementById("phone").className = "form-control";
                    }
                    else
                    {
                        document.getElementById("status4").setAttribute("style","display: block !important");
                        var d = document.getElementById("phone");
                        d.className += " inputImportant";
                    }
                break;

                default:
                break;
            }
        }
    }

}
