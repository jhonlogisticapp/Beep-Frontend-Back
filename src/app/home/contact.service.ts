import { Injectable }              from '@angular/core';
import { Http, Response }          from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable }              from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { ConstantUbeep } from './../constantsUbeep';

@Injectable()
export class HttpContactar {

    private _urlLogin:string = "assets/services/home/contact.service.php";
    
    constructor(private _http: Http, private _ConstantUbeep: ConstantUbeep){ 

    }

    cotizarServicio(name:string, lastname:string, email:string, phone:string, address:string, reason:string, type:string){

      let data = JSON.stringify({ 
        key:"3edcdb20e0030daab21d0ba9af4c0dc2",
        name: name+""+lastname,
        email: email,
        phone: phone,
        address: address,
        reason: reason,
        type: type,
        class: "1",
        url: this._ConstantUbeep.urlServer
      });

      // console.log("----");
      // console.log(data);

      let headers = new Headers({ "Access-Control-Allow-Origin": "*" });
      let options = new RequestOptions({headers: headers, method: "POST" });
      
      return this._http.post(this._urlLogin, data, options)
          .map(res => res.json())
          .catch(this.handleError);
    }

    private handleError (error: Response) {
        // console.error(error);
        return Observable.throw(error.json().error || ' error');
    }
}