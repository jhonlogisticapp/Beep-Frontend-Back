import { Component, OnInit, ViewChild, Injectable, Compiler, ViewContainerRef } from '@angular/core';
import { Subscription }                                                         from 'rxjs';
import { Ng2BootstrapModule, ModalDirective }                                   from 'ng2-bootstrap';
import { NgModel }                                                              from '@angular/forms';
import { CookieService }                                                        from 'angular2-cookie/services/cookies.service';
import { Router, ActivatedRoute }                                               from '@angular/router';

import { AppComponent }                                                         from './../app.component';
import { Contact }                                                              from "./contact";
import { HttpContactar }                                                        from "./contact.service";
import { HttpPassword }                                                         from "./password.service";
import { Password }                                                             from "./password";

@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [HttpContactar, HttpPassword]

})
export class HomeComponent implements OnInit {

  private viewContainerRef: ViewContainerRef;
  public dataContact = new Contact("", "", "", "", "", "", "");
  public loadingProcess = false;
  public responseMessage = "";
  postService : string;
  responsePassword : string;
  public statusForm = false;
  public formContactContainer = false;
  //---Carousel
  public myInterval:number = 5000;
  public noWrapSlides:boolean = false;
  public slides:Array<any> = [];
  @ViewChild('modalNewPassword') public modalNewPassword:ModalDirective;
  public dataNewPassword = new Password("", "");
  private subscription: Subscription;
  private tokenPassword: string;
  public loadingProcessPassword = false;


  public constructor(private _viewContainerRef: ViewContainerRef, private _compiler: Compiler, private _routerModule: Router, private _AppComponent: AppComponent, private _httpService:HttpContactar, private _httpPassword: HttpPassword, private activatedRoute: ActivatedRoute, private _cookieService:CookieService) {
    this.viewContainerRef = _viewContainerRef;
    this._compiler.clearCache();
    for (let i = 0; i < 3; i++) {
      this.addSlide();
    }
  }

  ngOnInit() {
    var thisClass = this;
    var myDiv = document.getElementById('container-info');
    // console.log(myDiv);
    scrollTo(document.body, myDiv.offsetTop, 10);

    function scrollTo(element, to, duration) {
      if (duration < 0) return;
      var difference = to - element.scrollTop;
      var perTick = difference / duration * 2;

      setTimeout(function() {
        element.scrollTop = element.scrollTop + perTick;
        scrollTo(element, to, duration - 2);
      }, 10);
    }

    this.subscription = this.activatedRoute.queryParams.subscribe(
      (param: any) => {
        // console.log(param);
        if(param['token']!=undefined)
        {
          this._cookieService.removeAll();
          let token = param['token'];
          this.tokenPassword = token;
          // console.log(this.tokenPassword);
          // console.log(token);
          setTimeout(function() {
            thisClass.modalNewPassword.show();
          }, 10);
        }
        
    });
  }

  ngOnDestroy() {
    // prevent memory leak by unsubscribing
    this.subscription.unsubscribe();
  }
 
  public addSlide():void {
    var countImg = this.slides.length+1;
    let newWidth = 'Banner' + countImg +'.png';
    this.slides.push({
      image: `./assets/img/${newWidth}`
    });
  }



  public showFormContact(statusView){

    switch (statusView) {
      case "show":
        this.formContactContainer = true;
        document.getElementById("titile-contact").className = "col-sm-12 text-center margin-bottom-24";
        setTimeout(function() {
          document.getElementById("name-contact").focus();
        }, 100);
        break;

      case "hide":
        document.getElementById("titile-contact").className = "col-sm-16";
        this.formContactContainer = false;
        break;
      
      default:
        this.showFormContact('hide');
        break;
    }
  }

  clickBtnBanner(type:string){
    if(type=="ingresar"){
      this._AppComponent.showChildModal();
    }else if(type=="formularioContacto"){
      this._routerModule.navigateByUrl('company');
    }
  }

  submitEmailContacto(data:any){
    let validType = false;
    let validName = false;
    let validLastname = false;
    let validPhone = false;
    let validAddress = false;
    let validEmail = false;
    let validReason = false;

    if(data.value.type.toString().trim().length > 0)
    {
      validType = true;
      document.getElementById("status1").setAttribute("style","display: none !important");
      document.getElementById("type").className = "form-control";
    }
    else
    {
      validType = false;
      document.getElementById("status1").setAttribute("style","display: block !important");
      var d = document.getElementById("type");
      d.className += " inputImportant";
    }

    if(data.value.name.toString().trim().length > 0)
    {
      validName = true;
      document.getElementById("status2").setAttribute("style","display: none !important");
      document.getElementById("name-contact").className = "form-control";
    }
    else
    {
      validName = false;
      document.getElementById("status2").setAttribute("style","display: block !important");
      var d = document.getElementById("name-contact");
      d.className += " inputImportant";
    }

    if(data.value.lastname.toString().trim().length > 0)
    {
      validLastname = true;
      document.getElementById("status3").setAttribute("style","display: none !important");
      document.getElementById("lastname").className = "form-control";
    }
    else
    {
      validLastname = false;
      document.getElementById("status3").setAttribute("style","display: block !important");
      var d = document.getElementById("lastname");
      d.className += " inputImportant";
    }

    if(data.value.phone.toString().trim().length > 0)
    {
      validPhone = true;
      document.getElementById("status4").setAttribute("style","display: none !important");
      document.getElementById("phone").className = "form-control";
    }
    else
    {
      validPhone = false;
      document.getElementById("status4").setAttribute("style","display: block !important");
      var d = document.getElementById("phone");
      d.className += " inputImportant";
    }

    if(data.value.address.toString().trim().length > 0)
    {
      validAddress = true;
      document.getElementById("status5").setAttribute("style","display: none !important");
      document.getElementById("address").className = "form-control";
    }
    else
    {
      validAddress = false;
      document.getElementById("status5").setAttribute("style","display: block !important");
      var d = document.getElementById("address");
      d.className += " inputImportant";
    }

    if(data.value.email.toString().trim().length > 0)
    {
      validEmail = true;
      document.getElementById("status6").setAttribute("style","display: none !important");
      document.getElementById("email").className = "form-control";
    }
    else
    {
      validEmail = false;
      document.getElementById("status6").setAttribute("style","display: block !important");
      var d = document.getElementById("email");
      d.className += " inputImportant";
    }

    if(data.value.reason.toString().trim().length > 0)
    {
      validReason = true;
      document.getElementById("status7").setAttribute("style","display: none !important");
      document.getElementById("reason").className = "form-control";
    }
    else
    {
      validReason = false;
      document.getElementById("status7").setAttribute("style","display: block !important");
      var d = document.getElementById("reason");
      d.className += " inputImportant";
    }

    if( validType && validName && validLastname && validPhone && validAddress && validEmail && validReason )
    {
      this.statusForm = false;
      this.loadingProcess = true;
      this.responseMessage="";
      // console.log(data);
      this._httpService.cotizarServicio(data.value.name, data.value.lastname, data.value.email, data.value.phone, data.value.address, data.value.reason, data.value.type).subscribe(
        data => this.postService = JSON.stringify(data),
        error => this.correoEnviado(this.postService, data),
        () => this.correoEnviado(this.postService, data)
      );
    }
    else
    {
      this.statusForm = true;
    }
  }

  correoEnviado(data:any, f:any){
    let dataResponse = JSON.parse(data);
    // console.log(dataResponse);
    var thisClass = this;
    this.loadingProcess = false;
    if(dataResponse.return){
      this.responseMessage=dataResponse.message;
      f.reset();
      this.dataContact = new Contact("", "", "", "", "", "", "");
      setTimeout(function() {
        // console.log(thisClass);
        thisClass.responseMessage="";
      }, 4000);
    }else{
      // console.log(thisClass);
      thisClass.responseMessage="Verifique los campos e intente nuevamente";
    }
  }

  changeValidateInput(type : string, data: any){
    // console.log(this.statusForm);
    // console.log(data.value);
    if(this.statusForm){
      switch (type) {
        case "type":
          if(data.value.type.toString().trim().length > 0)
          {
            document.getElementById("status1").setAttribute("style","display: none !important");
            document.getElementById("type").className = "form-control";
          }
          else
          {
            document.getElementById("status1").setAttribute("style","display: block !important");
            var d = document.getElementById("type");
            d.className += " inputImportant";
          }
        break;

        case "name":
          if(data.value.name.toString().trim().length > 0)
          {
            document.getElementById("status2").setAttribute("style","display: none !important");
            document.getElementById("name").className = "form-control";
          }
          else
          {
            document.getElementById("status2").setAttribute("style","display: block !important");
            var d = document.getElementById("name");
            d.className += " inputImportant";
          }
        break;

        case "lastname":
          // console.log(type);
          if(data.value.lastname.toString().trim().length > 0)
          {
            document.getElementById("status3").setAttribute("style","display: none !important");
            document.getElementById("lastname").className = "form-control";
          }
          else
          {
            document.getElementById("status3").setAttribute("style","display: block !important");
            var d = document.getElementById("lastname");
            d.className += " inputImportant";
          }
        break;

        case "email":
          if(data.value.email.toString().trim().length > 0)
          {
            document.getElementById("status6").setAttribute("style","display: none !important");
            document.getElementById("email").className = "form-control";
          }
          else
          {
            document.getElementById("status6").setAttribute("style","display: block !important");
            var d = document.getElementById("email");
            d.className += " inputImportant";
          }
        break;

        case "address":
          if(data.value.address.toString().trim().length > 0)
          {
            document.getElementById("status5").setAttribute("style","display: none !important");
            document.getElementById("address").className = "form-control";
          }
          else
          {
            document.getElementById("status5").setAttribute("style","display: block !important");
            var d = document.getElementById("address");
            d.className += " inputImportant";
          }
        break;

        case "reason":
          if(data.value.reason.toString().trim().length > 0)
          {
            document.getElementById("status7").setAttribute("style","display: none !important");
            document.getElementById("reason").className = "form-control";
          }
          else
          {
            document.getElementById("status7").setAttribute("style","display: block !important");
            var d = document.getElementById("reason");
            d.className += " inputImportant";
          }
        break;

        case "phone":
          if(data.value.phone.toString().trim().length > 0)
          {
            document.getElementById("status4").setAttribute("style","display: none !important");
            document.getElementById("phone").className = "form-control";
          }
          else
          {
            document.getElementById("status4").setAttribute("style","display: block !important");
            var d = document.getElementById("phone");
            d.className += " inputImportant";
          }
        break;

        default:
        break;
      }
    }
  }

  changeNewPassword(form: any){
    // console.log(form);
    if(this.statusNewPassword)
    {
      this.loadingProcessPassword = true;
      // console.log(this.passwordNew);
      // console.log("Muy bien!");
      this._httpPassword.updatePassword(this.tokenPassword, this.passwordNew).subscribe(
        data => this.responsePassword = JSON.stringify(data),
        error => this.responseNewPasswowrd(this.responsePassword),
        () => this.responseNewPasswowrd(this.responsePassword)
      );
    }
    else
    {
      // console.log("Ya tiene un error declarado");
    }
  }

  public messageCheckPassword = "Ingresa tu nueva contraseña";
  public messageErrorCheckPassword = null;
  public messageSuccessCheckPassword = null;
  public statusNewPassword = false;
  private passwordNew : string;

  checkPassword(form: any){
    if(form.value.passwordNew.toString().length<1 && form.value.passwordConfirm.toString().length<1)
    {
      this.statusNewPassword = false;
      this.messageErrorCheckPassword = "Los campos no pueden estar vacios";
    }
    else if( form.value.passwordNew!=undefined && form.value.passwordConfirm!=undefined )
    {
      var passwordNew = form.value.passwordNew.toString();
      var passwordConfirm = form.value.passwordConfirm.toString();
      if(passwordNew==passwordConfirm)
      {
        this.passwordNew = form.value.passwordConfirm.toString();
        // console.log(this.passwordNew);
        this.statusNewPassword = true;
        this.messageSuccessCheckPassword = "Perfecto";
        this.messageErrorCheckPassword = null;
      }
      else
      {
        this.statusNewPassword = false;
        this.messageErrorCheckPassword = "Las contraseñas no coinciden";
        this.messageSuccessCheckPassword = null;
      }
    }
    else
    {
      this.statusNewPassword = false;
    }
  }

  responseNewPasswowrd(data: any){
    var thisClass = this;
    let dataResponse = JSON.parse(data);
    // console.log(dataResponse);
    if(dataResponse.status == 200 && dataResponse.return && dataResponse.message == "Contraseña actualizada correctamente")
    {
      this.messageCheckPassword = dataResponse.message;
      this.messageSuccessCheckPassword = null;
      this.messageErrorCheckPassword = null;
      setTimeout(function() {
        this.loadingProcessPassword = false;
        thisClass.modalNewPassword.hide();
        thisClass._routerModule.navigateByUrl('home');
      }, 3000);
    }
    else
    {
      this.loadingProcessPassword = false;
      this.messageCheckPassword = dataResponse.message;
      this.messageSuccessCheckPassword = null;
      this.messageErrorCheckPassword = null;
    }
  }

}
