import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { Ng2BootstrapModule, DropdownModule, ModalModule, TooltipModule } from 'ng2-bootstrap';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { RouterModule } from '@angular/router';
import { PathLocationStrategy, LocationStrategy, HashLocationStrategy } from '@angular/common';

import { ConstantUbeep } from './constantsUbeep';

import { AppComponent } from './app.component';
import { ServicesComponent } from './services/services.component';
import { HomeComponent } from './home/home.component';
import { AgreementComponent } from './agreement/agreement.component';
import { AllianceComponent } from './alliance/alliance.component';
import { BeepComponent } from './beep/beep.component';
import { CompanyComponent } from './company/company.component';
import { ContactAdminComponent } from './contact-admin/contact-admin.component';
import { CorporativeComponent } from './corporative/corporative.component';
import { FaqComponent } from './faq/faq.component';
import { HistoryComponent } from './history/history.component';
import { InviteComponent } from './invite/invite.component';
import { PressComponent } from './press/press.component';
import { PrivacyPoliciesComponent } from './privacy-policies/privacy-policies.component';
import { ProfileComponent } from './profile/profile.component';
import { RegisterCompanyComponent } from './register-company/register-company.component';
import { SecurityComponent } from './security/security.component';
import { SecurityPoliciesComponent } from './security-policies/security-policies.component';
import { VehiclesComponent } from './vehicles/vehicles.component';
import { PaymentComponent } from './payment/payment.component';


@NgModule({
  declarations: [
    AppComponent,
    ServicesComponent,
    HomeComponent,
    AgreementComponent,
    AllianceComponent,
    BeepComponent,
    CompanyComponent,
    ContactAdminComponent,
    CorporativeComponent,
    FaqComponent,
    HistoryComponent,
    InviteComponent,
    PressComponent,
    PrivacyPoliciesComponent,
    ProfileComponent,
    RegisterCompanyComponent,
    SecurityComponent,
    SecurityPoliciesComponent,
    VehiclesComponent,
    PaymentComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    Ng2BootstrapModule,
    RouterModule.forRoot([
      { path: '', redirectTo: '/home',   pathMatch: 'full' },
      { path: 'home',                    component: HomeComponent},
      { path: 'services',                component: ServicesComponent  },
      { path: 'company',                 component: CompanyComponent },
      { path: 'vehicles',                component: VehiclesComponent },
      { path: 'profile',                 component: ProfileComponent },
      { path: 'agreement',               component: AgreementComponent },
      { path: 'alliance',                component: AllianceComponent },
      { path: 'history',                 component: HistoryComponent },
      { path: 'corporative',             component: CorporativeComponent },
      { path: 'invite',                  component: InviteComponent },
      { path: 'register-company',        component: RegisterCompanyComponent },
      { path: 'privacy-policies',        component: PrivacyPoliciesComponent },
      { path: 'security-policies',       component: SecurityPoliciesComponent },
      { path: 'beep',                    component: BeepComponent },
      { path: 'press',                   component: PressComponent },
      { path: 'faq',                     component: FaqComponent },
      { path: 'security',                component: SecurityComponent },
      { path: 'contact-admin',           component: ContactAdminComponent },
      { path: 'payment',                 component: PaymentComponent}
    ], { useHash: true }),
    DropdownModule.forRoot(),
    ModalModule.forRoot(),
    TooltipModule.forRoot()
  ],
  providers: [CookieService, ConstantUbeep, {provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule { }
