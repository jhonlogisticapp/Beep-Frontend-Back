/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { BeepComponent } from './beep.component';

describe('BeepComponent', () => {
  let component: BeepComponent;
  let fixture: ComponentFixture<BeepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
