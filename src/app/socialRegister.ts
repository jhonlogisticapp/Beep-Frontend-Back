export class SocialRegister {
    constructor(
        public email: string,
        public identify: string,
        public cellPhone: string,
        public id_agreement: string,
        public code_agreement: string
    ) { }
}