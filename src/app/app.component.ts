import {Component, ViewChild, ViewContainerRef, Injectable, Compiler, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {Ng2BootstrapModule, ModalDirective, TooltipModule, DropdownModule} from 'ng2-bootstrap';
import {Http, Response, Headers, RequestOptions} from '@angular/http'
import {CookieService} from 'angular2-cookie/services/cookies.service';
import {Router, ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';
import {NgModel} from '@angular/forms';

import {HttpForgotService} from "./../assets/services/auth/forgot/forgot.component";
import {HttpRegisterService} from "./../assets/services/auth/register/register.component";
import {LoginComponent} from './../assets/services/auth/login/login.component';
import {ServicesComponent} from './services/services.component';
import {HttpService} from './services/http.service';
import {HttpProfile} from "./profile/profile.service";
import {Login} from './login';
import {Register} from './register';
import {Forgot} from './forgot';
import {DataUser} from './data.user';
import {ConstantUbeep} from './constantsUbeep';
import {SocialRegister} from './socialRegister';
import {RegisterUser} from './userRegister';

declare const FB: any;
declare const gapi: any;
declare const SHA256: any;

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    providers: [HttpForgotService, HttpRegisterService, LoginComponent, HttpService, TooltipModule, HttpProfile]
})
export class AppComponent {

    public stringNavLogin = 'Ingresar';
    public statusLogin = false;
    public statusInputUserNameRegister = true;
    public statusInputPasswordRegister = true;
    public statusInputEmailRegister = true;
    public statusInputCellPhoneRegister = true;
    public disabled: boolean = false;
    public status: { isopen: boolean } = {isopen: false};
    public items: Array<string> = ['The first choice!', 'And another choice for you.', 'but wait! A third!'];
    public isCollapsed: boolean = true;
    public titleModal = 'Iniciar Sesión';
    public login = true;
    public register = false;
    public forgotpassword = false;
    public messageRegister = '';
    public messageLogin = '';
    public messageForgot = 'Escribe tu dirección de correo electrónico para reestablecer tu contraseña';
    public loadingProcess = false;
    public loadingFacebookProcess = false;
    public loadingFacebookGoogle = false;
    postMyCarToServer: any;
    postServiceLogin: string;
    postInitialSetup: string;
    @ViewChild('staticModal') public staticModal: ModalDirective;
    @ViewChild('modalLogUot') public modalLogUot: ModalDirective;
    @ViewChild('modalConfirmCancelService') public modalConfirmCancelService: ModalDirective;
    @ViewChild('modalExitViewServices') public modalExitViewServices: ModalDirective;

    @ViewChild('timeSocketModal') public timeSocketModal: ModalDirective;
    @ViewChild('modalCompleteDataUser') public modalCompleteDataUser: ModalDirective;
    @ViewChild('modalCompleteDataUserSocial') public modalCompleteDataUserSocial: ModalDirective;
    @ViewChild('modalCompleteDataUserSocialEmailSave') public modalCompleteDataUserSocialEmailSave: ModalDirective;

    public dataLogin = new Login('', '');
    public dataRegister = new Register('', '', '', '', '');
    public dataForgot = new Forgot('');
    public dataUser = new DataUser('', '');
    public socialRegister = new SocialRegister('', '', '', '', '');

    public objInitialSetup = [];
    public select_id_agreement = 0;

    private subscription: Subscription;
    public completeDataUser = false;
    public accessToken = null;

    public viewEmail = false;
    public viewHistory = false;

    typeRegisterSocial = '';

    public registerUserData = new RegisterUser();

    constructor(private _viewContainerRef: ViewContainerRef,
                public _cookieService: CookieService,
                private _routerModule: Router,
                private _compiler: Compiler,
                private _httpForgotService: HttpForgotService,
                private _httpRegisterService: HttpRegisterService,
                private _LoginComponent: LoginComponent,
                private _HttpService: HttpService,
                private activatedRoute: ActivatedRoute,
                private _ConstantUbeep: ConstantUbeep,
                private _HttpProfile: HttpProfile) {
        this._viewContainerRef = _viewContainerRef;
        this._compiler.clearCache();
        var thisClass = this;
        setTimeout(function () {
            //---Carga Facebook
            FB.init({
                appId: '555091548022084',
                cookie: false,
                xfbml: true,
                version: 'v2.8'
            });

            //---Carga Google
            gapi.load('client:auth2', thisClass.initClient());
            if (thisClass._cookieService.get('name') != undefined && thisClass._cookieService.get('id') != undefined) {
                thisClass.statusLogin = true;
                let nameUser = (thisClass._cookieService.get('name')).split(" ");
                thisClass.stringNavLogin = nameUser[0];
            }
        }, 600);

    }

    ngOnInit() {
        if (this._cookieService.get('credit') !== undefined) {
            this.viewHistory = true;
        } else {
            this.viewHistory = false;
        }

        this._cookieService.put('validRequiestService', 'true');
        const thisClass = this;
        this.subscription = this.activatedRoute.queryParams.subscribe(
            (param: any) => {
                // console.log(param);
                if (param['r'] === '1' || param['r'] === 1) {
                    setTimeout(function () {
                        thisClass.showChildModalRegisterInvite();
                    }, 10);
                }
            });

        window.addEventListener('scroll', function (event) {
            const sizePageY = window.scrollY || document.body.scrollTop;
            if (sizePageY > 49) {
                document.getElementById('header').style.minHeight = '10px';
                document.getElementById('header').style.height = '92px';
                document.getElementById('header').style.paddingTop = '13px';
                document.getElementById('logoUbeep').style.width = '208px';
            } else {
                document.getElementById('header').style.minHeight = '133px';
                document.getElementById('header').style.height = '133px';
                document.getElementById('header').style.paddingTop = '30px';
                document.getElementById('logoUbeep').style.width = '240px';
            }
        });

        document.getElementById('bodyUbeep').onclick = function () {
            if (document.getElementById('dropdown-beep') != null) {
                document.getElementById('dropdown-beep').style.display = 'none';
            }

            if (document.getElementById('dropdown-user-login') != null) {
                document.getElementById('dropdown-user-login').style.display = 'none';
            }
        }

        this._httpRegisterService.initialSetup(this._ConstantUbeep.urlServer).subscribe(
            data => this.postInitialSetup = JSON.stringify(data),
            error => console.log(''),
            () => this.initialSetup(this.postInitialSetup)
        );
    }

    initialSetup(data: any) {
        const dataResponse = JSON.parse(data);
        // console.log(dataResponse);
        if (dataResponse.return) {
            this.objInitialSetup = dataResponse.data;
            this.objInitialSetup.push({'name': 'Seleccione un convenio', 'id': 0});
        } else {
            // console.log(dataResponse.message);
            // alert("no entra!");
        }
    }

    showDropdownBeep() {
        document.getElementById('dropdown-beep').style.display = 'block';
        if (document.getElementById('dropdown-user-login') != null) {
            document.getElementById('dropdown-user-login').style.display = 'none';
        }
    }

    showDropdownUser() {
        document.getElementById('dropdown-user-login').style.display = 'block';
        if (document.getElementById('dropdown-beep') != null) {
            document.getElementById('dropdown-beep').style.display = 'none';
        }
    }

    // Registro de usuario
    registerUser(f: any) {
        this.loadingProcess = true;
        console.log(f.value, this.registerUserData);
        this._httpRegisterService.registerAcount(
            this.registerUserData.name,
            SHA256(this.registerUserData.pass),
            this.registerUserData.email,
            this.registerUserData.cellphone,
            this.registerUserData.identify,
            this._ConstantUbeep.urlServer,
            this.registerUserData.id_agreement,
            this.registerUserData.code_agreement).subscribe(
            data => this.postMyCarToServer = JSON.stringify(data),
            error => console.log(''),
            () => this.registerSuccess(this.postMyCarToServer, f)
        );
    }

    registerSuccess(data: any, f: any) {
        this.loadingProcess = false;
        const dataResponse = JSON.parse(data);
        // console.log(dataResponse);
        if (dataResponse.return && dataResponse.message === 'Usuario logueado exitosamente') {
            // alert("logeado");

            this._cookieService.put('statusServices', 'false');
            this._cookieService.put('id', dataResponse.data.user.id);
            this._cookieService.put('name', dataResponse.data.user.name);
            this._cookieService.put('movil_phone', dataResponse.data.user.phone);
            this._cookieService.put('social_id', dataResponse.data.user.social_id);
            this._cookieService.put('identify', dataResponse.data.user.identify);
            this._cookieService.put('email', dataResponse.data.user.email);

            // console.log(dataResponse.data.user.socket);
            const sockets = (dataResponse.data.user.socket).split(',');

            // console.log(sockets);

            this._cookieService.put('socket1', sockets[0]);
            this._cookieService.put('socket2', sockets[1]);
            this._cookieService.put('socket3', sockets[2]);

            this._cookieService.put('service_provider_id_1', dataResponse.data.menu[0].service_provider_id);
            this._cookieService.put('type_service_1', dataResponse.data.menu[0].type_service);
            this._cookieService.put('title_1', dataResponse.data.menu[0].title);
            this._cookieService.put('text_description_1', dataResponse.data.menu[0].text_description);
            this._cookieService.put('bag_services_1', dataResponse.data.menu[0].bag_services.length);

            this._cookieService.put('shipping_bag_id_1', dataResponse.data.menu[0].bag_services[0].shipping_bag_id);
            this._cookieService.put('title_shipping_bag_id_1', dataResponse.data.menu[0].bag_services[0].title);
            this._cookieService.put('subtitle_shipping_bag_id_1', dataResponse.data.menu[0].bag_services[0].subtitle);

            this._cookieService.put('shipping_bag_id_2', dataResponse.data.menu[0].bag_services[1].shipping_bag_id);
            this._cookieService.put('title_shipping_bag_id_2', dataResponse.data.menu[0].bag_services[1].title);
            this._cookieService.put('subtitle_shipping_bag_id_2', dataResponse.data.menu[0].bag_services[1].subtitle);

            this._cookieService.put('shipping_bag_id_3', dataResponse.data.menu[0].bag_services[2].shipping_bag_id);
            this._cookieService.put('title_shipping_bag_id_3', dataResponse.data.menu[0].bag_services[2].title);
            this._cookieService.put('subtitle_shipping_bag_id_3', dataResponse.data.menu[0].bag_services[2].subtitle);

            this._cookieService.put('service_provider_id_2', dataResponse.data.menu[1].service_provider_id);
            this._cookieService.put('type_service_2', dataResponse.data.menu[1].type_service);
            this._cookieService.put('title_2', dataResponse.data.menu[1].title);
            this._cookieService.put('text_description_2', dataResponse.data.menu[1].text_description);
            this._cookieService.put('bag_services_2', dataResponse.data.menu[1].bag_services.length);

            this._cookieService.put('shipping_bag_id_4', dataResponse.data.menu[1].bag_services[0].shipping_bag_id);
            this._cookieService.put('title_shipping_bag_id_4', dataResponse.data.menu[1].bag_services[0].title);
            this._cookieService.put('subtitle_shipping_bag_id_4', dataResponse.data.menu[1].bag_services[0].subtitle);

            if (dataResponse.data.user.credit_card[0]) {
                this._cookieService.put('credit_default', dataResponse.data.user.credit_card[0].default);
                this._cookieService.put('credit_franchise', dataResponse.data.user.credit_card[0].franchise);
                this._cookieService.put('credit_last_digits', dataResponse.data.user.credit_card[0].last_digits);
            }



            this._cookieService.put('pay_type', dataResponse.data.user.pay);

            if (dataResponse.data.user.credit !== undefined) {
                this._cookieService.put('credit_default', dataResponse.data.user.credit);
            }

            if (this._cookieService.get('name') !== undefined && this._cookieService.get('id') !== undefined) {
                const nameUser = (this._cookieService.get('name')).split(' ');
                this.statusLogin = true;
                this.stringNavLogin = nameUser[0];
            }
            this.dataLogin = new Login('', '');
            this.dataRegister = new Register('', '', '', '', '');
            this.dataForgot = new Forgot('');
            this.socialRegister = new SocialRegister('', '', '', '', '');
            this.registerUserData = new RegisterUser();
            this.staticModal.hide();

            if (dataResponse.data.user.phone.trim().length < 1 || dataResponse.data.user.identify.trim().length < 1) {
                this.completeDataUser = true;
                this.showChildModalCompleteData();
            }
            // console.log("hola yo soy el 2");
            // location.reload();
            // f.reset();
            // Redirecciona al home de usuario
            // this._routerModule.navigateByUrl('solicitar');
        } else if ((dataResponse.return === false && dataResponse.message === 'El correo ya se encuentra registrado , por favor vaya a la sección de logueo')) {
            alert("El correo ya se encuentra registrado , por favor vaya a la sección de logueo");
        } else if ((dataResponse.return === false && dataResponse.message === 'El correo ya se encuentra registrado con la red social Facebook, por favor vaya a la sección de logueo')) {
            this.messageRegister = dataResponse.message;
        } else {
            this.messageRegister = dataResponse.message;
            // alert("no entra!");
        }
    }


    // Login de usuario normal
    loginUser(f: any, type: any, typeSocial: string) {
        // console.log(this);
        // console.log("hara loginnnn", f, type);
        if (type) {
            this.loadingProcess = true;
            // Normal
            this._LoginComponent.loginUser(f.value.emailLogin, SHA256(f.value.passwordLogin), this._ConstantUbeep.urlServer).subscribe(
                data => this.postMyCarToServer = data,
                error => this.loginError(this.postMyCarToServer),
                () => this.loginSuccess(this.postMyCarToServer, f)
            );
        } else {
            // Facebook y Google
            this.typeRegisterSocial = typeSocial;
            this.accessToken = f;
            // this.showChildModalCompleteDataSocial();
            this._LoginComponent.loginCheckUserSocial(f, this._ConstantUbeep.urlServer).subscribe(
                data => this.postMyCarToServer = data,
                error => this.loginError(this.postMyCarToServer),
                () => this.loginSuccess(this.postMyCarToServer, f)
            );
        }
    }



    loginSuccess(data: any, f: any) {
        const dataResponse = data;
        // let dataResponse = JSON.parse(data);
        // console.log("LOGIN SUCCESS", data, f);
        // console.log(dataResponse);

        this.loadingProcess = false;
        this.loadingFacebookProcess = false;
        this.loadingFacebookGoogle = false;
        if (dataResponse.return && dataResponse.message === 'Usuario logueado exitosamente') {
            this.hideChildModalCompleteDataSocial();
            if (this._cookieService.get('name') !== undefined && this._cookieService.get('id') !== undefined) {
                const nameUser = (this._cookieService.get('name')).split(" ");
                this.statusLogin = true;
                this.stringNavLogin = nameUser[0];
            }
            if (this._cookieService.get('credit') !== undefined) {
                this.viewHistory = true;
            } else {
                this.viewHistory = false;
            }

            // f.reset();

            this.titleModal = 'Iniciar Sesión';
            this.messageForgot = 'Escribe tu dirección de correo electrónico para reestablecer tu contraseña';
            this.messageRegister = '';
            this.login = true;
            this.register = false;
            this.forgotpassword = false;
            this.staticModal.hide();
            this.loadingFacebookProcess = false;
            this.loadingFacebookGoogle = false;
            this.loadingProcess = false;
            this.viewEmail = false;
            this.staticModal.hide();
            this.registerUserData = new RegisterUser();
            // console.log(window.location.hash);
            this.dataLogin = new Login('', '');
            this.dataRegister = new Register('', '', '', '', '');
            this.dataForgot = new Forgot('');
            this.socialRegister = new SocialRegister('', '', '', '', '');

            if (dataResponse.data.user.credit_card[0]) {
                this._cookieService.put('credit_default', dataResponse.data.user.credit_card[0].default);
                this._cookieService.put('credit_franchise', dataResponse.data.user.credit_card[0].franchise);
                this._cookieService.put('credit_last_digits', dataResponse.data.user.credit_card[0].last_digits);
            }
            this._cookieService.put('pay_type', dataResponse.data.user.pay);

            if (window.location.hash === '#/services') {
                // console.log(this);
                ServicesComponent.prototype.loginSuccess();
                // console.log(ServiceComponent.prototype);
                // this._ServiceComponent.loginSuccess();
            }
            if (dataResponse.data.user.phone.trim().length < 1 || dataResponse.data.user.identify.trim().length < 1) {
                this.completeDataUser = true;
                this.showChildModalCompleteData();
            }
            // location.reload();
        } else if (!dataResponse.return && dataResponse.message === 'Email requerido') {
            // console.log("Email requeridoooooooo");
            // if ( this.dataUserLogin.email == null ) {
            document.getElementById('btnViewEmail').click();
            this.viewEmail = true;
            // } else {
            //   this.showChildModalCompleteDataUserSocialEmailSave();
            // }
        } else {
            this.messageLogin = dataResponse.message;
        }
    }

    loginUserFacebook(f: any) {
        this.loadingProcess = true;
        if (this.typeRegisterSocial === 'google') {
            this._LoginComponent.loginUserSocial(
                this.accessToken,
                this._ConstantUbeep.urlServer,
                f.value.id_agreement,
                f.value.code_agreement,
                f.value.cellPhone,
                f.value.identify
            ).subscribe(
                data => this.postMyCarToServer = data,
                error => this.loginError(this.postMyCarToServer),
                () => this.loginSuccess(this.postMyCarToServer, f)
            );
        } else {
            this._LoginComponent.loginUserFacebook(
                f.value.emailLogin,
                this.accessToken,
                this._ConstantUbeep.urlServer,
                f.value.id_agreement,
                f.value.code_agreement,
                f.value.cellPhone,
                f.value.identify
            ).subscribe(
                data => this.postMyCarToServer = data,
                error => this.loginError(this.postMyCarToServer),
                () => this.loginSuccess(this.postMyCarToServer, f)
            );
        }

    }

    loginError(data: any) {
        // console.log("LOGIN RROR", data);
        this.loadingProcess = false;
        this.messageLogin = "Credenciales incorrectas";
        this.loadingFacebookProcess = false;
        this.loadingFacebookProcess = false;
        this.loadingFacebookGoogle = false;
        this.loadingProcess = false;
    }

    // Cerrar sesion
    public logOutUser(type: string) {
        if (type === 'si') {
            this._cookieService.removeAll();
            this.stringNavLogin = 'Ingresar';
            this.statusLogin = false;
            this.titleModal = 'Iniciar Sesión';
            this.login = true;
            this.register = false;
            this.forgotpassword = false;
            this.modalLogUot.hide();
            this.registerUserData = new RegisterUser();
            this._routerModule.navigateByUrl('home');
            // location.reload();
        } else {
            this.modalLogUot.hide();
        }
    }

    // Recordar contraseña
    forgotPassword(f: any) {
        this.loadingProcess = true;
        this._httpForgotService.forgotAcount(f.value.emailForgot, this._ConstantUbeep.urlServer).subscribe(
            data => this.postMyCarToServer = JSON.stringify(data),
            error => console.log(""),
            () => this.forgotSuccess(this.postMyCarToServer, f, this.staticModal, this.messageForgot, this.titleModal, this.login, this.register, this.forgotpassword)
        );
    }

    forgotSuccess(data: any,
                  f: any,
                  modal: any,
                  messageForgot: string,
                  titleModal: string,
                  login: boolean,
                  register: boolean,
                  forgotpassword: boolean) {
        this.loadingProcess = false;
        const dataResponse = JSON.parse(data);
        if (dataResponse.return) {
            // alert("logeado");
            this.messageForgot = dataResponse.message;
            // setTimeout(function() {
            // setTimeout( function() {
            f.reset();
            titleModal = 'Iniciar Sesión';
            login = true;
            register = false;
            forgotpassword = false;
            messageForgot = 'Escribe tu dirección de correo electrónico para reestablecer tu contraseña';
            this.dataLogin = new Login('', '');
            this.dataRegister = new Register('', '', '', '', '');
            this.dataForgot = new Forgot('');
            // }, 0);
            // modal.hide();
            // }, 4000);
            // Redirecciona al home de usuario
            // this._routerModule.navigateByUrl('solicitar');
        } else {
            if (dataResponse.message === 'Trying to get property of non-object') {
                this.messageForgot = 'El usuario no aparece en nuestro sistema';
            } else {
                this.messageForgot = dataResponse.message;
            }
        }
    }

    // Google Login y Logout
    initClient() {
        var thisClass = this;
        gapi.client.init({
            apiKey: 'AIzaSyAMGlA0FkhU_w_l-C7aBM7HPPmtTPT2F_w',
            discoveryDocs: ['https://people.googleapis.com/$discovery/rest?version=v1'],
            clientId: '633339172985-i1h8150vqaavsnbeldhefu63h2h4ntgn.apps.googleusercontent.com',
            scope: 'profile'
        }).then(function () {
            // console.log("jkadsjkaku");
            // Listen for sign-in state changes.
            gapi.auth2.getAuthInstance().isSignedIn.listen(thisClass.updateSigninStatus);
            // Handle the initial sign-in state.
            thisClass.updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
        });
    }

    updateSigninStatus(isSignedIn) {
        const thisClass = this;
        if (isSignedIn) {
            thisClass.makeApiCall();
        }
    }

    makeApiCall() {
        const thisClass = this;
        gapi.client.people.people.get({
            resourceName: 'people/me'
        }).then(function (resp) {
            // console.log('Hello, ' + resp.result.names[0].givenName);
        }, function (reason) {
            // console.log('Error: ' + reason.result.error.message);
        });
    }

    handleAuthClick(event, type) {
        var thisClass = this;
        this.loadingFacebookGoogle = true;
        gapi.auth2.getAuthInstance().signIn().then(function (data) {
            // data.Zi[0].id_token
            // console.log(data);
            if (data.Zi.id_token !== undefined) {
                var dataLogin = {
                    value: {
                        emailLogin: '',
                        passwordLogin: '',
                        google_id: '',
                        name: ''
                    }
                };
                dataLogin.value.emailLogin = data.w3.U3;
                dataLogin.value.passwordLogin = SHA256(data.Zi.id_token);
                dataLogin.value.google_id = data.w3.Eea;
                dataLogin.value.name = data.w3.ig;
                thisClass.loginUser(data.Zi.id_token, false, 'google');
            } else {
                thisClass.loadingFacebookGoogle = false;
                // console.log("Error login google");
            }
        });
    }

    handleSignoutClick(event) {
        gapi.auth2.getAuthInstance().signOut();
    }

    //---Facebook Login y Logout
    onFacebookCheck(type: string) {
        var thisClass = this;
        this.loadingFacebookProcess = true;

        FB.login(function (data) {
            // console.log(data);
            FB.getLoginStatus(function (response) {
                // console.log(response);
                if (response.status == "connected") {
                    thisClass.getDataUserFacebook(thisClass, type, response);
                }
                else if (response.status == "not_authorized") {
                    thisClass.loadingFacebookProcess = false;
                    thisClass.loadingProcess = false;
                    thisClass.loadingFacebookProcess = false;
                    thisClass.loadingFacebookProcess = false;
                    thisClass.loadingFacebookGoogle = false;
                    thisClass.loadingProcess = false;
                }
                else {
                    thisClass.loadingFacebookProcess = false;
                    thisClass.loadingProcess = false;
                    thisClass.loadingFacebookProcess = false;
                    thisClass.loadingFacebookProcess = false;
                    thisClass.loadingFacebookGoogle = false;
                    thisClass.loadingProcess = false;
                }
            });
        }, {scope: 'email'});
    }

    public dataUserLogin = {token: null, email: null};

    getDataUserFacebook(thisClass: any, type: string, responseStatusLogin: any) {
        // console.log(responseStatusLogin);
        var url = '/me?fields=name,email';
        FB.api(url, function (userInfo) {
            // console.log(userInfo);
            if (type == "login") {
                setTimeout(function () {
                    thisClass.dataUserLogin.token = responseStatusLogin.authResponse.accessToken;
                    thisClass.dataUserLogin.email = userInfo.email;
                    thisClass.socialRegister.email = userInfo.email;
                    thisClass.loginUser(responseStatusLogin.authResponse.accessToken, false, 'facebook');
                }, 3000);
            }
        });
    }

    //---Dropdown
    public toggled(open: boolean): void {
        //console.log('Dropdown is now: ', open);
    }

    public toggleDropdown($event: MouseEvent): void {
        $event.preventDefault();
        $event.stopPropagation();
        this.status.isopen = !this.status.isopen;
    }

    //---Collapsed
    public collapsed(event: any): void {
        //console.log(event);
    }

    public expanded(event: any): void {
        //console.log(event);
    }

    //---Modal
    public changeContentModal(type: string): void {
        if (type == 'register') {
            this.titleModal = 'Registrarse';
            this.messageForgot = "Escribe tu dirección de correo electrónico para reestablecer tu contraseña";
            this.login = false;
            this.register = true;
            this.forgotpassword = false;
            this.messageRegister = "";
            this.loadingFacebookProcess = false;
            this.loadingFacebookGoogle = false;
            this.loadingProcess = false;
        }
        else if (type == 'forgotpassword') {
            this.titleModal = 'Recordar contraseña';
            this.messageForgot = "Escribe tu dirección de correo electrónico para reestablecer tu contraseña";
            this.login = false;
            this.register = false;
            this.forgotpassword = true;
            this.messageRegister = "";
            this.loadingFacebookProcess = false;
            this.loadingFacebookGoogle = false;
            this.loadingProcess = false;
        }
        else if (type == 'login') {
            this.titleModal = 'Iniciar Sesión';
            this.messageForgot = "Escribe tu dirección de correo electrónico para reestablecer tu contraseña";
            this.login = true;
            this.register = false;
            this.forgotpassword = false;
            this.messageRegister = "";
            this.loadingFacebookProcess = false;
            this.loadingFacebookGoogle = false;
            this.loadingProcess = false;
        }
    }

    public showChildModal(): void {
        this.loadingFacebookProcess = false;
        this.loadingFacebookGoogle = false;
        this.loadingProcess = false;
        this.staticModal.show();
        this.socialRegister = new SocialRegister("", "", "", "", "");
        this.dataLogin = new Login("", "");
        this.dataRegister = new Register("", "", "", "", "");
        this.dataForgot = new Forgot("");
    }

    public showChildModalCompleteData(): void {
        this.loadingProcess = false;
        this.dataUser = new DataUser("", "");
        this.modalCompleteDataUser.show();
    }

    public hideChildModalCompleteData(): void {
        this.loadingProcess = false;
        this.registerUserData = new RegisterUser();
        this.dataUser = new DataUser("", "");
        this.modalCompleteDataUser.hide();
    }

    public showChildModalCompleteDataSocial(): void {
        this.loadingProcess = false;
        this.dataUser = new DataUser("", "");
        this.modalCompleteDataUserSocial.show();
    }

    public hideChildModalCompleteDataSocial(): void {
        this.loadingProcess = false;
        this.dataUser = new DataUser("", "");
        this.registerUserData = new RegisterUser();
        this.modalCompleteDataUserSocial.hide();
    }

    public showChildModalCompleteDataUserSocialEmailSave(): void {
        this.loadingProcess = false;
        this.dataUser = new DataUser("", "");
        this.modalCompleteDataUserSocialEmailSave.show();
    }

    public hideChildModalCompleteDataUserSocialEmailSave(): void {
        this.loadingProcess = false;
        this.dataUser = new DataUser("", "");
        this.registerUserData = new RegisterUser();
        this.modalCompleteDataUserSocialEmailSave.hide();
    }

    public postServiceCompleteData: string;

    public completeData(f: any) {
        this.loadingProcess = true;
        // console.log(f.value.cellPhone, f.value.identify, f.value.id_agreement, f.value.code_agreement);
        this._HttpProfile.updateService(this._cookieService.get('id'), this._cookieService.get('name'), this._cookieService.get('email'), '', f.value.cellPhone, f.value.identify).subscribe(
            data => this.postServiceCompleteData = JSON.stringify(data),
            error => this.responseCompleteData(this.postServiceCompleteData, f),
            () => this.responseCompleteData(this.postServiceCompleteData, f)
        );
    }

    public completeDataSocial(f: any, type: string) {
        this.loadingProcess = true;
        if (type === 'emailSave') {
            this._LoginComponent.loginUserFacebook(this.dataUserLogin.email, this.dataUserLogin.token, this._ConstantUbeep.urlServer, f.value.id_agreement, f.value.code_agreement, f.value.cellPhone, f.value.identify).subscribe(
                data => this.postMyCarToServer = data,
                error => this.loginError(this.postMyCarToServer),
                () => this.loginSuccess(this.postMyCarToServer, f)
            );
        } else {
            this._LoginComponent.loginUserSocial(this.accessToken, this._ConstantUbeep.urlServer, f.value.id_agreement, f.value.code_agreement, f.value.cellPhone, f.value.identify).subscribe(
                data => this.postMyCarToServer = data,
                error => this.loginError(this.postMyCarToServer),
                () => this.loginSuccess(this.postMyCarToServer, f)
            );
        }
    }

    public messageCompleteData = "";

    public responseCompleteDataSocial(data, f) {
        let dataResponse = JSON.parse(data);
        // console.log(dataResponse);
        var thisClass = this;
        this.loadingProcess = false;
        if (dataResponse.return) {
            this._cookieService.put('movil_phone', f.value.cellPhone);
            this._cookieService.put('identify', f.value.identify);
            this.messageCompleteData = "Los datos se han completado";
            //f.reset();
            setTimeout(function () {
                // console.log(thisClass);
                thisClass.dataUser = new DataUser("", "");
                thisClass.messageCompleteData = "";
                thisClass.hideChildModalCompleteDataSocial();
            }, 4000);
        } else {
            // console.log(thisClass);
            thisClass.messageCompleteData = "Verifique los campos e intente nuevamente";
        }
    }

    public responseCompleteData(data, f) {
        let dataResponse = JSON.parse(data);
        // console.log(dataResponse);
        var thisClass = this;
        this.loadingProcess = false;
        if (dataResponse.return) {
            this._cookieService.put('movil_phone', f.value.cellPhone);
            this._cookieService.put('identify', f.value.identify);
            this.messageCompleteData = "Los datos se han completado";
            //f.reset();
            setTimeout(function () {
                // console.log(thisClass);
                thisClass.dataUser = new DataUser("", "");
                thisClass.messageCompleteData = "";
                thisClass.hideChildModalCompleteData();
            }, 4000);
        } else {
            // console.log(thisClass);
            thisClass.messageCompleteData = "Verifique los campos e intente nuevamente";
        }
    }

    public showChildModalRegisterInvite(): void {
        this.titleModal = 'Registrarse';
        this.messageForgot = "Escribe tu dirección de correo electrónico para reestablecer tu contraseña";
        this.messageRegister = "";
        this.messageLogin = "";
        this.login = false;
        this.register = true;
        this.forgotpassword = false;
        this.viewEmail = false;
        this.loadingFacebookProcess = false;
        this.loadingFacebookGoogle = false;
        this.loadingProcess = false;
        this.staticModal.show();
        this.dataLogin = new Login("", "");
        this.dataRegister = new Register("", "", "", "", "");
        this.dataForgot = new Forgot("");
        this.socialRegister = new SocialRegister("", "", "", "", "");
    }

    public hideChildModal() {
        this.titleModal = 'Iniciar Sesión';
        this.messageForgot = "Escribe tu dirección de correo electrónico para reestablecer tu contraseña";
        this.messageRegister = "";
        this.messageLogin = "";
        this.login = true;
        this.register = false;
        this.forgotpassword = false;
        this.completeDataUser = false;
        this.socialRegister = new SocialRegister("", "", "", "", "");
        this.registerUserData = new RegisterUser();
        this.staticModal.hide();
        this.viewEmail = false;
        this.loadingFacebookProcess = false;
        this.loadingFacebookGoogle = false;
        this.loadingProcess = false;
        this.viewEmail = false;
        this.dataLogin = new Login("", "");
        this.dataRegister = new Register("", "", "", "", "");
        this.dataForgot = new Forgot("");
    }

    //router
    public routeChange = null;

    public changeRoute(route) {
        // console.log(route);
        this.routeChange = route;
        if (window.location.hash == "#/services" && this._cookieService.get('orderFormService') == '3') {
            this.modalConfirmCancelService.show();
        }
        else {
            if (window.location.hash == "#/services") {
                this.modalExitViewServices.show();
            } else {
                if (route == "services") {
                    if (this._cookieService.get('id') != undefined) {
                        if (this._cookieService.get("validRequiestService") != "false") {
                            this._routerModule.navigateByUrl(this.routeChange);
                        }
                        else {
                            this.showTimeSocketModal();
                        }
                    } else {
                        //
                        this.showChildModal();
                    }
                }
                else {
                    this._routerModule.navigateByUrl(this.routeChange);
                }
            }
        }
    }

    confirmCancelService(resp: string) {
        // console.log(resp);
        var thisClass = this;
        if (resp == "si") {
            this.routeChange;
            this.cancelService();
        } else if (resp == "no") {
            this.modalConfirmCancelService.hide();
        } else {
            this.modalConfirmCancelService.hide();
        }
    }

    exitViewService(resp: string) {
        // console.log(resp);
        var thisClass = this;
        if (resp == "si") {
            this.modalExitViewServices.hide();
            this.routeChange;
            this._routerModule.navigateByUrl(this.routeChange);
        } else if (resp == "no") {
            this.modalExitViewServices.hide();
        } else {
            this.modalExitViewServices.hide();
        }
    }

    cancelService() {
        // console.log("vamo a cancelar el serv");
        var postCancelService = "";
        // console.log("cancelará el servicio");
        this._HttpService.cancelService(this._cookieService.get('shipping_id')).subscribe(
            data => postCancelService = JSON.stringify(data),
            error => this.responseCancelService(postCancelService),
            () => this.responseCancelService(postCancelService)
        );
    }

    responseCancelService(data) {
        // console.log(data);
        var dataPostServiceCancel = JSON.parse(data);
        // console.log(dataPostServiceCancel);
        if (dataPostServiceCancel.return) {
            // console.log(dataPostServiceCancel.message);
            this._cookieService.put('shipping_id', '')
            this._cookieService.put('statusServices', 'false');
            this._routerModule.navigateByUrl(this.routeChange);
            this.modalConfirmCancelService.hide();
        } else {
            // console.log("problemas al cancelar servicio");
            this._routerModule.navigateByUrl(this.routeChange);
            this.modalConfirmCancelService.hide();
        }
    }

    //test nueva ventana
    checkFacebook() {
        window.open(
            'https://www.facebook.com/login.php?skip_api_login=1&api_key=555091548022084&signed_next=1&next=https%3A%2F%2Fwww.facebook.com%2Fv2.8%2Fdialog%2Foauth%3Fredirect_uri%3Dhttp%253A%252F%252Fstaticxx.facebook.com%252Fconnect%252Fxd_arbiter%252Fr%252FD6ZfFsLEB4F.js%253Fversion%253D42%2523cb%253Df1eb560c3ad9428%2526domain%253Ddevelop.ubeep.co%2526origin%253Dhttp%25253A%25252F%25252Fdevelop.ubeep.co%25252Ff3260b47f6025e8%2526relation%253Dopener%2526frame%253Df54b9b3e521e8%26display%3Dpopup%26scope%3Demail%26response_type%3Dtoken%252Csigned_request%26domain%3Ddevelop.ubeep.co%26origin%3D1%26client_id%3D555091548022084%26ret%3Dlogin%26sdk%3Djoey%26logger_id%3D691cc260-fd5b-4e8f-8037-9eb677d9acf6&cancel_url=http%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2FD6ZfFsLEB4F.js%3Fversion%3D42%23cb%3Df1eb560c3ad9428%26domain%3Ddevelop.ubeep.co%26origin%3Dhttp%253A%252F%252Fdevelop.ubeep.co%252Ff3260b47f6025e8%26relation%3Dopener%26frame%3Df54b9b3e521e8%26error%3Daccess_denied%26error_code%3D200%26error_description%3DPermissions%2Berror%26error_reason%3Duser_denied%26e2e%3D%257B%257D&display=popup&locale=en_US&logger_id=691cc260-fd5b-4e8f-8037-9eb677d9acf6'
            , 'popup'
            , 'width=300,height=400'
        );
    }

    public setTimeoutServiceToken() {
        var thisClass = this;
        setTimeout(function () {
            // console.log(thisClass);
            // console.log(CookieService.prototype.get("validRequiestService"));
            CookieService.prototype.put("validRequiestService", "true");
        }, 180000);
    }

    public showTimeSocketModal(): void {
        this.timeSocketModal.show();
    }

    public hideTimeSocketModal(): void {
        this.timeSocketModal.hide();
    }

}
