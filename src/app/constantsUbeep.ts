import {Component, OnInit} from '@angular/core';

export class ConstantUbeep {

    // dev
    public urlServer = "http://52.43.247.174/";
    // public urlServer = 'http://api.localhost/';

    // prod
    // public urlServer = 'http://54.70.181.28/';

    // url web dev
    // public urlWeb = 'develop.ubeep.co/';

    // url web prod
    public urlWeb = 'ubeep.co/';

}
