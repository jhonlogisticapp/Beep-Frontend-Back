import { Component, ViewChild, Injectable, Compiler, ViewContainerRef, OnInit } from '@angular/core';
import { Ng2BootstrapModule, ModalDirective }                        		    from 'ng2-bootstrap';
import { NgModel }                                                   		    from '@angular/forms';
import { Router }                                                    		    from '@angular/router';
import { CookieService }                                             		    from 'angular2-cookie/services/cookies.service';
import { HttpHistory }                                               		    from "./history.service";

@Component({
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css'],
  providers: [ HttpHistory]
})
export class HistoryComponent implements OnInit {


	private viewContainerRef: ViewContainerRef;
	postServiceHistorys : string;
	postServiceHistory : string;
	public objHistory : any = [];
	public dataService = {};
	public activeHistory = null;

	public load = true;

	constructor(private _viewContainerRef: ViewContainerRef, private _compiler: Compiler, private _routerModule: Router, private _cookieService:CookieService, public _httpHistory : HttpHistory) {
    	this.viewContainerRef = _viewContainerRef;
    	this._compiler.clearCache();
    }

	ngOnInit() {
		var myDiv = document.getElementById('container-history');
		// console.log(myDiv);
		scrollTo(document.body, myDiv.offsetTop, 10);

		function scrollTo(element, to, duration) {
			if (duration < 0) return;
			var difference = to - element.scrollTop;
			var perTick = difference / duration * 2;

			setTimeout(function() {
			  element.scrollTop = element.scrollTop + perTick;
			  scrollTo(element, to, duration - 2);
			}, 10);
		}
	    this.getHistorory();
	}

	getHistorory(){
		// console.log("getHistorory");



		
	    this._httpHistory.getHistorys(this._cookieService.get('id'), "1").subscribe(
	      data => this.postServiceHistorys = JSON.stringify(data),
	      error => this.responseHistorys(this.postServiceHistorys),
	      () => this.responseHistorys(this.postServiceHistorys)
	    );
	}

	responseHistorys(data){
		// console.log(data);
		let dataResponse = JSON.parse(data);
		// console.log(dataResponse);
		var cont = 0;
		var thisClass = this;
		this.load = false;
		if(dataResponse.return){
			if(dataResponse.dataToday.length>0){
				dataResponse.dataToday.forEach(function(d,i){
					if(i==0){
						thisClass.getHistoryData(d.id, 0);
					}
					thisClass.objHistory.push(d);
					if(cont==i){
						if(dataResponse.dataPrevious.length>0){
							dataResponse.dataPrevious.forEach(function(dp,ip){
								thisClass.objHistory.push(dp);
							});
						}
					}
				});
			} else{
				if(dataResponse.dataPrevious.length>0){
					dataResponse.dataPrevious.forEach(function(dp,ip){
						if(ip==0){
							thisClass.getHistoryData(dp.id, 0);
						}
						thisClass.objHistory.push(dp);
					});
				}
			}
			// console.log("ok");
			//this.objHistory = dataResponse.dataPrevious;
		} else{
			// console.log("no ok");
		}
	}

	private getHistoryData(id, index){
		this.activeHistory = index;
		// console.log(event);
		// console.log(id);
		this._httpHistory.getHistory(id).subscribe(
	      data => this.postServiceHistory = JSON.stringify(data),
	      error => this.responseHistory(this.postServiceHistory),
	      () => this.responseHistory(this.postServiceHistory)
	    );
	}

	responseHistory(data){
		// console.log(data);
		let dataResponse = JSON.parse(data);
		// console.log(dataResponse);
		if(dataResponse.return){
			// console.log("ok");
			var rating = parseInt(dataResponse.data.rating);
			dataResponse.data.ratingImg = rating;
			this.dataService = dataResponse.data;
		} else{
			// console.log("no ok");
			this.dataService = {};
		}
	}

}
