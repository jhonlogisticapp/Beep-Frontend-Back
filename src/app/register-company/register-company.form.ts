export class Register {
    constructor(
        public email: string,
        public name: string,
        public phone: string
    ) {  }
}