import { Component, ViewChild, Injectable, Compiler, ViewContainerRef, OnInit } from '@angular/core';

import { Ng2BootstrapModule, ModalDirective }                           from 'ng2-bootstrap';
import { NgModel }                                                      from '@angular/forms';
import { Router }                                                       from '@angular/router';
import { HttpRegister }                                    	     from "./register-company.service";
import { Register }                              		      	 from "./register-company.form"; 

@Component({
  templateUrl: './register-company.component.html',
  styleUrls: ['./register-company.component.css'],
  providers: [HttpRegister]
})
export class RegisterCompanyComponent implements OnInit {

	private viewContainerRef: ViewContainerRef;
	public postService : string;
	public loadingProcess = false;
	public responseMessage = "";
	public dataRegister = new Register("", "", "");
    public statusForm = false;

	constructor(private _viewContainerRef: ViewContainerRef, private _compiler: Compiler, private _routerModule: Router, private _httpService: HttpRegister) {
    	this.viewContainerRef = _viewContainerRef;
    	this._compiler.clearCache();
    }

    ngOnInit() {
        var myDiv = document.getElementById('container-register-company');
        // console.log(myDiv);
        scrollTo(document.body, myDiv.offsetTop, 10);

        function scrollTo(element, to, duration) {
          if (duration < 0) return;
          var difference = to - element.scrollTop;
          var perTick = difference / duration * 2;

          setTimeout(function() {
            element.scrollTop = element.scrollTop + perTick;
            scrollTo(element, to, duration - 2);
          }, 10);
        }
    }

    sendEmail(data){
        let validEmail = false;
        let validName = false;
        let validPhone = false;
        if(data.value.email.toString().trim().length > 0)
        {
            validEmail = true;
            document.getElementById("status1").setAttribute("style","display: none !important");
            document.getElementById("email").className = "form-control";
        }
        else
        {
            validEmail = false;
            document.getElementById("status1").setAttribute("style","display: block !important");
            var d = document.getElementById("email");
            d.className += " inputImportant";
        }

        if(data.value.name.toString().trim().length > 0)
        {
            validName = true;
            document.getElementById("status2").setAttribute("style","display: none !important");
            document.getElementById("name").className = "form-control";
        }
        else
        {
            validName = false;
            document.getElementById("status2").setAttribute("style","display: block !important");
            var d = document.getElementById("name");
            d.className += " inputImportant";
        }
        if(data.value.phone.toString().trim().length > 0)
        {
            validPhone = true;
            document.getElementById("status3").setAttribute("style","display: none !important");
            document.getElementById("phone").className = "form-control";
        }
        else
        {
            validPhone = false;
            document.getElementById("status3").setAttribute("style","display: block !important");
            var d = document.getElementById("phone");
            d.className += " inputImportant";
        }
        if(validEmail && validName && validPhone)
        {
            this.statusForm = false;
        	this.loadingProcess = true;
    		this.responseMessage = "";
    		// console.log(data);
    		this._httpService.register(data.value.email, data.value.name, data.value.phone).subscribe(
              data => this.postService = JSON.stringify(data),
              error => this.responseSuccess(this.postService, data),
              () => this.responseSuccess(this.postService, data)
            );
        }
        else
        {
            this.statusForm = true;
        }
    }

    responseSuccess(resp, data){
    	let dataResponse = JSON.parse(resp);
    	// console.log(dataResponse);
    	// console.log(data);
    	var thisClass = this;
    	if(dataResponse.return && dataResponse.status == 200){
    		this.responseMessage = dataResponse.message;
        this.loadingProcess = false;
    		data.reset();
            this.dataRegister = new Register("", "", "");
    		setTimeout(function() {
    			thisClass.responseMessage = "";
    		}, 2000);
    	}else{
        this.loadingProcess = false;
    		this.responseMessage = "Por favor verifique el correo";
    	}
    }

    changeValidateInput(type : string, data: any){
        // console.log(this.statusForm);
        // console.log(data.value);
        if(this.statusForm){
            switch (type) {
                case "name":
                    if(data.value.name.toString().trim().length > 0)
                    {
                        document.getElementById("status2").setAttribute("style","display: none !important");
                        document.getElementById("name").className = "form-control";
                    }
                    else
                    {
                        document.getElementById("status2").setAttribute("style","display: block !important");
                        var d = document.getElementById("name");
                        d.className += " inputImportant";
                    }
                break;

                case "phone":
                    if(data.value.phone.toString().trim().length > 0)
                    {
                        document.getElementById("status3").setAttribute("style","display: none !important");
                        document.getElementById("phone").className = "form-control";
                    }
                    else
                    {
                        document.getElementById("status3").setAttribute("style","display: block !important");
                        var d = document.getElementById("phone");
                        d.className += " inputImportant";
                    }
                break;

                case "email":
                    if(data.value.email.toString().trim().length > 0)
                    {
                        document.getElementById("status1").setAttribute("style","display: none !important");
                        document.getElementById("email").className = "form-control";
                    }
                    else
                    {
                        document.getElementById("status1").setAttribute("style","display: block !important");
                        var d = document.getElementById("email");
                        d.className += " inputImportant";
                    }
                break;

                default:
                break;
            }
        }
    }

}
