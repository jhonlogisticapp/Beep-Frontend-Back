export class PaymentGateWay {
    constructor(public x_card_num: string = null,
                public platform: string = 'web',
                public client_id: string = null,
                public x_exp_date: string = null,
                public x_card_code: string = null){ }
}