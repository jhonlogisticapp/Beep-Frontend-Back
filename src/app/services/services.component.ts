import { Component, ViewChild, NgZone, ChangeDetectionStrategy, OnInit, Compiler, Injectable, ViewContainerRef, ChangeDetectorRef } from '@angular/core';
import { NgModel, FormControl, FormGroup, FormBuilder, Validators }                                              from '@angular/forms';
import { Ng2BootstrapModule, ModalDirective, TabsetConfig }                                                                    from 'ng2-bootstrap';
import { Http, Response, Headers, RequestOptions }                                                               from '@angular/http'
import { Observable }                                                                                            from 'rxjs/Rx';
import { Router }                                                                                                from '@angular/router';
import 'rxjs/Rx';
import { CookieService }                                                                                         from 'angular2-cookie/services/cookies.service';

import { AppComponent }                                                                                          from './../app.component';
import { HttpSolicitarService }                                                                                  from "./services.service";
import { ServiceTransport }                                                                                      from './service.transport';
import { ServiceSend }                                                                                           from './service.send';
import {PaymentGateWay} from "app/services/paymentGateWay";

declare var google: any;

@Component({
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css'],
  providers: [ HttpSolicitarService, FormBuilder, TabsetConfig]
})
export class ServicesComponent implements OnInit {


  private viewContainerRef: ViewContainerRef;
  //private _AppComponent: AppComponent;
  //private _httpService:HttpSolicitarService;
  public markerOrigin = null;
  public markerOriginStatus = true;
  public markerDestination = null;
  public markerDestinationStatus = false;
  public markerOriginSendStatus = false;
  public markerDestinationSendStatus = true;
  public markerUbeep = null;
  public googleMap = null;
  //---Datos del servicio
  public origin_address = null; //---Direccion de origen
  public origin_latitude = null;
  public origin_longitude = null;
  public destiny_address = null; //---Direccion de destino
  public destiny_latitude = null;
  public destiny_longitude = null;
  public type_service = null;
  public bag_id = null;
  public typeService = null //NOOOOOOOOOO
  public tip = null;
  public price_service = null;
  public declared_value = null;
  public distance = null;
  public origin_client = null;
  public amount = null;
  public pay = null;
  public time = null;
  public code_confirm = null;
  public origin_detail = null;
  public description_text = null;
  public destiny_detail = null;
  public destiny_name = null;
  public polyline = null;
  public type_service_string = "";
  public message_bond = "Ingresa el código de tu bono";
  public bond_service = "";
  public creditValue = "0";
  //---Vistas Generales
  public viewFromService = true;
  public viewContribution = false;
  public viewSuccess = false;
  public viewOriginTransport = true;
  public viewDestinationTransport = false;
  public viewOriginSend = false;
  public viewDestinationSend = false;
  public containerTypeVehicle = false;
  public containerDestinyName = false;
  public viewContainerBond = false;
  public viewCodeSuccess = false;
  public viewDataConfirm = false;
  public postService:string;
  public postServiceSocket:string;
  public postServicePushWeb:string;
  public postServiceCancelService:string;

  public loadingProcess = false;
  loadingProcessPaymentGateWayPost = false;
  public tab = { active: true};
  public formServicesTransport = FormGroup;
  public dataFOrm1;
  public dataFOrm2;
  public dataSocket = null;
  public loadingBoundProcess = false;
  @ViewChild('modalBono') public modalBono:ModalDirective;
  @ViewChild('modalLoading') public modalLoading:ModalDirective;
  @ViewChild('modalConfirmBeep') public modalConfirmBeep:ModalDirective;
  @ViewChild('modalCancelBeep') public modalCancelBeep:ModalDirective;

  @ViewChild('modalConfirmCancelBeep') public modalConfirmCancelBeep:ModalDirective;
  @ViewChild('modalCancelDriveBeep') public modalCancelDriveBeep:ModalDirective;

  public dataServiceTransport = new ServiceTransport("", "", "", "", "", "", "34", "", "");
  public dataServiceSend = new ServiceSend("", "", "", "", "", "", "", "", "37", "", "", "", "", "");
  public deliveryVerificationF : any;

  valuePayDefault = '5';
  quotation_id = '';
  constructor( private _viewContainerRef: ViewContainerRef,
               public _httpService: HttpSolicitarService,
               private _cookieService: CookieService,
               private _compiler: Compiler,
               private _routerModule: Router,
               public fb: FormBuilder,
               private _Http: Http,
               private _changeDetectorRef: ChangeDetectorRef) {
    this.viewContainerRef = _viewContainerRef;
    this._compiler.clearCache();
    window.onbeforeunload;
  }

  ngOnInit() {
      if (this._cookieService.get('pay_type') != undefined) {
          this.valuePayDefault = this._cookieService.get('pay_type');
      }

    this.loadTop();
    this._cookieService.put('statusServices', 'false');
    this.loadComponentsGoogleMaps();

  }

    refreshView() {
        this._changeDetectorRef.detectChanges();
    }

  loadTop () {
    var myDiv = document.getElementById('container-services');
    // console.log(myDiv);
    scrollTo(document.body, myDiv.offsetTop, 10);

    function scrollTo(element, to, duration) {
      if (duration < 0) return;
      var difference = to - element.scrollTop;
      var perTick = difference / duration * 2;

      setTimeout(function() {
        element.scrollTop = element.scrollTop + perTick;
        scrollTo(element, to, duration - 2);
      }, 10);
    }
  }
  public viewPayCredit = false;
  //---Cargo componentes de Google Maps
  loadComponentsGoogleMaps() {
    //---Muestro la primer parte del formulario del tab Transporte
    this.viewFromService = true;
    this._cookieService.put('orderFormService', '1');
    this.viewDestinationTransport = false;
    var thisClass = this;
    setTimeout(function() {
      var credito = thisClass._cookieService.get('credit');
      if(credito!=undefined){
        thisClass._httpService.getCredit(thisClass._cookieService.get("id")).subscribe(
            data => thisClass.responseGetCreditString = JSON.stringify(data),
            error => thisClass.responseGetCredit(thisClass.responseGetCreditString),
            () => thisClass.responseGetCredit(thisClass.responseGetCreditString)
        );
      } else {
        //document.getElementById("viewPayCredit").style.display = "none";
        thisClass.viewPayCredit = false;
      }
      //---Cargo el mapa
      var geocoder = new google.maps.Geocoder();
      var mapProp = {
        center: new google.maps.LatLng(4.6762881, -74.05177549999999),
        zoom: 18,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
      //---Almaceno el mapa en una variable
      thisClass.googleMap = map;
      navigator.geolocation.getCurrentPosition(function(data){
        thisClass.googleMap.panTo(new google.maps.LatLng(data.coords.latitude, data.coords.longitude));
      });
      thisClass.loadComponentPlaceInputOriginTransport();
      thisClass.loadComponentPlaceInputDestinationTransport();
      //---Click para crear o actualizar el Marker
      google.maps.event.addListener(map, 'click', function(event) {
        geocoder.geocode({
          latLng: event.latLng
        }, function(responses) {
          if (responses && responses.length > 0) {
            if(thisClass.markerOriginStatus){
              thisClass.dataServiceTransport.origin_address = responses[0].formatted_address;
              thisClass.dataServiceTransport.origin_latitude = event.latLng.lat();
              thisClass.dataServiceTransport.origin_longitude = event.latLng.lng();
              document.getElementById('autocompleteMapaOrigin').click();
              if(thisClass.statusFormTransport){
                thisClass.changeValidateInput('origin_latitude');
              }
              //document.getElementById("autocompleteMapaOrigin").setAttribute("value",responses[0].formatted_address);
              thisClass.origin_address = responses[0].formatted_address;
              thisClass.origin_latitude = event.latLng.lat();
              thisClass.origin_longitude = event.latLng.lng();
              var iconOrigin = {
                url: "./assets/img/pinOrigen.png",
                scaledSize: new google.maps.Size(29, 42),
                origin: new google.maps.Point(0,0),
                anchor: new google.maps.Point(0, 0)
              };
              if(thisClass.markerOrigin==null){
                thisClass.markerOrigin = new google.maps.Marker({
                  position: event.latLng,
                  animation:  google.maps.Animation.DROP,
                  icon: iconOrigin,
                  map: map
                });
                map.panTo(event.latLng);
              }else{
                thisClass.markerOrigin.setPosition(event.latLng);
                map.panTo(event.latLng);
              }
            }else if(thisClass.markerDestinationStatus){
              thisClass.dataServiceTransport.destiny_address = responses[0].formatted_address;
              thisClass.dataServiceTransport.destiny_latitude = event.latLng.lat();
              thisClass.dataServiceTransport.destiny_longitude = event.latLng.lng();
              document.getElementById('autocompleteMapaDestination').click();
              if(thisClass.statusFormTransport){
                thisClass.changeValidateInput('destiny_latitude');
              }
              //document.getElementById("autocompleteMapaDestination").setAttribute("value",responses[0].formatted_address);
              thisClass.destiny_address = responses[0].formatted_address;
              thisClass.destiny_latitude = event.latLng.lat();
              thisClass.destiny_longitude = event.latLng.lng();
              var iconDestination = {
                url: "./assets/img/pinDestino.png",
                scaledSize: new google.maps.Size(29, 42),
                origin: new google.maps.Point(0,0),
                anchor: new google.maps.Point(0, 0)
              };
              if(thisClass.markerDestination==null){
                thisClass.markerDestination = new google.maps.Marker({
                  position: event.latLng,
                  animation:  google.maps.Animation.DROP,
                  icon: iconDestination,
                  map: map
                });
                map.panTo(event.latLng);
              }else{
                thisClass.markerDestination.setPosition(event.latLng);
                map.panTo(event.latLng);
              }
            }else if(thisClass.markerOriginSendStatus){
              thisClass.dataServiceSend.origin_address = responses[0].formatted_address;
              thisClass.dataServiceSend.origin_latitude = event.latLng.lat();
              thisClass.dataServiceSend.origin_longitude = event.latLng.lng();
              document.getElementById('autocompleteMapaOriginSend').click();
              if(thisClass.statusFormTransport){
                thisClass.changeValidateInput('origin_latitude_send');
              }
              //document.getElementById("autocompleteMapaOriginSend").setAttribute("value",responses[0].formatted_address);
              thisClass.origin_address = responses[0].formatted_address;
              thisClass.origin_latitude = event.latLng.lat();
              thisClass.origin_longitude = event.latLng.lng();
              var iconOrigin = {
                url: "./assets/img/pinOrigen.png",
                scaledSize: new google.maps.Size(29, 42),
                origin: new google.maps.Point(0,0),
                anchor: new google.maps.Point(0, 0)
              };
              if(thisClass.markerOrigin==null){
                thisClass.markerOrigin = new google.maps.Marker({
                  position: event.latLng,
                  animation:  google.maps.Animation.DROP,
                  icon: iconOrigin,
                  map: map
                });
                map.panTo(event.latLng);
              }else{
                thisClass.markerOrigin.setPosition(event.latLng);
                map.panTo(event.latLng);
              }
            }else if(thisClass.markerDestinationSendStatus){
              thisClass.dataServiceSend.destiny_address = responses[0].formatted_address;
              thisClass.dataServiceSend.destiny_latitude = event.latLng.lat();
              thisClass.dataServiceSend.destiny_longitude = event.latLng.lng();
              document.getElementById('autocompleteMapaDestinationSend').click();
              if(thisClass.statusFormTransport){
                thisClass.changeValidateInput('destiny_latitude_send');
              }
              //document.getElementById("autocompleteMapaDestinationSend").setAttribute("value",responses[0].formatted_address);
              thisClass.destiny_address = responses[0].formatted_address;
              thisClass.destiny_latitude = event.latLng.lat();
              thisClass.destiny_longitude = event.latLng.lng();
              var iconDestination = {
                url: "./assets/img/pinDestino.png",
                scaledSize: new google.maps.Size(29, 42),
                origin: new google.maps.Point(0,0),
                anchor: new google.maps.Point(0, 0)
              };
              if(thisClass.markerDestination==null){
                thisClass.markerDestination = new google.maps.Marker({
                  position: event.latLng,
                  animation:  google.maps.Animation.DROP,
                  icon: iconDestination,
                  map: map
                });
                map.panTo(event.latLng);
              }else{
                thisClass.markerDestination.setPosition(event.latLng);
                map.panTo(event.latLng);
              }
            }else{
            }
          } else {
          }
        });
      });
    }, 1000);
    //form.controls['propinaSend'].setValue("selected.id");
  }
  //---Input Origen Transporte
  loadComponentPlaceInputOriginTransport(){
    var thisClass = this;
    setTimeout(function() {
      //---Inicio componente Google Place Input 'autocompleteMapaOrigin'
      var inputAutocompleteOrigin = document.getElementById('autocompleteMapaOrigin');
      var autocompleteServiceOrigin = new google.maps.places.Autocomplete(inputAutocompleteOrigin);
      google.maps.event.addListener(autocompleteServiceOrigin, 'place_changed', function () {
        var place = autocompleteServiceOrigin.getPlace();
        var iconOrigin = {
          url: "./assets/img/pinOrigen.png",
          scaledSize: new google.maps.Size(29, 42),
          origin: new google.maps.Point(0,0),
          anchor: new google.maps.Point(0, 0)
        };
        if(thisClass.statusFormTransport){
          thisClass.changeValidateInput('origin_latitude');
        }
        //---Almaceno la dirección
        thisClass.dataServiceTransport.origin_address = place.name;
        thisClass.dataServiceTransport.origin_latitude = place.geometry.location.lat();
        thisClass.dataServiceTransport.origin_longitude = place.geometry.location.lng();
        document.getElementById('autocompleteMapaOrigin').click();
        thisClass.origin_address = place.name;
        thisClass.origin_latitude = place.geometry.location.lat();
        thisClass.origin_longitude = place.geometry.location.lng();
        if(thisClass.markerOrigin==null){
          thisClass.markerOrigin = new google.maps.Marker({
            position: new google.maps.LatLng( place.geometry.location.lat(), place.geometry.location.lng()),
            animation:  google.maps.Animation.DROP,
            icon: iconOrigin,
            map: thisClass.googleMap
          });
          thisClass.googleMap.panTo(new google.maps.LatLng( place.geometry.location.lat(), place.geometry.location.lng()));
        }else{
          thisClass.markerOrigin.setPosition(new google.maps.LatLng( place.geometry.location.lat(), place.geometry.location.lng()));
          thisClass.googleMap.panTo(new google.maps.LatLng( place.geometry.location.lat(), place.geometry.location.lng()));
        }
      });
    }, 1000);
  }
  //---Input Destino Transporte
  loadComponentPlaceInputDestinationTransport(){
    var thisClass = this;
    setTimeout(function() {
      //---Inicio componente Google Place Input 'autocompleteMapaDestination'
      var inputAutocompleteDestination = document.getElementById('autocompleteMapaDestination');
      var autocompleteServiceDestination = new google.maps.places.Autocomplete(inputAutocompleteDestination);
      google.maps.event.addListener(autocompleteServiceDestination, 'place_changed', function () {
        var place = autocompleteServiceDestination.getPlace();
        var iconDestination = {
          url: "./assets/img/pinDestino.png",
          scaledSize: new google.maps.Size(29, 42),
          origin: new google.maps.Point(0,0),
          anchor: new google.maps.Point(0, 0)
        };
        if(thisClass.statusFormTransport){
          thisClass.changeValidateInput('destiny_latitude');
        }
        //---Almaceno la dirección
        thisClass.dataServiceTransport.destiny_address = place.name;
        thisClass.dataServiceTransport.destiny_latitude = place.geometry.location.lat();
        thisClass.dataServiceTransport.destiny_longitude = place.geometry.location.lng();
        document.getElementById('autocompleteMapaDestination').click();
        thisClass.destiny_address = place.name;
        thisClass.destiny_latitude = place.geometry.location.lat();
        thisClass.destiny_longitude = place.geometry.location.lng();
        if(thisClass.markerDestination==null){
          thisClass.markerDestination = new google.maps.Marker({
            position: new google.maps.LatLng( place.geometry.location.lat(), place.geometry.location.lng()),
            animation:  google.maps.Animation.DROP,
            icon: iconDestination,
            map: thisClass.googleMap
          });
          thisClass.googleMap.panTo(new google.maps.LatLng( place.geometry.location.lat(), place.geometry.location.lng()));
        }else{
          thisClass.markerDestination.setPosition(new google.maps.LatLng( place.geometry.location.lat(), place.geometry.location.lng()));
          thisClass.googleMap.panTo(new google.maps.LatLng( place.geometry.location.lat(), place.geometry.location.lng()));
        }
      });
    }, 1000);
  }
  //---Input Origen Envio
  loadComponentPlaceInputOriginSend(){
    var thisClass = this;
    setTimeout(function() {
      //---Inicio componente Google Place Input 'autocompleteMapaOriginSend'
      var inputAutocompleteOriginSend = document.getElementById('autocompleteMapaOriginSend');
      this.markerOriginStatus = false;
      this.markerDestinationStatus = false;
      this.markerOriginSendStatus = true;
      this.markerDestinationSendStatus = false;
      //document.getElementById('autocompleteMapaOriginSend').focus();
      var autocompleteServiceOriginSend = new google.maps.places.Autocomplete(inputAutocompleteOriginSend);
      google.maps.event.addListener(autocompleteServiceOriginSend, 'place_changed', function () {
        var place = autocompleteServiceOriginSend.getPlace();
        var iconOrigin = {
          url: "./assets/img/pinOrigen.png",
          scaledSize: new google.maps.Size(29, 42),
          origin: new google.maps.Point(0,0),
          anchor: new google.maps.Point(0, 0)
        };
        // console.log(thisClass.statusFormTransport);
        if(thisClass.statusFormTransport){
          thisClass.changeValidateInput('origin_latitude_send');
        }
        //---Almaceno la dirección
        thisClass.dataServiceSend.origin_address = place.name;
        thisClass.dataServiceSend.origin_latitude = place.geometry.location.lat();
        thisClass.dataServiceSend.origin_longitude = place.geometry.location.lng();
        document.getElementById('autocompleteMapaOriginSend').click();
        thisClass.origin_address = place.name;
        thisClass.origin_latitude = place.geometry.location.lat();
        thisClass.origin_longitude = place.geometry.location.lng();
        if(thisClass.markerOrigin==null){
          thisClass.markerOrigin = new google.maps.Marker({
            position: new google.maps.LatLng( place.geometry.location.lat(), place.geometry.location.lng()),
            animation:  google.maps.Animation.DROP,
            icon: iconOrigin,
            map: thisClass.googleMap
          });
          thisClass.googleMap.panTo(new google.maps.LatLng( place.geometry.location.lat(), place.geometry.location.lng()));
        }else{
          thisClass.markerOrigin.setPosition(new google.maps.LatLng( place.geometry.location.lat(), place.geometry.location.lng()));
          thisClass.googleMap.panTo(new google.maps.LatLng( place.geometry.location.lat(), place.geometry.location.lng()));
        }
      });
    }, 1000);
  }
  //---Input Destino Envio
  loadComponentPlaceInputDestinationSend(){
    var thisClass = this;
    setTimeout(function() {
      //---Inicio componente Google Place Input 'autocompleteMapaDestinationSend'
      var inputAutocompleteDestinationSend = document.getElementById('autocompleteMapaDestinationSend');
      var autocompleteServiceDestinationSend = new google.maps.places.Autocomplete(inputAutocompleteDestinationSend);
      google.maps.event.addListener(autocompleteServiceDestinationSend, 'place_changed', function () {
        var place = autocompleteServiceDestinationSend.getPlace();
        var iconDestination = {
          url: "./assets/img/pinDestino.png",
          scaledSize: new google.maps.Size(29, 42),
          origin: new google.maps.Point(0,0),
          anchor: new google.maps.Point(0, 0)
        };
        // console.log(thisClass.statusFormTransport);
        if(thisClass.statusFormTransport){
          thisClass.changeValidateInput('destiny_latitude_send');
        }
        //---Almaceno la dirección
        thisClass.dataServiceSend.destiny_address = place.name;
        thisClass.dataServiceSend.destiny_latitude = place.geometry.location.lat();
        thisClass.dataServiceSend.destiny_longitude = place.geometry.location.lng();
        document.getElementById('autocompleteMapaDestinationSend').click();
        thisClass.destiny_address = place.name;
        thisClass.destiny_latitude = place.geometry.location.lat();
        thisClass.destiny_longitude = place.geometry.location.lng();
        if(thisClass.markerDestination==null){
          thisClass.markerDestination = new google.maps.Marker({
            position: new google.maps.LatLng( place.geometry.location.lat(), place.geometry.location.lng()),
            animation:  google.maps.Animation.DROP,
            icon: iconDestination,
            map: thisClass.googleMap
          });
          thisClass.googleMap.panTo(new google.maps.LatLng( place.geometry.location.lat(), place.geometry.location.lng()));
        }else{
          thisClass.markerDestination.setPosition(new google.maps.LatLng( place.geometry.location.lat(), place.geometry.location.lng()));
          thisClass.googleMap.panTo(new google.maps.LatLng( place.geometry.location.lat(), place.geometry.location.lng()));
        }
      });
    }, 1000);
  }
  //---Detecta el input con el foco para poder saber cual marker poner
  detectInputMarker(type: string){
    // console.log(type);
    if(type=="originTransport"){
      this.markerOriginStatus = true;
      this.markerDestinationStatus = false;
      this.markerOriginSendStatus = false;
      this.markerDestinationSendStatus = false;
    }
    else if(type=="destinationTransport"){
      this.markerOriginStatus = false;
      this.markerDestinationStatus = true;
      this.markerOriginSendStatus = false;
      this.markerDestinationSendStatus = false;
    }
    else if(type=="originSend"){
      this.markerOriginStatus = false;
      this.markerDestinationStatus = false;
      this.markerOriginSendStatus = true;
      this.markerDestinationSendStatus = false;
    }
    else if(type=="destinationSend"){
      this.markerOriginStatus = false;
      this.markerDestinationStatus = false;
      this.markerOriginSendStatus = false;
      this.markerDestinationSendStatus = true;
    }
    else{
      this.markerOriginStatus = true;
      this.markerDestinationStatus = false;
      this.markerOriginSendStatus = false;
      this.markerDestinationSendStatus = false;
    }
  }
  //---Carga contenido del tab Envio
  public statusLoadTabSend = true;
  loadContentTabSend(type: number) {
    // console.log(this.statusLoadTabSend);
    if(this.statusLoadTabSend){
      this.statusLoadTabSend = false;
      var thisClass = this;
      this.detectInputMarker("originSend");
      this.viewOriginSend = true;
      document.getElementById("viewOriginSend").style.display = "block";
      this.viewDestinationSend = false;
      document.getElementById("viewDestinationSend").style.display = "none";
      this.markerOriginStatus = true;
      this.markerDestinationStatus = false;
      setTimeout(function() {
        thisClass.loadComponentPlaceInputOriginSend();
        thisClass.loadComponentPlaceInputDestinationSend();
      }, 1000);
    } else {
      // console.log("JAAAAAAAAAAAAAAAAAAAAA tOMALA!");
    }
  };
  //---Selecciona el tipo de vehiculo
  selectTypeVehicle(position:number){
    var inputRadio = document.getElementsByName("bag_id");
    this.dataServiceTransport.type_service = "34";
    if(this.statusFormTransport){
      this.changeValidateInput('bag_id');
    }
    if(position==0){
      var d = document.getElementById("containerVehicleVan");
      d.className += " containerVehicleSelect";

      document.getElementById("containerVehicleWhite").className = "";
      document.getElementById("containerVehicleElectric").className = "";

      inputRadio[1].removeAttribute("checked");
      inputRadio[2].removeAttribute("checked");
      inputRadio[position].setAttribute("checked","checked");
      document.getElementById("blanco").setAttribute("src","./assets/img/transporteBlanco.png");
      document.getElementById("electrico").setAttribute("src","./assets/img/transporteElectrico.png");
      document.getElementById("van").setAttribute("src","./assets/img/transporteVanSelected.png");
      this.bag_id = "16"; //this.bag_id = this._cookieService.get('shipping_bag_id_1');
      this.dataServiceTransport.bag_id = "16";
      this.type_service_string = "Van";
    }
    else if(position==1){
      var d = document.getElementById("containerVehicleWhite");
      d.className += " containerVehicleSelect";

      document.getElementById("containerVehicleVan").className = "";
      document.getElementById("containerVehicleElectric").className = "";

      inputRadio[0].removeAttribute("checked");
      inputRadio[2].removeAttribute("checked");
      inputRadio[position].setAttribute("checked","checked");
      document.getElementById("van").setAttribute("src","./assets/img/transporteVan.png");
      document.getElementById("electrico").setAttribute("src","./assets/img/transporteElectrico.png");
      document.getElementById("blanco").setAttribute("src","./assets/img/transporteBlancoSelected.png");
      this.bag_id = "25"; //this.bag_id = this._cookieService.get('shipping_bag_id_2');
      this.dataServiceTransport.bag_id = "25";
      this.type_service_string = "Blanco";
    }
    else if(position==2){
      var d = document.getElementById("containerVehicleElectric");
      d.className += " containerVehicleSelect";

      document.getElementById("containerVehicleVan").className = "";
      document.getElementById("containerVehicleWhite").className = "";

      inputRadio[0].removeAttribute("checked");
      inputRadio[1].removeAttribute("checked");
      inputRadio[position].setAttribute("checked","checked");
      document.getElementById("van").setAttribute("src","./assets/img/transporteVan.png");
      document.getElementById("blanco").setAttribute("src","./assets/img/transporteBlanco.png");
      document.getElementById("electrico").setAttribute("src","./assets/img/transporteElectricoSelected.png");
      this.bag_id = "26"; //this.bag_id = this._cookieService.get('shipping_bag_id_3');
      this.dataServiceTransport.bag_id = "26";
      this.type_service_string = "Eléctrico";
    }
  }
  public viewFormTarget = false;
  //---Selecciona el typo de pago
  selectTypePay(type:string){
    // console.log(type, "this is te oay");
    if(type=="0"){
      this.pay = "1";
      if (this._cookieService.get('credit_last_digits') !== undefined) {
          this.viewFormTarget = false;
          this.paymentGateWayPost();
          this.refreshView();
      } else {
          this.viewFormTarget = true;
          this.refreshView();
      }

    }
    else if(type=="1"){
      this.pay = "5";
      this.viewFormTarget = false;
    }
    else if(type=="2"){
      this.pay = "4";
      this.viewFormTarget = false;
    }
    else{
      this.pay = null;
      this.viewFormTarget = false;
    }
    this.changeValidateInput('pay');
  }
  //---Editar valores
  editInfo(typeInfo: string){
    var typeService = 1;
    if(this.type_service=="34"){
      this.tab.active = true;
      typeService = 1;
    }
    else if(this.type_service=="37"){
      this.tab.active = false;
      typeService = 2;
    }
    if(typeInfo=="origin"){
      if(typeService==1){
        this.nextProcess('viewFromService1', 'form', 0);
      }else{
        this.statusOriginSend = true;
        this.nextProcess('viewFromService3', 'form', 0);
      }
    }
    else if(typeInfo=="type_vehicle"){
      this.nextProcess('viewFromService1', 'form', 0);
    }
    else if(typeInfo=="destination"){
      if(typeService==1){
        this.nextProcess('viewFromService1', 'form', 0);
      }else{
        this.statusOriginSend = true;
        this.nextProcess('viewFromService3', 'form', 0);
      }
    }
    else if(typeInfo=="addressee"){
      this.statusOriginSend = false;
      this.loadComponentsGoogleMaps();
      this.nextProcess('viewFromService5', 'form', 0);
      //document.getElementById();
    }
  }
  setInputs(type: number){
    var thisClass = this;
    setTimeout(function() {
      thisClass.markerOrigin.setMap(null);
      thisClass.markerDestination.setMap(null);
      thisClass.markerOrigin=null;
      thisClass.markerDestination=null;
      var iconOrigin = {
        url: "./assets/img/pinOrigen.png",
        scaledSize: new google.maps.Size(29, 42),
        origin: new google.maps.Point(0,0),
        anchor: new google.maps.Point(0, 0)
      };
      thisClass.markerOrigin = new google.maps.Marker({
        position: new google.maps.LatLng(thisClass.origin_latitude, thisClass.origin_longitude),
        animation:  google.maps.Animation.DROP,
        icon: iconOrigin,
        map: thisClass.googleMap
      });
      thisClass.googleMap.panTo(new google.maps.LatLng(thisClass.origin_latitude, thisClass.origin_longitude));
      var iconDestination = {
        url: "./assets/img/pinDestino.png",
        scaledSize: new google.maps.Size(29, 42),
        origin: new google.maps.Point(0,0),
        anchor: new google.maps.Point(0, 0)
      };

      thisClass.markerDestination = new google.maps.Marker({
        position: new google.maps.LatLng(thisClass.destiny_latitude, thisClass.destiny_longitude),
        animation:  google.maps.Animation.DROP,
        icon: iconDestination,
        map: thisClass.googleMap
      });
      thisClass.googleMap.panTo(new google.maps.LatLng(thisClass.destiny_latitude, thisClass.destiny_longitude));
      /*if(type==1){
       console.log(thisClass.origin_address, thisClass.destiny_address, thisClass.type_service_string, thisClass.tip);
       document.getElementById("autocompleteMapaOrigin").setAttribute("value", thisClass.origin_address);
       document.getElementById("autocompleteMapaDestination").setAttribute("value", thisClass.destiny_address);
       //document.getElementById("propinaTransporte").removeAttribute("ngModel");
       document.getElementById("propinaTransporte").setAttribute("value", thisClass.tip);
       //document.getElementById("propinaTransporte").setAttribute("ngModel","");
       if(thisClass.type_service_string=="Blanco"){
       document.getElementById("vehiculoBlanco").setAttribute("checked", "checked");
       }
       else if(thisClass.type_service_string=="Eléctrico"){
       document.getElementById("vehiculoElectrico").setAttribute("checked", "checked");
       }
       else if(thisClass.type_service_string=="Van"){
       document.getElementById("vehiculoVan").setAttribute("checked", "checked");
       }
       }
       else if(type==2){
       console.log(thisClass.origin_address, thisClass.origin_detail, thisClass.destiny_address, thisClass.destiny_detail);
       document.getElementById("autocompleteMapaOriginSend").setAttribute("value", thisClass.origin_address);
       document.getElementById("detallesOrigenSend").setAttribute("value", thisClass.origin_detail);
       document.getElementById("autocompleteMapaDestinationSend").setAttribute("value", thisClass.destiny_address);
       document.getElementById("detallesDestinoSend").setAttribute("value", thisClass.destiny_detail);
       }
       else if(type==3){
       console.log(thisClass.description_text, thisClass.declared_value, thisClass.destiny_name, thisClass.tip);
       document.getElementById("autocompleteMapaOriginSend").setAttribute("value", thisClass.origin_address);
       document.getElementById("detallesOrigenSend").setAttribute("value", thisClass.origin_detail);
       document.getElementById("autocompleteMapaDestinationSend").setAttribute("value", thisClass.destiny_address);
       document.getElementById("detallesDestinoSend").setAttribute("value", thisClass.destiny_detail);
       document.getElementById("descripcionSend").setAttribute("value", thisClass.description_text);
       document.getElementById("valorDeclaradoSend").setAttribute("value", thisClass.declared_value);
       document.getElementById("destinatarioSend").setAttribute("value", thisClass.destiny_name);
       document.getElementById("propinaEnviar").setAttribute("value", thisClass.tip);
       }*/
    }, 3000);
  }
  public loginSuccess(){
    // console.log("login correcto");
    //this.payService();
    // console.log(this);
    if(CookieService.prototype.get('credit')!=undefined){
      //if(this._cookieService.get('credit')!=undefined){
      this.viewPayCredit = true;
      document.getElementById("viewPayCredit").style.display = "block";
      document.getElementById("cancelProcessLoading").click();
    } else {
      document.getElementById("btnPay").click();
    }
  }
  public statusOriginSend = false;
  //---Cambio de vista
  nextProcess(typeView:any, form:any, typeService: number) {
    // console.log(typeView, form, typeService);
    switch (typeView) {
      case "viewFromService1":
        this._cookieService.put('orderFormService', '1');
        this.viewFromService = true;
        this.viewContribution = false;
        this.viewSuccess = false;
        this.viewOriginTransport = true;
        this.viewDestinationTransport = false;
        this.markerOriginStatus = true;
        this.markerDestinationStatus = false;
        this.setInputs(1);
        this.loadComponentsGoogleMaps();
        //this.loadComponentPlaceInputOriginTransport();
        break;
      case "viewFromService2":
        this._cookieService.put('orderFormService', '1');
        this.viewFromService = true;
        this.viewContribution = false;
        this.viewSuccess = false;
        this.viewOriginTransport = false;
        this.viewDestinationTransport = true;
        this.markerOriginStatus = false;
        this.markerDestinationStatus = true;
        this.loadComponentPlaceInputDestinationTransport();
        break;
      case "viewFromService3":
        this._cookieService.put('orderFormService', '1');
        this.viewFromService = true;
        this.viewContribution = false;
        this.viewSuccess = false;
        this.viewOriginSend = true;
        this.viewDestinationSend = false;
        this.markerOriginStatus = true;
        this.markerDestinationStatus = false;
        this.setInputs(2);
        this.loadComponentPlaceInputOriginSend();
        this.loadComponentPlaceInputDestinationSend();
        setTimeout(function() {
          document.getElementById("viewOriginSend").style.display = "block";
          //this.viewDestinationSend = true;
          document.getElementById("viewDestinationSend").style.display = "none";
        }, 100);
        break;
      case "viewFromService4":
        let status1 = true;
        let status2 = true;
        let status3 = true;
        // console.log(form.value);
        if(form.value.origin_latitude.toString().trim().length<1){
          status1 = false;
          document.getElementById("status4").setAttribute("style","display: block !important");
        } else {
          status1 = true;
          document.getElementById("status4").setAttribute("style","display: none !important");
          document.getElementById("autocompleteMapaOriginSend").className = "form-control col-sm-12";
        }

        if(form.value.destiny_latitude.toString().trim().length<1){
          status2 = false;
          document.getElementById("status5").setAttribute("style","display: block !important");
        } else {
          status2 = true;
          document.getElementById("status5").setAttribute("style","display: none !important");
          document.getElementById("autocompleteMapaDestinationSend").className = "form-control col-sm-12";
        }
        if(status1 && status2){
          if(this.statusOriginSend)
          {
            this.onSubmitFormSend(form.value);
          }
          else
          {
            // console.log("jajajajajaajajajjajaja");
            this._cookieService.put('orderFormService', '1');
            this.statusFormTransport = false;
            this.viewFromService = true;
            this.viewContribution = false;
            this.viewSuccess = false;
            this.viewOriginSend = false;
            document.getElementById("viewOriginSend").style.display = "none";
            this.viewDestinationSend = true;
            document.getElementById("viewDestinationSend").style.display = "block";
            // console.log("jajajajajaajajajjajaja");
            this.markerOriginStatus = false;
            this.markerDestinationStatus = true;
            this.dataFOrm1 = form.value;
            // console.log(this.dataFOrm1.value);
            this.setInputs(3);
          }
        } else{
          this.statusFormTransport = true;
          if(!status1){
            var d = document.getElementById("autocompleteMapaOriginSend");
            d.className += " inputServiceImportant";
          }
          if(!status2){
            var d = document.getElementById("autocompleteMapaDestinationSend");
            d.className += " inputServiceImportant";
          }
        }

        break;
      case "processContribution":
        this.formServicesTransport = form.form;
        if(typeService == 1){
          if( (form.value.propinaTransporte).trim().length > 0 ){
            this.tip = form.value.propinaTransporte || "0";
          }else {
            this.tip = "0";
          }
        }
        else if(typeService == 2){
          if( (form.value.valorDeclaradoSend).trim().length > 0 ){
            this.declared_value = form.value.valorDeclaradoSend;
          }else {
            this.declared_value = null;
          }
        }
        this.processContribution(form, typeService);
        break;
      case "viewContribution":
        if( (form.value.valorDeclaradoSend).trim().length > 0 ){
          this.declared_value = form.value.valorDeclaradoSend;
        }else {
          this.declared_value = null;
        }
        this._cookieService.put('orderFormService', '2');
        this.viewFromService = false;
        this.viewContribution = true;
        document.getElementById("containerBond").setAttribute("style","display: none !important;");
        this.viewSuccess = false;
        document.getElementById("spinnerIcon").setAttribute("style","display: none !important;");
        document.getElementById("price_service").innerHTML = "$ "+this.price_service+" COP";
        document.getElementById("spinnerIconReal").setAttribute("style","display: none !important;");
        document.getElementById("price_service_real").innerHTML = "$ "+this.price_service+" COP";
        if(form=="transport"){
          this.containerTypeVehicle = true;
          this.containerDestinyName = false;
        }else{
          this.containerTypeVehicle = false;
          this.containerDestinyName = true;
        }
        break;
      case "viewSuccess":
        this._cookieService.put('orderFormService', '3');
        this.viewFromService = false;
        this.viewContribution = false;
        this.viewSuccess = true;
        break;
      case "viewFromService5":
        this._cookieService.put('orderFormService', '1');
        this.statusFormTransport = false;
        this.viewFromService = true;
        this.viewContribution = false;
        this.viewSuccess = false;
        this.viewOriginSend = false;
        setTimeout(function() {
          document.getElementById("viewOriginSend").style.display = "block";
          //this.viewDestinationSend = true;
          //document.getElementById("viewDestinationSend").style.display = "none";
          document.getElementById("changeProcessView").click();
        }, 100);
        /*this.markerOriginStatus = false;
         this.markerDestinationStatus = true;*/
      default:
        this._cookieService.put('orderFormService', '1');
        this.viewFromService = true;
        this.viewContribution = false;
        this.viewSuccess = false;
        this.viewOriginTransport = true;
        this.viewDestinationTransport = false;
    }
  }
  //---Servicio de cotización
  processContribution(form: any, typeService: number){
    // console.log(form.value);
    if(typeService==1){
      this.type_service = "34"; //this.type_service = this._cookieService.get('service_provider_id_1');
      if( (form.value.propinaTransporte).trim().length > 0 ){
        this.tip = form.value.propinaTransporte || "0";
      } else {
        this.tip = "0";
      }
    }
    else if(typeService==2){
      this.type_service_string = "Envío";
      if( (form.value.propinaEnviar).trim().length > 0 ){
        this.tip = form.value.propinaEnviar || "0";
      } else {
        this.tip = "0";
      }
      this.type_service = "37"; //this.type_service = this._cookieService.get('service_provider_id_2');
      this.bag_id = "18"; //this.bag_id = this._cookieService.get('shipping_bag_id_4');
      this.origin_detail = form.value.detallesOrigenSend;
      this.description_text = form.value.descripcionSend;
      this.destiny_detail = form.value.detallesDestinoSend;
      this.destiny_name = form.value.destinatarioSend;
    }
    else{
      this.type_service = "34"; //this.type_service = this._cookieService.get('service_provider_id_1');
      if( (form.value.propinaTransporte).trim().length > 0 ){
        this.tip = form.value.propinaTransporte || "0";
      } else {
        this.tip = "0";
      }
    }
    this.loadingProcess = true;
    this.origin_client = this._cookieService.get('id');
    this._httpService.postContribution(this.origin_latitude, this.origin_longitude, this.destiny_latitude, this.destiny_longitude, this.type_service, this.bag_id, this.tip, this.declared_value, this.dataFOrm1.destiny_address,this.dataFOrm1.origin_address, this.origin_client, this.valuePayDefault).subscribe(
        data => this.postService = JSON.stringify(data),
        error => this.responseContribution(this.postService),
        () => this.responseContribution(this.postService)
    );
  }
  //---Respuesta de la cotización
  responseContribution(data:any){
    // console.log("responseContribution");
    let dataResponse = JSON.parse(data);
    // console.log(dataResponse);
    this.loadingProcess = false;
    document.getElementById('cancelProcessLoading').click();
    if(dataResponse.return){
      this.loadTop();
      if(dataResponse.data.price=="El envio supera la distancia maxima"){
        this._cookieService.put('orderFormService', '1');
        this.viewFromService = true;
        this.viewContribution = false;
        this.viewSuccess = false;
      } else if(dataResponse.data.price==null){
        this._cookieService.put('orderFormService', '1');
        this.viewFromService = true;
        this.viewContribution = false;
        this.viewSuccess = false;
      } else {
        this.quotation_id = dataResponse.data.quotation_id;
        this.distance = (dataResponse.data.km).toString();
        this.amount = (dataResponse.data.price).toString();
        this.time = (dataResponse.data.time).toString();
        this.price_service=dataResponse.data.price;
        this.polyline = dataResponse.data.polyline;
        var thisClass= this;
        this._cookieService.put('orderFormService', '2');
        this.viewFromService = false;
        this.viewContribution = true;
        this.viewSuccess = false;
        if(this.type_service_string=="Envío"){
          thisClass.containerTypeVehicle = false;
          thisClass.containerDestinyName = true;
          document.getElementById('btnViewContributionSend').click();
          setTimeout(function() {
            thisClass.validateUserCorporative();
            document.getElementById("containerBond").setAttribute("style","display: none !important;");
            document.getElementById("spinnerIcon").setAttribute("style","display: none !important"!);
            document.getElementById("price_service").innerHTML = "$ "+thisClass.price_service+" COP";
            document.getElementById("spinnerIconReal").setAttribute("style","display: none !important;");
            document.getElementById("price_service_real").innerHTML = "$ "+thisClass.price_service+" COP";
          }, 3000);
        }else{
          thisClass.containerTypeVehicle = true;
          thisClass.containerDestinyName = false;
          document.getElementById('btnViewContribution').click();
          setTimeout(function() {
            thisClass.validateUserCorporative();
            document.getElementById("containerBond").setAttribute("style","display: none !important;");
            document.getElementById("spinnerIcon").setAttribute("style","display: none !important;");
            document.getElementById("price_service").innerHTML = "$ "+thisClass.price_service+" COP";
            document.getElementById("spinnerIconReal").setAttribute("style","display: none !important;");
            document.getElementById("price_service_real").innerHTML = "$ "+thisClass.price_service+" COP";
          }, 3000);
        }
      }
    }else{
      // console.log("Error cotizando");
      this.loadingProcess = false;
      document.getElementById('cancelProcessLoading').click();
    }
  }

  validateUserCorporative () {
    let credit = this._cookieService.get('credit');
    if ( credit !== undefined ) {
      document.getElementById('viewPayCredit').style.display = 'block';
    } else {
      document.getElementById('viewPayCredit').style.display = 'none';
      // document.getElementById("viewPayCredit").style.display = "none";
    }
  }
  //---Abre el modal de bono
  public showChildModal():void {
    if(this._cookieService.get('id')!=undefined){
      this.modalBono.show();
    }else{
      this.loadingProcess = false;
      document.getElementById('cancelProcessLoading').click();
      this._httpService.openModalLogin();
    }
  }

  //---Oculta el modal de bono
  public hideChildModal() {
    this.message_bond = "Ingresa el código de tu bono";
    this.modalBono.hide();
  }
  //---Vistas de bono
  showViewBond(dataResponse: any, status: boolean){
    // console.log(dataResponse, status);
    var thisClass = this;
    if(status){
      this.hideChildModal();
      this.message_bond = dataResponse.message;
      document.getElementById("containerBond").setAttribute("style","display: block !important;");
      document.getElementById("viewCodeSuccess").setAttribute("style","display: block !important;");
      document.getElementById("viewOpenModalBond").setAttribute("style","display: none !important;");
      document.getElementById("bond_service").innerHTML = "$ "+dataResponse.data[0].discount+" COP";
      this.bond_service = dataResponse.data[0].discount;
      //this.updateValueAmount(dataResponse.data[0].discount);
      var priceValue = parseInt(this.price_service);
      var discountValue = parseInt(dataResponse.data[0].discount);
      if(discountValue<=priceValue){
        this.price_service = (priceValue-discountValue);
        document.getElementById("spinnerIcon").setAttribute("style","display: none !important;");
        document.getElementById("price_service").innerHTML = "$ "+this.price_service+" COP";
        //document.getElementById("price_service").setAttribute("value","$ "+this.price_service+" COP");
      }else{
      }
    }else{
      this.message_bond = dataResponse.message;
      document.getElementById("viewOpenModalBond").setAttribute("style","display: block !important;");
      document.getElementById("viewCodeSuccess").setAttribute("style","display: none !important;");
      document.getElementById("containerBond").setAttribute("style","display: none !important;");
    }
  }
  //---Servicio de bono
  confirmCode(form: any){
    this.loadingBoundProcess = true;
    this.code_confirm = form.value.inputBono;
    this._httpService.confirmCode(this.code_confirm, this._cookieService.get('id'), this.amount).subscribe(
        data => this.postService = JSON.stringify(data),
        error => this.respConfirmCode(this.postService),
        () => this.respConfirmCode(this.postService)
    );
  }
  public code_value = null;
  // Respuesta del servicio de bono
  respConfirmCode(data:any){
    this.loadingBoundProcess = false;
    let dataResponse = JSON.parse(data);
    // console.log(dataResponse);
    if(dataResponse.return){
      if(dataResponse.message=="Bono aplicado exitosamente"){
        document.getElementById("code_value").innerHTML = "$ "+ dataResponse.data[0].discount;
        this.code_value = dataResponse.data[0].discount;
        this.showViewBond(dataResponse, true);
      }else{
        this.showViewBond(dataResponse, false);
      }
    }else{
      this.showViewBond(dataResponse, false);
    }
  }
  public statusFormTransport = false;
  onSubmitFormTransport(data) {
    let status1 = true;
    let status2 = true;
    let status3 = true;
    // console.log(data);
    if (data.origin_latitude.toString().trim().length < 1) {
      status1 = false;
      document.getElementById('status1').setAttribute("style","display: block !important");
    } else {
      status1 = true;
      document.getElementById("status1").setAttribute("style","display: none !important");
      document.getElementById("autocompleteMapaOrigin").className = "form-control col-sm-12";
    }
    // console.log(data.destiny_latitude.toString());
    if(data.destiny_latitude.toString().trim().length<1){
      status2 = false;
      document.getElementById("status2").setAttribute("style","display: block !important");
    } else {
      status2 = true;
      document.getElementById("status2").setAttribute("style","display: none !important");
      document.getElementById("autocompleteMapaDestination").className = "form-control col-sm-12";
    }
    // console.log(data.bag_id.toString());
    if(data.bag_id.toString().trim().length<1){
      status3 = false;
      document.getElementById("status3").setAttribute("style","display: block !important");
    } else {
      status3 = true;
      document.getElementById("status3").setAttribute("style","display: none !important");
    }
    if (status1 && status2 && status3) {
      this.statusFormTransport = false;
      this.containerTypeVehicle = true;
      this.containerDestinyName = false;
      this.type_service = "34";
      // console.log(data);
      this.dataFOrm1 = data;
      this.loadingProcess = true;
      this.tip = data.tip || "0";
      this._httpService.postContribution(data.origin_latitude, data.origin_longitude, data.destiny_latitude, data.destiny_longitude, data.type_service, data.bag_id, data.tip, data.declared_value, data.destiny_address,data.origin_address,this._cookieService.get('id'), this.valuePayDefault ).subscribe(
          data => this.postService = JSON.stringify(data),
          error => this.responseContribution(this.postService),
          () => this.responseContribution(this.postService)
      );
    } else{
      this.statusFormTransport=true;
      if(!status1){
        var d = document.getElementById("autocompleteMapaOrigin");
        d.className += " inputServiceImportant";
      }
      if(!status2){
        var d = document.getElementById("autocompleteMapaDestination");
        d.className += " inputServiceImportant";
      }
      if(!status3){
        //document.getElementById("").className = "inputServiceImportant";
      }
    }
  }
  changeValidateInput(type : string){
    // console.log(this.statusFormTransport);
    if(this.statusFormTransport){
      switch (type) {
        case "origin_latitude":
          // console.log(type);
          document.getElementById("status1").setAttribute("style","display: none !important");
          document.getElementById("autocompleteMapaOrigin").className = "form-control col-sm-12";
          break;
        case "destiny_latitude":
          // console.log(type);
          document.getElementById("status2").setAttribute("style","display: none !important");
          document.getElementById("autocompleteMapaDestination").className = "form-control col-sm-12";
          break;
        case "bag_id":
          // console.log(type);
          document.getElementById("status3").setAttribute("style","display: none !important");
          break;
        case "origin_latitude_send":
          // console.log(type);
          document.getElementById("status4").setAttribute("style","display: none !important");
          document.getElementById("autocompleteMapaOriginSend").className = "form-control col-sm-12";
          break;
        case "destiny_latitude_send":
          // console.log(type);
          document.getElementById("status5").setAttribute("style","display: none !important");
          document.getElementById("autocompleteMapaDestinationSend").className = "form-control col-sm-12";
          break;
        case "description_text":
          // console.log(type);
          document.getElementById("status6").setAttribute("style","display: none !important");
          document.getElementById("description_text").className = "form-control col-sm-12";
          break;
        case "declared_value":
          // console.log(type);
          document.getElementById("status7").setAttribute("style","display: none !important");
          document.getElementById("declared_value").className = "form-control col-sm-12";
          break;
        case "destiny_name":
          // console.log(type);
          document.getElementById("status8").setAttribute("style","display: none !important");
          document.getElementById("destiny_name").className = "form-control col-sm-12";
          break;
        case "pay":
          // console.log(type);
          document.getElementById("status9").setAttribute("style", "display: none !important;");
          break;

        default:
          // console.log(type);
          break;
      }
    }
  }
  onSubmitFormSend(data) {
    let status1 = true;
    let status2 = true;
    let status3 = true;

    // console.log(data);
    if(data.description_text.toString().trim().length<1){
      status1 = false;
      document.getElementById("status6").setAttribute("style","display: block !important");
    } else {
      status1 = true;
      document.getElementById("status6").setAttribute("style","display: none !important");
      document.getElementById("description_text").className = "form-control col-sm-12";
    }

    if(data.declared_value.toString().trim().length<1){
      status2 = false;
      document.getElementById("status7").setAttribute("style","display: block !important");
    } else {
      status2 = true;
      document.getElementById("status7").setAttribute("style","display: none !important");
      document.getElementById("declared_value").className = "form-control col-sm-12";
    }

    if(data.destiny_name.toString().trim().length<1){
      status3 = false;
      document.getElementById("status8").setAttribute("style","display: block !important");
    } else {
      status3 = true;
      document.getElementById("status8").setAttribute("style","display: none !important");
      document.getElementById("destiny_name").className = "form-control col-sm-12";
    }
    if(status1 && status2 && status3){
      this.statusFormTransport=false;
      this.type_service_string="Envío" ;
      // console.log(data);
      this.dataFOrm2 = data;
      this.loadingProcess = true;
      this.type_service = this._cookieService.get('service_provider_id_2') || "37";
      this.bag_id = this._cookieService.get('shipping_bag_id_4') || "18";
      this.destiny_name = data.destiny_name;
      // console.log("Holanda");
      // console.log(this.dataFOrm1);
      // console.log(this.dataFOrm1.origin_latitude, this.dataFOrm1.origin_longitude, this.dataFOrm1.destiny_latitude, this.dataFOrm1.destiny_longitude, this.type_service, this.bag_id, this.dataFOrm2.tip, this.dataFOrm2.declared_value);
      this.tip = this.dataFOrm2.tip || "0";
      this.origin_client = this._cookieService.get('id');
      this._httpService.postContribution(this.dataFOrm1.origin_latitude, this.dataFOrm1.origin_longitude, this.dataFOrm1.destiny_latitude, this.dataFOrm1.destiny_longitude, this.type_service, this.bag_id, this.dataFOrm2.tip, this.dataFOrm2.declared_value, this.dataFOrm1.destiny_address,this.dataFOrm1.origin_address, this.origin_client, this.valuePayDefault).subscribe(
          data => this.postService = JSON.stringify(data),
          error => this.responseContribution(this.postService),
          () => this.responseContribution(this.postService)
      );
    } else{
      this.statusFormTransport=true;
      if(!status1){
        var d = document.getElementById("description_text");
        d.className += " inputServiceImportant";
      }
      if(!status2){
        var d = document.getElementById("declared_value");
        d.className += " inputServiceImportant";
      }
      if(!status3){
        var d = document.getElementById("destiny_name");
        d.className += " inputServiceImportant";
      }
    }
  }
  //---Enviar solicitud de servicio
  payService(){
    //this.modalLoading.show();
    this.loadingProcess = true;
    if(this.pay!=null){
      this.statusFormTransport = false;
      document.getElementById("status9").setAttribute("style", "display: none !important;");
      if(this._cookieService.get('id')!=undefined){
        if(this._cookieService.get('id')!=undefined){
          this.origin_client = this._cookieService.get('id');
        }
        this._httpService.pagarServicio(this.distance, this.origin_client, this.origin_address, this.origin_latitude, this.origin_longitude, this.destiny_address, this.destiny_latitude, this.destiny_longitude, this.type_service, this.amount, this.pay, this.time, this.tip, this.bag_id, this.code_confirm, this.origin_detail, this.description_text, this.destiny_detail, this.destiny_name, this.polyline, this.quotation_id).subscribe(
            data => this.postService = JSON.stringify(data),
            error => this.pagoCorrecto(this.postService),
            () => this.pagoCorrecto(this.postService)
        );
      }else{
        this.loadingProcess = false;
        // console.log("esta en false");
        document.getElementById('cancelProcessLoading').click();
        this._httpService.openModalLogin();
      }
    }else{
      this.statusFormTransport = true;
      this.loadingProcess = false;
      document.getElementById("status9").setAttribute("style", "display: block !important;");
    }
  }
  responseGetCreditString: String;
  //---Respuesta de  la confirmacion
  pagoCorrecto(data:any){
    this.loadingProcess = false;
    // console.log("Respuesta de solicitud de servicio");
    document.getElementById('cancelProcessLoading').click();
    let dataResponse = JSON.parse(data);
    // console.log(dataResponse);
    if(dataResponse.return){
      //this.hideChildModal();
      document.getElementById('nextProcessViewSuccess').click();
    }else{
      document.getElementById("responsePayService").innerHTML = dataResponse.message;
      // console.log("Error confirmando");
    }
  }

  responseGetCredit(data: any){
    // console.log(data);
    let dataResponseGetCredit = JSON.parse(data);
    // console.log(dataResponseGetCredit);
    if ( dataResponseGetCredit.return ) {
      this.creditValue = dataResponseGetCredit.data.credit;
      document.getElementById("creditInput").setAttribute("value", "SALDO DISPONIBLE $"  + this.creditValue);
      this.viewPayCredit = true;
      // document.getElementById("viewPayCredit").style.display = "block";
      this._cookieService.put("credit", this.creditValue);
      // console.log(this._cookieService.get("credit"));
    }
    else {
      // console.log("problemas al actualizar el credito");
      this.creditValue = this._cookieService.get("credit");
      document.getElementById("creditInput").setAttribute("value", "SALDO DISPONIBLE $"  + this.creditValue);
      this.viewPayCredit = true;
      // document.getElementById("viewPayCredit").style.display = "block";
    }
  }
  //---Cambio vista solicitud exitosa
  nextProcessViewSuccess(){
    this._cookieService.put('orderFormService', '3');
    this.viewFromService = false;
    this.viewContribution = false;
    this.viewDataConfirm = false;
    this.viewSuccess = true;
    var dataPostService = JSON.parse(this.postService);
    // console.log(dataPostService);
    this._cookieService.put('shipping_id', dataPostService.data.shipping_id)
    this._cookieService.put('statusServices', 'true');
    this._httpService.socketOn(this._cookieService.get('socket1')).subscribe(
        data => this.postServiceSocket = JSON.stringify(data),
        error => this.responseSocket(this.postServiceSocket, 1),
        () => this.responseSocket(this.postServiceSocket, 1)
    );
  }
  confirmCancelBeep(type: string) {

    if(type=="si")
    {
      // console.log("vamo a cancelar el serv");
      var postCancelService = "";
      // console.log("cancelará el servicio");
      this._httpService.cancelService(this._cookieService.get('shipping_id')).subscribe(
          data => postCancelService = JSON.stringify(data),
          error => this.responseCancelService(postCancelService),
          () => this.responseCancelService(postCancelService)
      );
    }
    else if(type=="no")
    {
      this.modalConfirmCancelBeep.hide();
    }
    else
    {
      this.confirmCancelBeep("no");
    }

  }
  public cancelService(){
    this.modalConfirmCancelBeep.show();
  }

  responseCancelService(data){
    // console.log(data);
    var dataPostServiceCancel = JSON.parse(data);
    // console.log(dataPostServiceCancel);
    this._cookieService.put("validRequiestService","false");
    this._httpService.setTimeoutServiceTokenApp();
    this.modalConfirmCancelBeep.hide();
    if(dataPostServiceCancel.return){
      // console.log(dataPostServiceCancel.message);
      this._cookieService.put('shipping_id', '')
      this._cookieService.put('statusServices', 'false');
      this.showModalCancelBeep();
    }else{
      // console.log("problemas al cancelar servicio");
    }
  }
  responseSocket (data, type) {
    // console.log("respuesta del socket");
    // console.log(data);
    // console.log(type);
    var dataPost = JSON.parse(data);
    this._cookieService.put("validRequiestService","false");
    this._httpService.setTimeoutServiceTokenApp();
    if(dataPost.token=="Could not bind to socket↵" || dataPost.token=="Could not bind to socket")
    {

    }
    else if (dataPost.token!="Could not bind to socket↵" && dataPost.token!="Could not bind to socket")
    {
      if(type==1){
        //document.getElementById("btnDeliveryVerification").click();
        // console.log("HARA CLICK A PUSH WEB - CONFIRMAR");
        document.getElementById("btnPushWeb").click();
      }else if(type==2){
        if(dataPost.token!=undefined){
          this.pushWebCancelService();
        }else{
          // console.log("HARA CLICK A LLEGO BEEP");
          document.getElementById("btnShowModalConfirmBeep").click();
        }
      }else{

      }
    }
  }

  public showModalConfirmBeep(){
    this.modalConfirmBeep.show();
  }
  public hideModalConfirmBeep(){
    this.modalConfirmBeep.hide();
    this._routerModule.navigateByUrl('home');
  }
  pushWeb(){
    // console.log("pushWeb");
    // console.log(this);
    var dataPostSocket = JSON.parse(this.postServiceSocket);
    // console.log(dataPostSocket);
    var dataPostService = JSON.parse(this.postService);
    // console.log(dataPostService);
    // console.log("");
    var postServicePushWeb: string;
    this._httpService.pushWeb(dataPostSocket.token, dataPostService.data.shipping_id).subscribe(
        data => postServicePushWeb = JSON.stringify(data),
        error => this.responsPushWeb(postServicePushWeb),
        () => this.responsPushWeb(postServicePushWeb)
    );
  }
  pushWebCancelService(){
    // console.log("pushWeb");
    // console.log(this);
    var dataPostSocket = JSON.parse(this.postServiceSocketConfirm);
    // console.log(dataPostSocket);
    var dataPostService = JSON.parse(this.postService);
    // console.log(dataPostService);
    // console.log("");
    var postServicePushWeb: string;
    this._httpService.pushWeb(dataPostSocket.token, dataPostService.data.shipping_id).subscribe(
        data => postServicePushWeb = JSON.stringify(data),
        error => this.responsPushWeb(postServicePushWeb),
        () => this.responsPushWeb(postServicePushWeb)
    );
  }
  responsPushWeb(data){
    // console.log("la respuesta es");
    // console.log(data);
    // console.log(this);
    var dataPostService = JSON.parse(data);
    var thisClass = this;
    // console.log(dataPostService);
    if(dataPostService.data.type==1){
      // console.log("es 11111111111");
      this.postServicePushWeb = data;
      this.logisticresource_id = dataPostService.data.logisticId;
      this.name_employee = dataPostService.data.info.name_employee;
      this.lastName_employee = dataPostService.data.info.lastName_employee;
      this.vehicle_brand = dataPostService.data.info.vehicle_brand;
      this.vehicle_model = dataPostService.data.info.vehicle_model;
      this.icense_plate = dataPostService.data.info.icense_plate;
      this.arrival_time = dataPostService.data.info.arrival_time;
      this.security_code = dataPostService.data.info.security_code;
      this.picture_employee = dataPostService.data.info.picture_employee;
      var rating = parseInt(dataPostService.data.info.rating_employee);
      this.rating_employee = rating;
      document.getElementById("btnViewConfirmService").click();
      this.viewConfirmService(data);
    } else if(dataPostService.data.type==4){
      // console.log("es 44444444444444");
      this.showModalCancelBeep();
      /*setTimeout(function() {
       thisClass.hideModalCancelBeep();
       thisClass._routerModule.navigateByUrl('home');
       }, 2000);*/
    } else if(dataPostService.data.type==10){
      // console.log("es 101010101010");
      this.modalCancelDriveBeep.show();
      let sockets = (dataPostService.data.info.socket).split(",");
      // console.log(sockets);
      this._cookieService.put("socket1", sockets[0]);
      this._cookieService.put("socket2", sockets[1]);
      /*setTimeout(function() {
       // console.log("click para cerrar");
       document.getElementById("btnHideModalCancelBeep").click();
       }, 2000);*/
      setTimeout(function() {
        // console.log("otra vez sokcet PRUEBAAAAAA");
        thisClass.nextProcessViewSuccess();
      }, 1000);
    } else if(dataPostService.data.type==12){
      // console.log("llego el conductor");
      /*let sockets = (dataPostService.data.info.socket).split(",");
       console.log(sockets);
       this._cookieService.put("socket1", sockets[0]);
       this._cookieService.put("socket2", sockets[1]);*/
      this.showModalConfirmBeep();
      /*setTimeout(function() {
       thisClass.hideModalConfirmBeep();
       }, 2000);*/
    } else{
      // console.log("es esleeeeeeeeeee");
    }
  }
  public showModalCancelBeep(){
    this.modalCancelBeep.show();
  }
  public hideModalCancelBeep(){
    this.modalCancelBeep.hide();
    this._routerModule.navigateByUrl('home');
  }
  viewConfirmService(data){
    var dataPostService = JSON.parse(data);
    // console.log(dataPostService);
    if(dataPostService.data.type==1){
      this._cookieService.put('orderFormService', '3');
      this.viewDataConfirm = true;
      this.viewFromService = false;
      this.viewContribution = false;
      this.viewSuccess = false;
      this.viewOriginTransport = false;
      this.viewDestinationTransport = false;
      this.loadComponentsGoogleMapsPushWeb();
    }else{
      alert("servicio cancelado!!");
    }
  }
  btnViewConfirmService(){
    // console.log("CLICK!");
  }
  public logisticresource_id = "-";
  public name_employee = "-";
  public lastName_employee = "-";
  public vehicle_brand = "-";
  public vehicle_model = "-";
  public icense_plate = "-";
  public arrival_time = "-";
  public security_code = "-";
  public picture_employee = "-"
  public rating_employee : Number;
  public postServiceSocketConfirm:string;
  //cancela el servicio http://192.168.0.113/api_devel/Shipping/cancel
  loadComponentsGoogleMapsPushWeb() {
    // console.log("Cargara todo de push web google");
    //---Muestro la primer parte del formulario del tab Transporte
    var thisClass = this;
    // console.log("Hará el socket para recibir la confirmció de que el beep ya llego");
    this._httpService.socketOn(this._cookieService.get('socket2')).subscribe(
        data => this.postServiceSocketConfirm = JSON.stringify(data),
        error => this.responseSocket(this.postServiceSocketConfirm, 2),
        () => this.responseSocket(this.postServiceSocketConfirm, 2)
    );
    setTimeout(function() {
      //---Cargo el mapa
      var geocoder = new google.maps.Geocoder();
      var mapProp = {
        center: new google.maps.LatLng(4.6762881, -74.05177549999999),
        zoom: 18,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };

      var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
      thisClass.getPositionBeep(thisClass, map);
      if(thisClass.dataServiceTransport.origin_latitude)
      {
        var iconOrigin = {
          url: "./assets/img/pinOrigen.png",
          scaledSize: new google.maps.Size(29, 42),
          origin: new google.maps.Point(0,0),
          anchor: new google.maps.Point(0, 0)
        };
        thisClass.markerOrigin = new google.maps.Marker({
          position: new google.maps.LatLng(thisClass.dataServiceTransport.origin_latitude, thisClass.dataServiceTransport.origin_longitude),
          animation:  google.maps.Animation.DROP,
          icon: iconOrigin,
          map: map
        });
      }
      if(thisClass.dataServiceTransport.destiny_latitude)
      {
        var iconDestination = {
          url: "./assets/img/pinDestino.png",
          scaledSize: new google.maps.Size(29, 42),
          origin: new google.maps.Point(0,0),
          anchor: new google.maps.Point(0, 0)
        };
        thisClass.markerDestination = new google.maps.Marker({
          position: new google.maps.LatLng(thisClass.dataServiceTransport.destiny_latitude, thisClass.dataServiceTransport.destiny_longitude),
          animation:  google.maps.Animation.DROP,
          icon: iconDestination,
          map: map
        });
      }

      if(thisClass.dataServiceSend.origin_latitude)
      {
        var iconOrigin = {
          url: "./assets/img/pinOrigen.png",
          scaledSize: new google.maps.Size(29, 42),
          origin: new google.maps.Point(0,0),
          anchor: new google.maps.Point(0, 0)
        };
        thisClass.markerOrigin = new google.maps.Marker({
          position: new google.maps.LatLng(thisClass.dataServiceSend.origin_latitude, thisClass.dataServiceSend.origin_longitude),
          animation:  google.maps.Animation.DROP,
          icon: iconOrigin,
          map: map
        });
      }
      if(thisClass.dataServiceSend.destiny_latitude)
      {
        var iconDestination = {
          url: "./assets/img/pinDestino.png",
          scaledSize: new google.maps.Size(29, 42),
          origin: new google.maps.Point(0,0),
          anchor: new google.maps.Point(0, 0)
        };
        thisClass.markerDestination = new google.maps.Marker({
          position: new google.maps.LatLng(thisClass.dataServiceSend.destiny_latitude, thisClass.dataServiceSend.destiny_longitude),
          animation:  google.maps.Animation.DROP,
          icon: iconDestination,
          map: map
        });
      }
    }, 1000);

  }

  public timerPositionBeep = null;
  public responseServiceGetLocation : string;
  getPositionBeep(thisClass, map){
    // console.log("Get position beep");
    this.timerPositionBeep = setInterval(()=>{

      thisClass._httpService.getLocation(this.logisticresource_id).subscribe(
          data => thisClass.responseServiceGetLocation = JSON.stringify(data),
          error => thisClass.responseGetPositionBeep(thisClass.responseServiceGetLocation, map),
          () => thisClass.responseGetPositionBeep(thisClass.responseServiceGetLocation, map)
      );

    }, 10000);
  }

  positionUbeepOrigin : any;
  positionUbeepNew : any;

  responseGetPositionBeep(data, map) {
    var dataPostService = JSON.parse(data);
    // console.log(dataPostService);
    // console.log("Entro responseGetPositionBeep");
    if(this.markerUbeep==null)
    {
      // console.log("Entro diferente de null, crea el marker");
      var iconBeep = {
        url: "./assets/img/pinBeep.png",
        scaledSize: new google.maps.Size(29, 42),
        origin: new google.maps.Point(0,0),
        anchor: new google.maps.Point(0, 0)
      };
      this.positionUbeepOrigin = [parseFloat(dataPostService.data.latitud), parseFloat(dataPostService.data.longitud)];
      this.markerUbeep = new google.maps.Marker({
        position: new google.maps.LatLng(parseFloat(dataPostService.data.latitud), parseFloat(dataPostService.data.longitud)),
        animation:  google.maps.Animation.DROP,
        icon: iconBeep,
        map: map
      });
      map.panTo(new google.maps.LatLng(parseFloat(dataPostService.data.latitud), parseFloat(dataPostService.data.longitud)));
      // this.transition([parseFloat(dataPostService.data.latitud), parseFloat(dataPostService.data.longitud)]);
    }
    else
    {
      var thisClass = this;
      /*console.log("Entro NO es diferente de null");
       if( this.positionUbeepNew== null )
       {*/
      // console.log("Entro POSITION NUEVA ES NULL");
      //this.transition();
      this.i = 0;
      this.deltaLat = (parseFloat(dataPostService.data.latitud) - this.positionUbeepOrigin[0])/this.numDeltas;
      this.deltaLng = (parseFloat(dataPostService.data.longitud) - this.positionUbeepOrigin[1])/this.numDeltas;
      this.positionUbeepOrigin[0] += this.deltaLat;
      this.positionUbeepOrigin[1] += this.deltaLng;
      var latlng = new google.maps.LatLng(this.positionUbeepOrigin[0], this.positionUbeepOrigin[1]);
      // console.log("1.1 ",this.positionUbeepOrigin, this.positionUbeepNew);
      // console.log();
      // console.log("1.2 ",latlng);
      this.markerUbeep.setPosition(latlng);
      // console.log(this.i, this.numDeltas);
      if(this.i!=this.numDeltas)
      {
        this.i++;
        setTimeout(function() {
          thisClass.positionUbeepOrigin[0] += thisClass.deltaLat;
          thisClass.positionUbeepOrigin[1] += thisClass.deltaLng;
          var latlng = new google.maps.LatLng(thisClass.positionUbeepOrigin[0], thisClass.positionUbeepOrigin[1]);
          // console.log("2.1 ",this.positionUbeepOrigin, this.positionUbeepNew);
          // console.log();
          // console.log("2.2 ",latlng);
          thisClass.markerUbeep.setPosition(latlng);
          thisClass.positionUbeepNew = [parseFloat(dataPostService.data.latitud), parseFloat(dataPostService.data.longitud)];
        }, 10);
      }
      //}
    }
  }

  public numDeltas = 100;
  public delay = 10; //milliseconds
  public i = 0;
  public deltaLat;
  public deltaLng;
  transition(){
    this.i = 0;
    this.deltaLat = (this.positionUbeepNew[0] - this.positionUbeepOrigin[0])/this.numDeltas;
    this.deltaLng = (this.positionUbeepNew[1] - this.positionUbeepOrigin[1])/this.numDeltas;
    // console.log("hizo algo que no se que es");
    this.moveMarker();
  }

  moveMarker(){
    // console.log("ENTROOOOOOOOOOOOOOOOOOOOOOOOOOOOOO a mover el marker");
    // console.log(this.positionUbeepOrigin);
    // console.log(this.positionUbeepNew);
    this.positionUbeepOrigin[0] += this.deltaLat;
    this.positionUbeepOrigin[1] += this.deltaLng;
    // console.log(this.positionUbeepOrigin);
    var latlng = new google.maps.LatLng(this.positionUbeepOrigin[0], this.positionUbeepOrigin[1]);
    this.markerUbeep.setPosition(latlng);
    if(this.i!=this.numDeltas){
      this.i++;
      // console.log("ENTROOOOOOOOOOOOOOOOOOOOOOOOOOOOOO MOVIO MARKER");
      /*setTimeout(function() {
       setTimeout(this.moveMarker, this.delay);
       }, 1000);*/
    }
  }

  ngOnDestroy() {
    // console.log(this.timerPositionBeep);
    if(this.timerPositionBeep!=null)
    {
      clearInterval(this.timerPositionBeep);
      // console.log("finish interval");
    }
  }

  responDeliveryVerification(data){
    // console.log(data);
    document.getElementById("btnPushWeb").click();
  }
  public deliveryVerification(){
    // console.log("deliveryVerification");
    // console.log(this);
    // console.log(HttpSolicitarService.prototype);
    // console.log(ServicesComponent.prototype);
    //console.log(ServiceComponent.prototype.postService);
    //console.log(HttpSolicitarService.prototype.deliveryVerification);
    //console.log(ServiceComponent.prototype.postServiceDeliveryVerification);
    //console.log(ServiceComponent.prototype.responDeliveryVerification);
    //console.log(this);
    //console.log(this._httpService);
    //this._httpService.deliveryVerification(this.postService.data.shipping_id).subscribe(
    /*var dataPostService = JSON.parse(ServiceComponent.prototype.postService);
     console.log(dataPostService);*/
    /*HttpSolicitarService.prototype.deliveryVerification("2222").subscribe(
     data => this.postServiceDeliveryVerification = JSON.stringify(data),
     error => this.responDeliveryVerification(this.postServiceDeliveryVerification),
     () => this.responDeliveryVerification(this.postServiceDeliveryVerification)
     );*/
  }

  paymentGateWay = new PaymentGateWay();
  responsePaymentGateWayPost: string;
  responsePaymentGateWayTokenize: string;
    messagePaymentGateWayTokenize: string = '';
    messagePaymentGateWayPost: string = '';

  paymentGateWayPost() {
    const data = {
        quotation_id: this.quotation_id,
        client_id: this._cookieService.get('id'),
        platform: 'web'
    };
    console.log(data);
      this.loadingProcessPaymentGateWayPost = true;
      this._httpService.paymentGateWayPost(data).subscribe(
          data => this.responsePaymentGateWayPost = JSON.stringify(data),
          error => this.responsePaymentGateWay(this.responsePaymentGateWayPost, 'post'),
          () => this.responsePaymentGateWay(this.responsePaymentGateWayPost, 'post')
      );
  }

  paymentGateWayTokenize() {
      this.paymentGateWay.client_id = this._cookieService.get('id');
      console.log(this.paymentGateWay);
      this.loadingProcessPaymentGateWayPost = true;
      this._httpService.paymentGateWayTokenize(this.paymentGateWay).subscribe(
          data => this.responsePaymentGateWayTokenize = JSON.stringify(data),
          error => this.responsePaymentGateWay(this.responsePaymentGateWayTokenize, 'tokenize'),
          () => this.responsePaymentGateWay(this.responsePaymentGateWayTokenize, 'tokenize')
      );
  }

  responsePaymentGateWay(data: any, type: string) {
    const dataParse = JSON.parse(data);
    console.log(dataParse, type);
    this.loadingProcessPaymentGateWayPost = false;
    if (type === 'post') {
      if (dataParse.status === 210) {
        this.messagePaymentGateWayPost = dataParse.message;
        this.refreshView();
      } else if (dataParse.status === 211) {
          this.messagePaymentGateWayPost = dataParse.message;
          this.refreshView();
      } else if (dataParse.status === 409) {
          this.messagePaymentGateWayPost = 'Seleccione otro medio de pago';
          this.refreshView();
      } else if (dataParse.status === 200) {
          this.messagePaymentGateWayPost = dataParse.message;
          this.refreshView();
          // aprobado continua con el proceso de post
          // this.payService();
      }
    } else if (type === 'tokenize') {
        if (dataParse.status === 409) {
            this.messagePaymentGateWayTokenize = dataParse.message;
            this.refreshView();
        } else if (dataParse.status === 200) {
            this.messagePaymentGateWayTokenize = dataParse.message;
            this.refreshView();
            this.paymentGateWayPost();
        }
    }
  }
}
