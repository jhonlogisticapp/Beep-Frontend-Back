import { Injectable }              from '@angular/core';
import { Http, Response }          from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable }              from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { ConstantUbeep } from './../constantsUbeep';

@Injectable()
export class HttpLoginService {

  private _urlLogin:string = "src/app/nav/services/http.service.login.php";
  private _urlLoginFacebook:string = "src/app/nav/services/http.service.loginfacebook.php";
  private _urlLoginGoogle:string = "src/app/nav/services/http.service.logingoogle.php";
  
  constructor(private _http: Http, private _ConstantUbeep: ConstantUbeep){ 
  }

  loginUser(email:string,password:string ){

    let data = JSON.stringify({ 
      key:"3edcdb20e0030daab21d0ba9af4c0dc2",
      email: email,
      pass: password,
      platform: "web",
      url: this._ConstantUbeep.urlServer
    });

    // console.log("Datos del login");
    // console.log(data);
    
    let headers = new Headers({ "Access-Control-Allow-Origin": "*" });
    let options = new RequestOptions({headers: headers, method: "POST" });
    
    return this._http.post(this._urlLogin, data, options)
        .map(res => res.json())
        .catch(this.handleError);
  }

  loginUserFacebook(email:string,password:string,idFacebook:string,name:string){

    let data = JSON.stringify({ 
      key:"3edcdb20e0030daab21d0ba9af4c0dc2",
      email: email,
      pass: password,
      platform: "web",
      facebook_id: idFacebook,
      name: name,
      url: this._ConstantUbeep.urlServer
    });

    // console.log("Datos del login con Facebook ahora si!");
    // console.log(data);

    let headers = new Headers({ "Access-Control-Allow-Origin": "*" });
    let options = new RequestOptions({headers: headers, method: "POST" });
    
    return this._http.post(this._urlLoginFacebook, data, options)
        .map(res => res.json())
        .catch(this.handleError);  
  }

  loginUserGoogle(email:string,password:string,idGoogle:string,name:string){

    let data = JSON.stringify({ 
      key:"3edcdb20e0030daab21d0ba9af4c0dc2",
      email: email,
      pass: password,
      platform: "web",
      google_id: idGoogle,
      name: name,
      url: this._ConstantUbeep.urlServer
    });

    // console.log("Datos del login con GOOGLE ahora si!");
    // console.log(data);

    let headers = new Headers({ "Access-Control-Allow-Origin": "*" });
    let options = new RequestOptions({headers: headers, method: "POST" });
    
    return this._http.post(this._urlLoginGoogle, data, options)
        .map(res => res.json())
        .catch(this.handleError);  
  }

  private handleError (error: Response) {
      // console.error(error);
      return Observable.throw(error.json().error || ' error');
  }
}