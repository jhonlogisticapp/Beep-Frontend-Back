import { Injectable }              from '@angular/core';
import { Http, Response }          from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable }              from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { ConstantUbeep } from './../constantsUbeep';

@Injectable()
export class HttpRegisterService {

    private _urlLogin:string = "src/app/nav/services/http.service.register.php";
    
    constructor(private _http: Http, private _ConstantUbeep: ConstantUbeep){ }

    registerAcount(userNameRegister:string, passwordRegister:string, emailRegister:string, cellPhoneRegister:string, identifyRegister: string){
      
      let data = JSON.stringify({ 
        key:"3edcdb20e0030daab21d0ba9af4c0dc2",
        name: userNameRegister,
        email: emailRegister,
        pass: passwordRegister,
        cellphone: cellPhoneRegister,
        identify: identifyRegister,
        platform: "web",
        url: this._ConstantUbeep.urlServer
      });

      // console.log("Datos del registrooooooooooooooooooo");
      // console.log(data);

      let headers = new Headers({ "Access-Control-Allow-Origin": "*" });
      let options = new RequestOptions({headers: headers, method: "POST" });
      
      return this._http.post(this._urlLogin, data, options)
          .map(res => res.json())
          .catch(this.handleError);  
    }

    registerAcountFacebook(userNameRegister:string, emailRegister:string, passwordRegister:string, idFacebook:string ){

      let data = JSON.stringify({ 
        key:"3edcdb20e0030daab21d0ba9af4c0dc2",
        name: userNameRegister,
        email: emailRegister,
        pass: passwordRegister,
        idFacebook:idFacebook,
        platform: "web",
        url: this._ConstantUbeep.urlServer
      });

      // console.log("Datos del registro con Facebook");
      // console.log(data);

      let headers = new Headers({ "Access-Control-Allow-Origin": "*" });
      let options = new RequestOptions({headers: headers, method: "POST" });
      
      return this._http.post(this._urlLogin, data, options)
          .map(res => res.json())
          .catch(this.handleError);  
    }

    private handleError (error: Response) {
      // console.error(error);
      return Observable.throw(error.json().error || ' error');
    }
}