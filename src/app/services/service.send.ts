export class ServiceSend {
    constructor(
        public origin_address: string,
        public origin_detail: string,
        public origin_latitude: string,
		public origin_longitude: string,
		public destiny_address: string,
		public destiny_detail: string,
        public destiny_latitude: string,
		public destiny_longitude: string,
		public type_service: string, //Número identificador del Tipo de servicio
		public bag_id: string, //Número identificador del vehículo
		public description_text: string, //Descripción del servicio
		public declared_value: string, //Valor declarado
		public destiny_name: string, //Nombre del destinatario
		public tip: string //Propina
    ) {  }
}