export class ServiceTransport {
    constructor(
        public origin_address: string,
        public origin_latitude: string,
		public origin_longitude: string,
		public destiny_address: string,
		public destiny_latitude: string,
		public destiny_longitude: string,
		public type_service: string,
        public bag_id: string, //Número identificador del vehículo
		public tip: string //Propina

    ) {  }
}