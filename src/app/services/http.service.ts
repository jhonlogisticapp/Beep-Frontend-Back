import { Injectable }                from '@angular/core';
import { Http, Response }            from '@angular/http';
import { Headers, RequestOptions }   from '@angular/http';
import { Observable }                from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { ConstantUbeep } from './../constantsUbeep';

@Injectable()
export class HttpService {

    private _urlCancelService:string = "assets/services/services/cancel.service.php";
    
    constructor(private _http: Http, private _ConstantUbeep: ConstantUbeep){ }

    cancelService(shipping_id:string){

      let data = JSON.stringify({ 
        key:"3edcdb20e0030daab21d0ba9af4c0dc2",
        shipping_id: shipping_id,
        url: this._ConstantUbeep.urlServer
      });

      // console.log(data);

      let headers = new Headers({ "Access-Control-Allow-Origin": "*" });
      let options = new RequestOptions({headers: headers, method: "POST" });
      
      return this._http.post(this._urlCancelService, data, options)
        .map(res => res.json())
        .catch(this.handleError);

    }

    private handleError (error: Response) {
        // console.error(error);
        return Observable.throw(error.json().error || ' error');
    }
}