import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Headers, RequestOptions} from '@angular/http';
import {Observable}     from 'rxjs/Observable';
import { AppComponent }       from './../app.component';


import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { ConstantUbeep } from './../constantsUbeep';

@Injectable()
export class HttpSolicitarService {

    private _urlContribution:string = "assets/services/services/http.service.solicitar.php";
    private _urlContributionSend:string = "assets/services/services/http.service.solicitar.envio.php";
    private _urlPay:string = "assets/services/services/http.service.pagar.php";
    private _urlBond:string = "assets/services/services/http.service.bond.php";
    private _urlSocket:string = "assets/services/services/socket.on.php";
    private _urlDeliveryverification:string = "assets/services/services/deliveryverification.php";
    private _urlPushWeb:string = "assets/services/services/pushweb.php";
    public _urlPushWeb2:string = "assets/services/services/pushweb.php";
    private _urlCancelService:string = "assets/services/services/cancel.service.php";
    private _urlGetCredit: string = "assets/services/services/credit.service.php";
    private _urlGetLocation: string = "assets/services/services/get-location.service.php";
    private _urlPaymentTokenize: string = 'assets/services/services/paymentGateWayTokenize.php';
    private _urlPaymentPost: string = 'assets/services/services/paymentGateWayPost.php';


    constructor(private _http: Http, private _ConstantUbeep: ConstantUbeep){

    }

    public postContribution(origin_latitude:string, origin_longitude:string, destiny_latitude:string, destiny_longitude:string, type_service:string, bag_id:string, tip:string, declared_value: string, destination_address: string, origin_address: string, origin_client:string, pay: string ){

        let data = JSON.stringify({
            key:"3edcdb20e0030daab21d0ba9af4c0dc2",
            origin_latitude: origin_latitude,
            origin_longitude: origin_longitude,
            destiny_latitude: destiny_latitude,
            destiny_longitude: destiny_longitude,
            type_service: type_service,
            bag_id: bag_id,
            tip: tip || "0",
            declared_value: declared_value,
            url: this._ConstantUbeep.urlServer,
            destination_address: destination_address,
            origin_address: origin_address,
            origin_client: origin_client,
            pay: pay
        });

        // console.log(data);

        let headers = new Headers({ "Access-Control-Allow-Origin": "*" });
        let options = new RequestOptions({headers: headers, method: "POST" });

        var typePost = "";

        if(declared_value==undefined){
            typePost = this._urlContribution;
        }else{
            typePost = this._urlContributionSend;
        }

        // console.log(typePost);

        return this._http.post(typePost, data, options)
            .map(res => res.json())
            .catch(this.handleError);
    }

    public pagarServicio(distance:string, origin_client:string, origin_address:string, origin_latitude:string, origin_longitude:string, destiny_address:string, destiny_latitude:string, destiny_longitude:string, type_service:string, amount:string, pay:string, time:string, tip:string, bag_id:string, code_confirm:string, origin_detail:string, description_text:string, destiny_detail:string, destiny_name:string, polyline:string, quotation_id: string){

        let data = JSON.stringify({
            key:"3edcdb20e0030daab21d0ba9af4c0dc2",
            distance: distance,
            origin_client: origin_client,
            origin_address: origin_address,
            origin_latitude: origin_latitude,
            origin_longitude: origin_longitude,
            destiny_address: destiny_address,
            destiny_latitude: destiny_latitude,
            destiny_longitude: destiny_longitude,
            amount: amount,
            type_service: type_service,
            pay: pay,
            time: time,
            tip: tip || "0",
            platform: "web",
            bag_id: bag_id,
            code_confirm: code_confirm,
            origin_detail: origin_detail,
            description_text: description_text,
            destiny_detail: destiny_detail,
            destiny_name: destiny_name,
            polyline: polyline,
            url: this._ConstantUbeep.urlServer,
            quotation_id: quotation_id
        });

        // console.log(data);

        let headers = new Headers({ "Access-Control-Allow-Origin": "*" });
        let options = new RequestOptions({headers: headers, method: "POST" });

        return this._http.post(this._urlPay, data, options)
            .map(res => res.json())
            .catch(this.handleError);
    }

    public confirmCode(bond: string, client_id: string, amount: any){
        let data = JSON.stringify({
            key:"3edcdb20e0030daab21d0ba9af4c0dc2",
            bond: bond,
            client_id: client_id,
            amount: amount,
            platform: "web",
            url: this._ConstantUbeep.urlServer
        });

        // console.log(data);

        let headers = new Headers({ "Access-Control-Allow-Origin": "*" });
        let options = new RequestOptions({headers: headers, method: "POST" });

        return this._http.post(this._urlBond, data, options)
            .map(res => res.json())
            .catch(this.handleError);
    }

    public socketOn(port){
        // console.log("socketOn");

        let data = JSON.stringify({port : port});

        // console.log(data);

        return this._http.post(this._urlSocket, data)
            .map(this.responseSocket)
            .catch(this.handleError);
    }

    public responseSocket(res){
        // console.log("Respuesta del socket 1");
        // console.log(HttpSolicitarService.arguments);
        //console.log(HttpSolicitarService.caller);

        var data = {
            token: res._body,
            status: res.ok
        }

        // console.log(data);

        return data;
    }

    public pushWeb(token, shipping_id){

        // console.log(this);
        // console.log(Http.prototype);
        //return this._http.post("app/services/pushweb.php", data, options)
        let data = JSON.stringify({
            key:"3edcdb20e0030daab21d0ba9af4c0dc2",
            token: token,
            shipping_id: shipping_id,
            url: this._ConstantUbeep.urlServer
        });

        // console.log(data);

        let headers = new Headers({ "Access-Control-Allow-Origin": "*" });
        let options = new RequestOptions({headers: headers, method: "POST" });
        // console.log(this);
        return this._http.post(this._urlPushWeb2, data, options)
            .map(res => res.json())
            .catch(this.handleError);
    }

    public openModalLogin(){
        // console.log(document.getElementById("btnIngresar"));
        document.getElementById("btnIngresar").click();
        //AppComponent.prototype.showChildModal();
    }

    public cancelService(shipping_id){

        let data = JSON.stringify({
            key:"3edcdb20e0030daab21d0ba9af4c0dc2",
            shipping_id: shipping_id,
            url: this._ConstantUbeep.urlServer
        });

        // console.log(data);

        let headers = new Headers({ "Access-Control-Allow-Origin": "*" });
        let options = new RequestOptions({headers: headers, method: "POST" });

        return this._http.post(this._urlCancelService, data, options)
            .map(res => res.json())
            .catch(this.handleError);
    }

    private handleError (error: Response) {
        // console.log( JSON.stringify(error) );
        // console.error(error);
        return Observable.throw(error.json().error || ' error');
    }

    public getCredit(clientId: string) {
        let data = JSON.stringify({
            key:"3edcdb20e0030daab21d0ba9af4c0dc2",
            clientId: clientId,
            url: this._ConstantUbeep.urlServer
        });

        // console.log(data);

        let headers = new Headers({ "Access-Control-Allow-Origin": "*" });
        let options = new RequestOptions({headers: headers, method: "POST" });
        // console.log(this);
        return this._http.post(this._urlGetCredit, data, options)
            .map(res => res.json())
            .catch(this.handleError);
    }

    public getLocation( logisticresource_id: string ) {
        // console.log(logisticresource_id);
        const data = {
            key: "3edcdb20e0030daab21d0ba9af4c0dc2",
            logisticresource_id: logisticresource_id,
            platform: "web",
            url: this._ConstantUbeep.urlServer
        };
        // console.log("Datos del GETLOCATION ", data);
        const headers = new Headers({ 'Access-Control-Allow-Origin': '*' });
        const options = new RequestOptions({headers: headers, method: 'POST'});

        return this._http.post(this._urlGetLocation, data, options)
            .map(res => res.json())
            .catch(this.handleError);
    }

    public setTimeoutServiceTokenApp(){
        AppComponent.prototype.setTimeoutServiceToken();
    }

    public paymentGateWayTokenize(data) {
        data.key = '3edcdb20e0030daab21d0ba9af4c0dc2';
        data.url = this._ConstantUbeep.urlServer;
        const headers = new Headers({ 'Access-Control-Allow-Origin': '*' });
        const options = new RequestOptions({headers: headers, method: 'POST'});
        console.log(data);

        return this._http.post(this._urlPaymentTokenize, data, options)
            .map(res => res.json())
            .catch(this.handleError);
    }

    public paymentGateWayPost(data) {
        data.key = '3edcdb20e0030daab21d0ba9af4c0dc2';
        data.url = this._ConstantUbeep.urlServer;
        const headers = new Headers({ 'Access-Control-Allow-Origin': '*' });
        const options = new RequestOptions({headers: headers, method: 'POST'});
        console.log(data);

        return this._http.post(this._urlPaymentPost, data, options)
            .map(res => res.json())
            .catch(this.handleError);
    }
}
