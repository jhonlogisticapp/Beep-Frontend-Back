export class Register {
    constructor(
        public name: string,
        public pass: string,
        public email: string,
        public phone: string,
        public identify: string
    ) {  }
}