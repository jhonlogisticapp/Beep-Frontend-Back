import { Component, OnInit } from '@angular/core';

@Component({
  templateUrl: './vehicles.component.html',
  styleUrls: ['./vehicles.component.css']
})
export class VehiclesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
	var myDiv = document.getElementById('container-vehicles');
    // console.log(myDiv);
    scrollTo(document.body, myDiv.offsetTop, 10);

    function scrollTo(element, to, duration) {
      if (duration < 0) return;
      var difference = to - element.scrollTop;
      var perTick = difference / duration * 2;

      setTimeout(function() {
        element.scrollTop = element.scrollTop + perTick;
        scrollTo(element, to, duration - 2);
      }, 10);
    }
  }

}
