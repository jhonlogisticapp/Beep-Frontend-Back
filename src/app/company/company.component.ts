import { Component, ViewChild, NgZone, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { NgModel }                                               from '@angular/forms';
import { Http, Response, Headers, RequestOptions }               from '@angular/http'
import { HttpContactar }                                         from "./company.service";
import { Contact }                                               from "./company.form";

@Component({
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css'],
  providers: [HttpContactar]
})
export class CompanyComponent implements OnInit {



    public viewInfo = true;
    public viewForm = false;
    postService:string;
    public typeContact = "";
    public responseMessage = "";
    public loadingProcess = false;
    public dataContact = new Contact("", "", "", "", "", "", "");
    public statusForm = false;

	constructor( private _httpService:HttpContactar ) {
	}

	ngOnInit() {
      var myDiv = document.getElementById('container-drive');
      // console.log(myDiv);
      scrollTo(document.body, myDiv.offsetTop, 10);
      
      function scrollTo(element, to, duration) {
        if (duration < 0) return;
        var difference = to - element.scrollTop;
        var perTick = difference / duration * 2;

        setTimeout(function() {
          element.scrollTop = element.scrollTop + perTick;
          scrollTo(element, to, duration - 2);
        }, 10);
      }
    }

	nextView(view: string){
		// console.log(view);
		switch (view) {
			case "form":
				this.viewInfo = true;
				this.viewForm = true;
                /*setTimeout(function() {
                    document.getElementById("viewForm").focus();
                }, 100);*/
				break;
			
			default:
				this.viewInfo = true;
				this.viewForm = true;
				break;
		}
	}

	submitEmailContacto(data:any){
		
		let validType = false;
		let validName = false;
		let validLastname = false;
		let validPhone = false;
		let validAddress = false;
		let validEmail = false;
		let validReason = false;

        if(data.value.type.toString().trim().length > 0)
        {
            validType = true;
            document.getElementById("status1").setAttribute("style","display: none !important");
        }
        else
        {
            validType = false;
            document.getElementById("status1").setAttribute("style","display: block !important");
        }

        if(data.value.name.toString().trim().length > 0)
        {
            validName = true;
            document.getElementById("status2").setAttribute("style","display: none !important");
            document.getElementById("name").className = "form-control";
        }
        else
        {
            validName = false;
            document.getElementById("status2").setAttribute("style","display: block !important");
            var d = document.getElementById("name");
            d.className += " inputImportant";
        }

        if(data.value.lastname.toString().trim().length > 0)
        {
            validLastname = true;
            document.getElementById("status3").setAttribute("style","display: none !important");
            document.getElementById("lastname").className = "form-control";
        }
        else
        {
            validLastname = false;
            document.getElementById("status3").setAttribute("style","display: block !important");
            var d = document.getElementById("lastname");
            d.className += " inputImportant";
        }

        if(data.value.phone.toString().trim().length > 0)
        {
            validPhone = true;
            document.getElementById("status4").setAttribute("style","display: none !important");
            document.getElementById("phone").className = "form-control";
        }
        else
        {
            validPhone = false;
            document.getElementById("status4").setAttribute("style","display: block !important");
            var d = document.getElementById("phone");
            d.className += " inputImportant";
        }

        if(data.value.address.toString().trim().length > 0)
        {
            validAddress = true;
            document.getElementById("status5").setAttribute("style","display: none !important");
            document.getElementById("address").className = "form-control";
        }
        else
        {
            validAddress = false;
            document.getElementById("status5").setAttribute("style","display: block !important");
            var d = document.getElementById("address");
            d.className += " inputImportant";
        }

		if(data.value.email.toString().trim().length > 0)
        {
            validEmail = true;
            document.getElementById("status6").setAttribute("style","display: none !important");
            document.getElementById("email").className = "form-control";
        }
        else
        {
            validEmail = false;
            document.getElementById("status6").setAttribute("style","display: block !important");
            var d = document.getElementById("email");
            d.className += " inputImportant";
        }

		if(data.value.reason.toString().trim().length > 0)
        {
            validReason = true;
            document.getElementById("status7").setAttribute("style","display: none !important");
            document.getElementById("reason").className = "form-control";
        }
        else
        {
            validReason = false;
            document.getElementById("status7").setAttribute("style","display: block !important");
            var d = document.getElementById("reason");
            d.className += " inputImportant";
        }

        if( validType && validName && validLastname && validPhone && validAddress && validEmail && validReason )
        {
            this.statusForm = false;
			this.loadingProcess = true;
			this.responseMessage="";
			// console.log(data);
			// console.log(this.typeContact);
			this._httpService.contact(data.value.name, data.value.lastname, data.value.email, data.value.phone, data.value.address, data.value.reason, data.value.type).subscribe(
	          data => this.postService = JSON.stringify(data),
	          error => this.correoEnviado(this.postService, data),
	          () => this.correoEnviado(this.postService, data)
	        );
        }
        else
        {
            this.statusForm = true;
        }
	}

	changeType(type:string){
		if(type=="Tengo un vehículo"){
            document.getElementById("img-vehicle").setAttribute("style", "background-image: url('./assets/img/tengoUnVehiculoSelected.png');");
            document.getElementById("img-vehicles").setAttribute("style", "background-image: url('./assets/img/tengoUnaFlotaDeVehiculos.png');");
            document.getElementById("img-drive").setAttribute("style", "background-image: url('./assets/img/soyConductor.png');");
		}
		if(type=="Tengo una flota de vehículos"){
            document.getElementById("img-vehicle").setAttribute("style", "background-image: url('./assets/img/tengoUnVehiculo.png');");
            document.getElementById("img-vehicles").setAttribute("style", "background-image: url('./assets/img/tengoUnaFlotaDeVehiculosSelected.png');");
            document.getElementById("img-drive").setAttribute("style", "background-image: url('./assets/img/soyConductor.png');");
		}
		if(type=="Soy conductor"){
            document.getElementById("img-vehicle").setAttribute("style", "background-image: url('./assets/img/tengoUnVehiculo.png');");
            document.getElementById("img-vehicles").setAttribute("style", "background-image: url('./assets/img/tengoUnaFlotaDeVehiculos.png');");
            document.getElementById("img-drive").setAttribute("style", "background-image: url('./assets/img/soyConductorSelected.png');");
		}
        document.getElementById("status1").setAttribute("style","display: none !important");
		this.dataContact.type = type;
		this.typeContact = type;
	}

	correoEnviado(data:any, f:any){
		let dataResponse = JSON.parse(data);
		// console.log(dataResponse);
		var thisClass = this;
		this.loadingProcess = false;
		if(dataResponse.return){
			this.responseMessage=dataResponse.message;
			f.reset();
			this.dataContact = new Contact("", "", "", "", "", "", "");
			setTimeout(function() {
				// console.log(thisClass);
           		thisClass.responseMessage="";
        	}, 4000);
		}else{
			// console.log(thisClass);
           	thisClass.responseMessage="Verifique los campos e intente nuevamente";
		}
	}

    changeValidateInput(type : string, data: any){
        // console.log(this.statusForm);
        // console.log(data.value);
        if(this.statusForm){
            switch (type) {
                case "name":
                    if(data.value.name.toString().trim().length > 0)
                    {
                        document.getElementById("status2").setAttribute("style","display: none !important");
                        document.getElementById("name").className = "form-control";
                    }
                    else
                    {
                        document.getElementById("status2").setAttribute("style","display: block !important");
                        var d = document.getElementById("name");
                        d.className += " inputImportant";
                    }
                break;

                case "lastname":
                    console.log(type);
                    if(data.value.lastname.toString().trim().length > 0)
                    {
                        document.getElementById("status3").setAttribute("style","display: none !important");
                        document.getElementById("lastname").className = "form-control";
                    }
                    else
                    {
                        document.getElementById("status3").setAttribute("style","display: block !important");
                        var d = document.getElementById("lastname");
                        d.className += " inputImportant";
                    }
                break;

                case "email":
                    if(data.value.email.toString().trim().length > 0)
                    {
                        document.getElementById("status6").setAttribute("style","display: none !important");
                        document.getElementById("email").className = "form-control";
                    }
                    else
                    {
                        document.getElementById("status6").setAttribute("style","display: block !important");
                        var d = document.getElementById("email");
                        d.className += " inputImportant";
                    }
                break;

                case "address":
                    if(data.value.address.toString().trim().length > 0)
                    {
                        document.getElementById("status5").setAttribute("style","display: none !important");
                        document.getElementById("address").className = "form-control";
                    }
                    else
                    {
                        document.getElementById("status5").setAttribute("style","display: block !important");
                        var d = document.getElementById("address");
                        d.className += " inputImportant";
                    }
                break;

                case "reason":
                    if(data.value.reason.toString().trim().length > 0)
                    {
                        document.getElementById("status7").setAttribute("style","display: none !important");
                        document.getElementById("reason").className = "form-control";
                    }
                    else
                    {
                        document.getElementById("status7").setAttribute("style","display: block !important");
                        var d = document.getElementById("reason");
                        d.className += " inputImportant";
                    }
                break;

                case "phone":
                    if(data.value.phone.toString().trim().length > 0)
                    {
                        document.getElementById("status4").setAttribute("style","display: none !important");
                        document.getElementById("phone").className = "form-control";
                    }
                    else
                    {
                        document.getElementById("status4").setAttribute("style","display: block !important");
                        var d = document.getElementById("phone");
                        d.className += " inputImportant";
                    }
                break;

                default:
                break;
            }
        }
    }

}
