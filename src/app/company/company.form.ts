export class Contact {
    constructor(
        public name: string,
        public lastname: string,
        public email: string,
		public phone: string,
		public address: string,
		public reason: string,
        public type: string
    ) {  }
}