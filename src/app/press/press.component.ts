import { Component, ViewChild, NgZone, ChangeDetectionStrategy, OnInit } from '@angular/core';

import { NgModel }                                                       from '@angular/forms';
import { KitHttp }                                                       from './press.service';
import { Http, Response }                                                from '@angular/http';
import { ConstantUbeep }                                                 from './../constantsUbeep';

@Component({
	templateUrl: './press.component.html',
	styleUrls: ['./press.component.css'],
	providers: [KitHttp]
})
export class PressComponent implements OnInit {

    responseDownloadKitString: string;

    private urlDownloadKit:string = "assets/services/press/press.service.php";

    public urlDowloadKit = 'localhost/ubeep/app/press/services/downloadKit/service.php' ;

    constructor(private _kitHttp: KitHttp, private _http: Http, private _ConstantUbeep: ConstantUbeep) {

    }

	ngOnInit() {
        var myDiv = document.getElementById('container-press');
        // console.log(myDiv);
        scrollTo(document.body, myDiv.offsetTop, 10);

        function scrollTo(element, to, duration) {
            if (duration < 0) return;
            var difference = to - element.scrollTop;
            var perTick = difference / duration * 2;

            setTimeout(function() {
                element.scrollTop = element.scrollTop + perTick;
                scrollTo(element, to, duration - 2);
            }, 10);
        }
    }

    download() {

        this._kitHttp.downloadKit().subscribe(
            data => this.responseDownloadKitString = JSON.stringify(data),
            error => this.responseDownloadKit(),
            () => this.responseDownloadKit()
        );
        
    }

    responseDownloadKit() {
        // console.log("Download Success");
    }

}
