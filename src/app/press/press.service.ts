import { Injectable }                from '@angular/core';
import { Http, Response }            from '@angular/http';
import { Headers, RequestOptions }   from '@angular/http';
import { Observable }                from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class KitHttp {
    
    constructor(private _http: Http){ }

    downloadKit(){

      let headers = new Headers({ "Access-Control-Allow-Origin": "*" });
      let options = new RequestOptions({headers: headers, method: "POST" });
      
      return this._http.get('http://localhost/ubeep-web/app/press/services/downloadKit/service.php')
          .map(res => res.json())
          .catch(this.handleError);  
    }

    private handleError (error: Response) {
        // console.error(error);
        return Observable.throw(error.json().error || ' error');
    }
}