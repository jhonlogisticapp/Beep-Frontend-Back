import { Injectable }              from '@angular/core';
import { Http, Response }          from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable }              from 'rxjs/Observable';

import { ConstantUbeep } from './../constantsUbeep';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class HttpAgreement {

    private _Urlservice:string = "assets/services/agreement/agreement.service.php";
    
    constructor(private _http: Http, private _ConstantUbeep: ConstantUbeep){ 
      // console.log(window.location);
    }

    agreement(name:string, lastname:string, email:string, company:string, phone:string, rol:string, companySize:string){

      let data = JSON.stringify({ 
        key:"3edcdb20e0030daab21d0ba9af4c0dc2",
        name: name+" "+lastname,
        email: email,
        company: company,
        job: rol,
        companySize: companySize,
        phone: phone,
        class: "3",
        url: this._ConstantUbeep.urlServer
      });

      // console.log(data);

      let headers = new Headers({ "Access-Control-Allow-Origin": "*" });
      let options = new RequestOptions({headers: headers, method: "POST" });
      
      return this._http.post(this._Urlservice, data, options)
        .map(res => res.json())
        .catch(this.handleError);
    }

    private handleError (error: Response) {
      // console.error(error);
      return Observable.throw(error.json().error || ' error');
    }
}