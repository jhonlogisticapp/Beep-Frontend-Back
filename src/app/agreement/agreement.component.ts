import {Component, ViewChild, Injectable, Compiler, ViewContainerRef, OnInit} from '@angular/core';
import {Ng2BootstrapModule, ModalDirective} from 'ng2-bootstrap';
import {NgModel} from '@angular/forms';
import {Router} from '@angular/router';
import {HttpAgreement} from './agreement.service';
import {Agreement} from './agreement.form';
import {ConstantUbeep} from './../constantsUbeep';

@Component({
    templateUrl: './agreement.component.html',
    styleUrls: ['./agreement.component.css'],
    providers: [HttpAgreement]
})

export class AgreementComponent implements OnInit {

    private viewContainerRef: ViewContainerRef;
    public postService: string;
    public loadingProcess = false;
    public responseMessage = '';
    public dataAgreement = new Agreement('', '', '', '', '', '', '');
    public statusForm = false;

    constructor(
        private _viewContainerRef: ViewContainerRef,
        private _compiler: Compiler,
        private _routerModule: Router,
        private _httpService: HttpAgreement,
        private _ConstantUbeep: ConstantUbeep)
    {
        this.viewContainerRef = _viewContainerRef;
        this._compiler.clearCache();
    }

    ngOnInit() {
        let myDiv = document.getElementById('container-agreement');
        // console.log(myDiv);
        scrollTo(document.body, myDiv.offsetTop, 10);

        function scrollTo(element, to, duration) {
            if (duration < 0) return;
            var difference = to - element.scrollTop;
            var perTick = difference / duration * 2;

            setTimeout(function () {
                element.scrollTop = element.scrollTop + perTick;
                scrollTo(element, to, duration - 2);
            }, 10);
        }
    }

    sendEmail(data) {
        // console.log(data.value);
        let validName = false;
        let validLastname = false;
        let validEmail = false;
        let validCompany = false;
        let validRol = false;
        let validCompanySize = false;
        let validPhone = false;

        if (data.value.name.toString().trim().length > 0) {
            validName = true;
            document.getElementById("status1").setAttribute("style", "display: none !important");
            document.getElementById("name").className = "form-control";
        }
        else {
            validName = false;
            document.getElementById("status1").setAttribute("style", "display: block !important");
            var d = document.getElementById("name");
            d.className += " inputImportant";
        }

        if (data.value.lastname.toString().trim().length > 0) {
            validLastname = true;
            document.getElementById("status2").setAttribute("style", "display: none !important");
            document.getElementById("lastname").className = "form-control";
        }
        else {
            validLastname = false;
            document.getElementById("status2").setAttribute("style", "display: block !important");
            var d = document.getElementById("lastname");
            d.className += " inputImportant";
        }

        if (data.value.email.toString().trim().length > 0) {
            validEmail = true;
            document.getElementById("status3").setAttribute("style", "display: none !important");
            document.getElementById("email").className = "form-control";
        }
        else {
            validEmail = false;
            document.getElementById("status3").setAttribute("style", "display: block !important");
            var d = document.getElementById("email");
            d.className += " inputImportant";
        }

        if (data.value.company.toString().trim().length > 0) {
            validCompany = true;
            document.getElementById("status4").setAttribute("style", "display: none !important");
            document.getElementById("company").className = "form-control";
        }
        else {
            validCompany = false;
            document.getElementById("status4").setAttribute("style", "display: block !important");
            var d = document.getElementById("company");
            d.className += " inputImportant";
        }

        if (data.value.phone.toString().trim().length > 0) {
            validPhone = true;
            document.getElementById("status7").setAttribute("style", "display: none !important");
            document.getElementById("phone").className = "form-control";
        }
        else {
            validPhone = false;
            document.getElementById("status7").setAttribute("style", "display: block !important");
            var d = document.getElementById("phone");
            d.className += " inputImportant";
        }

        if (data.value.rol.toString().trim().length > 0) {
            validRol = true;
            document.getElementById("status5").setAttribute("style", "display: none !important");
            document.getElementById("rol").className = "form-control";
        }
        else {
            validRol = false;
            document.getElementById("status5").setAttribute("style", "display: block !important");
            var d = document.getElementById("rol");
            d.className += " inputImportant";
        }

        if (data.value.companySize.toString().trim().length > 0) {
            validCompanySize = true;
            document.getElementById("status6").setAttribute("style", "display: none !important");
            document.getElementById("companySize").className = "form-control";
        }
        else {
            validCompanySize = false;
            document.getElementById("status6").setAttribute("style", "display: block !important");
            var d = document.getElementById("companySize");
            d.className += " inputImportant";
        }

        if (validName && validLastname && validEmail && validCompany && validRol && validCompanySize && validPhone) {
            this.statusForm = false;
            this.loadingProcess = true;
            this.responseMessage = "";
            // console.log(data);
            // console.log(data.value.name, data.value.lastname, data.value.email, data.value.company, data.value.phone, data.value.rol, data.value.companySize);
            this._httpService.agreement(data.value.name, data.value.lastname, data.value.email, data.value.company, data.value.phone, data.value.rol, data.value.companySize).subscribe(
                data => this.postService = JSON.stringify(data),
                error => this.responseSuccess(this.postService, data),
                () => this.responseSuccess(this.postService, data)
            );
        }
        else {
            this.statusForm = true;
        }
    }

    responseSuccess(resp, data) {
        let dataResponse = JSON.parse(resp);
        // console.log(dataResponse);
        // console.log(data);
        var thisClass = this;
        if (dataResponse.return && dataResponse.status == 200) {
            this.responseMessage = dataResponse.message;
            this.loadingProcess = false;
            // console.log(data.value);
            data.reset();
            this.dataAgreement = new Agreement("", "", "", "", "", "", "");
            setTimeout(function () {
                thisClass.responseMessage = "";
                thisClass._routerModule.navigateByUrl('home');
            }, 2000);
        } else {
            this.loadingProcess = false;
            this.responseMessage = "Por favor verifique los campos";
        }
    }

    changeValidateInput(type: string, data: any) {
        // console.log(this.statusForm);
        // console.log(data.value);
        if (this.statusForm) {
            switch (type) {
                case "name":
                    if (data.value.name.toString().trim().length > 0) {
                        document.getElementById("status1").setAttribute("style", "display: none !important");
                        document.getElementById("name").className = "form-control";
                    }
                    else {
                        document.getElementById("status1").setAttribute("style", "display: block !important");
                        var d = document.getElementById("name");
                        d.className += " inputImportant";
                    }
                    break;

                case "lastname":
                    // console.log(type);
                    if (data.value.lastname.toString().trim().length > 0) {
                        document.getElementById("status2").setAttribute("style", "display: none !important");
                        document.getElementById("lastname").className = "form-control";
                    }
                    else {
                        document.getElementById("status2").setAttribute("style", "display: block !important");
                        var d = document.getElementById("lastname");
                        d.className += " inputImportant";
                    }
                    break;

                case "email":
                    if (data.value.email.toString().trim().length > 0) {
                        document.getElementById("status3").setAttribute("style", "display: none !important");
                        document.getElementById("email").className = "form-control";
                    }
                    else {
                        document.getElementById("status3").setAttribute("style", "display: block !important");
                        var d = document.getElementById("email");
                        d.className += " inputImportant";
                    }
                    break;

                case "company":
                    if (data.value.company.toString().trim().length > 0) {
                        document.getElementById("status4").setAttribute("style", "display: none !important");
                        document.getElementById("company").className = "form-control";
                    }
                    else {
                        document.getElementById("status4").setAttribute("style", "display: block !important");
                        var d = document.getElementById("company");
                        d.className += " inputImportant";
                    }
                    break;

                case "rol":
                    if (data.value.rol.toString().trim().length > 0) {
                        document.getElementById("status5").setAttribute("style", "display: none !important");
                        document.getElementById("rol").className = "form-control";
                    }
                    else {
                        document.getElementById("status5").setAttribute("style", "display: block !important");
                        var d = document.getElementById("rol");
                        d.className += " inputImportant";
                    }
                    break;

                case "companySize":
                    if (data.value.companySize.toString().trim().length > 0) {
                        document.getElementById("status6").setAttribute("style", "display: none !important");
                        document.getElementById("companySize").className = "form-control";
                    }
                    else {
                        document.getElementById("status6").setAttribute("style", "display: block !important");
                        var d = document.getElementById("companySize");
                        d.className += " inputImportant";
                    }
                    break;

                case "phone":
                    if (data.value.phone.toString().trim().length > 0) {
                        document.getElementById("status7").setAttribute("style", "display: none !important");
                        document.getElementById("phone").className = "form-control";
                    }
                    else {
                        document.getElementById("status7").setAttribute("style", "display: block !important");
                        var d = document.getElementById("phone");
                        d.className += " inputImportant";
                    }
                    break;

                default:

                    break;
            }

        }

    }

}
