export class Agreement {
    constructor(
        public name: string,
        public lastname: string,
        public email: string,
		public company: string,
		public rol: string,
        public companySize: string,
		public phone: string
    ) {  }
}