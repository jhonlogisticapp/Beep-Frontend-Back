import { Component, ViewChild, Injectable, Compiler, ViewContainerRef, OnInit } from '@angular/core';

import { Ng2BootstrapModule, ModalDirective }                           from 'ng2-bootstrap';
import { NgModel }                                                      from '@angular/forms';
import { Router }                                                       from '@angular/router';
import { HttpInvite }                                    	       	    from "./invite.service";
import { Invite }                              		      	      	    from "./invite.form";  

@Component({
  templateUrl: './invite.component.html',
  styleUrls: ['./invite.component.css'],
  providers: [HttpInvite]
})
export class InviteComponent implements OnInit {

	private viewContainerRef: ViewContainerRef;
	public postService : string;
	public loadingProcess = false;
	public responseMessage = "";
	public dataInvite = new Invite("");
    public statusForm = false;

	constructor(private _viewContainerRef: ViewContainerRef, private _compiler: Compiler, private _routerModule: Router, private _httpService: HttpInvite) {
    	this.viewContainerRef = _viewContainerRef;
    	this._compiler.clearCache();
    }

    ngOnInit() {
        var myDiv = document.getElementById('container-invite');
        // console.log(myDiv);
        scrollTo(document.body, myDiv.offsetTop, 10);

        function scrollTo(element, to, duration) {
          if (duration < 0) return;
          var difference = to - element.scrollTop;
          var perTick = difference / duration * 2;

          setTimeout(function() {
            element.scrollTop = element.scrollTop + perTick;
            scrollTo(element, to, duration - 2);
          }, 10);
        }
    }

    sendEmail(data){
        let validEmail = false;
        if(data.value.email.toString().trim().length > 0)
        {
            validEmail = true;
            document.getElementById("status1").setAttribute("style","display: none !important");
            document.getElementById("email").className = "form-control";
        }
        else
        {
            validEmail = false;
            document.getElementById("status1").setAttribute("style","display: block !important");
            var d = document.getElementById("email");
            d.className += " inputImportant";
        }

        if(validEmail)
        {
            this.statusForm = false;
        	this.loadingProcess = true;
    		this.responseMessage = "";
    		// console.log(data);
    		this._httpService.invite(data.value.email).subscribe(
              data => this.postService = JSON.stringify(data),
              error => this.responseSuccess(this.postService, data),
              () => this.responseSuccess(this.postService, data)
            );
        }
        else
        {
            this.statusForm = true;
        }
    }

    responseSuccess(resp, data){
    	let dataResponse = JSON.parse(resp);
    	// console.log(dataResponse);
    	// console.log(data);
    	var thisClass = this;
    	if(dataResponse.return && dataResponse.status == 200){
    		this.responseMessage = dataResponse.message;
        this.loadingProcess = false;
    		data.reset();
            this.dataInvite = new Invite("");
    		setTimeout(function() {
    			thisClass.responseMessage = "";
    		}, 2000);
    	}else{
        this.loadingProcess = false;
    		this.responseMessage = "Por favor verifique el correo";
    	}
    }

    changeValidateInput(type : string, data: any){
        // console.log(this.statusForm);
        // console.log(data.value);
        if(this.statusForm){
            switch (type) {
                case "email":
                    if(data.value.email.toString().trim().length > 0)
                    {
                        document.getElementById("status1").setAttribute("style","display: none !important");
                        document.getElementById("email").className = "form-control";
                    }
                    else
                    {
                        document.getElementById("status1").setAttribute("style","display: block !important");
                        var d = document.getElementById("email");
                        d.className += " inputImportant";
                    }
                break;

                default:
                break;
            }
        }
    }

}
