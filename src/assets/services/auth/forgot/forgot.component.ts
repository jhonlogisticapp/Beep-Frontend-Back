import { Injectable }                from '@angular/core';
import { Http, Response }            from '@angular/http';
import { Headers, RequestOptions }   from '@angular/http';
import { Observable }                from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Injectable()
export class HttpForgotService {

    private _urlLogin:string = "assets/services/auth/forgot/services/forgot.service.php";
    
    constructor(private _http: Http){ }

    forgotAcount(email:string, url: string){

      let data = JSON.stringify({ 
        key:"3edcdb20e0030daab21d0ba9af4c0dc2",
        email: email,
        url: url
      });

      let headers = new Headers({ "Access-Control-Allow-Origin": "*" });
      let options = new RequestOptions({headers: headers, method: "POST" });
      
      return this._http.post(this._urlLogin, data, options)
          .map(res => res.json())
          .catch(this.handleError);  
    }

    private handleError (error: Response) {
        // console.error(error);
        return Observable.throw(error.json().error || ' error');
    }
}