<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');

  function sendPost ($url, $data) {



    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $serverOutput = curl_exec ($ch);
    curl_close ($ch);
    echo $serverOutput;


  }

  //$url = 'http://52.43.247.174/api_devel/Shipping/post';

  $postdata = file_get_contents('php://input');
  $request = json_decode($postdata);



  $data = array(
    'key' => $request->key,
    'distance' => $request->distance,
    'origin_client' => $request->origin_client,
    'origin_address' => $request->origin_address,
    'origin_latitude' => $request->origin_latitude,
    'origin_longitude' => $request->origin_longitude,
    'destination_address' => $request->destiny_address,
    'destination_latitude' => $request->destiny_latitude,
    'destination_longitude' => $request->destiny_longitude,
    'type_service' => $request->type_service,
    'amount' => $request->amount,
    'pay' => $request->pay,
    'time' => $request->time,
    'tip' => $request->tip,
    'platform' => $request->platform,
    'bag_id' =>  $request->bag_id,
    'origin_detail' =>  $request->origin_detail,
    'description_text' =>  $request->description_text,
    'destination_detail' =>  $request->destiny_detail,
    'destination_name' =>  $request->destiny_name,
    'polyline' => $request->polyline,
    'quotation_id' => $request->quotation_id
  );

  $urlServer = $request->url;

  $url = $urlServer.'api_devel/request/post';
  //$url = $urlServer.'api_devel/Shipping/post';

  sendPost($url, $data);

?>
