webpackJsonp([0,4],{

/***/ 127:
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAUIAAABJCAYAAABIHS/rAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABZ0RVh0Q3JlYXRpb24gVGltZQAxNC8xMC8xNusgfikAAAAcdEVYdFNvZnR3YXJlAEFkb2JlIEZpcmV3b3JrcyBDUzbovLKMAAACSklEQVR4nO3awW3TYBjH4X+8ANmg3oCkC1DOqdSOwAZlA8IGdAPYIEjNGU/QlA2cDbpBOKQOVaHiRAx9n0fKIf4c6T399DmfJ3lGv5i3SS4fPrMkr567F+AftU3SJena9ebz08XdbpckmTxd6BfzaZJlkqu/OR3AkW2TLB8H8bch7BfzWZJVkpMjDgdwTF+TvGvXm/shhM2w8hDBLiIIvGwX2bfuoEkOj8Or+B8QqOF1v5h/Gr4MO8Jl7ASBWq6256dnSdI8nA47GAEqWib7HeHluHMAjObN9vy0FUKgurMm+5elAapqmzgpBmqbNX++B+BFmwohUJ4QAuUJIVCeEALlCSFQnhAC5QkhUJ4QAuUJIVCeEALlCSFQnhAC5QkhUJ4QAuUJIVCeEALlCSFQnhAC5QkhUJ4QAuUJIVCeEALlCSFQnhAC5QkhUJ4QAuUJIVCeEALlCSFQnhAC5QkhUJ4QAuUJIVCeEALlCSFQnhAC5QkhUJ4QAuUJIVCeEALlCSFQnhAC5QkhUJ4QAuUJIVCeEALlCSFQnhAC5QkhUJ4QAuUJIVDdfZPkfuwpAEZ01yT5PvYUACPqmySrsacAGFEnhEBl3cnNbd+0602f5HrsaQBG8DH5eWq8TNKPNQnACK5Pbm67JJkMV/rFfJbkW5LpSEMBHMtdu97Md7tdkkfvEbbrzV2St7EzBF62VfatO5g8vaNfzKdJPiR5f6ShAI6hT7Js15svw4VhR/hLCA+/WMzbJBdJLpPM4pEZ+P/0Sbok3eMADoYQ/gB4oF3A1uCSoAAAAABJRU5ErkJggg=="

/***/ }),

/***/ 128:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__constantsUbeep__ = __webpack_require__(13);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HttpProfile; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var HttpProfile = (function () {
    function HttpProfile(_http, _ConstantUbeep) {
        this._http = _http;
        this._ConstantUbeep = _ConstantUbeep;
        this._urlLogin = "assets/services/profile/profile.service.php";
        this._urlLoginComplete = "assets/services/profile/profile.service.complete.php";
    }
    HttpProfile.prototype.updateServiceComplete = function (client_id, name, email, local_phone, movil_phone, identify) {
        var data = JSON.stringify({
            key: "3edcdb20e0030daab21d0ba9af4c0dc2",
            client_id: client_id,
            name: name,
            email: email,
            local_phone: local_phone,
            movil_phone: movil_phone,
            identify: identify,
            url: this._ConstantUbeep.urlServer
        });
        // console.log(data);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ "Access-Control-Allow-Origin": "*" });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({ headers: headers, method: "POST" });
        return this._http.post(this._urlLoginComplete, data, options)
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    HttpProfile.prototype.updateService = function (client_id, name, email, local_phone, movil_phone, identify) {
        var data = JSON.stringify({
            key: "3edcdb20e0030daab21d0ba9af4c0dc2",
            client_id: client_id,
            name: name,
            email: email,
            local_phone: local_phone,
            movil_phone: movil_phone,
            identify: identify,
            url: this._ConstantUbeep.urlServer
        });
        // console.log(data);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ "Access-Control-Allow-Origin": "*" });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({ headers: headers, method: "POST" });
        return this._http.post(this._urlLogin, data, options)
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    HttpProfile.prototype.handleError = function (error) {
        // console.error(error);
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json().error || ' error');
    };
    return HttpProfile;
}());
HttpProfile = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_5__constantsUbeep__["a" /* ConstantUbeep */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__constantsUbeep__["a" /* ConstantUbeep */]) === "function" && _b || Object])
], HttpProfile);

var _a, _b;
//# sourceMappingURL=profile.service.js.map

/***/ }),

/***/ 129:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng2_bootstrap__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng2_bootstrap___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ng2_bootstrap__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_Rx__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular2_cookie_services_cookies_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular2_cookie_services_cookies_service___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_angular2_cookie_services_cookies_service__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_service__ = __webpack_require__(130);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__service_transport__ = __webpack_require__(396);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__service_send__ = __webpack_require__(395);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_app_services_paymentGateWay__ = __webpack_require__(394);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServicesComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var ServicesComponent = (function () {
    function ServicesComponent(_viewContainerRef, _httpService, _cookieService, _compiler, _routerModule, fb, _Http, _changeDetectorRef) {
        this._viewContainerRef = _viewContainerRef;
        this._httpService = _httpService;
        this._cookieService = _cookieService;
        this._compiler = _compiler;
        this._routerModule = _routerModule;
        this.fb = fb;
        this._Http = _Http;
        this._changeDetectorRef = _changeDetectorRef;
        //private _AppComponent: AppComponent;
        //private _httpService:HttpSolicitarService;
        this.markerOrigin = null;
        this.markerOriginStatus = true;
        this.markerDestination = null;
        this.markerDestinationStatus = false;
        this.markerOriginSendStatus = false;
        this.markerDestinationSendStatus = true;
        this.markerUbeep = null;
        this.googleMap = null;
        //---Datos del servicio
        this.origin_address = null; //---Direccion de origen
        this.origin_latitude = null;
        this.origin_longitude = null;
        this.destiny_address = null; //---Direccion de destino
        this.destiny_latitude = null;
        this.destiny_longitude = null;
        this.type_service = null;
        this.bag_id = null;
        this.typeService = null; //NOOOOOOOOOO
        this.tip = null;
        this.price_service = null;
        this.declared_value = null;
        this.distance = null;
        this.origin_client = null;
        this.amount = null;
        this.pay = null;
        this.time = null;
        this.code_confirm = null;
        this.origin_detail = null;
        this.description_text = null;
        this.destiny_detail = null;
        this.destiny_name = null;
        this.polyline = null;
        this.type_service_string = "";
        this.message_bond = "Ingresa el código de tu bono";
        this.bond_service = "";
        this.creditValue = "0";
        //---Vistas Generales
        this.viewFromService = true;
        this.viewContribution = false;
        this.viewSuccess = false;
        this.viewOriginTransport = true;
        this.viewDestinationTransport = false;
        this.viewOriginSend = false;
        this.viewDestinationSend = false;
        this.containerTypeVehicle = false;
        this.containerDestinyName = false;
        this.viewContainerBond = false;
        this.viewCodeSuccess = false;
        this.viewDataConfirm = false;
        this.loadingProcess = false;
        this.loadingProcessPaymentGateWayPost = false;
        this.tab = { active: true };
        this.formServicesTransport = __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormGroup"];
        this.dataSocket = null;
        this.loadingBoundProcess = false;
        this.dataServiceTransport = new __WEBPACK_IMPORTED_MODULE_8__service_transport__["a" /* ServiceTransport */]("", "", "", "", "", "", "34", "", "");
        this.dataServiceSend = new __WEBPACK_IMPORTED_MODULE_9__service_send__["a" /* ServiceSend */]("", "", "", "", "", "", "", "", "37", "", "", "", "", "");
        this.valuePayDefault = '5';
        this.quotation_id = '';
        this.viewPayCredit = false;
        //---Carga contenido del tab Envio
        this.statusLoadTabSend = true;
        this.viewFormTarget = false;
        this.statusOriginSend = false;
        this.code_value = null;
        this.statusFormTransport = false;
        this.logisticresource_id = "-";
        this.name_employee = "-";
        this.lastName_employee = "-";
        this.vehicle_brand = "-";
        this.vehicle_model = "-";
        this.icense_plate = "-";
        this.arrival_time = "-";
        this.security_code = "-";
        this.picture_employee = "-";
        this.timerPositionBeep = null;
        this.numDeltas = 100;
        this.delay = 10; //milliseconds
        this.i = 0;
        this.paymentGateWay = new __WEBPACK_IMPORTED_MODULE_10_app_services_paymentGateWay__["a" /* PaymentGateWay */]();
        this.messagePaymentGateWayTokenize = '';
        this.messagePaymentGateWayPost = '';
        this.viewContainerRef = _viewContainerRef;
        this._compiler.clearCache();
        window.onbeforeunload;
    }
    ServicesComponent.prototype.ngOnInit = function () {
        if (this._cookieService.get('pay_type') != undefined) {
            this.valuePayDefault = this._cookieService.get('pay_type');
        }
        this.loadTop();
        this._cookieService.put('statusServices', 'false');
        this.loadComponentsGoogleMaps();
    };
    ServicesComponent.prototype.refreshView = function () {
        this._changeDetectorRef.detectChanges();
    };
    ServicesComponent.prototype.loadTop = function () {
        var myDiv = document.getElementById('container-services');
        // console.log(myDiv);
        scrollTo(document.body, myDiv.offsetTop, 10);
        function scrollTo(element, to, duration) {
            if (duration < 0)
                return;
            var difference = to - element.scrollTop;
            var perTick = difference / duration * 2;
            setTimeout(function () {
                element.scrollTop = element.scrollTop + perTick;
                scrollTo(element, to, duration - 2);
            }, 10);
        }
    };
    //---Cargo componentes de Google Maps
    ServicesComponent.prototype.loadComponentsGoogleMaps = function () {
        //---Muestro la primer parte del formulario del tab Transporte
        this.viewFromService = true;
        this._cookieService.put('orderFormService', '1');
        this.viewDestinationTransport = false;
        var thisClass = this;
        setTimeout(function () {
            var credito = thisClass._cookieService.get('credit');
            if (credito != undefined) {
                thisClass._httpService.getCredit(thisClass._cookieService.get("id")).subscribe(function (data) { return thisClass.responseGetCreditString = JSON.stringify(data); }, function (error) { return thisClass.responseGetCredit(thisClass.responseGetCreditString); }, function () { return thisClass.responseGetCredit(thisClass.responseGetCreditString); });
            }
            else {
                //document.getElementById("viewPayCredit").style.display = "none";
                thisClass.viewPayCredit = false;
            }
            //---Cargo el mapa
            var geocoder = new google.maps.Geocoder();
            var mapProp = {
                center: new google.maps.LatLng(4.6762881, -74.05177549999999),
                zoom: 18,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
            //---Almaceno el mapa en una variable
            thisClass.googleMap = map;
            navigator.geolocation.getCurrentPosition(function (data) {
                thisClass.googleMap.panTo(new google.maps.LatLng(data.coords.latitude, data.coords.longitude));
            });
            thisClass.loadComponentPlaceInputOriginTransport();
            thisClass.loadComponentPlaceInputDestinationTransport();
            //---Click para crear o actualizar el Marker
            google.maps.event.addListener(map, 'click', function (event) {
                geocoder.geocode({
                    latLng: event.latLng
                }, function (responses) {
                    if (responses && responses.length > 0) {
                        if (thisClass.markerOriginStatus) {
                            thisClass.dataServiceTransport.origin_address = responses[0].formatted_address;
                            thisClass.dataServiceTransport.origin_latitude = event.latLng.lat();
                            thisClass.dataServiceTransport.origin_longitude = event.latLng.lng();
                            document.getElementById('autocompleteMapaOrigin').click();
                            if (thisClass.statusFormTransport) {
                                thisClass.changeValidateInput('origin_latitude');
                            }
                            //document.getElementById("autocompleteMapaOrigin").setAttribute("value",responses[0].formatted_address);
                            thisClass.origin_address = responses[0].formatted_address;
                            thisClass.origin_latitude = event.latLng.lat();
                            thisClass.origin_longitude = event.latLng.lng();
                            var iconOrigin = {
                                url: "./assets/img/pinOrigen.png",
                                scaledSize: new google.maps.Size(29, 42),
                                origin: new google.maps.Point(0, 0),
                                anchor: new google.maps.Point(0, 0)
                            };
                            if (thisClass.markerOrigin == null) {
                                thisClass.markerOrigin = new google.maps.Marker({
                                    position: event.latLng,
                                    animation: google.maps.Animation.DROP,
                                    icon: iconOrigin,
                                    map: map
                                });
                                map.panTo(event.latLng);
                            }
                            else {
                                thisClass.markerOrigin.setPosition(event.latLng);
                                map.panTo(event.latLng);
                            }
                        }
                        else if (thisClass.markerDestinationStatus) {
                            thisClass.dataServiceTransport.destiny_address = responses[0].formatted_address;
                            thisClass.dataServiceTransport.destiny_latitude = event.latLng.lat();
                            thisClass.dataServiceTransport.destiny_longitude = event.latLng.lng();
                            document.getElementById('autocompleteMapaDestination').click();
                            if (thisClass.statusFormTransport) {
                                thisClass.changeValidateInput('destiny_latitude');
                            }
                            //document.getElementById("autocompleteMapaDestination").setAttribute("value",responses[0].formatted_address);
                            thisClass.destiny_address = responses[0].formatted_address;
                            thisClass.destiny_latitude = event.latLng.lat();
                            thisClass.destiny_longitude = event.latLng.lng();
                            var iconDestination = {
                                url: "./assets/img/pinDestino.png",
                                scaledSize: new google.maps.Size(29, 42),
                                origin: new google.maps.Point(0, 0),
                                anchor: new google.maps.Point(0, 0)
                            };
                            if (thisClass.markerDestination == null) {
                                thisClass.markerDestination = new google.maps.Marker({
                                    position: event.latLng,
                                    animation: google.maps.Animation.DROP,
                                    icon: iconDestination,
                                    map: map
                                });
                                map.panTo(event.latLng);
                            }
                            else {
                                thisClass.markerDestination.setPosition(event.latLng);
                                map.panTo(event.latLng);
                            }
                        }
                        else if (thisClass.markerOriginSendStatus) {
                            thisClass.dataServiceSend.origin_address = responses[0].formatted_address;
                            thisClass.dataServiceSend.origin_latitude = event.latLng.lat();
                            thisClass.dataServiceSend.origin_longitude = event.latLng.lng();
                            document.getElementById('autocompleteMapaOriginSend').click();
                            if (thisClass.statusFormTransport) {
                                thisClass.changeValidateInput('origin_latitude_send');
                            }
                            //document.getElementById("autocompleteMapaOriginSend").setAttribute("value",responses[0].formatted_address);
                            thisClass.origin_address = responses[0].formatted_address;
                            thisClass.origin_latitude = event.latLng.lat();
                            thisClass.origin_longitude = event.latLng.lng();
                            var iconOrigin = {
                                url: "./assets/img/pinOrigen.png",
                                scaledSize: new google.maps.Size(29, 42),
                                origin: new google.maps.Point(0, 0),
                                anchor: new google.maps.Point(0, 0)
                            };
                            if (thisClass.markerOrigin == null) {
                                thisClass.markerOrigin = new google.maps.Marker({
                                    position: event.latLng,
                                    animation: google.maps.Animation.DROP,
                                    icon: iconOrigin,
                                    map: map
                                });
                                map.panTo(event.latLng);
                            }
                            else {
                                thisClass.markerOrigin.setPosition(event.latLng);
                                map.panTo(event.latLng);
                            }
                        }
                        else if (thisClass.markerDestinationSendStatus) {
                            thisClass.dataServiceSend.destiny_address = responses[0].formatted_address;
                            thisClass.dataServiceSend.destiny_latitude = event.latLng.lat();
                            thisClass.dataServiceSend.destiny_longitude = event.latLng.lng();
                            document.getElementById('autocompleteMapaDestinationSend').click();
                            if (thisClass.statusFormTransport) {
                                thisClass.changeValidateInput('destiny_latitude_send');
                            }
                            //document.getElementById("autocompleteMapaDestinationSend").setAttribute("value",responses[0].formatted_address);
                            thisClass.destiny_address = responses[0].formatted_address;
                            thisClass.destiny_latitude = event.latLng.lat();
                            thisClass.destiny_longitude = event.latLng.lng();
                            var iconDestination = {
                                url: "./assets/img/pinDestino.png",
                                scaledSize: new google.maps.Size(29, 42),
                                origin: new google.maps.Point(0, 0),
                                anchor: new google.maps.Point(0, 0)
                            };
                            if (thisClass.markerDestination == null) {
                                thisClass.markerDestination = new google.maps.Marker({
                                    position: event.latLng,
                                    animation: google.maps.Animation.DROP,
                                    icon: iconDestination,
                                    map: map
                                });
                                map.panTo(event.latLng);
                            }
                            else {
                                thisClass.markerDestination.setPosition(event.latLng);
                                map.panTo(event.latLng);
                            }
                        }
                        else {
                        }
                    }
                    else {
                    }
                });
            });
        }, 1000);
        //form.controls['propinaSend'].setValue("selected.id");
    };
    //---Input Origen Transporte
    ServicesComponent.prototype.loadComponentPlaceInputOriginTransport = function () {
        var thisClass = this;
        setTimeout(function () {
            //---Inicio componente Google Place Input 'autocompleteMapaOrigin'
            var inputAutocompleteOrigin = document.getElementById('autocompleteMapaOrigin');
            var autocompleteServiceOrigin = new google.maps.places.Autocomplete(inputAutocompleteOrigin);
            google.maps.event.addListener(autocompleteServiceOrigin, 'place_changed', function () {
                var place = autocompleteServiceOrigin.getPlace();
                var iconOrigin = {
                    url: "./assets/img/pinOrigen.png",
                    scaledSize: new google.maps.Size(29, 42),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(0, 0)
                };
                if (thisClass.statusFormTransport) {
                    thisClass.changeValidateInput('origin_latitude');
                }
                //---Almaceno la dirección
                thisClass.dataServiceTransport.origin_address = place.name;
                thisClass.dataServiceTransport.origin_latitude = place.geometry.location.lat();
                thisClass.dataServiceTransport.origin_longitude = place.geometry.location.lng();
                document.getElementById('autocompleteMapaOrigin').click();
                thisClass.origin_address = place.name;
                thisClass.origin_latitude = place.geometry.location.lat();
                thisClass.origin_longitude = place.geometry.location.lng();
                if (thisClass.markerOrigin == null) {
                    thisClass.markerOrigin = new google.maps.Marker({
                        position: new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng()),
                        animation: google.maps.Animation.DROP,
                        icon: iconOrigin,
                        map: thisClass.googleMap
                    });
                    thisClass.googleMap.panTo(new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng()));
                }
                else {
                    thisClass.markerOrigin.setPosition(new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng()));
                    thisClass.googleMap.panTo(new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng()));
                }
            });
        }, 1000);
    };
    //---Input Destino Transporte
    ServicesComponent.prototype.loadComponentPlaceInputDestinationTransport = function () {
        var thisClass = this;
        setTimeout(function () {
            //---Inicio componente Google Place Input 'autocompleteMapaDestination'
            var inputAutocompleteDestination = document.getElementById('autocompleteMapaDestination');
            var autocompleteServiceDestination = new google.maps.places.Autocomplete(inputAutocompleteDestination);
            google.maps.event.addListener(autocompleteServiceDestination, 'place_changed', function () {
                var place = autocompleteServiceDestination.getPlace();
                var iconDestination = {
                    url: "./assets/img/pinDestino.png",
                    scaledSize: new google.maps.Size(29, 42),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(0, 0)
                };
                if (thisClass.statusFormTransport) {
                    thisClass.changeValidateInput('destiny_latitude');
                }
                //---Almaceno la dirección
                thisClass.dataServiceTransport.destiny_address = place.name;
                thisClass.dataServiceTransport.destiny_latitude = place.geometry.location.lat();
                thisClass.dataServiceTransport.destiny_longitude = place.geometry.location.lng();
                document.getElementById('autocompleteMapaDestination').click();
                thisClass.destiny_address = place.name;
                thisClass.destiny_latitude = place.geometry.location.lat();
                thisClass.destiny_longitude = place.geometry.location.lng();
                if (thisClass.markerDestination == null) {
                    thisClass.markerDestination = new google.maps.Marker({
                        position: new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng()),
                        animation: google.maps.Animation.DROP,
                        icon: iconDestination,
                        map: thisClass.googleMap
                    });
                    thisClass.googleMap.panTo(new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng()));
                }
                else {
                    thisClass.markerDestination.setPosition(new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng()));
                    thisClass.googleMap.panTo(new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng()));
                }
            });
        }, 1000);
    };
    //---Input Origen Envio
    ServicesComponent.prototype.loadComponentPlaceInputOriginSend = function () {
        var thisClass = this;
        setTimeout(function () {
            //---Inicio componente Google Place Input 'autocompleteMapaOriginSend'
            var inputAutocompleteOriginSend = document.getElementById('autocompleteMapaOriginSend');
            this.markerOriginStatus = false;
            this.markerDestinationStatus = false;
            this.markerOriginSendStatus = true;
            this.markerDestinationSendStatus = false;
            //document.getElementById('autocompleteMapaOriginSend').focus();
            var autocompleteServiceOriginSend = new google.maps.places.Autocomplete(inputAutocompleteOriginSend);
            google.maps.event.addListener(autocompleteServiceOriginSend, 'place_changed', function () {
                var place = autocompleteServiceOriginSend.getPlace();
                var iconOrigin = {
                    url: "./assets/img/pinOrigen.png",
                    scaledSize: new google.maps.Size(29, 42),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(0, 0)
                };
                // console.log(thisClass.statusFormTransport);
                if (thisClass.statusFormTransport) {
                    thisClass.changeValidateInput('origin_latitude_send');
                }
                //---Almaceno la dirección
                thisClass.dataServiceSend.origin_address = place.name;
                thisClass.dataServiceSend.origin_latitude = place.geometry.location.lat();
                thisClass.dataServiceSend.origin_longitude = place.geometry.location.lng();
                document.getElementById('autocompleteMapaOriginSend').click();
                thisClass.origin_address = place.name;
                thisClass.origin_latitude = place.geometry.location.lat();
                thisClass.origin_longitude = place.geometry.location.lng();
                if (thisClass.markerOrigin == null) {
                    thisClass.markerOrigin = new google.maps.Marker({
                        position: new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng()),
                        animation: google.maps.Animation.DROP,
                        icon: iconOrigin,
                        map: thisClass.googleMap
                    });
                    thisClass.googleMap.panTo(new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng()));
                }
                else {
                    thisClass.markerOrigin.setPosition(new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng()));
                    thisClass.googleMap.panTo(new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng()));
                }
            });
        }, 1000);
    };
    //---Input Destino Envio
    ServicesComponent.prototype.loadComponentPlaceInputDestinationSend = function () {
        var thisClass = this;
        setTimeout(function () {
            //---Inicio componente Google Place Input 'autocompleteMapaDestinationSend'
            var inputAutocompleteDestinationSend = document.getElementById('autocompleteMapaDestinationSend');
            var autocompleteServiceDestinationSend = new google.maps.places.Autocomplete(inputAutocompleteDestinationSend);
            google.maps.event.addListener(autocompleteServiceDestinationSend, 'place_changed', function () {
                var place = autocompleteServiceDestinationSend.getPlace();
                var iconDestination = {
                    url: "./assets/img/pinDestino.png",
                    scaledSize: new google.maps.Size(29, 42),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(0, 0)
                };
                // console.log(thisClass.statusFormTransport);
                if (thisClass.statusFormTransport) {
                    thisClass.changeValidateInput('destiny_latitude_send');
                }
                //---Almaceno la dirección
                thisClass.dataServiceSend.destiny_address = place.name;
                thisClass.dataServiceSend.destiny_latitude = place.geometry.location.lat();
                thisClass.dataServiceSend.destiny_longitude = place.geometry.location.lng();
                document.getElementById('autocompleteMapaDestinationSend').click();
                thisClass.destiny_address = place.name;
                thisClass.destiny_latitude = place.geometry.location.lat();
                thisClass.destiny_longitude = place.geometry.location.lng();
                if (thisClass.markerDestination == null) {
                    thisClass.markerDestination = new google.maps.Marker({
                        position: new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng()),
                        animation: google.maps.Animation.DROP,
                        icon: iconDestination,
                        map: thisClass.googleMap
                    });
                    thisClass.googleMap.panTo(new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng()));
                }
                else {
                    thisClass.markerDestination.setPosition(new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng()));
                    thisClass.googleMap.panTo(new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng()));
                }
            });
        }, 1000);
    };
    //---Detecta el input con el foco para poder saber cual marker poner
    ServicesComponent.prototype.detectInputMarker = function (type) {
        // console.log(type);
        if (type == "originTransport") {
            this.markerOriginStatus = true;
            this.markerDestinationStatus = false;
            this.markerOriginSendStatus = false;
            this.markerDestinationSendStatus = false;
        }
        else if (type == "destinationTransport") {
            this.markerOriginStatus = false;
            this.markerDestinationStatus = true;
            this.markerOriginSendStatus = false;
            this.markerDestinationSendStatus = false;
        }
        else if (type == "originSend") {
            this.markerOriginStatus = false;
            this.markerDestinationStatus = false;
            this.markerOriginSendStatus = true;
            this.markerDestinationSendStatus = false;
        }
        else if (type == "destinationSend") {
            this.markerOriginStatus = false;
            this.markerDestinationStatus = false;
            this.markerOriginSendStatus = false;
            this.markerDestinationSendStatus = true;
        }
        else {
            this.markerOriginStatus = true;
            this.markerDestinationStatus = false;
            this.markerOriginSendStatus = false;
            this.markerDestinationSendStatus = false;
        }
    };
    ServicesComponent.prototype.loadContentTabSend = function (type) {
        // console.log(this.statusLoadTabSend);
        if (this.statusLoadTabSend) {
            this.statusLoadTabSend = false;
            var thisClass = this;
            this.detectInputMarker("originSend");
            this.viewOriginSend = true;
            document.getElementById("viewOriginSend").style.display = "block";
            this.viewDestinationSend = false;
            document.getElementById("viewDestinationSend").style.display = "none";
            this.markerOriginStatus = true;
            this.markerDestinationStatus = false;
            setTimeout(function () {
                thisClass.loadComponentPlaceInputOriginSend();
                thisClass.loadComponentPlaceInputDestinationSend();
            }, 1000);
        }
        else {
            // console.log("JAAAAAAAAAAAAAAAAAAAAA tOMALA!");
        }
    };
    ;
    //---Selecciona el tipo de vehiculo
    ServicesComponent.prototype.selectTypeVehicle = function (position) {
        var inputRadio = document.getElementsByName("bag_id");
        this.dataServiceTransport.type_service = "34";
        if (this.statusFormTransport) {
            this.changeValidateInput('bag_id');
        }
        if (position == 0) {
            var d = document.getElementById("containerVehicleVan");
            d.className += " containerVehicleSelect";
            document.getElementById("containerVehicleWhite").className = "";
            document.getElementById("containerVehicleElectric").className = "";
            inputRadio[1].removeAttribute("checked");
            inputRadio[2].removeAttribute("checked");
            inputRadio[position].setAttribute("checked", "checked");
            document.getElementById("blanco").setAttribute("src", "./assets/img/transporteBlanco.png");
            document.getElementById("electrico").setAttribute("src", "./assets/img/transporteElectrico.png");
            document.getElementById("van").setAttribute("src", "./assets/img/transporteVanSelected.png");
            this.bag_id = "16"; //this.bag_id = this._cookieService.get('shipping_bag_id_1');
            this.dataServiceTransport.bag_id = "16";
            this.type_service_string = "Van";
        }
        else if (position == 1) {
            var d = document.getElementById("containerVehicleWhite");
            d.className += " containerVehicleSelect";
            document.getElementById("containerVehicleVan").className = "";
            document.getElementById("containerVehicleElectric").className = "";
            inputRadio[0].removeAttribute("checked");
            inputRadio[2].removeAttribute("checked");
            inputRadio[position].setAttribute("checked", "checked");
            document.getElementById("van").setAttribute("src", "./assets/img/transporteVan.png");
            document.getElementById("electrico").setAttribute("src", "./assets/img/transporteElectrico.png");
            document.getElementById("blanco").setAttribute("src", "./assets/img/transporteBlancoSelected.png");
            this.bag_id = "25"; //this.bag_id = this._cookieService.get('shipping_bag_id_2');
            this.dataServiceTransport.bag_id = "25";
            this.type_service_string = "Blanco";
        }
        else if (position == 2) {
            var d = document.getElementById("containerVehicleElectric");
            d.className += " containerVehicleSelect";
            document.getElementById("containerVehicleVan").className = "";
            document.getElementById("containerVehicleWhite").className = "";
            inputRadio[0].removeAttribute("checked");
            inputRadio[1].removeAttribute("checked");
            inputRadio[position].setAttribute("checked", "checked");
            document.getElementById("van").setAttribute("src", "./assets/img/transporteVan.png");
            document.getElementById("blanco").setAttribute("src", "./assets/img/transporteBlanco.png");
            document.getElementById("electrico").setAttribute("src", "./assets/img/transporteElectricoSelected.png");
            this.bag_id = "26"; //this.bag_id = this._cookieService.get('shipping_bag_id_3');
            this.dataServiceTransport.bag_id = "26";
            this.type_service_string = "Eléctrico";
        }
    };
    //---Selecciona el typo de pago
    ServicesComponent.prototype.selectTypePay = function (type) {
        // console.log(type, "this is te oay");
        if (type == "0") {
            this.pay = "1";
            if (this._cookieService.get('credit_last_digits') !== undefined) {
                this.viewFormTarget = false;
                this.paymentGateWayPost();
                this.refreshView();
            }
            else {
                this.viewFormTarget = true;
                this.refreshView();
            }
        }
        else if (type == "1") {
            this.pay = "5";
            this.viewFormTarget = false;
        }
        else if (type == "2") {
            this.pay = "4";
            this.viewFormTarget = false;
        }
        else {
            this.pay = null;
            this.viewFormTarget = false;
        }
        this.changeValidateInput('pay');
    };
    //---Editar valores
    ServicesComponent.prototype.editInfo = function (typeInfo) {
        var typeService = 1;
        if (this.type_service == "34") {
            this.tab.active = true;
            typeService = 1;
        }
        else if (this.type_service == "37") {
            this.tab.active = false;
            typeService = 2;
        }
        if (typeInfo == "origin") {
            if (typeService == 1) {
                this.nextProcess('viewFromService1', 'form', 0);
            }
            else {
                this.statusOriginSend = true;
                this.nextProcess('viewFromService3', 'form', 0);
            }
        }
        else if (typeInfo == "type_vehicle") {
            this.nextProcess('viewFromService1', 'form', 0);
        }
        else if (typeInfo == "destination") {
            if (typeService == 1) {
                this.nextProcess('viewFromService1', 'form', 0);
            }
            else {
                this.statusOriginSend = true;
                this.nextProcess('viewFromService3', 'form', 0);
            }
        }
        else if (typeInfo == "addressee") {
            this.statusOriginSend = false;
            this.loadComponentsGoogleMaps();
            this.nextProcess('viewFromService5', 'form', 0);
            //document.getElementById();
        }
    };
    ServicesComponent.prototype.setInputs = function (type) {
        var thisClass = this;
        setTimeout(function () {
            thisClass.markerOrigin.setMap(null);
            thisClass.markerDestination.setMap(null);
            thisClass.markerOrigin = null;
            thisClass.markerDestination = null;
            var iconOrigin = {
                url: "./assets/img/pinOrigen.png",
                scaledSize: new google.maps.Size(29, 42),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(0, 0)
            };
            thisClass.markerOrigin = new google.maps.Marker({
                position: new google.maps.LatLng(thisClass.origin_latitude, thisClass.origin_longitude),
                animation: google.maps.Animation.DROP,
                icon: iconOrigin,
                map: thisClass.googleMap
            });
            thisClass.googleMap.panTo(new google.maps.LatLng(thisClass.origin_latitude, thisClass.origin_longitude));
            var iconDestination = {
                url: "./assets/img/pinDestino.png",
                scaledSize: new google.maps.Size(29, 42),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(0, 0)
            };
            thisClass.markerDestination = new google.maps.Marker({
                position: new google.maps.LatLng(thisClass.destiny_latitude, thisClass.destiny_longitude),
                animation: google.maps.Animation.DROP,
                icon: iconDestination,
                map: thisClass.googleMap
            });
            thisClass.googleMap.panTo(new google.maps.LatLng(thisClass.destiny_latitude, thisClass.destiny_longitude));
            /*if(type==1){
             console.log(thisClass.origin_address, thisClass.destiny_address, thisClass.type_service_string, thisClass.tip);
             document.getElementById("autocompleteMapaOrigin").setAttribute("value", thisClass.origin_address);
             document.getElementById("autocompleteMapaDestination").setAttribute("value", thisClass.destiny_address);
             //document.getElementById("propinaTransporte").removeAttribute("ngModel");
             document.getElementById("propinaTransporte").setAttribute("value", thisClass.tip);
             //document.getElementById("propinaTransporte").setAttribute("ngModel","");
             if(thisClass.type_service_string=="Blanco"){
             document.getElementById("vehiculoBlanco").setAttribute("checked", "checked");
             }
             else if(thisClass.type_service_string=="Eléctrico"){
             document.getElementById("vehiculoElectrico").setAttribute("checked", "checked");
             }
             else if(thisClass.type_service_string=="Van"){
             document.getElementById("vehiculoVan").setAttribute("checked", "checked");
             }
             }
             else if(type==2){
             console.log(thisClass.origin_address, thisClass.origin_detail, thisClass.destiny_address, thisClass.destiny_detail);
             document.getElementById("autocompleteMapaOriginSend").setAttribute("value", thisClass.origin_address);
             document.getElementById("detallesOrigenSend").setAttribute("value", thisClass.origin_detail);
             document.getElementById("autocompleteMapaDestinationSend").setAttribute("value", thisClass.destiny_address);
             document.getElementById("detallesDestinoSend").setAttribute("value", thisClass.destiny_detail);
             }
             else if(type==3){
             console.log(thisClass.description_text, thisClass.declared_value, thisClass.destiny_name, thisClass.tip);
             document.getElementById("autocompleteMapaOriginSend").setAttribute("value", thisClass.origin_address);
             document.getElementById("detallesOrigenSend").setAttribute("value", thisClass.origin_detail);
             document.getElementById("autocompleteMapaDestinationSend").setAttribute("value", thisClass.destiny_address);
             document.getElementById("detallesDestinoSend").setAttribute("value", thisClass.destiny_detail);
             document.getElementById("descripcionSend").setAttribute("value", thisClass.description_text);
             document.getElementById("valorDeclaradoSend").setAttribute("value", thisClass.declared_value);
             document.getElementById("destinatarioSend").setAttribute("value", thisClass.destiny_name);
             document.getElementById("propinaEnviar").setAttribute("value", thisClass.tip);
             }*/
        }, 3000);
    };
    ServicesComponent.prototype.loginSuccess = function () {
        // console.log("login correcto");
        //this.payService();
        // console.log(this);
        if (__WEBPACK_IMPORTED_MODULE_6_angular2_cookie_services_cookies_service__["CookieService"].prototype.get('credit') != undefined) {
            //if(this._cookieService.get('credit')!=undefined){
            this.viewPayCredit = true;
            document.getElementById("viewPayCredit").style.display = "block";
            document.getElementById("cancelProcessLoading").click();
        }
        else {
            document.getElementById("btnPay").click();
        }
    };
    //---Cambio de vista
    ServicesComponent.prototype.nextProcess = function (typeView, form, typeService) {
        // console.log(typeView, form, typeService);
        switch (typeView) {
            case "viewFromService1":
                this._cookieService.put('orderFormService', '1');
                this.viewFromService = true;
                this.viewContribution = false;
                this.viewSuccess = false;
                this.viewOriginTransport = true;
                this.viewDestinationTransport = false;
                this.markerOriginStatus = true;
                this.markerDestinationStatus = false;
                this.setInputs(1);
                this.loadComponentsGoogleMaps();
                //this.loadComponentPlaceInputOriginTransport();
                break;
            case "viewFromService2":
                this._cookieService.put('orderFormService', '1');
                this.viewFromService = true;
                this.viewContribution = false;
                this.viewSuccess = false;
                this.viewOriginTransport = false;
                this.viewDestinationTransport = true;
                this.markerOriginStatus = false;
                this.markerDestinationStatus = true;
                this.loadComponentPlaceInputDestinationTransport();
                break;
            case "viewFromService3":
                this._cookieService.put('orderFormService', '1');
                this.viewFromService = true;
                this.viewContribution = false;
                this.viewSuccess = false;
                this.viewOriginSend = true;
                this.viewDestinationSend = false;
                this.markerOriginStatus = true;
                this.markerDestinationStatus = false;
                this.setInputs(2);
                this.loadComponentPlaceInputOriginSend();
                this.loadComponentPlaceInputDestinationSend();
                setTimeout(function () {
                    document.getElementById("viewOriginSend").style.display = "block";
                    //this.viewDestinationSend = true;
                    document.getElementById("viewDestinationSend").style.display = "none";
                }, 100);
                break;
            case "viewFromService4":
                var status1 = true;
                var status2 = true;
                var status3 = true;
                // console.log(form.value);
                if (form.value.origin_latitude.toString().trim().length < 1) {
                    status1 = false;
                    document.getElementById("status4").setAttribute("style", "display: block !important");
                }
                else {
                    status1 = true;
                    document.getElementById("status4").setAttribute("style", "display: none !important");
                    document.getElementById("autocompleteMapaOriginSend").className = "form-control col-sm-12";
                }
                if (form.value.destiny_latitude.toString().trim().length < 1) {
                    status2 = false;
                    document.getElementById("status5").setAttribute("style", "display: block !important");
                }
                else {
                    status2 = true;
                    document.getElementById("status5").setAttribute("style", "display: none !important");
                    document.getElementById("autocompleteMapaDestinationSend").className = "form-control col-sm-12";
                }
                if (status1 && status2) {
                    if (this.statusOriginSend) {
                        this.onSubmitFormSend(form.value);
                    }
                    else {
                        // console.log("jajajajajaajajajjajaja");
                        this._cookieService.put('orderFormService', '1');
                        this.statusFormTransport = false;
                        this.viewFromService = true;
                        this.viewContribution = false;
                        this.viewSuccess = false;
                        this.viewOriginSend = false;
                        document.getElementById("viewOriginSend").style.display = "none";
                        this.viewDestinationSend = true;
                        document.getElementById("viewDestinationSend").style.display = "block";
                        // console.log("jajajajajaajajajjajaja");
                        this.markerOriginStatus = false;
                        this.markerDestinationStatus = true;
                        this.dataFOrm1 = form.value;
                        // console.log(this.dataFOrm1.value);
                        this.setInputs(3);
                    }
                }
                else {
                    this.statusFormTransport = true;
                    if (!status1) {
                        var d = document.getElementById("autocompleteMapaOriginSend");
                        d.className += " inputServiceImportant";
                    }
                    if (!status2) {
                        var d = document.getElementById("autocompleteMapaDestinationSend");
                        d.className += " inputServiceImportant";
                    }
                }
                break;
            case "processContribution":
                this.formServicesTransport = form.form;
                if (typeService == 1) {
                    if ((form.value.propinaTransporte).trim().length > 0) {
                        this.tip = form.value.propinaTransporte || "0";
                    }
                    else {
                        this.tip = "0";
                    }
                }
                else if (typeService == 2) {
                    if ((form.value.valorDeclaradoSend).trim().length > 0) {
                        this.declared_value = form.value.valorDeclaradoSend;
                    }
                    else {
                        this.declared_value = null;
                    }
                }
                this.processContribution(form, typeService);
                break;
            case "viewContribution":
                if ((form.value.valorDeclaradoSend).trim().length > 0) {
                    this.declared_value = form.value.valorDeclaradoSend;
                }
                else {
                    this.declared_value = null;
                }
                this._cookieService.put('orderFormService', '2');
                this.viewFromService = false;
                this.viewContribution = true;
                document.getElementById("containerBond").setAttribute("style", "display: none !important;");
                this.viewSuccess = false;
                document.getElementById("spinnerIcon").setAttribute("style", "display: none !important;");
                document.getElementById("price_service").innerHTML = "$ " + this.price_service + " COP";
                document.getElementById("spinnerIconReal").setAttribute("style", "display: none !important;");
                document.getElementById("price_service_real").innerHTML = "$ " + this.price_service + " COP";
                if (form == "transport") {
                    this.containerTypeVehicle = true;
                    this.containerDestinyName = false;
                }
                else {
                    this.containerTypeVehicle = false;
                    this.containerDestinyName = true;
                }
                break;
            case "viewSuccess":
                this._cookieService.put('orderFormService', '3');
                this.viewFromService = false;
                this.viewContribution = false;
                this.viewSuccess = true;
                break;
            case "viewFromService5":
                this._cookieService.put('orderFormService', '1');
                this.statusFormTransport = false;
                this.viewFromService = true;
                this.viewContribution = false;
                this.viewSuccess = false;
                this.viewOriginSend = false;
                setTimeout(function () {
                    document.getElementById("viewOriginSend").style.display = "block";
                    //this.viewDestinationSend = true;
                    //document.getElementById("viewDestinationSend").style.display = "none";
                    document.getElementById("changeProcessView").click();
                }, 100);
            /*this.markerOriginStatus = false;
             this.markerDestinationStatus = true;*/
            default:
                this._cookieService.put('orderFormService', '1');
                this.viewFromService = true;
                this.viewContribution = false;
                this.viewSuccess = false;
                this.viewOriginTransport = true;
                this.viewDestinationTransport = false;
        }
    };
    //---Servicio de cotización
    ServicesComponent.prototype.processContribution = function (form, typeService) {
        var _this = this;
        // console.log(form.value);
        if (typeService == 1) {
            this.type_service = "34"; //this.type_service = this._cookieService.get('service_provider_id_1');
            if ((form.value.propinaTransporte).trim().length > 0) {
                this.tip = form.value.propinaTransporte || "0";
            }
            else {
                this.tip = "0";
            }
        }
        else if (typeService == 2) {
            this.type_service_string = "Envío";
            if ((form.value.propinaEnviar).trim().length > 0) {
                this.tip = form.value.propinaEnviar || "0";
            }
            else {
                this.tip = "0";
            }
            this.type_service = "37"; //this.type_service = this._cookieService.get('service_provider_id_2');
            this.bag_id = "18"; //this.bag_id = this._cookieService.get('shipping_bag_id_4');
            this.origin_detail = form.value.detallesOrigenSend;
            this.description_text = form.value.descripcionSend;
            this.destiny_detail = form.value.detallesDestinoSend;
            this.destiny_name = form.value.destinatarioSend;
        }
        else {
            this.type_service = "34"; //this.type_service = this._cookieService.get('service_provider_id_1');
            if ((form.value.propinaTransporte).trim().length > 0) {
                this.tip = form.value.propinaTransporte || "0";
            }
            else {
                this.tip = "0";
            }
        }
        this.loadingProcess = true;
        this.origin_client = this._cookieService.get('id');
        this._httpService.postContribution(this.origin_latitude, this.origin_longitude, this.destiny_latitude, this.destiny_longitude, this.type_service, this.bag_id, this.tip, this.declared_value, this.dataFOrm1.destiny_address, this.dataFOrm1.origin_address, this.origin_client, this.valuePayDefault).subscribe(function (data) { return _this.postService = JSON.stringify(data); }, function (error) { return _this.responseContribution(_this.postService); }, function () { return _this.responseContribution(_this.postService); });
    };
    //---Respuesta de la cotización
    ServicesComponent.prototype.responseContribution = function (data) {
        // console.log("responseContribution");
        var dataResponse = JSON.parse(data);
        // console.log(dataResponse);
        this.loadingProcess = false;
        document.getElementById('cancelProcessLoading').click();
        if (dataResponse.return) {
            this.loadTop();
            if (dataResponse.data.price == "El envio supera la distancia maxima") {
                this._cookieService.put('orderFormService', '1');
                this.viewFromService = true;
                this.viewContribution = false;
                this.viewSuccess = false;
            }
            else if (dataResponse.data.price == null) {
                this._cookieService.put('orderFormService', '1');
                this.viewFromService = true;
                this.viewContribution = false;
                this.viewSuccess = false;
            }
            else {
                this.quotation_id = dataResponse.data.quotation_id;
                this.distance = (dataResponse.data.km).toString();
                this.amount = (dataResponse.data.price).toString();
                this.time = (dataResponse.data.time).toString();
                this.price_service = dataResponse.data.price;
                this.polyline = dataResponse.data.polyline;
                var thisClass = this;
                this._cookieService.put('orderFormService', '2');
                this.viewFromService = false;
                this.viewContribution = true;
                this.viewSuccess = false;
                if (this.type_service_string == "Envío") {
                    thisClass.containerTypeVehicle = false;
                    thisClass.containerDestinyName = true;
                    document.getElementById('btnViewContributionSend').click();
                    setTimeout(function () {
                        thisClass.validateUserCorporative();
                        document.getElementById("containerBond").setAttribute("style", "display: none !important;");
                        document.getElementById("spinnerIcon").setAttribute("style", "display: none !important");
                        document.getElementById("price_service").innerHTML = "$ " + thisClass.price_service + " COP";
                        document.getElementById("spinnerIconReal").setAttribute("style", "display: none !important;");
                        document.getElementById("price_service_real").innerHTML = "$ " + thisClass.price_service + " COP";
                    }, 3000);
                }
                else {
                    thisClass.containerTypeVehicle = true;
                    thisClass.containerDestinyName = false;
                    document.getElementById('btnViewContribution').click();
                    setTimeout(function () {
                        thisClass.validateUserCorporative();
                        document.getElementById("containerBond").setAttribute("style", "display: none !important;");
                        document.getElementById("spinnerIcon").setAttribute("style", "display: none !important;");
                        document.getElementById("price_service").innerHTML = "$ " + thisClass.price_service + " COP";
                        document.getElementById("spinnerIconReal").setAttribute("style", "display: none !important;");
                        document.getElementById("price_service_real").innerHTML = "$ " + thisClass.price_service + " COP";
                    }, 3000);
                }
            }
        }
        else {
            // console.log("Error cotizando");
            this.loadingProcess = false;
            document.getElementById('cancelProcessLoading').click();
        }
    };
    ServicesComponent.prototype.validateUserCorporative = function () {
        var credit = this._cookieService.get('credit');
        if (credit !== undefined) {
            document.getElementById('viewPayCredit').style.display = 'block';
        }
        else {
            document.getElementById('viewPayCredit').style.display = 'none';
            // document.getElementById("viewPayCredit").style.display = "none";
        }
    };
    //---Abre el modal de bono
    ServicesComponent.prototype.showChildModal = function () {
        if (this._cookieService.get('id') != undefined) {
            this.modalBono.show();
        }
        else {
            this.loadingProcess = false;
            document.getElementById('cancelProcessLoading').click();
            this._httpService.openModalLogin();
        }
    };
    //---Oculta el modal de bono
    ServicesComponent.prototype.hideChildModal = function () {
        this.message_bond = "Ingresa el código de tu bono";
        this.modalBono.hide();
    };
    //---Vistas de bono
    ServicesComponent.prototype.showViewBond = function (dataResponse, status) {
        // console.log(dataResponse, status);
        var thisClass = this;
        if (status) {
            this.hideChildModal();
            this.message_bond = dataResponse.message;
            document.getElementById("containerBond").setAttribute("style", "display: block !important;");
            document.getElementById("viewCodeSuccess").setAttribute("style", "display: block !important;");
            document.getElementById("viewOpenModalBond").setAttribute("style", "display: none !important;");
            document.getElementById("bond_service").innerHTML = "$ " + dataResponse.data[0].discount + " COP";
            this.bond_service = dataResponse.data[0].discount;
            //this.updateValueAmount(dataResponse.data[0].discount);
            var priceValue = parseInt(this.price_service);
            var discountValue = parseInt(dataResponse.data[0].discount);
            if (discountValue <= priceValue) {
                this.price_service = (priceValue - discountValue);
                document.getElementById("spinnerIcon").setAttribute("style", "display: none !important;");
                document.getElementById("price_service").innerHTML = "$ " + this.price_service + " COP";
                //document.getElementById("price_service").setAttribute("value","$ "+this.price_service+" COP");
            }
            else {
            }
        }
        else {
            this.message_bond = dataResponse.message;
            document.getElementById("viewOpenModalBond").setAttribute("style", "display: block !important;");
            document.getElementById("viewCodeSuccess").setAttribute("style", "display: none !important;");
            document.getElementById("containerBond").setAttribute("style", "display: none !important;");
        }
    };
    //---Servicio de bono
    ServicesComponent.prototype.confirmCode = function (form) {
        var _this = this;
        this.loadingBoundProcess = true;
        this.code_confirm = form.value.inputBono;
        this._httpService.confirmCode(this.code_confirm, this._cookieService.get('id'), this.amount).subscribe(function (data) { return _this.postService = JSON.stringify(data); }, function (error) { return _this.respConfirmCode(_this.postService); }, function () { return _this.respConfirmCode(_this.postService); });
    };
    // Respuesta del servicio de bono
    ServicesComponent.prototype.respConfirmCode = function (data) {
        this.loadingBoundProcess = false;
        var dataResponse = JSON.parse(data);
        // console.log(dataResponse);
        if (dataResponse.return) {
            if (dataResponse.message == "Bono aplicado exitosamente") {
                document.getElementById("code_value").innerHTML = "$ " + dataResponse.data[0].discount;
                this.code_value = dataResponse.data[0].discount;
                this.showViewBond(dataResponse, true);
            }
            else {
                this.showViewBond(dataResponse, false);
            }
        }
        else {
            this.showViewBond(dataResponse, false);
        }
    };
    ServicesComponent.prototype.onSubmitFormTransport = function (data) {
        var _this = this;
        var status1 = true;
        var status2 = true;
        var status3 = true;
        // console.log(data);
        if (data.origin_latitude.toString().trim().length < 1) {
            status1 = false;
            document.getElementById('status1').setAttribute("style", "display: block !important");
        }
        else {
            status1 = true;
            document.getElementById("status1").setAttribute("style", "display: none !important");
            document.getElementById("autocompleteMapaOrigin").className = "form-control col-sm-12";
        }
        // console.log(data.destiny_latitude.toString());
        if (data.destiny_latitude.toString().trim().length < 1) {
            status2 = false;
            document.getElementById("status2").setAttribute("style", "display: block !important");
        }
        else {
            status2 = true;
            document.getElementById("status2").setAttribute("style", "display: none !important");
            document.getElementById("autocompleteMapaDestination").className = "form-control col-sm-12";
        }
        // console.log(data.bag_id.toString());
        if (data.bag_id.toString().trim().length < 1) {
            status3 = false;
            document.getElementById("status3").setAttribute("style", "display: block !important");
        }
        else {
            status3 = true;
            document.getElementById("status3").setAttribute("style", "display: none !important");
        }
        if (status1 && status2 && status3) {
            this.statusFormTransport = false;
            this.containerTypeVehicle = true;
            this.containerDestinyName = false;
            this.type_service = "34";
            // console.log(data);
            this.dataFOrm1 = data;
            this.loadingProcess = true;
            this.tip = data.tip || "0";
            this._httpService.postContribution(data.origin_latitude, data.origin_longitude, data.destiny_latitude, data.destiny_longitude, data.type_service, data.bag_id, data.tip, data.declared_value, data.destiny_address, data.origin_address, this._cookieService.get('id'), this.valuePayDefault).subscribe(function (data) { return _this.postService = JSON.stringify(data); }, function (error) { return _this.responseContribution(_this.postService); }, function () { return _this.responseContribution(_this.postService); });
        }
        else {
            this.statusFormTransport = true;
            if (!status1) {
                var d = document.getElementById("autocompleteMapaOrigin");
                d.className += " inputServiceImportant";
            }
            if (!status2) {
                var d = document.getElementById("autocompleteMapaDestination");
                d.className += " inputServiceImportant";
            }
            if (!status3) {
                //document.getElementById("").className = "inputServiceImportant";
            }
        }
    };
    ServicesComponent.prototype.changeValidateInput = function (type) {
        // console.log(this.statusFormTransport);
        if (this.statusFormTransport) {
            switch (type) {
                case "origin_latitude":
                    // console.log(type);
                    document.getElementById("status1").setAttribute("style", "display: none !important");
                    document.getElementById("autocompleteMapaOrigin").className = "form-control col-sm-12";
                    break;
                case "destiny_latitude":
                    // console.log(type);
                    document.getElementById("status2").setAttribute("style", "display: none !important");
                    document.getElementById("autocompleteMapaDestination").className = "form-control col-sm-12";
                    break;
                case "bag_id":
                    // console.log(type);
                    document.getElementById("status3").setAttribute("style", "display: none !important");
                    break;
                case "origin_latitude_send":
                    // console.log(type);
                    document.getElementById("status4").setAttribute("style", "display: none !important");
                    document.getElementById("autocompleteMapaOriginSend").className = "form-control col-sm-12";
                    break;
                case "destiny_latitude_send":
                    // console.log(type);
                    document.getElementById("status5").setAttribute("style", "display: none !important");
                    document.getElementById("autocompleteMapaDestinationSend").className = "form-control col-sm-12";
                    break;
                case "description_text":
                    // console.log(type);
                    document.getElementById("status6").setAttribute("style", "display: none !important");
                    document.getElementById("description_text").className = "form-control col-sm-12";
                    break;
                case "declared_value":
                    // console.log(type);
                    document.getElementById("status7").setAttribute("style", "display: none !important");
                    document.getElementById("declared_value").className = "form-control col-sm-12";
                    break;
                case "destiny_name":
                    // console.log(type);
                    document.getElementById("status8").setAttribute("style", "display: none !important");
                    document.getElementById("destiny_name").className = "form-control col-sm-12";
                    break;
                case "pay":
                    // console.log(type);
                    document.getElementById("status9").setAttribute("style", "display: none !important;");
                    break;
                default:
                    // console.log(type);
                    break;
            }
        }
    };
    ServicesComponent.prototype.onSubmitFormSend = function (data) {
        var _this = this;
        var status1 = true;
        var status2 = true;
        var status3 = true;
        // console.log(data);
        if (data.description_text.toString().trim().length < 1) {
            status1 = false;
            document.getElementById("status6").setAttribute("style", "display: block !important");
        }
        else {
            status1 = true;
            document.getElementById("status6").setAttribute("style", "display: none !important");
            document.getElementById("description_text").className = "form-control col-sm-12";
        }
        if (data.declared_value.toString().trim().length < 1) {
            status2 = false;
            document.getElementById("status7").setAttribute("style", "display: block !important");
        }
        else {
            status2 = true;
            document.getElementById("status7").setAttribute("style", "display: none !important");
            document.getElementById("declared_value").className = "form-control col-sm-12";
        }
        if (data.destiny_name.toString().trim().length < 1) {
            status3 = false;
            document.getElementById("status8").setAttribute("style", "display: block !important");
        }
        else {
            status3 = true;
            document.getElementById("status8").setAttribute("style", "display: none !important");
            document.getElementById("destiny_name").className = "form-control col-sm-12";
        }
        if (status1 && status2 && status3) {
            this.statusFormTransport = false;
            this.type_service_string = "Envío";
            // console.log(data);
            this.dataFOrm2 = data;
            this.loadingProcess = true;
            this.type_service = this._cookieService.get('service_provider_id_2') || "37";
            this.bag_id = this._cookieService.get('shipping_bag_id_4') || "18";
            this.destiny_name = data.destiny_name;
            // console.log("Holanda");
            // console.log(this.dataFOrm1);
            // console.log(this.dataFOrm1.origin_latitude, this.dataFOrm1.origin_longitude, this.dataFOrm1.destiny_latitude, this.dataFOrm1.destiny_longitude, this.type_service, this.bag_id, this.dataFOrm2.tip, this.dataFOrm2.declared_value);
            this.tip = this.dataFOrm2.tip || "0";
            this.origin_client = this._cookieService.get('id');
            this._httpService.postContribution(this.dataFOrm1.origin_latitude, this.dataFOrm1.origin_longitude, this.dataFOrm1.destiny_latitude, this.dataFOrm1.destiny_longitude, this.type_service, this.bag_id, this.dataFOrm2.tip, this.dataFOrm2.declared_value, this.dataFOrm1.destiny_address, this.dataFOrm1.origin_address, this.origin_client, this.valuePayDefault).subscribe(function (data) { return _this.postService = JSON.stringify(data); }, function (error) { return _this.responseContribution(_this.postService); }, function () { return _this.responseContribution(_this.postService); });
        }
        else {
            this.statusFormTransport = true;
            if (!status1) {
                var d = document.getElementById("description_text");
                d.className += " inputServiceImportant";
            }
            if (!status2) {
                var d = document.getElementById("declared_value");
                d.className += " inputServiceImportant";
            }
            if (!status3) {
                var d = document.getElementById("destiny_name");
                d.className += " inputServiceImportant";
            }
        }
    };
    //---Enviar solicitud de servicio
    ServicesComponent.prototype.payService = function () {
        var _this = this;
        //this.modalLoading.show();
        this.loadingProcess = true;
        if (this.pay != null) {
            this.statusFormTransport = false;
            document.getElementById("status9").setAttribute("style", "display: none !important;");
            if (this._cookieService.get('id') != undefined) {
                if (this._cookieService.get('id') != undefined) {
                    this.origin_client = this._cookieService.get('id');
                }
                this._httpService.pagarServicio(this.distance, this.origin_client, this.origin_address, this.origin_latitude, this.origin_longitude, this.destiny_address, this.destiny_latitude, this.destiny_longitude, this.type_service, this.amount, this.pay, this.time, this.tip, this.bag_id, this.code_confirm, this.origin_detail, this.description_text, this.destiny_detail, this.destiny_name, this.polyline, this.quotation_id).subscribe(function (data) { return _this.postService = JSON.stringify(data); }, function (error) { return _this.pagoCorrecto(_this.postService); }, function () { return _this.pagoCorrecto(_this.postService); });
            }
            else {
                this.loadingProcess = false;
                // console.log("esta en false");
                document.getElementById('cancelProcessLoading').click();
                this._httpService.openModalLogin();
            }
        }
        else {
            this.statusFormTransport = true;
            this.loadingProcess = false;
            document.getElementById("status9").setAttribute("style", "display: block !important;");
        }
    };
    //---Respuesta de  la confirmacion
    ServicesComponent.prototype.pagoCorrecto = function (data) {
        this.loadingProcess = false;
        // console.log("Respuesta de solicitud de servicio");
        document.getElementById('cancelProcessLoading').click();
        var dataResponse = JSON.parse(data);
        // console.log(dataResponse);
        if (dataResponse.return) {
            //this.hideChildModal();
            document.getElementById('nextProcessViewSuccess').click();
        }
        else {
            document.getElementById("responsePayService").innerHTML = dataResponse.message;
            // console.log("Error confirmando");
        }
    };
    ServicesComponent.prototype.responseGetCredit = function (data) {
        // console.log(data);
        var dataResponseGetCredit = JSON.parse(data);
        // console.log(dataResponseGetCredit);
        if (dataResponseGetCredit.return) {
            this.creditValue = dataResponseGetCredit.data.credit;
            document.getElementById("creditInput").setAttribute("value", "SALDO DISPONIBLE $" + this.creditValue);
            this.viewPayCredit = true;
            // document.getElementById("viewPayCredit").style.display = "block";
            this._cookieService.put("credit", this.creditValue);
            // console.log(this._cookieService.get("credit"));
        }
        else {
            // console.log("problemas al actualizar el credito");
            this.creditValue = this._cookieService.get("credit");
            document.getElementById("creditInput").setAttribute("value", "SALDO DISPONIBLE $" + this.creditValue);
            this.viewPayCredit = true;
            // document.getElementById("viewPayCredit").style.display = "block";
        }
    };
    //---Cambio vista solicitud exitosa
    ServicesComponent.prototype.nextProcessViewSuccess = function () {
        var _this = this;
        this._cookieService.put('orderFormService', '3');
        this.viewFromService = false;
        this.viewContribution = false;
        this.viewDataConfirm = false;
        this.viewSuccess = true;
        var dataPostService = JSON.parse(this.postService);
        // console.log(dataPostService);
        this._cookieService.put('shipping_id', dataPostService.data.shipping_id);
        this._cookieService.put('statusServices', 'true');
        this._httpService.socketOn(this._cookieService.get('socket1')).subscribe(function (data) { return _this.postServiceSocket = JSON.stringify(data); }, function (error) { return _this.responseSocket(_this.postServiceSocket, 1); }, function () { return _this.responseSocket(_this.postServiceSocket, 1); });
    };
    ServicesComponent.prototype.confirmCancelBeep = function (type) {
        var _this = this;
        if (type == "si") {
            // console.log("vamo a cancelar el serv");
            var postCancelService = "";
            // console.log("cancelará el servicio");
            this._httpService.cancelService(this._cookieService.get('shipping_id')).subscribe(function (data) { return postCancelService = JSON.stringify(data); }, function (error) { return _this.responseCancelService(postCancelService); }, function () { return _this.responseCancelService(postCancelService); });
        }
        else if (type == "no") {
            this.modalConfirmCancelBeep.hide();
        }
        else {
            this.confirmCancelBeep("no");
        }
    };
    ServicesComponent.prototype.cancelService = function () {
        this.modalConfirmCancelBeep.show();
    };
    ServicesComponent.prototype.responseCancelService = function (data) {
        // console.log(data);
        var dataPostServiceCancel = JSON.parse(data);
        // console.log(dataPostServiceCancel);
        this._cookieService.put("validRequiestService", "false");
        this._httpService.setTimeoutServiceTokenApp();
        this.modalConfirmCancelBeep.hide();
        if (dataPostServiceCancel.return) {
            // console.log(dataPostServiceCancel.message);
            this._cookieService.put('shipping_id', '');
            this._cookieService.put('statusServices', 'false');
            this.showModalCancelBeep();
        }
        else {
            // console.log("problemas al cancelar servicio");
        }
    };
    ServicesComponent.prototype.responseSocket = function (data, type) {
        // console.log("respuesta del socket");
        // console.log(data);
        // console.log(type);
        var dataPost = JSON.parse(data);
        this._cookieService.put("validRequiestService", "false");
        this._httpService.setTimeoutServiceTokenApp();
        if (dataPost.token == "Could not bind to socket↵" || dataPost.token == "Could not bind to socket") {
        }
        else if (dataPost.token != "Could not bind to socket↵" && dataPost.token != "Could not bind to socket") {
            if (type == 1) {
                //document.getElementById("btnDeliveryVerification").click();
                // console.log("HARA CLICK A PUSH WEB - CONFIRMAR");
                document.getElementById("btnPushWeb").click();
            }
            else if (type == 2) {
                if (dataPost.token != undefined) {
                    this.pushWebCancelService();
                }
                else {
                    // console.log("HARA CLICK A LLEGO BEEP");
                    document.getElementById("btnShowModalConfirmBeep").click();
                }
            }
            else {
            }
        }
    };
    ServicesComponent.prototype.showModalConfirmBeep = function () {
        this.modalConfirmBeep.show();
    };
    ServicesComponent.prototype.hideModalConfirmBeep = function () {
        this.modalConfirmBeep.hide();
        this._routerModule.navigateByUrl('home');
    };
    ServicesComponent.prototype.pushWeb = function () {
        var _this = this;
        // console.log("pushWeb");
        // console.log(this);
        var dataPostSocket = JSON.parse(this.postServiceSocket);
        // console.log(dataPostSocket);
        var dataPostService = JSON.parse(this.postService);
        // console.log(dataPostService);
        // console.log("");
        var postServicePushWeb;
        this._httpService.pushWeb(dataPostSocket.token, dataPostService.data.shipping_id).subscribe(function (data) { return postServicePushWeb = JSON.stringify(data); }, function (error) { return _this.responsPushWeb(postServicePushWeb); }, function () { return _this.responsPushWeb(postServicePushWeb); });
    };
    ServicesComponent.prototype.pushWebCancelService = function () {
        var _this = this;
        // console.log("pushWeb");
        // console.log(this);
        var dataPostSocket = JSON.parse(this.postServiceSocketConfirm);
        // console.log(dataPostSocket);
        var dataPostService = JSON.parse(this.postService);
        // console.log(dataPostService);
        // console.log("");
        var postServicePushWeb;
        this._httpService.pushWeb(dataPostSocket.token, dataPostService.data.shipping_id).subscribe(function (data) { return postServicePushWeb = JSON.stringify(data); }, function (error) { return _this.responsPushWeb(postServicePushWeb); }, function () { return _this.responsPushWeb(postServicePushWeb); });
    };
    ServicesComponent.prototype.responsPushWeb = function (data) {
        // console.log("la respuesta es");
        // console.log(data);
        // console.log(this);
        var dataPostService = JSON.parse(data);
        var thisClass = this;
        // console.log(dataPostService);
        if (dataPostService.data.type == 1) {
            // console.log("es 11111111111");
            this.postServicePushWeb = data;
            this.logisticresource_id = dataPostService.data.logisticId;
            this.name_employee = dataPostService.data.info.name_employee;
            this.lastName_employee = dataPostService.data.info.lastName_employee;
            this.vehicle_brand = dataPostService.data.info.vehicle_brand;
            this.vehicle_model = dataPostService.data.info.vehicle_model;
            this.icense_plate = dataPostService.data.info.icense_plate;
            this.arrival_time = dataPostService.data.info.arrival_time;
            this.security_code = dataPostService.data.info.security_code;
            this.picture_employee = dataPostService.data.info.picture_employee;
            var rating = parseInt(dataPostService.data.info.rating_employee);
            this.rating_employee = rating;
            document.getElementById("btnViewConfirmService").click();
            this.viewConfirmService(data);
        }
        else if (dataPostService.data.type == 4) {
            // console.log("es 44444444444444");
            this.showModalCancelBeep();
            /*setTimeout(function() {
             thisClass.hideModalCancelBeep();
             thisClass._routerModule.navigateByUrl('home');
             }, 2000);*/
        }
        else if (dataPostService.data.type == 10) {
            // console.log("es 101010101010");
            this.modalCancelDriveBeep.show();
            var sockets = (dataPostService.data.info.socket).split(",");
            // console.log(sockets);
            this._cookieService.put("socket1", sockets[0]);
            this._cookieService.put("socket2", sockets[1]);
            /*setTimeout(function() {
             // console.log("click para cerrar");
             document.getElementById("btnHideModalCancelBeep").click();
             }, 2000);*/
            setTimeout(function () {
                // console.log("otra vez sokcet PRUEBAAAAAA");
                thisClass.nextProcessViewSuccess();
            }, 1000);
        }
        else if (dataPostService.data.type == 12) {
            // console.log("llego el conductor");
            /*let sockets = (dataPostService.data.info.socket).split(",");
             console.log(sockets);
             this._cookieService.put("socket1", sockets[0]);
             this._cookieService.put("socket2", sockets[1]);*/
            this.showModalConfirmBeep();
            /*setTimeout(function() {
             thisClass.hideModalConfirmBeep();
             }, 2000);*/
        }
        else {
            // console.log("es esleeeeeeeeeee");
        }
    };
    ServicesComponent.prototype.showModalCancelBeep = function () {
        this.modalCancelBeep.show();
    };
    ServicesComponent.prototype.hideModalCancelBeep = function () {
        this.modalCancelBeep.hide();
        this._routerModule.navigateByUrl('home');
    };
    ServicesComponent.prototype.viewConfirmService = function (data) {
        var dataPostService = JSON.parse(data);
        // console.log(dataPostService);
        if (dataPostService.data.type == 1) {
            this._cookieService.put('orderFormService', '3');
            this.viewDataConfirm = true;
            this.viewFromService = false;
            this.viewContribution = false;
            this.viewSuccess = false;
            this.viewOriginTransport = false;
            this.viewDestinationTransport = false;
            this.loadComponentsGoogleMapsPushWeb();
        }
        else {
            alert("servicio cancelado!!");
        }
    };
    ServicesComponent.prototype.btnViewConfirmService = function () {
        // console.log("CLICK!");
    };
    //cancela el servicio http://192.168.0.113/api_devel/Shipping/cancel
    ServicesComponent.prototype.loadComponentsGoogleMapsPushWeb = function () {
        var _this = this;
        // console.log("Cargara todo de push web google");
        //---Muestro la primer parte del formulario del tab Transporte
        var thisClass = this;
        // console.log("Hará el socket para recibir la confirmció de que el beep ya llego");
        this._httpService.socketOn(this._cookieService.get('socket2')).subscribe(function (data) { return _this.postServiceSocketConfirm = JSON.stringify(data); }, function (error) { return _this.responseSocket(_this.postServiceSocketConfirm, 2); }, function () { return _this.responseSocket(_this.postServiceSocketConfirm, 2); });
        setTimeout(function () {
            //---Cargo el mapa
            var geocoder = new google.maps.Geocoder();
            var mapProp = {
                center: new google.maps.LatLng(4.6762881, -74.05177549999999),
                zoom: 18,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
            thisClass.getPositionBeep(thisClass, map);
            if (thisClass.dataServiceTransport.origin_latitude) {
                var iconOrigin = {
                    url: "./assets/img/pinOrigen.png",
                    scaledSize: new google.maps.Size(29, 42),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(0, 0)
                };
                thisClass.markerOrigin = new google.maps.Marker({
                    position: new google.maps.LatLng(thisClass.dataServiceTransport.origin_latitude, thisClass.dataServiceTransport.origin_longitude),
                    animation: google.maps.Animation.DROP,
                    icon: iconOrigin,
                    map: map
                });
            }
            if (thisClass.dataServiceTransport.destiny_latitude) {
                var iconDestination = {
                    url: "./assets/img/pinDestino.png",
                    scaledSize: new google.maps.Size(29, 42),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(0, 0)
                };
                thisClass.markerDestination = new google.maps.Marker({
                    position: new google.maps.LatLng(thisClass.dataServiceTransport.destiny_latitude, thisClass.dataServiceTransport.destiny_longitude),
                    animation: google.maps.Animation.DROP,
                    icon: iconDestination,
                    map: map
                });
            }
            if (thisClass.dataServiceSend.origin_latitude) {
                var iconOrigin = {
                    url: "./assets/img/pinOrigen.png",
                    scaledSize: new google.maps.Size(29, 42),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(0, 0)
                };
                thisClass.markerOrigin = new google.maps.Marker({
                    position: new google.maps.LatLng(thisClass.dataServiceSend.origin_latitude, thisClass.dataServiceSend.origin_longitude),
                    animation: google.maps.Animation.DROP,
                    icon: iconOrigin,
                    map: map
                });
            }
            if (thisClass.dataServiceSend.destiny_latitude) {
                var iconDestination = {
                    url: "./assets/img/pinDestino.png",
                    scaledSize: new google.maps.Size(29, 42),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(0, 0)
                };
                thisClass.markerDestination = new google.maps.Marker({
                    position: new google.maps.LatLng(thisClass.dataServiceSend.destiny_latitude, thisClass.dataServiceSend.destiny_longitude),
                    animation: google.maps.Animation.DROP,
                    icon: iconDestination,
                    map: map
                });
            }
        }, 1000);
    };
    ServicesComponent.prototype.getPositionBeep = function (thisClass, map) {
        var _this = this;
        // console.log("Get position beep");
        this.timerPositionBeep = setInterval(function () {
            thisClass._httpService.getLocation(_this.logisticresource_id).subscribe(function (data) { return thisClass.responseServiceGetLocation = JSON.stringify(data); }, function (error) { return thisClass.responseGetPositionBeep(thisClass.responseServiceGetLocation, map); }, function () { return thisClass.responseGetPositionBeep(thisClass.responseServiceGetLocation, map); });
        }, 10000);
    };
    ServicesComponent.prototype.responseGetPositionBeep = function (data, map) {
        var dataPostService = JSON.parse(data);
        // console.log(dataPostService);
        // console.log("Entro responseGetPositionBeep");
        if (this.markerUbeep == null) {
            // console.log("Entro diferente de null, crea el marker");
            var iconBeep = {
                url: "./assets/img/pinBeep.png",
                scaledSize: new google.maps.Size(29, 42),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(0, 0)
            };
            this.positionUbeepOrigin = [parseFloat(dataPostService.data.latitud), parseFloat(dataPostService.data.longitud)];
            this.markerUbeep = new google.maps.Marker({
                position: new google.maps.LatLng(parseFloat(dataPostService.data.latitud), parseFloat(dataPostService.data.longitud)),
                animation: google.maps.Animation.DROP,
                icon: iconBeep,
                map: map
            });
            map.panTo(new google.maps.LatLng(parseFloat(dataPostService.data.latitud), parseFloat(dataPostService.data.longitud)));
            // this.transition([parseFloat(dataPostService.data.latitud), parseFloat(dataPostService.data.longitud)]);
        }
        else {
            var thisClass = this;
            /*console.log("Entro NO es diferente de null");
             if( this.positionUbeepNew== null )
             {*/
            // console.log("Entro POSITION NUEVA ES NULL");
            //this.transition();
            this.i = 0;
            this.deltaLat = (parseFloat(dataPostService.data.latitud) - this.positionUbeepOrigin[0]) / this.numDeltas;
            this.deltaLng = (parseFloat(dataPostService.data.longitud) - this.positionUbeepOrigin[1]) / this.numDeltas;
            this.positionUbeepOrigin[0] += this.deltaLat;
            this.positionUbeepOrigin[1] += this.deltaLng;
            var latlng = new google.maps.LatLng(this.positionUbeepOrigin[0], this.positionUbeepOrigin[1]);
            // console.log("1.1 ",this.positionUbeepOrigin, this.positionUbeepNew);
            // console.log();
            // console.log("1.2 ",latlng);
            this.markerUbeep.setPosition(latlng);
            // console.log(this.i, this.numDeltas);
            if (this.i != this.numDeltas) {
                this.i++;
                setTimeout(function () {
                    thisClass.positionUbeepOrigin[0] += thisClass.deltaLat;
                    thisClass.positionUbeepOrigin[1] += thisClass.deltaLng;
                    var latlng = new google.maps.LatLng(thisClass.positionUbeepOrigin[0], thisClass.positionUbeepOrigin[1]);
                    // console.log("2.1 ",this.positionUbeepOrigin, this.positionUbeepNew);
                    // console.log();
                    // console.log("2.2 ",latlng);
                    thisClass.markerUbeep.setPosition(latlng);
                    thisClass.positionUbeepNew = [parseFloat(dataPostService.data.latitud), parseFloat(dataPostService.data.longitud)];
                }, 10);
            }
            //}
        }
    };
    ServicesComponent.prototype.transition = function () {
        this.i = 0;
        this.deltaLat = (this.positionUbeepNew[0] - this.positionUbeepOrigin[0]) / this.numDeltas;
        this.deltaLng = (this.positionUbeepNew[1] - this.positionUbeepOrigin[1]) / this.numDeltas;
        // console.log("hizo algo que no se que es");
        this.moveMarker();
    };
    ServicesComponent.prototype.moveMarker = function () {
        // console.log("ENTROOOOOOOOOOOOOOOOOOOOOOOOOOOOOO a mover el marker");
        // console.log(this.positionUbeepOrigin);
        // console.log(this.positionUbeepNew);
        this.positionUbeepOrigin[0] += this.deltaLat;
        this.positionUbeepOrigin[1] += this.deltaLng;
        // console.log(this.positionUbeepOrigin);
        var latlng = new google.maps.LatLng(this.positionUbeepOrigin[0], this.positionUbeepOrigin[1]);
        this.markerUbeep.setPosition(latlng);
        if (this.i != this.numDeltas) {
            this.i++;
            // console.log("ENTROOOOOOOOOOOOOOOOOOOOOOOOOOOOOO MOVIO MARKER");
            /*setTimeout(function() {
             setTimeout(this.moveMarker, this.delay);
             }, 1000);*/
        }
    };
    ServicesComponent.prototype.ngOnDestroy = function () {
        // console.log(this.timerPositionBeep);
        if (this.timerPositionBeep != null) {
            clearInterval(this.timerPositionBeep);
            // console.log("finish interval");
        }
    };
    ServicesComponent.prototype.responDeliveryVerification = function (data) {
        // console.log(data);
        document.getElementById("btnPushWeb").click();
    };
    ServicesComponent.prototype.deliveryVerification = function () {
        // console.log("deliveryVerification");
        // console.log(this);
        // console.log(HttpSolicitarService.prototype);
        // console.log(ServicesComponent.prototype);
        //console.log(ServiceComponent.prototype.postService);
        //console.log(HttpSolicitarService.prototype.deliveryVerification);
        //console.log(ServiceComponent.prototype.postServiceDeliveryVerification);
        //console.log(ServiceComponent.prototype.responDeliveryVerification);
        //console.log(this);
        //console.log(this._httpService);
        //this._httpService.deliveryVerification(this.postService.data.shipping_id).subscribe(
        /*var dataPostService = JSON.parse(ServiceComponent.prototype.postService);
         console.log(dataPostService);*/
        /*HttpSolicitarService.prototype.deliveryVerification("2222").subscribe(
         data => this.postServiceDeliveryVerification = JSON.stringify(data),
         error => this.responDeliveryVerification(this.postServiceDeliveryVerification),
         () => this.responDeliveryVerification(this.postServiceDeliveryVerification)
         );*/
    };
    ServicesComponent.prototype.paymentGateWayPost = function () {
        var _this = this;
        var data = {
            quotation_id: this.quotation_id,
            client_id: this._cookieService.get('id'),
            platform: 'web'
        };
        console.log(data);
        this.loadingProcessPaymentGateWayPost = true;
        this._httpService.paymentGateWayPost(data).subscribe(function (data) { return _this.responsePaymentGateWayPost = JSON.stringify(data); }, function (error) { return _this.responsePaymentGateWay(_this.responsePaymentGateWayPost, 'post'); }, function () { return _this.responsePaymentGateWay(_this.responsePaymentGateWayPost, 'post'); });
    };
    ServicesComponent.prototype.paymentGateWayTokenize = function () {
        var _this = this;
        this.paymentGateWay.client_id = this._cookieService.get('id');
        console.log(this.paymentGateWay);
        this.loadingProcessPaymentGateWayPost = true;
        this._httpService.paymentGateWayTokenize(this.paymentGateWay).subscribe(function (data) { return _this.responsePaymentGateWayTokenize = JSON.stringify(data); }, function (error) { return _this.responsePaymentGateWay(_this.responsePaymentGateWayTokenize, 'tokenize'); }, function () { return _this.responsePaymentGateWay(_this.responsePaymentGateWayTokenize, 'tokenize'); });
    };
    ServicesComponent.prototype.responsePaymentGateWay = function (data, type) {
        var dataParse = JSON.parse(data);
        console.log(dataParse, type);
        this.loadingProcessPaymentGateWayPost = false;
        if (type === 'post') {
            if (dataParse.status === 210) {
                this.messagePaymentGateWayPost = dataParse.message;
                this.refreshView();
            }
            else if (dataParse.status === 211) {
                this.messagePaymentGateWayPost = dataParse.message;
                this.refreshView();
            }
            else if (dataParse.status === 409) {
                this.messagePaymentGateWayPost = 'Seleccione otro medio de pago';
                this.refreshView();
            }
            else if (dataParse.status === 200) {
                this.messagePaymentGateWayPost = dataParse.message;
                this.refreshView();
                // aprobado continua con el proceso de post
                // this.payService();
            }
        }
        else if (type === 'tokenize') {
            if (dataParse.status === 409) {
                this.messagePaymentGateWayTokenize = dataParse.message;
                this.refreshView();
            }
            else if (dataParse.status === 200) {
                this.messagePaymentGateWayTokenize = dataParse.message;
                this.refreshView();
                this.paymentGateWayPost();
            }
        }
    };
    return ServicesComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('modalBono'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2_ng2_bootstrap__["ModalDirective"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ng2_bootstrap__["ModalDirective"]) === "function" && _a || Object)
], ServicesComponent.prototype, "modalBono", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('modalLoading'),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2_ng2_bootstrap__["ModalDirective"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ng2_bootstrap__["ModalDirective"]) === "function" && _b || Object)
], ServicesComponent.prototype, "modalLoading", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('modalConfirmBeep'),
    __metadata("design:type", typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2_ng2_bootstrap__["ModalDirective"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ng2_bootstrap__["ModalDirective"]) === "function" && _c || Object)
], ServicesComponent.prototype, "modalConfirmBeep", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('modalCancelBeep'),
    __metadata("design:type", typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2_ng2_bootstrap__["ModalDirective"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ng2_bootstrap__["ModalDirective"]) === "function" && _d || Object)
], ServicesComponent.prototype, "modalCancelBeep", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('modalConfirmCancelBeep'),
    __metadata("design:type", typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_2_ng2_bootstrap__["ModalDirective"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ng2_bootstrap__["ModalDirective"]) === "function" && _e || Object)
], ServicesComponent.prototype, "modalConfirmCancelBeep", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('modalCancelDriveBeep'),
    __metadata("design:type", typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_2_ng2_bootstrap__["ModalDirective"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ng2_bootstrap__["ModalDirective"]) === "function" && _f || Object)
], ServicesComponent.prototype, "modalCancelDriveBeep", void 0);
ServicesComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__(522),
        styles: [__webpack_require__(476)],
        providers: [__WEBPACK_IMPORTED_MODULE_7__services_service__["a" /* HttpSolicitarService */], __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"], __WEBPACK_IMPORTED_MODULE_2_ng2_bootstrap__["TabsetConfig"]]
    }),
    __metadata("design:paramtypes", [typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_7__services_service__["a" /* HttpSolicitarService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__services_service__["a" /* HttpSolicitarService */]) === "function" && _h || Object, typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_6_angular2_cookie_services_cookies_service__["CookieService"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6_angular2_cookie_services_cookies_service__["CookieService"]) === "function" && _j || Object, typeof (_k = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["Compiler"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["Compiler"]) === "function" && _k || Object, typeof (_l = typeof __WEBPACK_IMPORTED_MODULE_4__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__angular_router__["b" /* Router */]) === "function" && _l || Object, typeof (_m = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"]) === "function" && _m || Object, typeof (_o = typeof __WEBPACK_IMPORTED_MODULE_3__angular_http__["d" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_http__["d" /* Http */]) === "function" && _o || Object, typeof (_p = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"]) === "function" && _p || Object])
], ServicesComponent);

var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p;
//# sourceMappingURL=services.component.js.map

/***/ }),

/***/ 13:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConstantUbeep; });
var ConstantUbeep = (function () {
    function ConstantUbeep() {
        // dev
        this.urlServer = "http://52.43.247.174/";
        // public urlServer = 'http://api.localhost/';
        // prod
        // public urlServer = 'http://54.70.181.28/';
        // url web dev
        // public urlWeb = 'develop.ubeep.co/';
        // url web prod
        this.urlWeb = 'ubeep.co/';
    }
    return ConstantUbeep;
}());

//# sourceMappingURL=constantsUbeep.js.map

/***/ }),

/***/ 130:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_catch__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__constantsUbeep__ = __webpack_require__(13);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HttpSolicitarService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var HttpSolicitarService = (function () {
    function HttpSolicitarService(_http, _ConstantUbeep) {
        this._http = _http;
        this._ConstantUbeep = _ConstantUbeep;
        this._urlContribution = "assets/services/services/http.service.solicitar.php";
        this._urlContributionSend = "assets/services/services/http.service.solicitar.envio.php";
        this._urlPay = "assets/services/services/http.service.pagar.php";
        this._urlBond = "assets/services/services/http.service.bond.php";
        this._urlSocket = "assets/services/services/socket.on.php";
        this._urlDeliveryverification = "assets/services/services/deliveryverification.php";
        this._urlPushWeb = "assets/services/services/pushweb.php";
        this._urlPushWeb2 = "assets/services/services/pushweb.php";
        this._urlCancelService = "assets/services/services/cancel.service.php";
        this._urlGetCredit = "assets/services/services/credit.service.php";
        this._urlGetLocation = "assets/services/services/get-location.service.php";
        this._urlPaymentTokenize = 'assets/services/services/paymentGateWayTokenize.php';
        this._urlPaymentPost = 'assets/services/services/paymentGateWayPost.php';
    }
    HttpSolicitarService.prototype.postContribution = function (origin_latitude, origin_longitude, destiny_latitude, destiny_longitude, type_service, bag_id, tip, declared_value, destination_address, origin_address, origin_client, pay) {
        var data = JSON.stringify({
            key: "3edcdb20e0030daab21d0ba9af4c0dc2",
            origin_latitude: origin_latitude,
            origin_longitude: origin_longitude,
            destiny_latitude: destiny_latitude,
            destiny_longitude: destiny_longitude,
            type_service: type_service,
            bag_id: bag_id,
            tip: tip || "0",
            declared_value: declared_value,
            url: this._ConstantUbeep.urlServer,
            destination_address: destination_address,
            origin_address: origin_address,
            origin_client: origin_client,
            pay: pay
        });
        // console.log(data);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ "Access-Control-Allow-Origin": "*" });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({ headers: headers, method: "POST" });
        var typePost = "";
        if (declared_value == undefined) {
            typePost = this._urlContribution;
        }
        else {
            typePost = this._urlContributionSend;
        }
        // console.log(typePost);
        return this._http.post(typePost, data, options)
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    HttpSolicitarService.prototype.pagarServicio = function (distance, origin_client, origin_address, origin_latitude, origin_longitude, destiny_address, destiny_latitude, destiny_longitude, type_service, amount, pay, time, tip, bag_id, code_confirm, origin_detail, description_text, destiny_detail, destiny_name, polyline, quotation_id) {
        var data = JSON.stringify({
            key: "3edcdb20e0030daab21d0ba9af4c0dc2",
            distance: distance,
            origin_client: origin_client,
            origin_address: origin_address,
            origin_latitude: origin_latitude,
            origin_longitude: origin_longitude,
            destiny_address: destiny_address,
            destiny_latitude: destiny_latitude,
            destiny_longitude: destiny_longitude,
            amount: amount,
            type_service: type_service,
            pay: pay,
            time: time,
            tip: tip || "0",
            platform: "web",
            bag_id: bag_id,
            code_confirm: code_confirm,
            origin_detail: origin_detail,
            description_text: description_text,
            destiny_detail: destiny_detail,
            destiny_name: destiny_name,
            polyline: polyline,
            url: this._ConstantUbeep.urlServer,
            quotation_id: quotation_id
        });
        // console.log(data);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ "Access-Control-Allow-Origin": "*" });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({ headers: headers, method: "POST" });
        return this._http.post(this._urlPay, data, options)
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    HttpSolicitarService.prototype.confirmCode = function (bond, client_id, amount) {
        var data = JSON.stringify({
            key: "3edcdb20e0030daab21d0ba9af4c0dc2",
            bond: bond,
            client_id: client_id,
            amount: amount,
            platform: "web",
            url: this._ConstantUbeep.urlServer
        });
        // console.log(data);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ "Access-Control-Allow-Origin": "*" });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({ headers: headers, method: "POST" });
        return this._http.post(this._urlBond, data, options)
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    HttpSolicitarService.prototype.socketOn = function (port) {
        // console.log("socketOn");
        var data = JSON.stringify({ port: port });
        // console.log(data);
        return this._http.post(this._urlSocket, data)
            .map(this.responseSocket)
            .catch(this.handleError);
    };
    HttpSolicitarService.prototype.responseSocket = function (res) {
        // console.log("Respuesta del socket 1");
        // console.log(HttpSolicitarService.arguments);
        //console.log(HttpSolicitarService.caller);
        var data = {
            token: res._body,
            status: res.ok
        };
        // console.log(data);
        return data;
    };
    HttpSolicitarService.prototype.pushWeb = function (token, shipping_id) {
        // console.log(this);
        // console.log(Http.prototype);
        //return this._http.post("app/services/pushweb.php", data, options)
        var data = JSON.stringify({
            key: "3edcdb20e0030daab21d0ba9af4c0dc2",
            token: token,
            shipping_id: shipping_id,
            url: this._ConstantUbeep.urlServer
        });
        // console.log(data);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ "Access-Control-Allow-Origin": "*" });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({ headers: headers, method: "POST" });
        // console.log(this);
        return this._http.post(this._urlPushWeb2, data, options)
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    HttpSolicitarService.prototype.openModalLogin = function () {
        // console.log(document.getElementById("btnIngresar"));
        document.getElementById("btnIngresar").click();
        //AppComponent.prototype.showChildModal();
    };
    HttpSolicitarService.prototype.cancelService = function (shipping_id) {
        var data = JSON.stringify({
            key: "3edcdb20e0030daab21d0ba9af4c0dc2",
            shipping_id: shipping_id,
            url: this._ConstantUbeep.urlServer
        });
        // console.log(data);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ "Access-Control-Allow-Origin": "*" });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({ headers: headers, method: "POST" });
        return this._http.post(this._urlCancelService, data, options)
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    HttpSolicitarService.prototype.handleError = function (error) {
        // console.log( JSON.stringify(error) );
        // console.error(error);
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json().error || ' error');
    };
    HttpSolicitarService.prototype.getCredit = function (clientId) {
        var data = JSON.stringify({
            key: "3edcdb20e0030daab21d0ba9af4c0dc2",
            clientId: clientId,
            url: this._ConstantUbeep.urlServer
        });
        // console.log(data);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ "Access-Control-Allow-Origin": "*" });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({ headers: headers, method: "POST" });
        // console.log(this);
        return this._http.post(this._urlGetCredit, data, options)
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    HttpSolicitarService.prototype.getLocation = function (logisticresource_id) {
        // console.log(logisticresource_id);
        var data = {
            key: "3edcdb20e0030daab21d0ba9af4c0dc2",
            logisticresource_id: logisticresource_id,
            platform: "web",
            url: this._ConstantUbeep.urlServer
        };
        // console.log("Datos del GETLOCATION ", data);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ 'Access-Control-Allow-Origin': '*' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({ headers: headers, method: 'POST' });
        return this._http.post(this._urlGetLocation, data, options)
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    HttpSolicitarService.prototype.setTimeoutServiceTokenApp = function () {
        __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* AppComponent */].prototype.setTimeoutServiceToken();
    };
    HttpSolicitarService.prototype.paymentGateWayTokenize = function (data) {
        data.key = '3edcdb20e0030daab21d0ba9af4c0dc2';
        data.url = this._ConstantUbeep.urlServer;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ 'Access-Control-Allow-Origin': '*' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({ headers: headers, method: 'POST' });
        console.log(data);
        return this._http.post(this._urlPaymentTokenize, data, options)
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    HttpSolicitarService.prototype.paymentGateWayPost = function (data) {
        data.key = '3edcdb20e0030daab21d0ba9af4c0dc2';
        data.url = this._ConstantUbeep.urlServer;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ 'Access-Control-Allow-Origin': '*' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({ headers: headers, method: 'POST' });
        console.log(data);
        return this._http.post(this._urlPaymentPost, data, options)
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    return HttpSolicitarService;
}());
HttpSolicitarService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_6__constantsUbeep__["a" /* ConstantUbeep */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__constantsUbeep__["a" /* ConstantUbeep */]) === "function" && _b || Object])
], HttpSolicitarService);

var _a, _b;
//# sourceMappingURL=services.service.js.map

/***/ }),

/***/ 340:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "soyConductor.6c2f5e8d477089b34967.png";

/***/ }),

/***/ 341:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "soyConductorSelected.32bbaf153305e4437dbc.png";

/***/ }),

/***/ 342:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "tengoUnVehiculo.d322ec8e83253003c7b7.png";

/***/ }),

/***/ 343:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "tengoUnaFlotaDeVehiculos.d9039a37cf29faaa2f00.png";

/***/ }),

/***/ 344:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "tengoUnaFlotaDeVehiculosSelected.71671767c542566d84ac.png";

/***/ }),

/***/ 345:
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = 345;


/***/ }),

/***/ 346:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__(350);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__(357);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__(403);




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 351:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__agreement_service__ = __webpack_require__(353);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__agreement_form__ = __webpack_require__(352);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__constantsUbeep__ = __webpack_require__(13);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AgreementComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AgreementComponent = (function () {
    function AgreementComponent(_viewContainerRef, _compiler, _routerModule, _httpService, _ConstantUbeep) {
        this._viewContainerRef = _viewContainerRef;
        this._compiler = _compiler;
        this._routerModule = _routerModule;
        this._httpService = _httpService;
        this._ConstantUbeep = _ConstantUbeep;
        this.loadingProcess = false;
        this.responseMessage = '';
        this.dataAgreement = new __WEBPACK_IMPORTED_MODULE_3__agreement_form__["a" /* Agreement */]('', '', '', '', '', '', '');
        this.statusForm = false;
        this.viewContainerRef = _viewContainerRef;
        this._compiler.clearCache();
    }
    AgreementComponent.prototype.ngOnInit = function () {
        var myDiv = document.getElementById('container-agreement');
        // console.log(myDiv);
        scrollTo(document.body, myDiv.offsetTop, 10);
        function scrollTo(element, to, duration) {
            if (duration < 0)
                return;
            var difference = to - element.scrollTop;
            var perTick = difference / duration * 2;
            setTimeout(function () {
                element.scrollTop = element.scrollTop + perTick;
                scrollTo(element, to, duration - 2);
            }, 10);
        }
    };
    AgreementComponent.prototype.sendEmail = function (data) {
        var _this = this;
        // console.log(data.value);
        var validName = false;
        var validLastname = false;
        var validEmail = false;
        var validCompany = false;
        var validRol = false;
        var validCompanySize = false;
        var validPhone = false;
        if (data.value.name.toString().trim().length > 0) {
            validName = true;
            document.getElementById("status1").setAttribute("style", "display: none !important");
            document.getElementById("name").className = "form-control";
        }
        else {
            validName = false;
            document.getElementById("status1").setAttribute("style", "display: block !important");
            var d = document.getElementById("name");
            d.className += " inputImportant";
        }
        if (data.value.lastname.toString().trim().length > 0) {
            validLastname = true;
            document.getElementById("status2").setAttribute("style", "display: none !important");
            document.getElementById("lastname").className = "form-control";
        }
        else {
            validLastname = false;
            document.getElementById("status2").setAttribute("style", "display: block !important");
            var d = document.getElementById("lastname");
            d.className += " inputImportant";
        }
        if (data.value.email.toString().trim().length > 0) {
            validEmail = true;
            document.getElementById("status3").setAttribute("style", "display: none !important");
            document.getElementById("email").className = "form-control";
        }
        else {
            validEmail = false;
            document.getElementById("status3").setAttribute("style", "display: block !important");
            var d = document.getElementById("email");
            d.className += " inputImportant";
        }
        if (data.value.company.toString().trim().length > 0) {
            validCompany = true;
            document.getElementById("status4").setAttribute("style", "display: none !important");
            document.getElementById("company").className = "form-control";
        }
        else {
            validCompany = false;
            document.getElementById("status4").setAttribute("style", "display: block !important");
            var d = document.getElementById("company");
            d.className += " inputImportant";
        }
        if (data.value.phone.toString().trim().length > 0) {
            validPhone = true;
            document.getElementById("status7").setAttribute("style", "display: none !important");
            document.getElementById("phone").className = "form-control";
        }
        else {
            validPhone = false;
            document.getElementById("status7").setAttribute("style", "display: block !important");
            var d = document.getElementById("phone");
            d.className += " inputImportant";
        }
        if (data.value.rol.toString().trim().length > 0) {
            validRol = true;
            document.getElementById("status5").setAttribute("style", "display: none !important");
            document.getElementById("rol").className = "form-control";
        }
        else {
            validRol = false;
            document.getElementById("status5").setAttribute("style", "display: block !important");
            var d = document.getElementById("rol");
            d.className += " inputImportant";
        }
        if (data.value.companySize.toString().trim().length > 0) {
            validCompanySize = true;
            document.getElementById("status6").setAttribute("style", "display: none !important");
            document.getElementById("companySize").className = "form-control";
        }
        else {
            validCompanySize = false;
            document.getElementById("status6").setAttribute("style", "display: block !important");
            var d = document.getElementById("companySize");
            d.className += " inputImportant";
        }
        if (validName && validLastname && validEmail && validCompany && validRol && validCompanySize && validPhone) {
            this.statusForm = false;
            this.loadingProcess = true;
            this.responseMessage = "";
            // console.log(data);
            // console.log(data.value.name, data.value.lastname, data.value.email, data.value.company, data.value.phone, data.value.rol, data.value.companySize);
            this._httpService.agreement(data.value.name, data.value.lastname, data.value.email, data.value.company, data.value.phone, data.value.rol, data.value.companySize).subscribe(function (data) { return _this.postService = JSON.stringify(data); }, function (error) { return _this.responseSuccess(_this.postService, data); }, function () { return _this.responseSuccess(_this.postService, data); });
        }
        else {
            this.statusForm = true;
        }
    };
    AgreementComponent.prototype.responseSuccess = function (resp, data) {
        var dataResponse = JSON.parse(resp);
        // console.log(dataResponse);
        // console.log(data);
        var thisClass = this;
        if (dataResponse.return && dataResponse.status == 200) {
            this.responseMessage = dataResponse.message;
            this.loadingProcess = false;
            // console.log(data.value);
            data.reset();
            this.dataAgreement = new __WEBPACK_IMPORTED_MODULE_3__agreement_form__["a" /* Agreement */]("", "", "", "", "", "", "");
            setTimeout(function () {
                thisClass.responseMessage = "";
                thisClass._routerModule.navigateByUrl('home');
            }, 2000);
        }
        else {
            this.loadingProcess = false;
            this.responseMessage = "Por favor verifique los campos";
        }
    };
    AgreementComponent.prototype.changeValidateInput = function (type, data) {
        // console.log(this.statusForm);
        // console.log(data.value);
        if (this.statusForm) {
            switch (type) {
                case "name":
                    if (data.value.name.toString().trim().length > 0) {
                        document.getElementById("status1").setAttribute("style", "display: none !important");
                        document.getElementById("name").className = "form-control";
                    }
                    else {
                        document.getElementById("status1").setAttribute("style", "display: block !important");
                        var d = document.getElementById("name");
                        d.className += " inputImportant";
                    }
                    break;
                case "lastname":
                    // console.log(type);
                    if (data.value.lastname.toString().trim().length > 0) {
                        document.getElementById("status2").setAttribute("style", "display: none !important");
                        document.getElementById("lastname").className = "form-control";
                    }
                    else {
                        document.getElementById("status2").setAttribute("style", "display: block !important");
                        var d = document.getElementById("lastname");
                        d.className += " inputImportant";
                    }
                    break;
                case "email":
                    if (data.value.email.toString().trim().length > 0) {
                        document.getElementById("status3").setAttribute("style", "display: none !important");
                        document.getElementById("email").className = "form-control";
                    }
                    else {
                        document.getElementById("status3").setAttribute("style", "display: block !important");
                        var d = document.getElementById("email");
                        d.className += " inputImportant";
                    }
                    break;
                case "company":
                    if (data.value.company.toString().trim().length > 0) {
                        document.getElementById("status4").setAttribute("style", "display: none !important");
                        document.getElementById("company").className = "form-control";
                    }
                    else {
                        document.getElementById("status4").setAttribute("style", "display: block !important");
                        var d = document.getElementById("company");
                        d.className += " inputImportant";
                    }
                    break;
                case "rol":
                    if (data.value.rol.toString().trim().length > 0) {
                        document.getElementById("status5").setAttribute("style", "display: none !important");
                        document.getElementById("rol").className = "form-control";
                    }
                    else {
                        document.getElementById("status5").setAttribute("style", "display: block !important");
                        var d = document.getElementById("rol");
                        d.className += " inputImportant";
                    }
                    break;
                case "companySize":
                    if (data.value.companySize.toString().trim().length > 0) {
                        document.getElementById("status6").setAttribute("style", "display: none !important");
                        document.getElementById("companySize").className = "form-control";
                    }
                    else {
                        document.getElementById("status6").setAttribute("style", "display: block !important");
                        var d = document.getElementById("companySize");
                        d.className += " inputImportant";
                    }
                    break;
                case "phone":
                    if (data.value.phone.toString().trim().length > 0) {
                        document.getElementById("status7").setAttribute("style", "display: none !important");
                        document.getElementById("phone").className = "form-control";
                    }
                    else {
                        document.getElementById("status7").setAttribute("style", "display: block !important");
                        var d = document.getElementById("phone");
                        d.className += " inputImportant";
                    }
                    break;
                default:
                    break;
            }
        }
    };
    return AgreementComponent;
}());
AgreementComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__(504),
        styles: [__webpack_require__(458)],
        providers: [__WEBPACK_IMPORTED_MODULE_2__agreement_service__["a" /* HttpAgreement */]]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["Compiler"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["Compiler"]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__agreement_service__["a" /* HttpAgreement */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__agreement_service__["a" /* HttpAgreement */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__constantsUbeep__["a" /* ConstantUbeep */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__constantsUbeep__["a" /* ConstantUbeep */]) === "function" && _e || Object])
], AgreementComponent);

var _a, _b, _c, _d, _e;
//# sourceMappingURL=agreement.component.js.map

/***/ }),

/***/ 352:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Agreement; });
var Agreement = (function () {
    function Agreement(name, lastname, email, company, rol, companySize, phone) {
        this.name = name;
        this.lastname = lastname;
        this.email = email;
        this.company = company;
        this.rol = rol;
        this.companySize = companySize;
        this.phone = phone;
    }
    return Agreement;
}());

//# sourceMappingURL=agreement.form.js.map

/***/ }),

/***/ 353:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__constantsUbeep__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_catch__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_catch__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HttpAgreement; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var HttpAgreement = (function () {
    function HttpAgreement(_http, _ConstantUbeep) {
        this._http = _http;
        this._ConstantUbeep = _ConstantUbeep;
        this._Urlservice = "assets/services/agreement/agreement.service.php";
        // console.log(window.location);
    }
    HttpAgreement.prototype.agreement = function (name, lastname, email, company, phone, rol, companySize) {
        var data = JSON.stringify({
            key: "3edcdb20e0030daab21d0ba9af4c0dc2",
            name: name + " " + lastname,
            email: email,
            company: company,
            job: rol,
            companySize: companySize,
            phone: phone,
            class: "3",
            url: this._ConstantUbeep.urlServer
        });
        // console.log(data);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ "Access-Control-Allow-Origin": "*" });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({ headers: headers, method: "POST" });
        return this._http.post(this._Urlservice, data, options)
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    HttpAgreement.prototype.handleError = function (error) {
        // console.error(error);
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json().error || ' error');
    };
    return HttpAgreement;
}());
HttpAgreement = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__constantsUbeep__["a" /* ConstantUbeep */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__constantsUbeep__["a" /* ConstantUbeep */]) === "function" && _b || Object])
], HttpAgreement);

var _a, _b;
//# sourceMappingURL=agreement.service.js.map

/***/ }),

/***/ 354:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__alliance_service__ = __webpack_require__(356);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__alliance_form__ = __webpack_require__(355);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AllianceComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AllianceComponent = (function () {
    function AllianceComponent(_viewContainerRef, _compiler, _routerModule, _httpService) {
        this._viewContainerRef = _viewContainerRef;
        this._compiler = _compiler;
        this._routerModule = _routerModule;
        this._httpService = _httpService;
        this.loadingProcess = false;
        this.responseMessage = "";
        this.dataAlliance = new __WEBPACK_IMPORTED_MODULE_3__alliance_form__["a" /* Alliance */]("", "", "", "", "", "", "");
        this.statusForm = false;
        this.viewContainerRef = _viewContainerRef;
        this._compiler.clearCache();
    }
    AllianceComponent.prototype.ngOnInit = function () {
        var myDiv = document.getElementById('container-agreement');
        // console.log(myDiv);
        scrollTo(document.body, myDiv.offsetTop, 10);
        function scrollTo(element, to, duration) {
            if (duration < 0)
                return;
            var difference = to - element.scrollTop;
            var perTick = difference / duration * 2;
            setTimeout(function () {
                element.scrollTop = element.scrollTop + perTick;
                scrollTo(element, to, duration - 2);
            }, 10);
        }
    };
    AllianceComponent.prototype.sendEmail = function (data) {
        var _this = this;
        // console.log(data.value);
        var validName = false;
        var validLastname = false;
        var validEmail = false;
        var validCompany = false;
        var validRol = false;
        var validCompanySize = false;
        var validPhone = false;
        if (data.value.name.toString().trim().length > 0) {
            validName = true;
            document.getElementById("status1").setAttribute("style", "display: none !important");
            document.getElementById("name").className = "form-control";
        }
        else {
            validName = false;
            document.getElementById("status1").setAttribute("style", "display: block !important");
            var d = document.getElementById("name");
            d.className += " inputImportant";
        }
        if (data.value.lastname.toString().trim().length > 0) {
            validLastname = true;
            document.getElementById("status2").setAttribute("style", "display: none !important");
            document.getElementById("lastname").className = "form-control";
        }
        else {
            validLastname = false;
            document.getElementById("status2").setAttribute("style", "display: block !important");
            var d = document.getElementById("lastname");
            d.className += " inputImportant";
        }
        if (data.value.email.toString().trim().length > 0) {
            validEmail = true;
            document.getElementById("status3").setAttribute("style", "display: none !important");
            document.getElementById("email").className = "form-control";
        }
        else {
            validEmail = false;
            document.getElementById("status3").setAttribute("style", "display: block !important");
            var d = document.getElementById("email");
            d.className += " inputImportant";
        }
        if (data.value.company.toString().trim().length > 0) {
            validCompany = true;
            document.getElementById("status4").setAttribute("style", "display: none !important");
            document.getElementById("company").className = "form-control";
        }
        else {
            validCompany = false;
            document.getElementById("status4").setAttribute("style", "display: block !important");
            var d = document.getElementById("company");
            d.className += " inputImportant";
        }
        if (data.value.phone.toString().trim().length > 0) {
            validPhone = true;
            document.getElementById("status7").setAttribute("style", "display: none !important");
            document.getElementById("phone").className = "form-control";
        }
        else {
            validPhone = false;
            document.getElementById("status7").setAttribute("style", "display: block !important");
            var d = document.getElementById("phone");
            d.className += " inputImportant";
        }
        if (data.value.rol.toString().trim().length > 0) {
            validRol = true;
            document.getElementById("status5").setAttribute("style", "display: none !important");
            document.getElementById("rol").className = "form-control";
        }
        else {
            validRol = false;
            document.getElementById("status5").setAttribute("style", "display: block !important");
            var d = document.getElementById("rol");
            d.className += " inputImportant";
        }
        if (data.value.companySize.toString().trim().length > 0) {
            validCompanySize = true;
            document.getElementById("status6").setAttribute("style", "display: none !important");
            document.getElementById("companySize").className = "form-control";
        }
        else {
            validCompanySize = false;
            document.getElementById("status6").setAttribute("style", "display: block !important");
            var d = document.getElementById("companySize");
            d.className += " inputImportant";
        }
        if (validName && validLastname && validEmail && validCompany && validRol && validCompanySize && validPhone) {
            this.statusForm = false;
            this.loadingProcess = true;
            this.responseMessage = "";
            // console.log(data);
            // console.log(data.value.name, data.value.lastname, data.value.email, data.value.company, data.value.phone, data.value.rol, data.value.companySize);
            this._httpService.alliance(data.value.name, data.value.lastname, data.value.email, data.value.company, data.value.phone, data.value.rol, data.value.companySize).subscribe(function (data) { return _this.postService = JSON.stringify(data); }, function (error) { return _this.responseSuccess(_this.postService, data); }, function () { return _this.responseSuccess(_this.postService, data); });
        }
        else {
            this.statusForm = true;
        }
    };
    AllianceComponent.prototype.responseSuccess = function (resp, data) {
        var dataResponse = JSON.parse(resp);
        // console.log(dataResponse);
        // console.log(data);
        var thisClass = this;
        if (dataResponse.return && dataResponse.status == 200) {
            this.responseMessage = dataResponse.message;
            this.loadingProcess = false;
            data.reset();
            this.dataAlliance = new __WEBPACK_IMPORTED_MODULE_3__alliance_form__["a" /* Alliance */]("", "", "", "", "", "", "");
            setTimeout(function () {
                thisClass.responseMessage = "";
                thisClass._routerModule.navigateByUrl('home');
            }, 2000);
        }
        else {
            this.loadingProcess = false;
            this.responseMessage = "Por favor verifique los campos";
        }
    };
    AllianceComponent.prototype.changeValidateInput = function (type, data) {
        // console.log(this.statusForm);
        // console.log(data.value);
        if (this.statusForm) {
            switch (type) {
                case "name":
                    if (data.value.name.toString().trim().length > 0) {
                        document.getElementById("status1").setAttribute("style", "display: none !important");
                        document.getElementById("name").className = "form-control";
                    }
                    else {
                        document.getElementById("status1").setAttribute("style", "display: block !important");
                        var d = document.getElementById("name");
                        d.className += " inputImportant";
                    }
                    break;
                case "lastname":
                    // console.log(type);
                    if (data.value.lastname.toString().trim().length > 0) {
                        document.getElementById("status2").setAttribute("style", "display: none !important");
                        document.getElementById("lastname").className = "form-control";
                    }
                    else {
                        document.getElementById("status2").setAttribute("style", "display: block !important");
                        var d = document.getElementById("lastname");
                        d.className += " inputImportant";
                    }
                    break;
                case "email":
                    if (data.value.email.toString().trim().length > 0) {
                        document.getElementById("status3").setAttribute("style", "display: none !important");
                        document.getElementById("email").className = "form-control";
                    }
                    else {
                        document.getElementById("status3").setAttribute("style", "display: block !important");
                        var d = document.getElementById("email");
                        d.className += " inputImportant";
                    }
                    break;
                case "company":
                    if (data.value.company.toString().trim().length > 0) {
                        document.getElementById("status4").setAttribute("style", "display: none !important");
                        document.getElementById("company").className = "form-control";
                    }
                    else {
                        document.getElementById("status4").setAttribute("style", "display: block !important");
                        var d = document.getElementById("company");
                        d.className += " inputImportant";
                    }
                    break;
                case "rol":
                    if (data.value.rol.toString().trim().length > 0) {
                        document.getElementById("status5").setAttribute("style", "display: none !important");
                        document.getElementById("rol").className = "form-control";
                    }
                    else {
                        document.getElementById("status5").setAttribute("style", "display: block !important");
                        var d = document.getElementById("rol");
                        d.className += " inputImportant";
                    }
                    break;
                case "companySize":
                    if (data.value.companySize.toString().trim().length > 0) {
                        document.getElementById("status6").setAttribute("style", "display: none !important");
                        document.getElementById("companySize").className = "form-control";
                    }
                    else {
                        document.getElementById("status6").setAttribute("style", "display: block !important");
                        var d = document.getElementById("companySize");
                        d.className += " inputImportant";
                    }
                    break;
                case "phone":
                    if (data.value.phone.toString().trim().length > 0) {
                        document.getElementById("status7").setAttribute("style", "display: none !important");
                        document.getElementById("phone").className = "form-control";
                    }
                    else {
                        document.getElementById("status7").setAttribute("style", "display: block !important");
                        var d = document.getElementById("phone");
                        d.className += " inputImportant";
                    }
                    break;
                default:
                    break;
            }
        }
    };
    return AllianceComponent;
}());
AllianceComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__(505),
        styles: [__webpack_require__(459)],
        providers: [__WEBPACK_IMPORTED_MODULE_2__alliance_service__["a" /* HttpAlliance */]]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["Compiler"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["Compiler"]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__alliance_service__["a" /* HttpAlliance */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__alliance_service__["a" /* HttpAlliance */]) === "function" && _d || Object])
], AllianceComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=alliance.component.js.map

/***/ }),

/***/ 355:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Alliance; });
var Alliance = (function () {
    function Alliance(name, lastname, email, company, rol, companySize, phone) {
        this.name = name;
        this.lastname = lastname;
        this.email = email;
        this.company = company;
        this.rol = rol;
        this.companySize = companySize;
        this.phone = phone;
    }
    return Alliance;
}());

//# sourceMappingURL=alliance.form.js.map

/***/ }),

/***/ 356:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__constantsUbeep__ = __webpack_require__(13);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HttpAlliance; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var HttpAlliance = (function () {
    function HttpAlliance(_http, _ConstantUbeep) {
        this._http = _http;
        this._ConstantUbeep = _ConstantUbeep;
        this._urlLogin = "assets/services/alliance/alliance.service.php";
    }
    HttpAlliance.prototype.alliance = function (name, lastname, email, company, phone, rol, companySize) {
        var data = JSON.stringify({
            key: "3edcdb20e0030daab21d0ba9af4c0dc2",
            name: name + " " + lastname,
            email: email,
            company: company,
            job: rol,
            companySize: companySize,
            phone: phone,
            class: "2",
            url: this._ConstantUbeep.urlServer
        });
        // console.log(data);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ "Access-Control-Allow-Origin": "*" });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({ headers: headers, method: "POST" });
        return this._http.post(this._urlLogin, data, options)
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    HttpAlliance.prototype.handleError = function (error) {
        // console.error(error);
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json().error || ' error');
    };
    return HttpAlliance;
}());
HttpAlliance = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_5__constantsUbeep__["a" /* ConstantUbeep */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__constantsUbeep__["a" /* ConstantUbeep */]) === "function" && _b || Object])
], HttpAlliance);

var _a, _b;
//# sourceMappingURL=alliance.service.js.map

/***/ }),

/***/ 357:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_bootstrap__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_bootstrap___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_ng2_bootstrap__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angular2_cookie_services_cookies_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angular2_cookie_services_cookies_service___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_angular2_cookie_services_cookies_service__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_router__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_common__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__constantsUbeep__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__app_component__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_services_component__ = __webpack_require__(129);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__home_home_component__ = __webpack_require__(374);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__agreement_agreement_component__ = __webpack_require__(351);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__alliance_alliance_component__ = __webpack_require__(354);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__beep_beep_component__ = __webpack_require__(358);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__company_company_component__ = __webpack_require__(359);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__contact_admin_contact_admin_component__ = __webpack_require__(362);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__corporative_corporative_component__ = __webpack_require__(365);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__faq_faq_component__ = __webpack_require__(368);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__history_history_component__ = __webpack_require__(370);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__invite_invite_component__ = __webpack_require__(377);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__press_press_component__ = __webpack_require__(382);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__privacy_policies_privacy_policies_component__ = __webpack_require__(384);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__profile_profile_component__ = __webpack_require__(385);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__register_company_register_company_component__ = __webpack_require__(387);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__security_security_component__ = __webpack_require__(392);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__security_policies_security_policies_component__ = __webpack_require__(391);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__vehicles_vehicles_component__ = __webpack_require__(399);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__payment_payment_component__ = __webpack_require__(381);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





























var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_9__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_10__services_services_component__["a" /* ServicesComponent */],
            __WEBPACK_IMPORTED_MODULE_11__home_home_component__["a" /* HomeComponent */],
            __WEBPACK_IMPORTED_MODULE_12__agreement_agreement_component__["a" /* AgreementComponent */],
            __WEBPACK_IMPORTED_MODULE_13__alliance_alliance_component__["a" /* AllianceComponent */],
            __WEBPACK_IMPORTED_MODULE_14__beep_beep_component__["a" /* BeepComponent */],
            __WEBPACK_IMPORTED_MODULE_15__company_company_component__["a" /* CompanyComponent */],
            __WEBPACK_IMPORTED_MODULE_16__contact_admin_contact_admin_component__["a" /* ContactAdminComponent */],
            __WEBPACK_IMPORTED_MODULE_17__corporative_corporative_component__["a" /* CorporativeComponent */],
            __WEBPACK_IMPORTED_MODULE_18__faq_faq_component__["a" /* FaqComponent */],
            __WEBPACK_IMPORTED_MODULE_19__history_history_component__["a" /* HistoryComponent */],
            __WEBPACK_IMPORTED_MODULE_20__invite_invite_component__["a" /* InviteComponent */],
            __WEBPACK_IMPORTED_MODULE_21__press_press_component__["a" /* PressComponent */],
            __WEBPACK_IMPORTED_MODULE_22__privacy_policies_privacy_policies_component__["a" /* PrivacyPoliciesComponent */],
            __WEBPACK_IMPORTED_MODULE_23__profile_profile_component__["a" /* ProfileComponent */],
            __WEBPACK_IMPORTED_MODULE_24__register_company_register_company_component__["a" /* RegisterCompanyComponent */],
            __WEBPACK_IMPORTED_MODULE_25__security_security_component__["a" /* SecurityComponent */],
            __WEBPACK_IMPORTED_MODULE_26__security_policies_security_policies_component__["a" /* SecurityPoliciesComponent */],
            __WEBPACK_IMPORTED_MODULE_27__vehicles_vehicles_component__["a" /* VehiclesComponent */],
            __WEBPACK_IMPORTED_MODULE_28__payment_payment_component__["a" /* PaymentComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormsModule"],
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_4_ng2_bootstrap__["Ng2BootstrapModule"],
            __WEBPACK_IMPORTED_MODULE_6__angular_router__["a" /* RouterModule */].forRoot([
                { path: '', redirectTo: '/home', pathMatch: 'full' },
                { path: 'home', component: __WEBPACK_IMPORTED_MODULE_11__home_home_component__["a" /* HomeComponent */] },
                { path: 'services', component: __WEBPACK_IMPORTED_MODULE_10__services_services_component__["a" /* ServicesComponent */] },
                { path: 'company', component: __WEBPACK_IMPORTED_MODULE_15__company_company_component__["a" /* CompanyComponent */] },
                { path: 'vehicles', component: __WEBPACK_IMPORTED_MODULE_27__vehicles_vehicles_component__["a" /* VehiclesComponent */] },
                { path: 'profile', component: __WEBPACK_IMPORTED_MODULE_23__profile_profile_component__["a" /* ProfileComponent */] },
                { path: 'agreement', component: __WEBPACK_IMPORTED_MODULE_12__agreement_agreement_component__["a" /* AgreementComponent */] },
                { path: 'alliance', component: __WEBPACK_IMPORTED_MODULE_13__alliance_alliance_component__["a" /* AllianceComponent */] },
                { path: 'history', component: __WEBPACK_IMPORTED_MODULE_19__history_history_component__["a" /* HistoryComponent */] },
                { path: 'corporative', component: __WEBPACK_IMPORTED_MODULE_17__corporative_corporative_component__["a" /* CorporativeComponent */] },
                { path: 'invite', component: __WEBPACK_IMPORTED_MODULE_20__invite_invite_component__["a" /* InviteComponent */] },
                { path: 'register-company', component: __WEBPACK_IMPORTED_MODULE_24__register_company_register_company_component__["a" /* RegisterCompanyComponent */] },
                { path: 'privacy-policies', component: __WEBPACK_IMPORTED_MODULE_22__privacy_policies_privacy_policies_component__["a" /* PrivacyPoliciesComponent */] },
                { path: 'security-policies', component: __WEBPACK_IMPORTED_MODULE_26__security_policies_security_policies_component__["a" /* SecurityPoliciesComponent */] },
                { path: 'beep', component: __WEBPACK_IMPORTED_MODULE_14__beep_beep_component__["a" /* BeepComponent */] },
                { path: 'press', component: __WEBPACK_IMPORTED_MODULE_21__press_press_component__["a" /* PressComponent */] },
                { path: 'faq', component: __WEBPACK_IMPORTED_MODULE_18__faq_faq_component__["a" /* FaqComponent */] },
                { path: 'security', component: __WEBPACK_IMPORTED_MODULE_25__security_security_component__["a" /* SecurityComponent */] },
                { path: 'contact-admin', component: __WEBPACK_IMPORTED_MODULE_16__contact_admin_contact_admin_component__["a" /* ContactAdminComponent */] },
                { path: 'payment', component: __WEBPACK_IMPORTED_MODULE_28__payment_payment_component__["a" /* PaymentComponent */] }
            ], { useHash: true }),
            __WEBPACK_IMPORTED_MODULE_4_ng2_bootstrap__["DropdownModule"].forRoot(),
            __WEBPACK_IMPORTED_MODULE_4_ng2_bootstrap__["ModalModule"].forRoot(),
            __WEBPACK_IMPORTED_MODULE_4_ng2_bootstrap__["TooltipModule"].forRoot()
        ],
        providers: [__WEBPACK_IMPORTED_MODULE_5_angular2_cookie_services_cookies_service__["CookieService"], __WEBPACK_IMPORTED_MODULE_8__constantsUbeep__["a" /* ConstantUbeep */], { provide: __WEBPACK_IMPORTED_MODULE_7__angular_common__["LocationStrategy"], useClass: __WEBPACK_IMPORTED_MODULE_7__angular_common__["HashLocationStrategy"] }],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_9__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 358:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BeepComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BeepComponent = (function () {
    function BeepComponent() {
    }
    BeepComponent.prototype.ngOnInit = function () {
        var myDiv = document.getElementById('container-beep');
        // console.log(myDiv);
        scrollTo(document.body, myDiv.offsetTop, 10);
        function scrollTo(element, to, duration) {
            if (duration < 0)
                return;
            var difference = to - element.scrollTop;
            var perTick = difference / duration * 2;
            setTimeout(function () {
                element.scrollTop = element.scrollTop + perTick;
                scrollTo(element, to, duration - 2);
            }, 10);
        }
    };
    return BeepComponent;
}());
BeepComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__(507),
        styles: [__webpack_require__(461)]
    }),
    __metadata("design:paramtypes", [])
], BeepComponent);

//# sourceMappingURL=beep.component.js.map

/***/ }),

/***/ 359:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__company_service__ = __webpack_require__(361);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__company_form__ = __webpack_require__(360);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CompanyComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CompanyComponent = (function () {
    function CompanyComponent(_httpService) {
        this._httpService = _httpService;
        this.viewInfo = true;
        this.viewForm = false;
        this.typeContact = "";
        this.responseMessage = "";
        this.loadingProcess = false;
        this.dataContact = new __WEBPACK_IMPORTED_MODULE_2__company_form__["a" /* Contact */]("", "", "", "", "", "", "");
        this.statusForm = false;
    }
    CompanyComponent.prototype.ngOnInit = function () {
        var myDiv = document.getElementById('container-drive');
        // console.log(myDiv);
        scrollTo(document.body, myDiv.offsetTop, 10);
        function scrollTo(element, to, duration) {
            if (duration < 0)
                return;
            var difference = to - element.scrollTop;
            var perTick = difference / duration * 2;
            setTimeout(function () {
                element.scrollTop = element.scrollTop + perTick;
                scrollTo(element, to, duration - 2);
            }, 10);
        }
    };
    CompanyComponent.prototype.nextView = function (view) {
        // console.log(view);
        switch (view) {
            case "form":
                this.viewInfo = true;
                this.viewForm = true;
                /*setTimeout(function() {
                    document.getElementById("viewForm").focus();
                }, 100);*/
                break;
            default:
                this.viewInfo = true;
                this.viewForm = true;
                break;
        }
    };
    CompanyComponent.prototype.submitEmailContacto = function (data) {
        var _this = this;
        var validType = false;
        var validName = false;
        var validLastname = false;
        var validPhone = false;
        var validAddress = false;
        var validEmail = false;
        var validReason = false;
        if (data.value.type.toString().trim().length > 0) {
            validType = true;
            document.getElementById("status1").setAttribute("style", "display: none !important");
        }
        else {
            validType = false;
            document.getElementById("status1").setAttribute("style", "display: block !important");
        }
        if (data.value.name.toString().trim().length > 0) {
            validName = true;
            document.getElementById("status2").setAttribute("style", "display: none !important");
            document.getElementById("name").className = "form-control";
        }
        else {
            validName = false;
            document.getElementById("status2").setAttribute("style", "display: block !important");
            var d = document.getElementById("name");
            d.className += " inputImportant";
        }
        if (data.value.lastname.toString().trim().length > 0) {
            validLastname = true;
            document.getElementById("status3").setAttribute("style", "display: none !important");
            document.getElementById("lastname").className = "form-control";
        }
        else {
            validLastname = false;
            document.getElementById("status3").setAttribute("style", "display: block !important");
            var d = document.getElementById("lastname");
            d.className += " inputImportant";
        }
        if (data.value.phone.toString().trim().length > 0) {
            validPhone = true;
            document.getElementById("status4").setAttribute("style", "display: none !important");
            document.getElementById("phone").className = "form-control";
        }
        else {
            validPhone = false;
            document.getElementById("status4").setAttribute("style", "display: block !important");
            var d = document.getElementById("phone");
            d.className += " inputImportant";
        }
        if (data.value.address.toString().trim().length > 0) {
            validAddress = true;
            document.getElementById("status5").setAttribute("style", "display: none !important");
            document.getElementById("address").className = "form-control";
        }
        else {
            validAddress = false;
            document.getElementById("status5").setAttribute("style", "display: block !important");
            var d = document.getElementById("address");
            d.className += " inputImportant";
        }
        if (data.value.email.toString().trim().length > 0) {
            validEmail = true;
            document.getElementById("status6").setAttribute("style", "display: none !important");
            document.getElementById("email").className = "form-control";
        }
        else {
            validEmail = false;
            document.getElementById("status6").setAttribute("style", "display: block !important");
            var d = document.getElementById("email");
            d.className += " inputImportant";
        }
        if (data.value.reason.toString().trim().length > 0) {
            validReason = true;
            document.getElementById("status7").setAttribute("style", "display: none !important");
            document.getElementById("reason").className = "form-control";
        }
        else {
            validReason = false;
            document.getElementById("status7").setAttribute("style", "display: block !important");
            var d = document.getElementById("reason");
            d.className += " inputImportant";
        }
        if (validType && validName && validLastname && validPhone && validAddress && validEmail && validReason) {
            this.statusForm = false;
            this.loadingProcess = true;
            this.responseMessage = "";
            // console.log(data);
            // console.log(this.typeContact);
            this._httpService.contact(data.value.name, data.value.lastname, data.value.email, data.value.phone, data.value.address, data.value.reason, data.value.type).subscribe(function (data) { return _this.postService = JSON.stringify(data); }, function (error) { return _this.correoEnviado(_this.postService, data); }, function () { return _this.correoEnviado(_this.postService, data); });
        }
        else {
            this.statusForm = true;
        }
    };
    CompanyComponent.prototype.changeType = function (type) {
        if (type == "Tengo un vehículo") {
            document.getElementById("img-vehicle").setAttribute("style", "background-image: url('./assets/img/tengoUnVehiculoSelected.png');");
            document.getElementById("img-vehicles").setAttribute("style", "background-image: url('./assets/img/tengoUnaFlotaDeVehiculos.png');");
            document.getElementById("img-drive").setAttribute("style", "background-image: url('./assets/img/soyConductor.png');");
        }
        if (type == "Tengo una flota de vehículos") {
            document.getElementById("img-vehicle").setAttribute("style", "background-image: url('./assets/img/tengoUnVehiculo.png');");
            document.getElementById("img-vehicles").setAttribute("style", "background-image: url('./assets/img/tengoUnaFlotaDeVehiculosSelected.png');");
            document.getElementById("img-drive").setAttribute("style", "background-image: url('./assets/img/soyConductor.png');");
        }
        if (type == "Soy conductor") {
            document.getElementById("img-vehicle").setAttribute("style", "background-image: url('./assets/img/tengoUnVehiculo.png');");
            document.getElementById("img-vehicles").setAttribute("style", "background-image: url('./assets/img/tengoUnaFlotaDeVehiculos.png');");
            document.getElementById("img-drive").setAttribute("style", "background-image: url('./assets/img/soyConductorSelected.png');");
        }
        document.getElementById("status1").setAttribute("style", "display: none !important");
        this.dataContact.type = type;
        this.typeContact = type;
    };
    CompanyComponent.prototype.correoEnviado = function (data, f) {
        var dataResponse = JSON.parse(data);
        // console.log(dataResponse);
        var thisClass = this;
        this.loadingProcess = false;
        if (dataResponse.return) {
            this.responseMessage = dataResponse.message;
            f.reset();
            this.dataContact = new __WEBPACK_IMPORTED_MODULE_2__company_form__["a" /* Contact */]("", "", "", "", "", "", "");
            setTimeout(function () {
                // console.log(thisClass);
                thisClass.responseMessage = "";
            }, 4000);
        }
        else {
            // console.log(thisClass);
            thisClass.responseMessage = "Verifique los campos e intente nuevamente";
        }
    };
    CompanyComponent.prototype.changeValidateInput = function (type, data) {
        // console.log(this.statusForm);
        // console.log(data.value);
        if (this.statusForm) {
            switch (type) {
                case "name":
                    if (data.value.name.toString().trim().length > 0) {
                        document.getElementById("status2").setAttribute("style", "display: none !important");
                        document.getElementById("name").className = "form-control";
                    }
                    else {
                        document.getElementById("status2").setAttribute("style", "display: block !important");
                        var d = document.getElementById("name");
                        d.className += " inputImportant";
                    }
                    break;
                case "lastname":
                    console.log(type);
                    if (data.value.lastname.toString().trim().length > 0) {
                        document.getElementById("status3").setAttribute("style", "display: none !important");
                        document.getElementById("lastname").className = "form-control";
                    }
                    else {
                        document.getElementById("status3").setAttribute("style", "display: block !important");
                        var d = document.getElementById("lastname");
                        d.className += " inputImportant";
                    }
                    break;
                case "email":
                    if (data.value.email.toString().trim().length > 0) {
                        document.getElementById("status6").setAttribute("style", "display: none !important");
                        document.getElementById("email").className = "form-control";
                    }
                    else {
                        document.getElementById("status6").setAttribute("style", "display: block !important");
                        var d = document.getElementById("email");
                        d.className += " inputImportant";
                    }
                    break;
                case "address":
                    if (data.value.address.toString().trim().length > 0) {
                        document.getElementById("status5").setAttribute("style", "display: none !important");
                        document.getElementById("address").className = "form-control";
                    }
                    else {
                        document.getElementById("status5").setAttribute("style", "display: block !important");
                        var d = document.getElementById("address");
                        d.className += " inputImportant";
                    }
                    break;
                case "reason":
                    if (data.value.reason.toString().trim().length > 0) {
                        document.getElementById("status7").setAttribute("style", "display: none !important");
                        document.getElementById("reason").className = "form-control";
                    }
                    else {
                        document.getElementById("status7").setAttribute("style", "display: block !important");
                        var d = document.getElementById("reason");
                        d.className += " inputImportant";
                    }
                    break;
                case "phone":
                    if (data.value.phone.toString().trim().length > 0) {
                        document.getElementById("status4").setAttribute("style", "display: none !important");
                        document.getElementById("phone").className = "form-control";
                    }
                    else {
                        document.getElementById("status4").setAttribute("style", "display: block !important");
                        var d = document.getElementById("phone");
                        d.className += " inputImportant";
                    }
                    break;
                default:
                    break;
            }
        }
    };
    return CompanyComponent;
}());
CompanyComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__(508),
        styles: [__webpack_require__(462)],
        providers: [__WEBPACK_IMPORTED_MODULE_1__company_service__["a" /* HttpContactar */]]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__company_service__["a" /* HttpContactar */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__company_service__["a" /* HttpContactar */]) === "function" && _a || Object])
], CompanyComponent);

var _a;
//# sourceMappingURL=company.component.js.map

/***/ }),

/***/ 360:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Contact; });
var Contact = (function () {
    function Contact(name, lastname, email, phone, address, reason, type) {
        this.name = name;
        this.lastname = lastname;
        this.email = email;
        this.phone = phone;
        this.address = address;
        this.reason = reason;
        this.type = type;
    }
    return Contact;
}());

//# sourceMappingURL=company.form.js.map

/***/ }),

/***/ 361:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__constantsUbeep__ = __webpack_require__(13);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HttpContactar; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var HttpContactar = (function () {
    function HttpContactar(_http, _ConstantUbeep) {
        this._http = _http;
        this._ConstantUbeep = _ConstantUbeep;
        this._urlLogin = "assets/services/company/contact.service.php";
    }
    HttpContactar.prototype.contact = function (name, lastname, email, phone, address, reason, type) {
        var data = JSON.stringify({
            key: "3edcdb20e0030daab21d0ba9af4c0dc2",
            name: name + "" + lastname,
            email: email,
            phone: phone,
            address: address,
            reason: reason,
            type: type,
            class: "1",
            url: this._ConstantUbeep.urlServer
        });
        // console.log(data);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ "Access-Control-Allow-Origin": "*" });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({ headers: headers, method: "POST" });
        return this._http.post(this._urlLogin, data, options)
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    HttpContactar.prototype.handleError = function (error) {
        // console.error(error);
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json().error || ' error');
    };
    return HttpContactar;
}());
HttpContactar = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_5__constantsUbeep__["a" /* ConstantUbeep */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__constantsUbeep__["a" /* ConstantUbeep */]) === "function" && _b || Object])
], HttpContactar);

var _a, _b;
//# sourceMappingURL=company.service.js.map

/***/ }),

/***/ 362:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__contact_admin_service__ = __webpack_require__(364);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contact_admin_form__ = __webpack_require__(363);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactAdminComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ContactAdminComponent = (function () {
    function ContactAdminComponent(_httpService) {
        this._httpService = _httpService;
        this.dataContact = new __WEBPACK_IMPORTED_MODULE_2__contact_admin_form__["a" /* Contact */]("", "", "", "", "");
        this.loadingProcess = false;
        this.responseMessage = "";
        this.statusForm = false;
    }
    ContactAdminComponent.prototype.ngOnInit = function () {
        var myDiv = document.getElementById('container-contact-admin');
        // console.log(myDiv);
        scrollTo(document.body, myDiv.offsetTop, 10);
        function scrollTo(element, to, duration) {
            if (duration < 0)
                return;
            var difference = to - element.scrollTop;
            var perTick = difference / duration * 2;
            setTimeout(function () {
                element.scrollTop = element.scrollTop + perTick;
                scrollTo(element, to, duration - 2);
            }, 10);
        }
    };
    ContactAdminComponent.prototype.submitEmailContact = function (data) {
        var _this = this;
        var validName = false;
        var validLastname = false;
        var validEmail = false;
        var validPhone = false;
        var validReason = false;
        if (data.value.name.toString().trim().length > 0) {
            validName = true;
            document.getElementById("status1").setAttribute("style", "display: none !important");
            document.getElementById("name").className = "form-control";
        }
        else {
            validName = false;
            document.getElementById("status1").setAttribute("style", "display: block !important");
            var d = document.getElementById("name");
            d.className += " inputImportant";
        }
        if (data.value.lastname.toString().trim().length > 0) {
            validLastname = true;
            document.getElementById("status2").setAttribute("style", "display: none !important");
            document.getElementById("lastname").className = "form-control";
        }
        else {
            validLastname = false;
            document.getElementById("status2").setAttribute("style", "display: block !important");
            var d = document.getElementById("lastname");
            d.className += " inputImportant";
        }
        if (data.value.email.toString().trim().length > 0) {
            validEmail = true;
            document.getElementById("status3").setAttribute("style", "display: none !important");
            document.getElementById("email").className = "form-control";
        }
        else {
            validEmail = false;
            document.getElementById("status3").setAttribute("style", "display: block !important");
            var d = document.getElementById("email");
            d.className += " inputImportant";
        }
        if (data.value.phone.toString().trim().length > 0) {
            validPhone = true;
            document.getElementById("status4").setAttribute("style", "display: none !important");
            document.getElementById("phone").className = "form-control";
        }
        else {
            validPhone = false;
            document.getElementById("status4").setAttribute("style", "display: block !important");
            var d = document.getElementById("phone");
            d.className += " inputImportant";
        }
        if (data.value.reason.toString().trim().length > 0) {
            validReason = true;
            document.getElementById("status5").setAttribute("style", "display: none !important");
            document.getElementById("reason").className = "form-control";
        }
        else {
            validReason = false;
            document.getElementById("status5").setAttribute("style", "display: block !important");
            var d = document.getElementById("reason");
            d.className += " inputImportant";
        }
        if (validName && validLastname && validEmail && validPhone && validReason) {
            this.statusForm = false;
            this.loadingProcess = true;
            this.responseMessage = "";
            // console.log(data.value);
            this._httpService.contact(data.value.name, data.value.lastname, data.value.email, data.value.phone, data.value.reason).subscribe(function (data) { return _this.strResponseService = JSON.stringify(data); }, function (error) { return _this.responseService(_this.strResponseService, data); }, function () { return _this.responseService(_this.strResponseService, data); });
        }
        else {
            this.statusForm = true;
        }
    };
    ContactAdminComponent.prototype.responseService = function (data, f) {
        var dataResponse = JSON.parse(data);
        // console.log(dataResponse);
        var thisClass = this;
        this.loadingProcess = false;
        if (dataResponse.return) {
            this.responseMessage = dataResponse.message;
            f.reset();
            this.dataContact = new __WEBPACK_IMPORTED_MODULE_2__contact_admin_form__["a" /* Contact */]("", "", "", "", "");
            setTimeout(function () {
                // console.log(thisClass);
                thisClass.responseMessage = "";
            }, 4000);
        }
        else {
            // console.log(thisClass);
            thisClass.responseMessage = "Verifique los campos e intente nuevamente";
        }
    };
    ContactAdminComponent.prototype.changeValidateInput = function (type, data) {
        // console.log(this.statusForm);
        // console.log(data.value);
        if (this.statusForm) {
            switch (type) {
                case "name":
                    if (data.value.name.toString().trim().length > 0) {
                        document.getElementById("status1").setAttribute("style", "display: none !important");
                        document.getElementById("name").className = "form-control";
                    }
                    else {
                        document.getElementById("status1").setAttribute("style", "display: block !important");
                        var d = document.getElementById("name");
                        d.className += " inputImportant";
                    }
                    break;
                case "lastname":
                    // console.log(type);
                    if (data.value.lastname.toString().trim().length > 0) {
                        document.getElementById("status2").setAttribute("style", "display: none !important");
                        document.getElementById("lastname").className = "form-control";
                    }
                    else {
                        document.getElementById("status2").setAttribute("style", "display: block !important");
                        var d = document.getElementById("lastname");
                        d.className += " inputImportant";
                    }
                    break;
                case "email":
                    if (data.value.email.toString().trim().length > 0) {
                        document.getElementById("status3").setAttribute("style", "display: none !important");
                        document.getElementById("email").className = "form-control";
                    }
                    else {
                        document.getElementById("status3").setAttribute("style", "display: block !important");
                        var d = document.getElementById("email");
                        d.className += " inputImportant";
                    }
                    break;
                case "reason":
                    if (data.value.reason.toString().trim().length > 0) {
                        document.getElementById("status5").setAttribute("style", "display: none !important");
                        document.getElementById("reason").className = "form-control";
                    }
                    else {
                        document.getElementById("status5").setAttribute("style", "display: block !important");
                        var d = document.getElementById("reason");
                        d.className += " inputImportant";
                    }
                    break;
                case "phone":
                    if (data.value.phone.toString().trim().length > 0) {
                        document.getElementById("status4").setAttribute("style", "display: none !important");
                        document.getElementById("phone").className = "form-control";
                    }
                    else {
                        document.getElementById("status4").setAttribute("style", "display: block !important");
                        var d = document.getElementById("phone");
                        d.className += " inputImportant";
                    }
                    break;
                default:
                    break;
            }
        }
    };
    return ContactAdminComponent;
}());
ContactAdminComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__(509),
        styles: [__webpack_require__(463)],
        providers: [__WEBPACK_IMPORTED_MODULE_1__contact_admin_service__["a" /* HttpContactar */]]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__contact_admin_service__["a" /* HttpContactar */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__contact_admin_service__["a" /* HttpContactar */]) === "function" && _a || Object])
], ContactAdminComponent);

var _a;
//# sourceMappingURL=contact-admin.component.js.map

/***/ }),

/***/ 363:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Contact; });
var Contact = (function () {
    function Contact(name, lastname, email, phone, reason) {
        this.name = name;
        this.lastname = lastname;
        this.email = email;
        this.phone = phone;
        this.reason = reason;
    }
    return Contact;
}());

//# sourceMappingURL=contact-admin.form.js.map

/***/ }),

/***/ 364:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__constantsUbeep__ = __webpack_require__(13);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HttpContactar; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var HttpContactar = (function () {
    function HttpContactar(_http, _ConstantUbeep) {
        this._http = _http;
        this._ConstantUbeep = _ConstantUbeep;
        this._urlLogin = "assets/services/contact-admin/contact-admin.service.php";
    }
    HttpContactar.prototype.contact = function (name, lastname, email, phone, reason) {
        var data = JSON.stringify({
            key: "3edcdb20e0030daab21d0ba9af4c0dc2",
            name: name + "" + lastname,
            email: email,
            phone: phone,
            reason: reason,
            class: "6",
            url: this._ConstantUbeep.urlServer
        });
        // console.log(data);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ "Access-Control-Allow-Origin": "*" });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({ headers: headers, method: "POST" });
        return this._http.post(this._urlLogin, data, options)
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    HttpContactar.prototype.handleError = function (error) {
        // console.error(error);
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json().error || ' error');
    };
    return HttpContactar;
}());
HttpContactar = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_5__constantsUbeep__["a" /* ConstantUbeep */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__constantsUbeep__["a" /* ConstantUbeep */]) === "function" && _b || Object])
], HttpContactar);

var _a, _b;
//# sourceMappingURL=contact-admin.service.js.map

/***/ }),

/***/ 365:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angular2_cookie_services_cookies_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angular2_cookie_services_cookies_service___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_angular2_cookie_services_cookies_service__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__corporative_service__ = __webpack_require__(366);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_services_service__ = __webpack_require__(130);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CorporativeComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CorporativeComponent = (function () {
    function CorporativeComponent(_viewContainerRef, _compiler, _routerModule, _cookieService, _httpHistory, _httpService) {
        this._viewContainerRef = _viewContainerRef;
        this._compiler = _compiler;
        this._routerModule = _routerModule;
        this._cookieService = _cookieService;
        this._httpHistory = _httpHistory;
        this._httpService = _httpService;
        this.objHistory = [];
        this.dataService = {};
        this.activeHistory = null;
        this.load = true;
        this.valueCredit = null;
        this.viewContainerRef = _viewContainerRef;
        this._compiler.clearCache();
    }
    CorporativeComponent.prototype.ngOnInit = function () {
        var _this = this;
        var myDiv = document.getElementById('container-history');
        // console.log(myDiv);
        scrollTo(document.body, myDiv.offsetTop, 10);
        function scrollTo(element, to, duration) {
            if (duration < 0)
                return;
            var difference = to - element.scrollTop;
            var perTick = difference / duration * 2;
            setTimeout(function () {
                element.scrollTop = element.scrollTop + perTick;
                scrollTo(element, to, duration - 2);
            }, 10);
        }
        this._httpService.getCredit(this._cookieService.get("id")).subscribe(function (data) { return _this.responseGetCreditString = JSON.stringify(data); }, function (error) { return _this.responseGetCredit(_this.responseGetCreditString); }, function () { return _this.responseGetCredit(_this.responseGetCreditString); });
    };
    CorporativeComponent.prototype.getHistorory = function () {
        var _this = this;
        // console.log("getHistorory");
        this._httpHistory.getHistorys(this._cookieService.get('id'), "2").subscribe(function (data) { return _this.postServiceHistorys = JSON.stringify(data); }, function (error) { return _this.responseHistorys(_this.postServiceHistorys); }, function () { return _this.responseHistorys(_this.postServiceHistorys); });
    };
    CorporativeComponent.prototype.responseHistorys = function (data) {
        // console.log(data);
        var dataResponse = JSON.parse(data);
        // console.log(dataResponse);
        var cont = 0;
        var thisClass = this;
        this.load = false;
        if (dataResponse.return) {
            if (dataResponse.dataToday.length > 0) {
                dataResponse.dataToday.forEach(function (d, i) {
                    if (i == 0) {
                        thisClass.getHistoryData(d.id, 0);
                    }
                    thisClass.objHistory.push(d);
                    if (cont == i) {
                        if (dataResponse.dataPrevious.length > 0) {
                            dataResponse.dataPrevious.forEach(function (dp, ip) {
                                thisClass.objHistory.push(dp);
                            });
                        }
                    }
                });
            }
            else {
                if (dataResponse.dataPrevious.length > 0) {
                    dataResponse.dataPrevious.forEach(function (dp, ip) {
                        if (ip == 0) {
                            thisClass.getHistoryData(dp.id, 0);
                        }
                        thisClass.objHistory.push(dp);
                    });
                }
            }
            // console.log("ok");
            //this.objHistory = dataResponse.dataPrevious;
        }
        else {
            // console.log("no ok");
        }
    };
    CorporativeComponent.prototype.getHistoryData = function (id, index) {
        var _this = this;
        this.activeHistory = index;
        // console.log(event);
        // console.log(id);
        this._httpHistory.getHistory(id).subscribe(function (data) { return _this.postServiceHistory = JSON.stringify(data); }, function (error) { return _this.responseHistory(_this.postServiceHistory); }, function () { return _this.responseHistory(_this.postServiceHistory); });
    };
    CorporativeComponent.prototype.responseHistory = function (data) {
        // console.log(data);
        var dataResponse = JSON.parse(data);
        // console.log(dataResponse);
        if (dataResponse.return) {
            // console.log("ok");
            var rating = parseInt(dataResponse.data.rating);
            dataResponse.data.ratingImg = rating;
            this.dataService = dataResponse.data;
        }
        else {
            // console.log("no ok");
            this.dataService = {};
        }
    };
    CorporativeComponent.prototype.responseGetCredit = function (data) {
        this.getHistorory();
        // console.log(data);
        var dataResponseGetCredit = JSON.parse(data);
        // console.log(dataResponseGetCredit);
        if (dataResponseGetCredit.return) {
            var creditValue = dataResponseGetCredit.data.credit;
            this.valueCredit = creditValue;
            this._cookieService.put("credit", creditValue);
            // console.log(this._cookieService.get("credit"));
        }
        else {
            // console.log("problemas al actualizar el credito");
            var creditValue = this._cookieService.get("credit");
            this.valueCredit = creditValue;
        }
    };
    return CorporativeComponent;
}());
CorporativeComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__(510),
        styles: [__webpack_require__(464)],
        providers: [__WEBPACK_IMPORTED_MODULE_3__corporative_service__["a" /* HttpHistoryCorporative */], __WEBPACK_IMPORTED_MODULE_4__services_services_service__["a" /* HttpSolicitarService */]]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["Compiler"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["Compiler"]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2_angular2_cookie_services_cookies_service__["CookieService"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_angular2_cookie_services_cookies_service__["CookieService"]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_3__corporative_service__["a" /* HttpHistoryCorporative */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__corporative_service__["a" /* HttpHistoryCorporative */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_4__services_services_service__["a" /* HttpSolicitarService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_services_service__["a" /* HttpSolicitarService */]) === "function" && _f || Object])
], CorporativeComponent);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=corporative.component.js.map

/***/ }),

/***/ 366:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__constantsUbeep__ = __webpack_require__(13);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HttpHistoryCorporative; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var HttpHistoryCorporative = (function () {
    function HttpHistoryCorporative(_http, _ConstantUbeep) {
        this._http = _http;
        this._ConstantUbeep = _ConstantUbeep;
        this._urlHistorys = "assets/services/corporative/historys.service.php";
        this._urlHistory = "assets/services/corporative/history.service.php";
    }
    HttpHistoryCorporative.prototype.getHistorys = function (clientId, type) {
        var data = JSON.stringify({
            key: "3edcdb20e0030daab21d0ba9af4c0dc2",
            clientId: clientId,
            type: type,
            url: this._ConstantUbeep.urlServer
        });
        // console.log(data);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ "Access-Control-Allow-Origin": "*" });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({ headers: headers, method: "POST" });
        return this._http.post(this._urlHistorys, data, options)
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    HttpHistoryCorporative.prototype.getHistory = function (shippingId) {
        var data = JSON.stringify({
            key: "3edcdb20e0030daab21d0ba9af4c0dc2",
            shippingId: shippingId,
            url: this._ConstantUbeep.urlServer
        });
        // console.log(data);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ "Access-Control-Allow-Origin": "*" });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({ headers: headers, method: "POST" });
        return this._http.post(this._urlHistory, data, options)
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    HttpHistoryCorporative.prototype.handleError = function (error) {
        // console.error(error);
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json().error || ' error');
    };
    return HttpHistoryCorporative;
}());
HttpHistoryCorporative = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_5__constantsUbeep__["a" /* ConstantUbeep */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__constantsUbeep__["a" /* ConstantUbeep */]) === "function" && _b || Object])
], HttpHistoryCorporative);

var _a, _b;
//# sourceMappingURL=corporative.service.js.map

/***/ }),

/***/ 367:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DataUser; });
var DataUser = (function () {
    function DataUser(phone, identify) {
        this.phone = phone;
        this.identify = identify;
    }
    return DataUser;
}());

//# sourceMappingURL=data.user.js.map

/***/ }),

/***/ 368:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FaqComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FaqComponent = (function () {
    function FaqComponent() {
    }
    FaqComponent.prototype.ngOnInit = function () {
        var myDiv = document.getElementById('container-faq');
        // console.log(myDiv);
        scrollTo(document.body, myDiv.offsetTop, 10);
        function scrollTo(element, to, duration) {
            if (duration < 0)
                return;
            var difference = to - element.scrollTop;
            var perTick = difference / duration * 2;
            setTimeout(function () {
                element.scrollTop = element.scrollTop + perTick;
                scrollTo(element, to, duration - 2);
            }, 10);
        }
    };
    return FaqComponent;
}());
FaqComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__(511),
        styles: [__webpack_require__(465)]
    }),
    __metadata("design:paramtypes", [])
], FaqComponent);

//# sourceMappingURL=faq.component.js.map

/***/ }),

/***/ 369:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Forgot; });
var Forgot = (function () {
    function Forgot(email) {
        this.email = email;
    }
    return Forgot;
}());

//# sourceMappingURL=forgot.js.map

/***/ }),

/***/ 370:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angular2_cookie_services_cookies_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angular2_cookie_services_cookies_service___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_angular2_cookie_services_cookies_service__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__history_service__ = __webpack_require__(371);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HistoryComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HistoryComponent = (function () {
    function HistoryComponent(_viewContainerRef, _compiler, _routerModule, _cookieService, _httpHistory) {
        this._viewContainerRef = _viewContainerRef;
        this._compiler = _compiler;
        this._routerModule = _routerModule;
        this._cookieService = _cookieService;
        this._httpHistory = _httpHistory;
        this.objHistory = [];
        this.dataService = {};
        this.activeHistory = null;
        this.load = true;
        this.viewContainerRef = _viewContainerRef;
        this._compiler.clearCache();
    }
    HistoryComponent.prototype.ngOnInit = function () {
        var myDiv = document.getElementById('container-history');
        // console.log(myDiv);
        scrollTo(document.body, myDiv.offsetTop, 10);
        function scrollTo(element, to, duration) {
            if (duration < 0)
                return;
            var difference = to - element.scrollTop;
            var perTick = difference / duration * 2;
            setTimeout(function () {
                element.scrollTop = element.scrollTop + perTick;
                scrollTo(element, to, duration - 2);
            }, 10);
        }
        this.getHistorory();
    };
    HistoryComponent.prototype.getHistorory = function () {
        // console.log("getHistorory");
        var _this = this;
        this._httpHistory.getHistorys(this._cookieService.get('id'), "1").subscribe(function (data) { return _this.postServiceHistorys = JSON.stringify(data); }, function (error) { return _this.responseHistorys(_this.postServiceHistorys); }, function () { return _this.responseHistorys(_this.postServiceHistorys); });
    };
    HistoryComponent.prototype.responseHistorys = function (data) {
        // console.log(data);
        var dataResponse = JSON.parse(data);
        // console.log(dataResponse);
        var cont = 0;
        var thisClass = this;
        this.load = false;
        if (dataResponse.return) {
            if (dataResponse.dataToday.length > 0) {
                dataResponse.dataToday.forEach(function (d, i) {
                    if (i == 0) {
                        thisClass.getHistoryData(d.id, 0);
                    }
                    thisClass.objHistory.push(d);
                    if (cont == i) {
                        if (dataResponse.dataPrevious.length > 0) {
                            dataResponse.dataPrevious.forEach(function (dp, ip) {
                                thisClass.objHistory.push(dp);
                            });
                        }
                    }
                });
            }
            else {
                if (dataResponse.dataPrevious.length > 0) {
                    dataResponse.dataPrevious.forEach(function (dp, ip) {
                        if (ip == 0) {
                            thisClass.getHistoryData(dp.id, 0);
                        }
                        thisClass.objHistory.push(dp);
                    });
                }
            }
            // console.log("ok");
            //this.objHistory = dataResponse.dataPrevious;
        }
        else {
            // console.log("no ok");
        }
    };
    HistoryComponent.prototype.getHistoryData = function (id, index) {
        var _this = this;
        this.activeHistory = index;
        // console.log(event);
        // console.log(id);
        this._httpHistory.getHistory(id).subscribe(function (data) { return _this.postServiceHistory = JSON.stringify(data); }, function (error) { return _this.responseHistory(_this.postServiceHistory); }, function () { return _this.responseHistory(_this.postServiceHistory); });
    };
    HistoryComponent.prototype.responseHistory = function (data) {
        // console.log(data);
        var dataResponse = JSON.parse(data);
        // console.log(dataResponse);
        if (dataResponse.return) {
            // console.log("ok");
            var rating = parseInt(dataResponse.data.rating);
            dataResponse.data.ratingImg = rating;
            this.dataService = dataResponse.data;
        }
        else {
            // console.log("no ok");
            this.dataService = {};
        }
    };
    return HistoryComponent;
}());
HistoryComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__(512),
        styles: [__webpack_require__(466)],
        providers: [__WEBPACK_IMPORTED_MODULE_3__history_service__["a" /* HttpHistory */]]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["Compiler"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["Compiler"]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2_angular2_cookie_services_cookies_service__["CookieService"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_angular2_cookie_services_cookies_service__["CookieService"]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_3__history_service__["a" /* HttpHistory */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__history_service__["a" /* HttpHistory */]) === "function" && _e || Object])
], HistoryComponent);

var _a, _b, _c, _d, _e;
//# sourceMappingURL=history.component.js.map

/***/ }),

/***/ 371:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__constantsUbeep__ = __webpack_require__(13);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HttpHistory; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var HttpHistory = (function () {
    function HttpHistory(_http, _ConstantUbeep) {
        this._http = _http;
        this._ConstantUbeep = _ConstantUbeep;
        this._urlHistorys = "assets/services/history/historys.service.php";
        this._urlHistory = "assets/services/history/history.service.php";
    }
    HttpHistory.prototype.getHistorys = function (clientId, type) {
        var data = JSON.stringify({
            key: "3edcdb20e0030daab21d0ba9af4c0dc2",
            clientId: clientId,
            type: type,
            url: this._ConstantUbeep.urlServer
        });
        // console.log(data);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ "Access-Control-Allow-Origin": "*" });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({ headers: headers, method: "POST" });
        return this._http.post(this._urlHistorys, data, options)
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    HttpHistory.prototype.getHistory = function (shippingId) {
        var data = JSON.stringify({
            key: "3edcdb20e0030daab21d0ba9af4c0dc2",
            shippingId: shippingId,
            url: this._ConstantUbeep.urlServer
        });
        // console.log(data);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ "Access-Control-Allow-Origin": "*" });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({ headers: headers, method: "POST" });
        return this._http.post(this._urlHistory, data, options)
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    HttpHistory.prototype.handleError = function (error) {
        // console.error(error);
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json().error || ' error');
    };
    return HttpHistory;
}());
HttpHistory = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_5__constantsUbeep__["a" /* ConstantUbeep */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__constantsUbeep__["a" /* ConstantUbeep */]) === "function" && _b || Object])
], HttpHistory);

var _a, _b;
//# sourceMappingURL=history.service.js.map

/***/ }),

/***/ 372:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__constantsUbeep__ = __webpack_require__(13);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HttpContactar; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var HttpContactar = (function () {
    function HttpContactar(_http, _ConstantUbeep) {
        this._http = _http;
        this._ConstantUbeep = _ConstantUbeep;
        this._urlLogin = "assets/services/home/contact.service.php";
    }
    HttpContactar.prototype.cotizarServicio = function (name, lastname, email, phone, address, reason, type) {
        var data = JSON.stringify({
            key: "3edcdb20e0030daab21d0ba9af4c0dc2",
            name: name + "" + lastname,
            email: email,
            phone: phone,
            address: address,
            reason: reason,
            type: type,
            class: "1",
            url: this._ConstantUbeep.urlServer
        });
        // console.log("----");
        // console.log(data);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ "Access-Control-Allow-Origin": "*" });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({ headers: headers, method: "POST" });
        return this._http.post(this._urlLogin, data, options)
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    HttpContactar.prototype.handleError = function (error) {
        // console.error(error);
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json().error || ' error');
    };
    return HttpContactar;
}());
HttpContactar = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_5__constantsUbeep__["a" /* ConstantUbeep */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__constantsUbeep__["a" /* ConstantUbeep */]) === "function" && _b || Object])
], HttpContactar);

var _a, _b;
//# sourceMappingURL=contact.service.js.map

/***/ }),

/***/ 373:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Contact; });
var Contact = (function () {
    function Contact(name, lastname, email, phone, address, reason, type) {
        this.name = name;
        this.lastname = lastname;
        this.email = email;
        this.phone = phone;
        this.address = address;
        this.reason = reason;
        this.type = type;
    }
    return Contact;
}());

//# sourceMappingURL=contact.js.map

/***/ }),

/***/ 374:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angular2_cookie_services_cookies_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angular2_cookie_services_cookies_service___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_angular2_cookie_services_cookies_service__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__contact__ = __webpack_require__(373);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__contact_service__ = __webpack_require__(372);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__password_service__ = __webpack_require__(375);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__password__ = __webpack_require__(376);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var HomeComponent = (function () {
    function HomeComponent(_viewContainerRef, _compiler, _routerModule, _AppComponent, _httpService, _httpPassword, activatedRoute, _cookieService) {
        this._viewContainerRef = _viewContainerRef;
        this._compiler = _compiler;
        this._routerModule = _routerModule;
        this._AppComponent = _AppComponent;
        this._httpService = _httpService;
        this._httpPassword = _httpPassword;
        this.activatedRoute = activatedRoute;
        this._cookieService = _cookieService;
        this.dataContact = new __WEBPACK_IMPORTED_MODULE_5__contact__["a" /* Contact */]("", "", "", "", "", "", "");
        this.loadingProcess = false;
        this.responseMessage = "";
        this.statusForm = false;
        this.formContactContainer = false;
        //---Carousel
        this.myInterval = 5000;
        this.noWrapSlides = false;
        this.slides = [];
        this.dataNewPassword = new __WEBPACK_IMPORTED_MODULE_8__password__["a" /* Password */]("", "");
        this.loadingProcessPassword = false;
        this.messageCheckPassword = "Ingresa tu nueva contraseña";
        this.messageErrorCheckPassword = null;
        this.messageSuccessCheckPassword = null;
        this.statusNewPassword = false;
        this.viewContainerRef = _viewContainerRef;
        this._compiler.clearCache();
        for (var i = 0; i < 3; i++) {
            this.addSlide();
        }
    }
    HomeComponent.prototype.ngOnInit = function () {
        var _this = this;
        var thisClass = this;
        var myDiv = document.getElementById('container-info');
        // console.log(myDiv);
        scrollTo(document.body, myDiv.offsetTop, 10);
        function scrollTo(element, to, duration) {
            if (duration < 0)
                return;
            var difference = to - element.scrollTop;
            var perTick = difference / duration * 2;
            setTimeout(function () {
                element.scrollTop = element.scrollTop + perTick;
                scrollTo(element, to, duration - 2);
            }, 10);
        }
        this.subscription = this.activatedRoute.queryParams.subscribe(function (param) {
            // console.log(param);
            if (param['token'] != undefined) {
                _this._cookieService.removeAll();
                var token = param['token'];
                _this.tokenPassword = token;
                // console.log(this.tokenPassword);
                // console.log(token);
                setTimeout(function () {
                    thisClass.modalNewPassword.show();
                }, 10);
            }
        });
    };
    HomeComponent.prototype.ngOnDestroy = function () {
        // prevent memory leak by unsubscribing
        this.subscription.unsubscribe();
    };
    HomeComponent.prototype.addSlide = function () {
        var countImg = this.slides.length + 1;
        var newWidth = 'Banner' + countImg + '.png';
        this.slides.push({
            image: "./assets/img/" + newWidth
        });
    };
    HomeComponent.prototype.showFormContact = function (statusView) {
        switch (statusView) {
            case "show":
                this.formContactContainer = true;
                document.getElementById("titile-contact").className = "col-sm-12 text-center margin-bottom-24";
                setTimeout(function () {
                    document.getElementById("name-contact").focus();
                }, 100);
                break;
            case "hide":
                document.getElementById("titile-contact").className = "col-sm-16";
                this.formContactContainer = false;
                break;
            default:
                this.showFormContact('hide');
                break;
        }
    };
    HomeComponent.prototype.clickBtnBanner = function (type) {
        if (type == "ingresar") {
            this._AppComponent.showChildModal();
        }
        else if (type == "formularioContacto") {
            this._routerModule.navigateByUrl('company');
        }
    };
    HomeComponent.prototype.submitEmailContacto = function (data) {
        var _this = this;
        var validType = false;
        var validName = false;
        var validLastname = false;
        var validPhone = false;
        var validAddress = false;
        var validEmail = false;
        var validReason = false;
        if (data.value.type.toString().trim().length > 0) {
            validType = true;
            document.getElementById("status1").setAttribute("style", "display: none !important");
            document.getElementById("type").className = "form-control";
        }
        else {
            validType = false;
            document.getElementById("status1").setAttribute("style", "display: block !important");
            var d = document.getElementById("type");
            d.className += " inputImportant";
        }
        if (data.value.name.toString().trim().length > 0) {
            validName = true;
            document.getElementById("status2").setAttribute("style", "display: none !important");
            document.getElementById("name-contact").className = "form-control";
        }
        else {
            validName = false;
            document.getElementById("status2").setAttribute("style", "display: block !important");
            var d = document.getElementById("name-contact");
            d.className += " inputImportant";
        }
        if (data.value.lastname.toString().trim().length > 0) {
            validLastname = true;
            document.getElementById("status3").setAttribute("style", "display: none !important");
            document.getElementById("lastname").className = "form-control";
        }
        else {
            validLastname = false;
            document.getElementById("status3").setAttribute("style", "display: block !important");
            var d = document.getElementById("lastname");
            d.className += " inputImportant";
        }
        if (data.value.phone.toString().trim().length > 0) {
            validPhone = true;
            document.getElementById("status4").setAttribute("style", "display: none !important");
            document.getElementById("phone").className = "form-control";
        }
        else {
            validPhone = false;
            document.getElementById("status4").setAttribute("style", "display: block !important");
            var d = document.getElementById("phone");
            d.className += " inputImportant";
        }
        if (data.value.address.toString().trim().length > 0) {
            validAddress = true;
            document.getElementById("status5").setAttribute("style", "display: none !important");
            document.getElementById("address").className = "form-control";
        }
        else {
            validAddress = false;
            document.getElementById("status5").setAttribute("style", "display: block !important");
            var d = document.getElementById("address");
            d.className += " inputImportant";
        }
        if (data.value.email.toString().trim().length > 0) {
            validEmail = true;
            document.getElementById("status6").setAttribute("style", "display: none !important");
            document.getElementById("email").className = "form-control";
        }
        else {
            validEmail = false;
            document.getElementById("status6").setAttribute("style", "display: block !important");
            var d = document.getElementById("email");
            d.className += " inputImportant";
        }
        if (data.value.reason.toString().trim().length > 0) {
            validReason = true;
            document.getElementById("status7").setAttribute("style", "display: none !important");
            document.getElementById("reason").className = "form-control";
        }
        else {
            validReason = false;
            document.getElementById("status7").setAttribute("style", "display: block !important");
            var d = document.getElementById("reason");
            d.className += " inputImportant";
        }
        if (validType && validName && validLastname && validPhone && validAddress && validEmail && validReason) {
            this.statusForm = false;
            this.loadingProcess = true;
            this.responseMessage = "";
            // console.log(data);
            this._httpService.cotizarServicio(data.value.name, data.value.lastname, data.value.email, data.value.phone, data.value.address, data.value.reason, data.value.type).subscribe(function (data) { return _this.postService = JSON.stringify(data); }, function (error) { return _this.correoEnviado(_this.postService, data); }, function () { return _this.correoEnviado(_this.postService, data); });
        }
        else {
            this.statusForm = true;
        }
    };
    HomeComponent.prototype.correoEnviado = function (data, f) {
        var dataResponse = JSON.parse(data);
        // console.log(dataResponse);
        var thisClass = this;
        this.loadingProcess = false;
        if (dataResponse.return) {
            this.responseMessage = dataResponse.message;
            f.reset();
            this.dataContact = new __WEBPACK_IMPORTED_MODULE_5__contact__["a" /* Contact */]("", "", "", "", "", "", "");
            setTimeout(function () {
                // console.log(thisClass);
                thisClass.responseMessage = "";
            }, 4000);
        }
        else {
            // console.log(thisClass);
            thisClass.responseMessage = "Verifique los campos e intente nuevamente";
        }
    };
    HomeComponent.prototype.changeValidateInput = function (type, data) {
        // console.log(this.statusForm);
        // console.log(data.value);
        if (this.statusForm) {
            switch (type) {
                case "type":
                    if (data.value.type.toString().trim().length > 0) {
                        document.getElementById("status1").setAttribute("style", "display: none !important");
                        document.getElementById("type").className = "form-control";
                    }
                    else {
                        document.getElementById("status1").setAttribute("style", "display: block !important");
                        var d = document.getElementById("type");
                        d.className += " inputImportant";
                    }
                    break;
                case "name":
                    if (data.value.name.toString().trim().length > 0) {
                        document.getElementById("status2").setAttribute("style", "display: none !important");
                        document.getElementById("name").className = "form-control";
                    }
                    else {
                        document.getElementById("status2").setAttribute("style", "display: block !important");
                        var d = document.getElementById("name");
                        d.className += " inputImportant";
                    }
                    break;
                case "lastname":
                    // console.log(type);
                    if (data.value.lastname.toString().trim().length > 0) {
                        document.getElementById("status3").setAttribute("style", "display: none !important");
                        document.getElementById("lastname").className = "form-control";
                    }
                    else {
                        document.getElementById("status3").setAttribute("style", "display: block !important");
                        var d = document.getElementById("lastname");
                        d.className += " inputImportant";
                    }
                    break;
                case "email":
                    if (data.value.email.toString().trim().length > 0) {
                        document.getElementById("status6").setAttribute("style", "display: none !important");
                        document.getElementById("email").className = "form-control";
                    }
                    else {
                        document.getElementById("status6").setAttribute("style", "display: block !important");
                        var d = document.getElementById("email");
                        d.className += " inputImportant";
                    }
                    break;
                case "address":
                    if (data.value.address.toString().trim().length > 0) {
                        document.getElementById("status5").setAttribute("style", "display: none !important");
                        document.getElementById("address").className = "form-control";
                    }
                    else {
                        document.getElementById("status5").setAttribute("style", "display: block !important");
                        var d = document.getElementById("address");
                        d.className += " inputImportant";
                    }
                    break;
                case "reason":
                    if (data.value.reason.toString().trim().length > 0) {
                        document.getElementById("status7").setAttribute("style", "display: none !important");
                        document.getElementById("reason").className = "form-control";
                    }
                    else {
                        document.getElementById("status7").setAttribute("style", "display: block !important");
                        var d = document.getElementById("reason");
                        d.className += " inputImportant";
                    }
                    break;
                case "phone":
                    if (data.value.phone.toString().trim().length > 0) {
                        document.getElementById("status4").setAttribute("style", "display: none !important");
                        document.getElementById("phone").className = "form-control";
                    }
                    else {
                        document.getElementById("status4").setAttribute("style", "display: block !important");
                        var d = document.getElementById("phone");
                        d.className += " inputImportant";
                    }
                    break;
                default:
                    break;
            }
        }
    };
    HomeComponent.prototype.changeNewPassword = function (form) {
        var _this = this;
        // console.log(form);
        if (this.statusNewPassword) {
            this.loadingProcessPassword = true;
            // console.log(this.passwordNew);
            // console.log("Muy bien!");
            this._httpPassword.updatePassword(this.tokenPassword, this.passwordNew).subscribe(function (data) { return _this.responsePassword = JSON.stringify(data); }, function (error) { return _this.responseNewPasswowrd(_this.responsePassword); }, function () { return _this.responseNewPasswowrd(_this.responsePassword); });
        }
        else {
            // console.log("Ya tiene un error declarado");
        }
    };
    HomeComponent.prototype.checkPassword = function (form) {
        if (form.value.passwordNew.toString().length < 1 && form.value.passwordConfirm.toString().length < 1) {
            this.statusNewPassword = false;
            this.messageErrorCheckPassword = "Los campos no pueden estar vacios";
        }
        else if (form.value.passwordNew != undefined && form.value.passwordConfirm != undefined) {
            var passwordNew = form.value.passwordNew.toString();
            var passwordConfirm = form.value.passwordConfirm.toString();
            if (passwordNew == passwordConfirm) {
                this.passwordNew = form.value.passwordConfirm.toString();
                // console.log(this.passwordNew);
                this.statusNewPassword = true;
                this.messageSuccessCheckPassword = "Perfecto";
                this.messageErrorCheckPassword = null;
            }
            else {
                this.statusNewPassword = false;
                this.messageErrorCheckPassword = "Las contraseñas no coinciden";
                this.messageSuccessCheckPassword = null;
            }
        }
        else {
            this.statusNewPassword = false;
        }
    };
    HomeComponent.prototype.responseNewPasswowrd = function (data) {
        var thisClass = this;
        var dataResponse = JSON.parse(data);
        // console.log(dataResponse);
        if (dataResponse.status == 200 && dataResponse.return && dataResponse.message == "Contraseña actualizada correctamente") {
            this.messageCheckPassword = dataResponse.message;
            this.messageSuccessCheckPassword = null;
            this.messageErrorCheckPassword = null;
            setTimeout(function () {
                this.loadingProcessPassword = false;
                thisClass.modalNewPassword.hide();
                thisClass._routerModule.navigateByUrl('home');
            }, 3000);
        }
        else {
            this.loadingProcessPassword = false;
            this.messageCheckPassword = dataResponse.message;
            this.messageSuccessCheckPassword = null;
            this.messageErrorCheckPassword = null;
        }
    };
    return HomeComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('modalNewPassword'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__["ModalDirective"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__["ModalDirective"]) === "function" && _a || Object)
], HomeComponent.prototype, "modalNewPassword", void 0);
HomeComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__(513),
        styles: [__webpack_require__(467)],
        providers: [__WEBPACK_IMPORTED_MODULE_6__contact_service__["a" /* HttpContactar */], __WEBPACK_IMPORTED_MODULE_7__password_service__["a" /* HttpPassword */]]
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["Compiler"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["Compiler"]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_6__contact_service__["a" /* HttpContactar */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__contact_service__["a" /* HttpContactar */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_7__password_service__["a" /* HttpPassword */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__password_service__["a" /* HttpPassword */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["c" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["c" /* ActivatedRoute */]) === "function" && _h || Object, typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_2_angular2_cookie_services_cookies_service__["CookieService"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_angular2_cookie_services_cookies_service__["CookieService"]) === "function" && _j || Object])
], HomeComponent);

var _a, _b, _c, _d, _e, _f, _g, _h, _j;
//# sourceMappingURL=home.component.js.map

/***/ }),

/***/ 375:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__constantsUbeep__ = __webpack_require__(13);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HttpPassword; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var HttpPassword = (function () {
    function HttpPassword(_http, _ConstantUbeep) {
        this._http = _http;
        this._ConstantUbeep = _ConstantUbeep;
        this._urlNewPass = "assets/services/home/password.service.php";
    }
    HttpPassword.prototype.updatePassword = function (token, pass) {
        var data = JSON.stringify({
            key: "3edcdb20e0030daab21d0ba9af4c0dc2",
            token: token,
            pass: SHA256(pass),
            url: this._ConstantUbeep.urlServer
        });
        // console.log(data);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ "Access-Control-Allow-Origin": "*" });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({ headers: headers, method: "POST" });
        return this._http.post(this._urlNewPass, data, options)
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    HttpPassword.prototype.handleError = function (error) {
        // console.error(error);
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json().error || ' error');
    };
    return HttpPassword;
}());
HttpPassword = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_5__constantsUbeep__["a" /* ConstantUbeep */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__constantsUbeep__["a" /* ConstantUbeep */]) === "function" && _b || Object])
], HttpPassword);

var _a, _b;
//# sourceMappingURL=password.service.js.map

/***/ }),

/***/ 376:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Password; });
var Password = (function () {
    function Password(passwordNew, PasswordConfirm) {
        this.passwordNew = passwordNew;
        this.PasswordConfirm = PasswordConfirm;
    }
    return Password;
}());

//# sourceMappingURL=password.js.map

/***/ }),

/***/ 377:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__invite_service__ = __webpack_require__(379);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__invite_form__ = __webpack_require__(378);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InviteComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var InviteComponent = (function () {
    function InviteComponent(_viewContainerRef, _compiler, _routerModule, _httpService) {
        this._viewContainerRef = _viewContainerRef;
        this._compiler = _compiler;
        this._routerModule = _routerModule;
        this._httpService = _httpService;
        this.loadingProcess = false;
        this.responseMessage = "";
        this.dataInvite = new __WEBPACK_IMPORTED_MODULE_3__invite_form__["a" /* Invite */]("");
        this.statusForm = false;
        this.viewContainerRef = _viewContainerRef;
        this._compiler.clearCache();
    }
    InviteComponent.prototype.ngOnInit = function () {
        var myDiv = document.getElementById('container-invite');
        // console.log(myDiv);
        scrollTo(document.body, myDiv.offsetTop, 10);
        function scrollTo(element, to, duration) {
            if (duration < 0)
                return;
            var difference = to - element.scrollTop;
            var perTick = difference / duration * 2;
            setTimeout(function () {
                element.scrollTop = element.scrollTop + perTick;
                scrollTo(element, to, duration - 2);
            }, 10);
        }
    };
    InviteComponent.prototype.sendEmail = function (data) {
        var _this = this;
        var validEmail = false;
        if (data.value.email.toString().trim().length > 0) {
            validEmail = true;
            document.getElementById("status1").setAttribute("style", "display: none !important");
            document.getElementById("email").className = "form-control";
        }
        else {
            validEmail = false;
            document.getElementById("status1").setAttribute("style", "display: block !important");
            var d = document.getElementById("email");
            d.className += " inputImportant";
        }
        if (validEmail) {
            this.statusForm = false;
            this.loadingProcess = true;
            this.responseMessage = "";
            // console.log(data);
            this._httpService.invite(data.value.email).subscribe(function (data) { return _this.postService = JSON.stringify(data); }, function (error) { return _this.responseSuccess(_this.postService, data); }, function () { return _this.responseSuccess(_this.postService, data); });
        }
        else {
            this.statusForm = true;
        }
    };
    InviteComponent.prototype.responseSuccess = function (resp, data) {
        var dataResponse = JSON.parse(resp);
        // console.log(dataResponse);
        // console.log(data);
        var thisClass = this;
        if (dataResponse.return && dataResponse.status == 200) {
            this.responseMessage = dataResponse.message;
            this.loadingProcess = false;
            data.reset();
            this.dataInvite = new __WEBPACK_IMPORTED_MODULE_3__invite_form__["a" /* Invite */]("");
            setTimeout(function () {
                thisClass.responseMessage = "";
            }, 2000);
        }
        else {
            this.loadingProcess = false;
            this.responseMessage = "Por favor verifique el correo";
        }
    };
    InviteComponent.prototype.changeValidateInput = function (type, data) {
        // console.log(this.statusForm);
        // console.log(data.value);
        if (this.statusForm) {
            switch (type) {
                case "email":
                    if (data.value.email.toString().trim().length > 0) {
                        document.getElementById("status1").setAttribute("style", "display: none !important");
                        document.getElementById("email").className = "form-control";
                    }
                    else {
                        document.getElementById("status1").setAttribute("style", "display: block !important");
                        var d = document.getElementById("email");
                        d.className += " inputImportant";
                    }
                    break;
                default:
                    break;
            }
        }
    };
    return InviteComponent;
}());
InviteComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__(514),
        styles: [__webpack_require__(468)],
        providers: [__WEBPACK_IMPORTED_MODULE_2__invite_service__["a" /* HttpInvite */]]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["Compiler"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["Compiler"]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__invite_service__["a" /* HttpInvite */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__invite_service__["a" /* HttpInvite */]) === "function" && _d || Object])
], InviteComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=invite.component.js.map

/***/ }),

/***/ 378:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Invite; });
var Invite = (function () {
    function Invite(email) {
        this.email = email;
    }
    return Invite;
}());

//# sourceMappingURL=invite.form.js.map

/***/ }),

/***/ 379:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__constantsUbeep__ = __webpack_require__(13);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HttpInvite; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var HttpInvite = (function () {
    function HttpInvite(_http, _ConstantUbeep) {
        this._http = _http;
        this._ConstantUbeep = _ConstantUbeep;
        this._urlLogin = "assets/services/invite/invite.service.php";
    }
    HttpInvite.prototype.invite = function (email) {
        var data = JSON.stringify({
            key: "3edcdb20e0030daab21d0ba9af4c0dc2",
            email: email,
            class: "4",
            url: this._ConstantUbeep.urlServer
        });
        // console.log(data);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ "Access-Control-Allow-Origin": "*" });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({ headers: headers, method: "POST" });
        return this._http.post(this._urlLogin, data, options)
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    HttpInvite.prototype.handleError = function (error) {
        // console.error(error);
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json().error || ' error');
    };
    return HttpInvite;
}());
HttpInvite = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_5__constantsUbeep__["a" /* ConstantUbeep */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__constantsUbeep__["a" /* ConstantUbeep */]) === "function" && _b || Object])
], HttpInvite);

var _a, _b;
//# sourceMappingURL=invite.service.js.map

/***/ }),

/***/ 380:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Login; });
var Login = (function () {
    function Login(email, pass) {
        this.email = email;
        this.pass = pass;
    }
    return Login;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 381:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaymentComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PaymentComponent = (function () {
    function PaymentComponent() {
    }
    PaymentComponent.prototype.ngOnInit = function () {
    };
    return PaymentComponent;
}());
PaymentComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-payment',
        template: __webpack_require__(515),
        styles: [__webpack_require__(469)]
    }),
    __metadata("design:paramtypes", [])
], PaymentComponent);

//# sourceMappingURL=payment.component.js.map

/***/ }),

/***/ 382:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__press_service__ = __webpack_require__(383);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__constantsUbeep__ = __webpack_require__(13);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PressComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PressComponent = (function () {
    function PressComponent(_kitHttp, _http, _ConstantUbeep) {
        this._kitHttp = _kitHttp;
        this._http = _http;
        this._ConstantUbeep = _ConstantUbeep;
        this.urlDownloadKit = "assets/services/press/press.service.php";
        this.urlDowloadKit = 'localhost/ubeep/app/press/services/downloadKit/service.php';
    }
    PressComponent.prototype.ngOnInit = function () {
        var myDiv = document.getElementById('container-press');
        // console.log(myDiv);
        scrollTo(document.body, myDiv.offsetTop, 10);
        function scrollTo(element, to, duration) {
            if (duration < 0)
                return;
            var difference = to - element.scrollTop;
            var perTick = difference / duration * 2;
            setTimeout(function () {
                element.scrollTop = element.scrollTop + perTick;
                scrollTo(element, to, duration - 2);
            }, 10);
        }
    };
    PressComponent.prototype.download = function () {
        var _this = this;
        this._kitHttp.downloadKit().subscribe(function (data) { return _this.responseDownloadKitString = JSON.stringify(data); }, function (error) { return _this.responseDownloadKit(); }, function () { return _this.responseDownloadKit(); });
    };
    PressComponent.prototype.responseDownloadKit = function () {
        // console.log("Download Success");
    };
    return PressComponent;
}());
PressComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__(516),
        styles: [__webpack_require__(470)],
        providers: [__WEBPACK_IMPORTED_MODULE_1__press_service__["a" /* KitHttp */]]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__press_service__["a" /* KitHttp */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__press_service__["a" /* KitHttp */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* Http */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__constantsUbeep__["a" /* ConstantUbeep */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__constantsUbeep__["a" /* ConstantUbeep */]) === "function" && _c || Object])
], PressComponent);

var _a, _b, _c;
//# sourceMappingURL=press.component.js.map

/***/ }),

/***/ 383:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return KitHttp; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var KitHttp = (function () {
    function KitHttp(_http) {
        this._http = _http;
    }
    KitHttp.prototype.downloadKit = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ "Access-Control-Allow-Origin": "*" });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({ headers: headers, method: "POST" });
        return this._http.get('http://localhost/ubeep-web/app/press/services/downloadKit/service.php')
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    KitHttp.prototype.handleError = function (error) {
        // console.error(error);
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json().error || ' error');
    };
    return KitHttp;
}());
KitHttp = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */]) === "function" && _a || Object])
], KitHttp);

var _a;
//# sourceMappingURL=press.service.js.map

/***/ }),

/***/ 384:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PrivacyPoliciesComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PrivacyPoliciesComponent = (function () {
    function PrivacyPoliciesComponent() {
    }
    PrivacyPoliciesComponent.prototype.ngOnInit = function () {
        var myDiv = document.getElementById('container-privacy-policy');
        // console.log(myDiv);
        scrollTo(document.body, myDiv.offsetTop, 10);
        function scrollTo(element, to, duration) {
            if (duration < 0)
                return;
            var difference = to - element.scrollTop;
            var perTick = difference / duration * 2;
            setTimeout(function () {
                element.scrollTop = element.scrollTop + perTick;
                scrollTo(element, to, duration - 2);
            }, 10);
        }
    };
    return PrivacyPoliciesComponent;
}());
PrivacyPoliciesComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__(517),
        styles: [__webpack_require__(471)]
    }),
    __metadata("design:paramtypes", [])
], PrivacyPoliciesComponent);

//# sourceMappingURL=privacy-policies.component.js.map

/***/ }),

/***/ 385:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angular2_cookie_services_cookies_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angular2_cookie_services_cookies_service___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_angular2_cookie_services_cookies_service__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__profile_form__ = __webpack_require__(386);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__profile_service__ = __webpack_require__(128);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__(55);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ProfileComponent = (function () {
    function ProfileComponent(_viewContainerRef, _compiler, _httpService, _cookieService, _AppComponent) {
        this._viewContainerRef = _viewContainerRef;
        this._compiler = _compiler;
        this._httpService = _httpService;
        this._cookieService = _cookieService;
        this._AppComponent = _AppComponent;
        this.dataProfile = new __WEBPACK_IMPORTED_MODULE_2__profile_form__["a" /* Profile */](this._cookieService.get('name'), this._cookieService.get('identify'), this._cookieService.get('email'), this._cookieService.get('movil_phone'), this._cookieService.get('id'));
        this.loadingProcess = false;
        this.responseMessage = "";
        this.viewContainerRef = _viewContainerRef;
        this._compiler.clearCache();
    }
    ProfileComponent.prototype.ngOnInit = function () {
        var myDiv = document.getElementById('containerProfile');
        // console.log(myDiv);
        scrollTo(document.body, myDiv.offsetTop, 10);
        function scrollTo(element, to, duration) {
            if (duration < 0)
                return;
            var difference = to - element.scrollTop;
            var perTick = difference / duration * 2;
            setTimeout(function () {
                element.scrollTop = element.scrollTop + perTick;
                scrollTo(element, to, duration - 2);
            }, 10);
        }
    };
    ProfileComponent.prototype.submitProfile = function (data) {
        var _this = this;
        // console.log(data.value);
        this.loadingProcess = true;
        this.responseMessage = "";
        // console.log(data);
        this._httpService.updateService(data.value.client_id, data.value.name, data.value.email, "", data.value.movil_phone, data.value.identify).subscribe(function (data) { return _this.postService = JSON.stringify(data); }, function (error) { return _this.responseService(_this.postService, data); }, function () { return _this.responseService(_this.postService, data); });
    };
    ProfileComponent.prototype.responseService = function (data, f) {
        var dataResponse = JSON.parse(data);
        // console.log(dataResponse);
        var thisClass = this;
        this.loadingProcess = false;
        if (dataResponse.return) {
            var nameUser = (f.value.name).split(" ");
            this._AppComponent.stringNavLogin = nameUser[0];
            this._cookieService.put('name', f.value.name);
            this._cookieService.put('email', f.value.email);
            this._cookieService.put('movil_phone', f.value.movil_phone);
            this._cookieService.put('local_phone', f.value.local_phone);
            this._cookieService.put('identify', f.value.identify);
            this.responseMessage = "Los datos se han actualizado correctamente";
            //f.reset();
            setTimeout(function () {
                // console.log(thisClass);
                thisClass.responseMessage = "";
            }, 4000);
        }
        else {
            // console.log(thisClass);
            thisClass.responseMessage = "Verifique los campos e intente nuevamente";
        }
    };
    return ProfileComponent;
}());
ProfileComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__(518),
        styles: [__webpack_require__(472)],
        providers: [__WEBPACK_IMPORTED_MODULE_3__profile_service__["a" /* HttpProfile */]]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["Compiler"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["Compiler"]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__profile_service__["a" /* HttpProfile */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__profile_service__["a" /* HttpProfile */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1_angular2_cookie_services_cookies_service__["CookieService"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_angular2_cookie_services_cookies_service__["CookieService"]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */]) === "function" && _e || Object])
], ProfileComponent);

var _a, _b, _c, _d, _e;
//# sourceMappingURL=profile.component.js.map

/***/ }),

/***/ 386:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Profile; });
var Profile = (function () {
    function Profile(name, identify, email, movil_phone, client_id) {
        this.name = name;
        this.identify = identify;
        this.email = email;
        this.movil_phone = movil_phone;
        this.client_id = client_id;
    }
    return Profile;
}());

//# sourceMappingURL=profile.form.js.map

/***/ }),

/***/ 387:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__register_company_service__ = __webpack_require__(389);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__register_company_form__ = __webpack_require__(388);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterCompanyComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RegisterCompanyComponent = (function () {
    function RegisterCompanyComponent(_viewContainerRef, _compiler, _routerModule, _httpService) {
        this._viewContainerRef = _viewContainerRef;
        this._compiler = _compiler;
        this._routerModule = _routerModule;
        this._httpService = _httpService;
        this.loadingProcess = false;
        this.responseMessage = "";
        this.dataRegister = new __WEBPACK_IMPORTED_MODULE_3__register_company_form__["a" /* Register */]("", "", "");
        this.statusForm = false;
        this.viewContainerRef = _viewContainerRef;
        this._compiler.clearCache();
    }
    RegisterCompanyComponent.prototype.ngOnInit = function () {
        var myDiv = document.getElementById('container-register-company');
        // console.log(myDiv);
        scrollTo(document.body, myDiv.offsetTop, 10);
        function scrollTo(element, to, duration) {
            if (duration < 0)
                return;
            var difference = to - element.scrollTop;
            var perTick = difference / duration * 2;
            setTimeout(function () {
                element.scrollTop = element.scrollTop + perTick;
                scrollTo(element, to, duration - 2);
            }, 10);
        }
    };
    RegisterCompanyComponent.prototype.sendEmail = function (data) {
        var _this = this;
        var validEmail = false;
        var validName = false;
        var validPhone = false;
        if (data.value.email.toString().trim().length > 0) {
            validEmail = true;
            document.getElementById("status1").setAttribute("style", "display: none !important");
            document.getElementById("email").className = "form-control";
        }
        else {
            validEmail = false;
            document.getElementById("status1").setAttribute("style", "display: block !important");
            var d = document.getElementById("email");
            d.className += " inputImportant";
        }
        if (data.value.name.toString().trim().length > 0) {
            validName = true;
            document.getElementById("status2").setAttribute("style", "display: none !important");
            document.getElementById("name").className = "form-control";
        }
        else {
            validName = false;
            document.getElementById("status2").setAttribute("style", "display: block !important");
            var d = document.getElementById("name");
            d.className += " inputImportant";
        }
        if (data.value.phone.toString().trim().length > 0) {
            validPhone = true;
            document.getElementById("status3").setAttribute("style", "display: none !important");
            document.getElementById("phone").className = "form-control";
        }
        else {
            validPhone = false;
            document.getElementById("status3").setAttribute("style", "display: block !important");
            var d = document.getElementById("phone");
            d.className += " inputImportant";
        }
        if (validEmail && validName && validPhone) {
            this.statusForm = false;
            this.loadingProcess = true;
            this.responseMessage = "";
            // console.log(data);
            this._httpService.register(data.value.email, data.value.name, data.value.phone).subscribe(function (data) { return _this.postService = JSON.stringify(data); }, function (error) { return _this.responseSuccess(_this.postService, data); }, function () { return _this.responseSuccess(_this.postService, data); });
        }
        else {
            this.statusForm = true;
        }
    };
    RegisterCompanyComponent.prototype.responseSuccess = function (resp, data) {
        var dataResponse = JSON.parse(resp);
        // console.log(dataResponse);
        // console.log(data);
        var thisClass = this;
        if (dataResponse.return && dataResponse.status == 200) {
            this.responseMessage = dataResponse.message;
            this.loadingProcess = false;
            data.reset();
            this.dataRegister = new __WEBPACK_IMPORTED_MODULE_3__register_company_form__["a" /* Register */]("", "", "");
            setTimeout(function () {
                thisClass.responseMessage = "";
            }, 2000);
        }
        else {
            this.loadingProcess = false;
            this.responseMessage = "Por favor verifique el correo";
        }
    };
    RegisterCompanyComponent.prototype.changeValidateInput = function (type, data) {
        // console.log(this.statusForm);
        // console.log(data.value);
        if (this.statusForm) {
            switch (type) {
                case "name":
                    if (data.value.name.toString().trim().length > 0) {
                        document.getElementById("status2").setAttribute("style", "display: none !important");
                        document.getElementById("name").className = "form-control";
                    }
                    else {
                        document.getElementById("status2").setAttribute("style", "display: block !important");
                        var d = document.getElementById("name");
                        d.className += " inputImportant";
                    }
                    break;
                case "phone":
                    if (data.value.phone.toString().trim().length > 0) {
                        document.getElementById("status3").setAttribute("style", "display: none !important");
                        document.getElementById("phone").className = "form-control";
                    }
                    else {
                        document.getElementById("status3").setAttribute("style", "display: block !important");
                        var d = document.getElementById("phone");
                        d.className += " inputImportant";
                    }
                    break;
                case "email":
                    if (data.value.email.toString().trim().length > 0) {
                        document.getElementById("status1").setAttribute("style", "display: none !important");
                        document.getElementById("email").className = "form-control";
                    }
                    else {
                        document.getElementById("status1").setAttribute("style", "display: block !important");
                        var d = document.getElementById("email");
                        d.className += " inputImportant";
                    }
                    break;
                default:
                    break;
            }
        }
    };
    return RegisterCompanyComponent;
}());
RegisterCompanyComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__(519),
        styles: [__webpack_require__(473)],
        providers: [__WEBPACK_IMPORTED_MODULE_2__register_company_service__["a" /* HttpRegister */]]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["Compiler"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["Compiler"]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__register_company_service__["a" /* HttpRegister */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__register_company_service__["a" /* HttpRegister */]) === "function" && _d || Object])
], RegisterCompanyComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=register-company.component.js.map

/***/ }),

/***/ 388:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Register; });
var Register = (function () {
    function Register(email, name, phone) {
        this.email = email;
        this.name = name;
        this.phone = phone;
    }
    return Register;
}());

//# sourceMappingURL=register-company.form.js.map

/***/ }),

/***/ 389:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__constantsUbeep__ = __webpack_require__(13);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HttpRegister; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var HttpRegister = (function () {
    function HttpRegister(_http, _ConstantUbeep) {
        this._http = _http;
        this._ConstantUbeep = _ConstantUbeep;
        this._urlLogin = "assets/services/register-company/register-company.service.php";
    }
    HttpRegister.prototype.register = function (email, name, phone) {
        var data = JSON.stringify({
            key: "3edcdb20e0030daab21d0ba9af4c0dc2",
            email: email,
            name: name,
            phone: phone,
            class: "5",
            url: this._ConstantUbeep.urlServer
        });
        // console.log(data);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ "Access-Control-Allow-Origin": "*" });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({ headers: headers, method: "POST" });
        return this._http.post(this._urlLogin, data, options)
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    HttpRegister.prototype.handleError = function (error) {
        // console.error(error);
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json().error || ' error');
    };
    return HttpRegister;
}());
HttpRegister = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_5__constantsUbeep__["a" /* ConstantUbeep */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__constantsUbeep__["a" /* ConstantUbeep */]) === "function" && _b || Object])
], HttpRegister);

var _a, _b;
//# sourceMappingURL=register-company.service.js.map

/***/ }),

/***/ 390:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Register; });
var Register = (function () {
    function Register(name, pass, email, phone, identify) {
        this.name = name;
        this.pass = pass;
        this.email = email;
        this.phone = phone;
        this.identify = identify;
    }
    return Register;
}());

//# sourceMappingURL=register.js.map

/***/ }),

/***/ 391:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SecurityPoliciesComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SecurityPoliciesComponent = (function () {
    function SecurityPoliciesComponent() {
    }
    SecurityPoliciesComponent.prototype.ngOnInit = function () {
        var myDiv = document.getElementById('container-security-policies');
        // console.log(myDiv);
        scrollTo(document.body, myDiv.offsetTop, 10);
        function scrollTo(element, to, duration) {
            if (duration < 0)
                return;
            var difference = to - element.scrollTop;
            var perTick = difference / duration * 2;
            setTimeout(function () {
                element.scrollTop = element.scrollTop + perTick;
                scrollTo(element, to, duration - 2);
            }, 10);
        }
    };
    return SecurityPoliciesComponent;
}());
SecurityPoliciesComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__(520),
        styles: [__webpack_require__(474)]
    }),
    __metadata("design:paramtypes", [])
], SecurityPoliciesComponent);

//# sourceMappingURL=security-policies.component.js.map

/***/ }),

/***/ 392:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SecurityComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SecurityComponent = (function () {
    function SecurityComponent() {
    }
    SecurityComponent.prototype.ngOnInit = function () {
        var myDiv = document.getElementById('container-security');
        // console.log(myDiv);
        scrollTo(document.body, myDiv.offsetTop, 10);
        function scrollTo(element, to, duration) {
            if (duration < 0)
                return;
            var difference = to - element.scrollTop;
            var perTick = difference / duration * 2;
            setTimeout(function () {
                element.scrollTop = element.scrollTop + perTick;
                scrollTo(element, to, duration - 2);
            }, 10);
        }
    };
    return SecurityComponent;
}());
SecurityComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__(521),
        styles: [__webpack_require__(475)]
    }),
    __metadata("design:paramtypes", [])
], SecurityComponent);

//# sourceMappingURL=security.component.js.map

/***/ }),

/***/ 393:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__constantsUbeep__ = __webpack_require__(13);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HttpService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var HttpService = (function () {
    function HttpService(_http, _ConstantUbeep) {
        this._http = _http;
        this._ConstantUbeep = _ConstantUbeep;
        this._urlCancelService = "assets/services/services/cancel.service.php";
    }
    HttpService.prototype.cancelService = function (shipping_id) {
        var data = JSON.stringify({
            key: "3edcdb20e0030daab21d0ba9af4c0dc2",
            shipping_id: shipping_id,
            url: this._ConstantUbeep.urlServer
        });
        // console.log(data);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ "Access-Control-Allow-Origin": "*" });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({ headers: headers, method: "POST" });
        return this._http.post(this._urlCancelService, data, options)
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    HttpService.prototype.handleError = function (error) {
        // console.error(error);
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json().error || ' error');
    };
    return HttpService;
}());
HttpService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_5__constantsUbeep__["a" /* ConstantUbeep */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__constantsUbeep__["a" /* ConstantUbeep */]) === "function" && _b || Object])
], HttpService);

var _a, _b;
//# sourceMappingURL=http.service.js.map

/***/ }),

/***/ 394:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaymentGateWay; });
var PaymentGateWay = (function () {
    function PaymentGateWay(x_card_num, platform, client_id, x_exp_date, x_card_code) {
        if (x_card_num === void 0) { x_card_num = null; }
        if (platform === void 0) { platform = 'web'; }
        if (client_id === void 0) { client_id = null; }
        if (x_exp_date === void 0) { x_exp_date = null; }
        if (x_card_code === void 0) { x_card_code = null; }
        this.x_card_num = x_card_num;
        this.platform = platform;
        this.client_id = client_id;
        this.x_exp_date = x_exp_date;
        this.x_card_code = x_card_code;
    }
    return PaymentGateWay;
}());

//# sourceMappingURL=paymentGateWay.js.map

/***/ }),

/***/ 395:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServiceSend; });
var ServiceSend = (function () {
    function ServiceSend(origin_address, origin_detail, origin_latitude, origin_longitude, destiny_address, destiny_detail, destiny_latitude, destiny_longitude, type_service, //Número identificador del Tipo de servicio
        bag_id, //Número identificador del vehículo
        description_text, //Descripción del servicio
        declared_value, //Valor declarado
        destiny_name, //Nombre del destinatario
        tip //Propina
    ) {
        this.origin_address = origin_address;
        this.origin_detail = origin_detail;
        this.origin_latitude = origin_latitude;
        this.origin_longitude = origin_longitude;
        this.destiny_address = destiny_address;
        this.destiny_detail = destiny_detail;
        this.destiny_latitude = destiny_latitude;
        this.destiny_longitude = destiny_longitude;
        this.type_service = type_service;
        this.bag_id = bag_id;
        this.description_text = description_text;
        this.declared_value = declared_value;
        this.destiny_name = destiny_name;
        this.tip = tip; //Propina
    }
    return ServiceSend;
}());

//# sourceMappingURL=service.send.js.map

/***/ }),

/***/ 396:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServiceTransport; });
var ServiceTransport = (function () {
    function ServiceTransport(origin_address, origin_latitude, origin_longitude, destiny_address, destiny_latitude, destiny_longitude, type_service, bag_id, //Número identificador del vehículo
        tip //Propina
    ) {
        this.origin_address = origin_address;
        this.origin_latitude = origin_latitude;
        this.origin_longitude = origin_longitude;
        this.destiny_address = destiny_address;
        this.destiny_latitude = destiny_latitude;
        this.destiny_longitude = destiny_longitude;
        this.type_service = type_service;
        this.bag_id = bag_id;
        this.tip = tip; //Propina
    }
    return ServiceTransport;
}());

//# sourceMappingURL=service.transport.js.map

/***/ }),

/***/ 397:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SocialRegister; });
var SocialRegister = (function () {
    function SocialRegister(email, identify, cellPhone, id_agreement, code_agreement) {
        this.email = email;
        this.identify = identify;
        this.cellPhone = cellPhone;
        this.id_agreement = id_agreement;
        this.code_agreement = code_agreement;
    }
    return SocialRegister;
}());

//# sourceMappingURL=socialRegister.js.map

/***/ }),

/***/ 398:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterUser; });
var RegisterUser = (function () {
    function RegisterUser(email, pass, name, localphone, cellphone, identify, id_agreement, code_agreement) {
        if (email === void 0) { email = null; }
        if (pass === void 0) { pass = null; }
        if (name === void 0) { name = null; }
        if (localphone === void 0) { localphone = null; }
        if (cellphone === void 0) { cellphone = null; }
        if (identify === void 0) { identify = null; }
        if (id_agreement === void 0) { id_agreement = null; }
        if (code_agreement === void 0) { code_agreement = null; }
        this.email = email;
        this.pass = pass;
        this.name = name;
        this.localphone = localphone;
        this.cellphone = cellphone;
        this.identify = identify;
        this.id_agreement = id_agreement;
        this.code_agreement = code_agreement;
    }
    return RegisterUser;
}());

//# sourceMappingURL=userRegister.js.map

/***/ }),

/***/ 399:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VehiclesComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var VehiclesComponent = (function () {
    function VehiclesComponent() {
    }
    VehiclesComponent.prototype.ngOnInit = function () {
        var myDiv = document.getElementById('container-vehicles');
        // console.log(myDiv);
        scrollTo(document.body, myDiv.offsetTop, 10);
        function scrollTo(element, to, duration) {
            if (duration < 0)
                return;
            var difference = to - element.scrollTop;
            var perTick = difference / duration * 2;
            setTimeout(function () {
                element.scrollTop = element.scrollTop + perTick;
                scrollTo(element, to, duration - 2);
            }, 10);
        }
    };
    return VehiclesComponent;
}());
VehiclesComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__(523),
        styles: [__webpack_require__(477)]
    }),
    __metadata("design:paramtypes", [])
], VehiclesComponent);

//# sourceMappingURL=vehicles.component.js.map

/***/ }),

/***/ 400:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HttpForgotService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var HttpForgotService = (function () {
    function HttpForgotService(_http) {
        this._http = _http;
        this._urlLogin = "assets/services/auth/forgot/services/forgot.service.php";
    }
    HttpForgotService.prototype.forgotAcount = function (email, url) {
        var data = JSON.stringify({
            key: "3edcdb20e0030daab21d0ba9af4c0dc2",
            email: email,
            url: url
        });
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ "Access-Control-Allow-Origin": "*" });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({ headers: headers, method: "POST" });
        return this._http.post(this._urlLogin, data, options)
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    HttpForgotService.prototype.handleError = function (error) {
        // console.error(error);
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json().error || ' error');
    };
    return HttpForgotService;
}());
HttpForgotService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */]) === "function" && _a || Object])
], HttpForgotService);

var _a;
//# sourceMappingURL=forgot.component.js.map

/***/ }),

/***/ 401:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_cookie_services_cookies_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_cookie_services_cookies_service___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angular2_cookie_services_cookies_service__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LoginComponent = (function () {
    function LoginComponent(_http) {
        this._http = _http;
        this._urlLogin = "assets/services/auth/login/services/login.service.php"; //ok
        this._urlLoginSocial = "assets/services/auth/login/services/login.social.service.php"; //ok
        this._urlLoginFacebook = "assets/services/auth/login/services/login.facebook.php"; //ok
        this._urlLoginCheck = "assets/services/auth/login/services/login.check.user.php";
    }
    //---Login de usuario normal
    LoginComponent.prototype.loginUser = function (email, password, url) {
        var data = JSON.stringify({
            key: "3edcdb20e0030daab21d0ba9af4c0dc2",
            email: email,
            pass: password,
            platform: "web",
            url: url
        });
        // console.log("Datos del login nuevo!");
        // console.log(data);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ "Access-Control-Allow-Origin": "*" });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({ headers: headers, method: "POST" });
        return this._http.post(this._urlLogin, data, options)
            .map(this.loginSuccess)
            .catch(this.handleError);
    };
    LoginComponent.prototype.loginUserSocial = function (password, url, id_agreement, code_agreement, cellPhoneRegister, identifyRegister) {
        // console.log(password, url, id_agreement, code_agreement, cellPhoneRegister, identifyRegister);
        var data = JSON.stringify({
            key: "3edcdb20e0030daab21d0ba9af4c0dc2",
            pass: password,
            platform: "web",
            url: url,
            id_agreement: id_agreement,
            code_agreement: code_agreement,
            cellphone: cellPhoneRegister,
            identify: identifyRegister
        });
        // console.log("Datos del login nuevo!");
        // console.log(data);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ "Access-Control-Allow-Origin": "*" });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({ headers: headers, method: "POST" });
        return this._http.post(this._urlLoginSocial, data, options)
            .map(this.loginSuccess)
            .catch(this.handleError);
    };
    LoginComponent.prototype.loginCheckUserSocial = function (password, url) {
        var data = JSON.stringify({
            key: "3edcdb20e0030daab21d0ba9af4c0dc2",
            pass: password,
            platform: "web",
            url: url
        });
        // console.log("Datos del login nuevo!");
        // console.log(data);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ "Access-Control-Allow-Origin": "*" });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({ headers: headers, method: "POST" });
        return this._http.post(this._urlLoginCheck, data, options)
            .map(this.loginSuccess)
            .catch(this.handleError);
    };
    LoginComponent.prototype.loginUserFacebook = function (email, password, url, id_agreement, code_agreement, cellPhoneRegister, identifyRegister) {
        var data = JSON.stringify({
            key: "3edcdb20e0030daab21d0ba9af4c0dc2",
            email: email,
            pass: password,
            platform: "web",
            url: url,
            id_agreement: id_agreement,
            code_agreement: code_agreement,
            cellphone: cellPhoneRegister,
            identify: identifyRegister,
        });
        // console.log("Datos del login nuevo!");
        // console.log(data);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ "Access-Control-Allow-Origin": "*" });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({ headers: headers, method: "POST" });
        return this._http.post(this._urlLoginFacebook, data, options)
            .map(this.loginSuccess)
            .catch(this.handleError);
    };
    LoginComponent.prototype.loginSuccess = function (res) {
        // console.log(this);
        var data = res.json();
        // console.log(data);
        //return true;
        //this.loadingProcess = false;
        //let dataResponse = JSON.parse(data);
        var dataResponse = data;
        // console.log(dataResponse);
        if (dataResponse.return && dataResponse.message === "Usuario logueado exitosamente") {
            //alert("logeado");
            __WEBPACK_IMPORTED_MODULE_3_angular2_cookie_services_cookies_service__["CookieService"].prototype.put('statusServices', 'false');
            __WEBPACK_IMPORTED_MODULE_3_angular2_cookie_services_cookies_service__["CookieService"].prototype.put('id', dataResponse.data.user.id);
            __WEBPACK_IMPORTED_MODULE_3_angular2_cookie_services_cookies_service__["CookieService"].prototype.put('name', dataResponse.data.user.name);
            __WEBPACK_IMPORTED_MODULE_3_angular2_cookie_services_cookies_service__["CookieService"].prototype.put('movil_phone', dataResponse.data.user.phone);
            __WEBPACK_IMPORTED_MODULE_3_angular2_cookie_services_cookies_service__["CookieService"].prototype.put('social_id', dataResponse.data.user.social_id);
            __WEBPACK_IMPORTED_MODULE_3_angular2_cookie_services_cookies_service__["CookieService"].prototype.put('identify', dataResponse.data.user.identify);
            __WEBPACK_IMPORTED_MODULE_3_angular2_cookie_services_cookies_service__["CookieService"].prototype.put('email', dataResponse.data.user.email);
            // console.log(dataResponse.data.user.socket);
            if (dataResponse.data.user.socket != null) {
                var sockets = (dataResponse.data.user.socket).split(",");
                // console.log(sockets);
                __WEBPACK_IMPORTED_MODULE_3_angular2_cookie_services_cookies_service__["CookieService"].prototype.put('socket1', sockets[0]);
                __WEBPACK_IMPORTED_MODULE_3_angular2_cookie_services_cookies_service__["CookieService"].prototype.put('socket2', sockets[1]);
                __WEBPACK_IMPORTED_MODULE_3_angular2_cookie_services_cookies_service__["CookieService"].prototype.put('socket3', sockets[2]);
            }
            //CookieService.prototype.put('local_phone', dataResponse.data.user.email);
            __WEBPACK_IMPORTED_MODULE_3_angular2_cookie_services_cookies_service__["CookieService"].prototype.put('service_provider_id_1', dataResponse.data.menu[0].service_provider_id);
            __WEBPACK_IMPORTED_MODULE_3_angular2_cookie_services_cookies_service__["CookieService"].prototype.put('type_service_1', dataResponse.data.menu[0].type_service);
            __WEBPACK_IMPORTED_MODULE_3_angular2_cookie_services_cookies_service__["CookieService"].prototype.put('title_1', dataResponse.data.menu[0].title);
            __WEBPACK_IMPORTED_MODULE_3_angular2_cookie_services_cookies_service__["CookieService"].prototype.put('text_description_1', dataResponse.data.menu[0].text_description);
            __WEBPACK_IMPORTED_MODULE_3_angular2_cookie_services_cookies_service__["CookieService"].prototype.put('bag_services_1', dataResponse.data.menu[0].bag_services.length);
            __WEBPACK_IMPORTED_MODULE_3_angular2_cookie_services_cookies_service__["CookieService"].prototype.put('shipping_bag_id_1', dataResponse.data.menu[0].bag_services[0].shipping_bag_id);
            __WEBPACK_IMPORTED_MODULE_3_angular2_cookie_services_cookies_service__["CookieService"].prototype.put('title_shipping_bag_id_1', dataResponse.data.menu[0].bag_services[0].title);
            __WEBPACK_IMPORTED_MODULE_3_angular2_cookie_services_cookies_service__["CookieService"].prototype.put('subtitle_shipping_bag_id_1', dataResponse.data.menu[0].bag_services[0].subtitle);
            __WEBPACK_IMPORTED_MODULE_3_angular2_cookie_services_cookies_service__["CookieService"].prototype.put('shipping_bag_id_2', dataResponse.data.menu[0].bag_services[1].shipping_bag_id);
            __WEBPACK_IMPORTED_MODULE_3_angular2_cookie_services_cookies_service__["CookieService"].prototype.put('title_shipping_bag_id_2', dataResponse.data.menu[0].bag_services[1].title);
            __WEBPACK_IMPORTED_MODULE_3_angular2_cookie_services_cookies_service__["CookieService"].prototype.put('subtitle_shipping_bag_id_2', dataResponse.data.menu[0].bag_services[1].subtitle);
            __WEBPACK_IMPORTED_MODULE_3_angular2_cookie_services_cookies_service__["CookieService"].prototype.put('shipping_bag_id_3', dataResponse.data.menu[0].bag_services[2].shipping_bag_id);
            __WEBPACK_IMPORTED_MODULE_3_angular2_cookie_services_cookies_service__["CookieService"].prototype.put('title_shipping_bag_id_3', dataResponse.data.menu[0].bag_services[2].title);
            __WEBPACK_IMPORTED_MODULE_3_angular2_cookie_services_cookies_service__["CookieService"].prototype.put('subtitle_shipping_bag_id_3', dataResponse.data.menu[0].bag_services[2].subtitle);
            __WEBPACK_IMPORTED_MODULE_3_angular2_cookie_services_cookies_service__["CookieService"].prototype.put('service_provider_id_2', dataResponse.data.menu[1].service_provider_id);
            __WEBPACK_IMPORTED_MODULE_3_angular2_cookie_services_cookies_service__["CookieService"].prototype.put('type_service_2', dataResponse.data.menu[1].type_service);
            __WEBPACK_IMPORTED_MODULE_3_angular2_cookie_services_cookies_service__["CookieService"].prototype.put('title_2', dataResponse.data.menu[1].title);
            __WEBPACK_IMPORTED_MODULE_3_angular2_cookie_services_cookies_service__["CookieService"].prototype.put('text_description_2', dataResponse.data.menu[1].text_description);
            __WEBPACK_IMPORTED_MODULE_3_angular2_cookie_services_cookies_service__["CookieService"].prototype.put('bag_services_2', dataResponse.data.menu[1].bag_services.length);
            __WEBPACK_IMPORTED_MODULE_3_angular2_cookie_services_cookies_service__["CookieService"].prototype.put('shipping_bag_id_4', dataResponse.data.menu[1].bag_services[0].shipping_bag_id);
            __WEBPACK_IMPORTED_MODULE_3_angular2_cookie_services_cookies_service__["CookieService"].prototype.put('title_shipping_bag_id_4', dataResponse.data.menu[1].bag_services[0].title);
            __WEBPACK_IMPORTED_MODULE_3_angular2_cookie_services_cookies_service__["CookieService"].prototype.put('subtitle_shipping_bag_id_4', dataResponse.data.menu[1].bag_services[0].subtitle);
            if (dataResponse.data.user.credit != undefined) {
                __WEBPACK_IMPORTED_MODULE_3_angular2_cookie_services_cookies_service__["CookieService"].prototype.put('credit', dataResponse.data.user.credit);
            }
            if (__WEBPACK_IMPORTED_MODULE_3_angular2_cookie_services_cookies_service__["CookieService"].prototype.get('name') != undefined && __WEBPACK_IMPORTED_MODULE_3_angular2_cookie_services_cookies_service__["CookieService"].prototype.get('id') != undefined) {
                var nameUser = (__WEBPACK_IMPORTED_MODULE_3_angular2_cookie_services_cookies_service__["CookieService"].prototype.get('name')).split(" ");
            }
            // console.log(res.json());
            //res => res.json();
            return res.json();
        }
        else if (!dataResponse.return && dataResponse.message === "Email requerido") {
            return { return: false, message: dataResponse.message };
        }
        else {
            return { return: false, message: "Credenciales incorrectas" };
        }
    };
    LoginComponent.prototype.handleError = function (error) {
        // console.error(error);
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || ' error');
    };
    return LoginComponent;
}());
LoginComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        providers: []
    }),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */]) === "function" && _a || Object])
], LoginComponent);

var _a;
//# sourceMappingURL=login.component.js.map

/***/ }),

/***/ 402:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HttpRegisterService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var HttpRegisterService = (function () {
    function HttpRegisterService(_http) {
        this._http = _http;
        this._urlLogin = "assets/services/auth/register/services/register.service.php";
        this._urlInitialSetup = "assets/services/auth/register/services/initial.service.php";
    }
    HttpRegisterService.prototype.registerAcount = function (userNameRegister, passwordRegister, emailRegister, cellPhoneRegister, identifyRegister, url, id_agreement, code_agreement) {
        var data = JSON.stringify({
            key: "3edcdb20e0030daab21d0ba9af4c0dc2",
            name: userNameRegister,
            email: emailRegister,
            pass: passwordRegister,
            cellphone: cellPhoneRegister,
            identify: identifyRegister,
            platform: "web",
            url: url,
            id_agreement: id_agreement,
            code_agreement: code_agreement
        });
        // console.log("Datos del registrooooooooooooooooooo");
        console.log(data);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ "Access-Control-Allow-Origin": "*" });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({ headers: headers, method: "POST" });
        return this._http.post(this._urlLogin, data, options)
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    HttpRegisterService.prototype.initialSetup = function (url) {
        var data = JSON.stringify({
            key: "3edcdb20e0030daab21d0ba9af4c0dc2",
            url: url
        });
        // console.log("trae las empresas");
        // console.log(data);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ "Access-Control-Allow-Origin": "*" });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({ headers: headers, method: "POST" });
        return this._http.post(this._urlInitialSetup, data, options)
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    HttpRegisterService.prototype.handleError = function (error) {
        // console.error(error);
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json().error || ' error');
    };
    return HttpRegisterService;
}());
HttpRegisterService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */]) === "function" && _a || Object])
], HttpRegisterService);

var _a;
//# sourceMappingURL=register.component.js.map

/***/ }),

/***/ 403:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ 458:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)();
// imports


// module
exports.push([module.i, "#container-agreement {\n\tmargin-top: 132px;\n    overflow: hidden;\n\tmargin-bottom: 38px;\n\tpadding-top: 80px;\n}\n\n#container-agreement #container-benefits{\n    margin-bottom: 118px;\n    margin-top: 70px;\n}\n\n#container-agreement li{\n\tpadding: 0px;\n}\n\n\n#container-agreement .padding-left-12{\n\tpadding-left: 12px;\n}\n\n#container-agreement .margin-top-20{\n\tmargin-top: 20px;\n}\n\n#container-agreement .margin-bottom-20{\n\tmargin-bottom: 20px;\n}\n\n#container-agreement #formContact{\n\tmargin-top: 30px;\n}\n\n\n#container-agreement .btn-contact{\n\tbackground-color: #e45d2f;\n\tbackground-repeat: no-repeat;\n\tbackground-size: 100% 100%;\n\twidth : 140px;\n\tcolor: #FFFFFF;\n\tmargin-top: 35px;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 459:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)();
// imports


// module
exports.push([module.i, "#container-agreement {\n\tmargin-top: 132px;\n    overflow: hidden;\n\tmargin-bottom: 38px;\n\tpadding-top: 80px;\n}\n\n#container-agreement #container-benefits{\n    margin-bottom: 118px;\n    margin-top: 70px;\n}\n\n#container-agreement li{\n\tpadding: 0px;\n}\n\n\n#container-agreement .padding-left-12{\n\tpadding-left: 12px;\n}\n\n#container-agreement .margin-top-20{\n\tmargin-top: 20px;\n}\n\n#container-agreement .margin-bottom-20{\n\tmargin-bottom: 20px;\n}\n\n#container-agreement #formContact{\n\tmargin-top: 30px;\n}\n\n\n#container-agreement .btn-contact{\n\tbackground-repeat: no-repeat;\n\tbackground-color: #e45d2f ;\n\tbackground-size: 100% 100%;\n\twidth : 140px;\n\tcolor: #FFFFFF;\n\tmargin-top: 35px;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 460:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)();
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 461:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)();
// imports


// module
exports.push([module.i, "#container-beep{\n\tmargin-top: 132px;\n\toverflow: hidden;\n\tmargin-bottom: 38px;\n}\n\n#container-beep .container{\n\tpadding-left: 77px;\n\tpadding-right: 77px;\n}\n\n#container-beep .container .title{\n\tmargin-bottom: 60px;\n\tmargin-top: 60px;\n\tfont-size: 1.3em;\n}\n\n#container-beep .container .content{\n\tfont-size: 1.2em;\t\n}\n\n#container-beep .img-screenshots{\n\twidth: 320px;\n\tmargin-bottom: 50px;\n\tmargin-top: 35px;\n}\n\n#container-beep .img-store{\n\twidth: 135px;\n\tmargin-bottom: 60px;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 462:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)();
// imports


// module
exports.push([module.i, "#container-drive{\n\tmargin-top: 132px;\n    overflow: hidden;\n\tmargin-bottom: 38px;\n}\n\n#container-drive .container-info{\n\tmargin-top: 110px;\n}\n\n#container-drive input{\n\tborder: 1px solid #a7a6a7;\n}\n\n#container-drive .container-img-drive, #container-drive .container-drive{\n\twidth: 216px;\n\theight: 144px;\n\ttext-align: center;\n\tdisplay: inline-block;\n}\n\n#container-drive .container-drive div{\n    background: #3a383b;\n    padding: 12px;\n}\n\n#container-drive .container-drive div:hover{\n\tcursor: pointer;\n\tcolor: #E35021 !important;\n\tfont-weight: bold;\n\ttext-decoration: none;\n}\n\n#container-drive .container-text-img{\n    color: #FFFFFF;\n}\n\n#container-drive .container-text-img:hover{\n\ttext-decoration: none;\n\tcolor: #E35021 !important;\n\tfont-weight: bold;\n}\n\n#container-drive .container-drive #img-vehicle{\n\tbackground-image: url(" + __webpack_require__(342) + ");\n\tbackground-size: 100% 100%;\n\tcursor: pointer;\n}\n\n#container-drive .container-drive #img-vehicles{\n\tbackground-image: url(" + __webpack_require__(343) + ");\n\tbackground-size: 100% 100%;\n\tcursor: pointer;\n}\n\n#container-drive .container-drive #img-drive{\n\tbackground-image: url(" + __webpack_require__(340) + ");\n\tbackground-size: 100% 100%;\n\tcursor: pointer;\n}\n\n#container-drive .container-drive #img-vehicle:hover{\n\tcursor: pointer;\n\tbackground-image: url(" + __webpack_require__(76) + ");\n}\n\n#container-drive .container-drive #img-vehicle:active{\n\tcursor: pointer;\n\tbackground-image: url(" + __webpack_require__(76) + ");\n}\n\n#container-drive .container-drive #img-vehicles:hover{\n\tcursor: pointer;\n\tbackground-image: url(" + __webpack_require__(344) + ");\n}\n\n#container-drive .container-drive #img-drive:hover{\n\tcursor: pointer;\n\tbackground-image: url(" + __webpack_require__(341) + ");\n}\n\n#container-drive .container-img-drive img{\n\twidth: 100%;\n\theight: 100%;\n}\n\n#container-drive .container-imgs-driver{\n\tmargin-bottom: 50px;\n}\n\n.btn-contact{\n\tbackground-image: url(" + __webpack_require__(785) + ")!important;\n\tbackground-color: #e04b31;\n\tbackground-repeat: no-repeat;\n\tbackground-color: transparent !important;\n\tbackground-size: 100% 100%;\n\twidth : 140px;\n\tcolor: #FFFFFF;\n\tmargin-top: 35px;\n}\n\n.container-text-img{\n\tmargin-top: -6px;\n}\n\n#container-drive .container-banner{\n\theight: 366px;\n\twidth: 100%;\n\tbackground-image: url(" + __webpack_require__(780) + ");\n\tbackground-size: 100%;\n\tbackground-repeat: no-repeat;\n}\n\n#container-drive .container-banner #container-img-phone{\n    width: 165px;\n    display: inline-block;\n}\n\n#container-drive .container-banner #container-img-phone img{\n    width: 100%;\n}\n\n#container-drive .container-info{\n\n}\n\n#container-drive #container-img{\n    width: 65px;\n    display: inline-block;\n    margin-bottom: 28px;\n    height: 76px;\n}\n\n#container-drive #container-img img{\n\twidth: 100%\n}\n\n.padding-border{\n    padding-left: 32px;\n\tpadding-right: 33px;\n}\n\n#container-drive .container-info hr{\n\tbackground: #e45d2f;\n\theight: 1px;\n}\n\n.margin-top-100{\n\tmargin-top: 100px;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 463:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)();
// imports


// module
exports.push([module.i, "#container-contact-admin{\n\tmargin-top: 132px;\n    overflow: hidden;\n\tmargin-bottom: 38px;\n}\n\n#container-contact-admin .title{\n\tmargin-bottom: 60px;\n\tmargin-top: 60px;\n\tfont-size: 1.3em;\n}\n\n\n#container-contact-admin .form-group{\n\toverflow: hidden;\n}\n\n#container-contact-admin .form-group textarea {\n    resize: none;\n}\n\n#container-contact-admin .container-button{\n\tmargin-top: 80px;\n\tmargin-bottom: 80px;\n}\n\n#container-contact-admin .container-button button{\n\twidth: 160px;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 464:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)();
// imports


// module
exports.push([module.i, "#container-history {\n\tmargin-top: 132px;\n    overflow: hidden;\n\tmargin-bottom: 38px;\n\tpadding-top: 80px;\n}\n\n#container-history .container-services{\n\toverflow-y: scroll;\n    direction: rtl;\n    height: 712px;\n}\n\n\n.selected{\n\tbackground: #3a383b !important;\n    color: white !important;\n}\n\n#container-history .container-services .content-history{\n\tmin-height: 65px;\n\toverflow: hidden;\n\tborder: 1px solid ;\n\tdirection: initial !important;\n\tposition: relative;\n\tcursor: pointer;\n}\n\n\n#container-history .container-services .padding-borders{\n\tpadding-left: 42px;\n\tpadding-right: 15px;\n\tpadding-top: 8px;\n\tpadding-bottom: 8px;\n}\n\n.container-arrow-right{\n\tposition: absolute;\n    right: 10px;\n    top: 24px;\n}\n\n.container-arrow-right .arrow-right{\n\theight: 11px;\n}\n\n#container-history #containerPolyline{\n\twidth: 100%;\n    height: 320px;\n    margin-top: 40px;\n    margin-bottom: 40px;\n    text-align: center;\n}\n\n#container-history #containerPolyline img{\n\theight: 100%;\n}\n\n#containerImgDriver{\n\twidth: 100px;\n\theight: 100px;\n    display: inline-block;\n    position: relative;\n    /*top: -130px;*/\n    /*margin-right: 16px;*/\n}\n\n#img-driver{\n\twidth: 100px;\n\theight: 100px;\n\tborder-radius: 20px;\n}\n\n.containerData{\n\tdisplay: inline-block;\n}\n\n.padding-left-75{\n\tpadding-left: 75px;\n}\n\n.border-right{\n\tborder-right: 1px solid lightgray;\n}\n\n.float-right{\n\tfloat: right;\n}\n\n.margin-bottom-25{\n\tmargin-bottom: 25px;\n}\n\n.margin-bottom-60{\n\tmargin-bottom: 60px;\n}\n\n.margin-bottom-40{\n\tmargin-bottom: 40px;\n}\n\n.container-circle{\n\tdisplay: inline-block;\n}\n\n.icon-circle-orange{\n\theight: 10px;\n    width: 10px;\n    background: #e45a30;\n    border-radius: 50%;\n}\n\n.icon-circle-black{\n\theight: 10px;\n    width: 10px;\n    background: #3a383b;\n    border-radius: 50%;\n}\n\n::-webkit-scrollbar {\n    width: 8px;\n}\n \n::-webkit-scrollbar-track {\n    border-radius: 0px;\n    background: #3a383b;\n}\n\n::-webkit-scrollbar-thumb {\n    border-radius: 0px;\n    background: rgb(226, 73, 50);\n}\n\n#containerRating{\n\twidth: 113px;\n}\n\n#containerRating img{\n\twidth: 19px;\n\theight: 19px;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 465:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)();
// imports


// module
exports.push([module.i, "#container-faq{\n\tmargin-top: 132px;\n\toverflow: hidden;\n\tmargin-bottom: 38px;\n\tpadding-top: 80px;\n\tpadding-bottom: 80px;\n}\n\n#container-faq p{\n\tmargin-bottom: 30px;\n}\n\n#container-faq .container-title{\n\tmargin-bottom: 60px;\n\tfont-size: 1.7em;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 466:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)();
// imports


// module
exports.push([module.i, "#container-history {\n\tmargin-top: 132px;\n    overflow: hidden;\n\tmargin-bottom: 38px;\n\tpadding-top: 80px;\n}\n\n#container-history .container-services{\n\toverflow-y: scroll;\n    direction: rtl;\n    height: 712px;\n}\n\n\n.selected{\n\tbackground: #3a383b !important;\n    color: white !important;\n}\n\n#container-history .container-services .content-history{\n\tmin-height: 65px;\n\toverflow: hidden;\n\tborder: 1px solid ;\n\tdirection: initial !important;\n\tposition: relative;\n\tcursor: pointer;\n}\n\n\n#container-history .container-services .padding-borders{\n\tpadding-left: 42px;\n\tpadding-right: 15px;\n\tpadding-top: 8px;\n\tpadding-bottom: 8px;\n}\n\n.container-arrow-right{\n\tposition: absolute;\n    right: 10px;\n    top: 24px;\n}\n\n.container-arrow-right .arrow-right{\n\theight: 11px;\n}\n\n#container-history #containerPolyline{\n\twidth: 100%;\n    height: 320px;\n    margin-top: 40px;\n    margin-bottom: 40px;\n    text-align: center;\n}\n\n#container-history #containerPolyline img{\n\theight: 100%;\n}\n\n#containerImgDriver{\n\twidth: 100px;\n\theight: 100px;\n    display: inline-block;\n    position: relative;\n    /*top: -130px;*/\n    /*margin-right: 16px;*/\n}\n\n#img-driver{\n\twidth: 100px;\n\theight: 100px;\n\tborder-radius: 20px;\n}\n\n.containerData{\n\tdisplay: inline-block;\n}\n\n.padding-left-75{\n\tpadding-left: 75px;\n}\n\n.border-right{\n\tborder-right: 1px solid lightgray;\n}\n\n.float-right{\n\tfloat: right;\n}\n\n.margin-bottom-25{\n\tmargin-bottom: 25px;\n}\n\n.margin-bottom-60{\n\tmargin-bottom: 60px;\n}\n\n.margin-bottom-40{\n\tmargin-bottom: 40px;\n}\n\n.container-circle{\n\tdisplay: inline-block;\n}\n\n.icon-circle-orange{\n\theight: 10px;\n    width: 10px;\n    background: #e45a30;\n    border-radius: 50%;\n}\n\n.icon-circle-black{\n\theight: 10px;\n    width: 10px;\n    background: #3a383b;\n    border-radius: 50%;\n}\n\n::-webkit-scrollbar {\n    width: 8px;\n}\n \n::-webkit-scrollbar-track {\n    border-radius: 0px;\n    background: #3a383b;\n}\n\n::-webkit-scrollbar-thumb {\n    border-radius: 0px;\n    background: rgb(226, 73, 50);\n}\n\n#containerRating{\n\twidth: 113px;\n}\n\n#containerRating img{\n\twidth: 19px;\n\theight: 19px;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 467:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)();
// imports


// module
exports.push([module.i, "#container-contact .container-img-drive, #container-contact .container-drive{\n\twidth: 216px;\n\theight: 144px;\n\ttext-align: center;\n\tdisplay: inline-block;\n}\n\n#container-contact .container-drive div{\n    background: #3a383b;\n    padding: 12px;\n}\n\n#container-contact .container-drive div:hover{\n\tcursor: pointer;\n\tcolor: #E35021 !important;\n\tfont-weight: bold;\n\ttext-decoration: none;\n}\n\n#container-contact .container-text-img{\n    color: #FFFFFF;\n}\n\n#container-contact .container-text-img:hover{\n\ttext-decoration: none;\n\tcolor: #E35021 !important;\n\tfont-weight: bold;\n}\n\n#container-contact .container-drive #img-vehicle{\n\tbackground-image: url(" + __webpack_require__(342) + ");\n\tbackground-size: 100% 100%;\n}\n\n#container-contact .container-drive #img-vehicles{\n\tbackground-image: url(" + __webpack_require__(343) + ");\n\tbackground-size: 100% 100%;\n}\n\n#container-contact .container-drive #img-drive{\n\tbackground-image: url(" + __webpack_require__(340) + ");\n\tbackground-size: 100% 100%;\n}\n\n#container-contact .container-drive #img-vehicle:hover{\n\tcursor: pointer;\n\tbackground-image: url(" + __webpack_require__(76) + ");\n}\n\n#container-contact .container-drive #img-vehicle:active{\n\tcursor: pointer;\n\tbackground-image: url(" + __webpack_require__(76) + ");\n}\n\n#container-contact .container-drive #img-vehicles:hover{\n\tcursor: pointer;\n\tbackground-image: url(" + __webpack_require__(344) + ");\n}\n\n#container-contact .container-drive #img-drive:hover{\n\tcursor: pointer;\n\tbackground-image: url(" + __webpack_require__(341) + ");\n}\n\n#container-contact .container-img-drive img{\n\twidth: 100%;\n\theight: 100%;\n}\n\n#container-contact .container-imgs-driver{\n\tmargin-bottom: 50px;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 468:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)();
// imports


// module
exports.push([module.i, "#container-invite{\n\tmargin-top: 132px;\n    overflow: hidden;\n}\n\n#container-invite .container-background{\n\tbackground-image: url(" + __webpack_require__(782) + ");\n\tbackground-size: 100% auto;\n    background-repeat: no-repeat;\n\theight: 800px;\n\tbackground-position-y: -60px;\n\tpadding-top: 183px;\n}\n\n.panel{\n\tborder: 0px solid transparent !important;\n\tbackground: transparent !important;\n}\n\n.panel-heading{\n\tbackground: #3a383b;\n\tborder-color: transparent !important;\n\tborder-top-left-radius: 15px;\n    border-top-right-radius: 15px;\n    color: white;\n}\n\n.panel-body{\n\tbackground: rgba(58, 56, 59, 0.54);\n\tborder-bottom-left-radius: 15px;\n    border-bottom-right-radius: 15px;\n    color: white;\n    min-height: 300px;\n    padding: 40px;\n}\n\ninput{\n\tborder: 0px solid transparent;\n\theight: 65px;\n\tfont-size: 1.5em;\n}\n\n.btn-lg{\n\theight: 65px;\n}\n\n.margin-bottom-50{\n\tmargin-bottom: 50px;\n\toverflow: hidden;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 469:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)();
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 470:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)();
// imports


// module
exports.push([module.i, "#container-press{\n\tmargin-top: 132px;\n\toverflow: hidden;\n\tmargin-bottom: 38px;\n}\n\n#container-press .container .content-img{\n    padding-top: 80px;\n    padding-bottom: 80px;\n}\n\n#container-press .container .content-img figure {\n\tdisplay: inline-block;\n}\n\n#container-press .container .content-img figure .img-screenshots{\n\twidth: 100%;\n\tmargin-bottom: 80px;\n\tmargin-top: 80px;\n}\n\n#container-press .content-download{\n\tbackground-color: #f4f4f4;\n}\n\n#container-press .content-download .container{\n\tpadding-left: 77px;\n\tpadding-right: 77px;\n\tpadding-top: 30px;\n\tpadding-bottom: 30px;\n}\n\n#container-press .content-download .img-beep{\n\twidth: 160px;\n}\n\n#container-press .content-download .btn-orange{\n    width: 162px;\n}\n\n.padding-40{\n\tpadding-top: 40px;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 471:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)();
// imports


// module
exports.push([module.i, "#container-privacy-policy{\n\tmargin-top: 132px;\n    overflow: hidden;\n    background: #f4f4f4;\n    min-height: 300px;\n}\n\n\n#container-privacy-policy .container{\n\tbackground: #FFFFFF;\n\tpadding: 50px;\n}\n\n#logo{\n\ttext-align: center;\n\tpadding-bottom: 40px;\n}\n\n.container-policy{\n\ttext-align: justify;\n\tmargin-bottom: 20px;\n    margin-top: 10px;\n    overflow: hidden;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 472:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)();
// imports


// module
exports.push([module.i, "#containerProfile{\n\tmargin-top: 132px;\n\tpadding-bottom: 140px;\n\tpadding-top: 44px;\n}\n\n#containerProfile .container-picture{\n\ttext-align: center;\n}\n\n#containerProfile #container-img-profile{\n\twidth: 184px;\n    height: 184px;\n    display: inline-block;\n}\n\n#containerProfile #container-img-profile img{\n\twidth: 100%;\n\theight: 100%;\n}\n\n#containerProfile .container-form{\n\tborder-left: 1px solid #f1b0a8;\n\tpadding-left: 26px;\n}\n\n.btn-contact{\n\tbackground-repeat: no-repeat;\n\tbackground-color: #e45d2f !important;\n\tbackground-size: 100% 100%;\n\tcolor: #FFFFFF;\n}\n\n.container-title{\n\tmargin-bottom: 40px;\n}\n\n.container-buttons{\n\tmargin-top: 50px;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 473:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)();
// imports


// module
exports.push([module.i, "#container-register-company{\n\tmargin-top: 132px;\n    overflow: hidden;\n}\n\n#container-register-company .container-background{\n\tbackground-image: url(" + __webpack_require__(783) + ");\n\tbackground-size: 100% auto;\n    background-repeat: no-repeat;\n\theight: 800px;\n\tbackground-position-y: -60px;\n\tpadding-top: 100px;\n}\n\n.panel{\n\tborder: 0px solid transparent !important;\n\tbackground: transparent !important;\n}\n\n.panel-heading{\n\tbackground: #3a383b;\n\tborder-color: transparent !important;\n\tborder-top-left-radius: 15px;\n    border-top-right-radius: 15px;\n    color: white;\n}\n\n.panel-body{\n\tbackground: rgba(58, 56, 59, 0.54);\n\tborder-bottom-left-radius: 15px;\n    border-bottom-right-radius: 15px;\n    color: white;\n    min-height: 300px;\n    padding: 40px;\n}\n\ninput{\n\tborder: 0px solid transparent;\n\theight: 65px;\n\tfont-size: 1.5em;\n}\n\n.btn-lg{\n\theight: 65px;\n}\n\n.margin-bottom-50{\n\tmargin-bottom: 50px;\n\toverflow: hidden;\n}\n\n.form-group{\n\toverflow: hidden;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 474:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)();
// imports


// module
exports.push([module.i, "#container-security-policies{\n\tmargin-top: 132px;\n    overflow: hidden;\n    background: #f4f4f4;\n    min-height: 300px;\n}\n\n\n#container-security-policies .container{\n\tbackground: #FFFFFF;\n\tpadding: 50px;\n}\n\n#logo{\n\ttext-align: center;\n\tpadding-bottom: 40px;\n}\n\n.container-policy{\n\ttext-align: justify;\n\tmargin-bottom: 20px;\n    margin-top: 10px;\n    overflow: hidden;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 475:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)();
// imports


// module
exports.push([module.i, "#container-security{\n\toverflow: hidden;\n\tmargin-bottom: 38px;\n\tpadding-top: 132px;\n\tpadding-bottom: 80px;\n}\n\n#container-security #banner{\n\theight: 560px;\n\tbackground-image: url(" + __webpack_require__(784) + ");\n\tbackground-size: auto 100%;\n    background-repeat: no-repeat;\n    background-position: center;\n    padding-top: 208px;\n}\n\n#container-security .container-info{\n\tpadding-top: 100px;\n\tfont-size: 1.2em;\n    /*text-align: justify;*/\n}\n\n#container-security .container-info hr{\n\tmargin-top: 0px !important;\n    margin-bottom: 10px !important;\n    border: 0;\n    border-top: 1px solid #E35021 !important;\n}\n\n#container-security .container-info .row{\n\tmargin-bottom: 40px;\n}\n\n.padding-r-40{\n\tpadding-right: 40px;\n}\n\n\n.padding-l-40{\n\tpadding-right: 40px;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 476:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)();
// imports


// module
exports.push([module.i, "@font-face {font-family: \"GothamRnd-Medium\";\r\n    src: url(" + __webpack_require__(779) + ");\r\n}\r\n\r\n#container-services{\r\n\tbackground-color: #FFFFFF;\r\n\tmin-height: 628px;\r\n\tmargin-top: 132px;\r\n\toverflow: hidden;\r\n}\r\n#container-services #googleMap {\r\n  height: 771px;\r\n  width: 100%;\r\n}\r\n\r\n.btn-cancel-type-pay{\r\n\tcolor: #e34522;\r\n\tcursor: pointer;\r\n\ttext-decoration: underline;\r\n}\r\n\r\n.nav-tabs {\r\n  color:  red !important;\r\n}\r\n\r\n\r\n#container-services .saldo-disponible{\r\n\tbackground: #e55e2f;\r\n    color: #FFFFFF;\r\n    font-weight: bold;\r\n    padding: 10px;\r\n    text-align: center;\r\n}\r\n#container-services .nav{\r\n\tborder-bottom: 1px solid #3A383B !important;\r\n}\r\n#container-services tab .content-tab-transport{\r\n\tpadding-top: 35px;\r\n\theight: 670px;\r\n\tposition: relative;\r\n}\r\n#container-services tab .form-group{\r\n\t/*padding-bottom: 85px; */\r\n}\r\n#container-services tab .form-group input[type=text]{\r\n\tdisplay: block;\r\n    width: 100%;\r\n    height: 34px;\r\n    padding: 6px 12px;\r\n    font-size: 14px;\r\n    font-style: italic;\r\n    line-height: 1.42857143;\r\n    color: #555555;\r\n    background-color: #FFFFFF;\r\n    background-image: none;\r\n    border-bottom: 1px solid #CCCCCC;\r\n    border-top: 0px solid transparent;\r\n    border-left: 0px solid transparent;\r\n    border-right: 0px solid transparent;\r\n    border-radius: 0px;\r\n    box-shadow: inset 0 0px 0px rgba(0,0,0,0);\r\n    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;\r\n}\r\n.inputTextLabel{\r\n\tdisplay: block;\r\n    cursor: default;\r\n    width: 100%;\r\n    height: 34px;\r\n    font-size: 14px;\r\n    line-height: 1.42857143;\r\n    color: #3f3f3f;\r\n    background-color: transparent !important;\r\n    background-image: none;\r\n    border-bottom: 0px solid transparent;\r\n    border-top: 0px solid transparent;\r\n    border-left: 0px solid transparent;\r\n    border-right: 0px solid transparent;\r\n    border-radius: 0px;\r\n    box-shadow: inset 0 0px 0px rgba(0,0,0,0);\r\n    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;\r\n}\r\n#container-services tab .form-group label{\r\n\tfont-weight: 600;\r\n}\r\n\r\n#container-services tab #container-img-types-services .col-sm-4{\r\n\tmargin-top: 25px;\r\n}\r\n#container-services tab #container-img-types-services .col-sm-4 label{\r\n    margin-top: 14px;\r\n}\r\n#container-services tab #container-img-types-services figure{\r\n\tdisplay: inline-block;\r\n\twidth: 90px;\r\n\theight: 38px;\r\n}\r\n#container-services tab #container-img-types-services figure img{\r\n\twidth: 100%;\r\n}\r\n#container-services tab #container-img-types-services label {\r\n  cursor: pointer;\r\n}\r\ninput[type=\"radio\"] {\r\n  display: none;\r\n}\r\ninput[type=\"radio\"] + span {\r\n\tbackground-color: #fefefe;\r\n    border: 1px solid;\r\n    border-color: #c2c2c2 #c2c2c2 #c2c2c2 #c2c2c2;\r\n    border-radius: 50px;\r\n    display: inline-block;\r\n    float: left;\r\n    margin-right: 7px;\r\n    padding: 7px;\r\n    position: relative;\r\n    -webkit-appearance: none;\r\n}\r\ninput[type=\"radio\"]:checked + span {\r\n    color: #d40000;\r\n    background: #c2c2c2;\r\n}\r\ninput[type=\"radio\"]:checked + span:after {\r\n\tbackground: #d40000;\r\n    border-radius: 50px;\r\n    content: \" \";\r\n    height: 10px;\r\n    left: 2px;\r\n    position: absolute;\r\n    top: 2px;\r\n    width: 10px;\r\n}\r\n.btn-cancel{\r\n  ./assets/fonts/gotham-rounded/GothamRnd-Medium.otf\r\n\tbackground-image: url(" + __webpack_require__(127) + ")!important;\r\n\tbackground-repeat: no-repeat;\r\n\tbackground-color: transparent !important;\r\n\tbackground-size: 100% 100%;\r\n\tmin-width : 210px;\r\n\tcolor: #FFFFFF;\r\n\ttext-align: center;\r\n}\r\n#container-services .btn-continue{\r\n\tbackground-image: url(" + __webpack_require__(127) + ")!important;\r\n\tbackground-repeat: no-repeat;\r\n\tbackground-color: transparent !important;\r\n\tbackground-size: 100% 100%;\r\n\twidth : 170px;\r\n\tcolor: #FFFFFF;\r\n\ttext-align: center;\r\n\tposition: absolute;\r\n    right: 20px;\r\n    bottom: 20px;\r\n}\r\n#container-services .btn-back{\r\n\tbackground-image: url(" + __webpack_require__(127) + ")!important;\r\n\tbackground-repeat: no-repeat;\r\n\tbackground-color: transparent !important;\r\n\tbackground-size: 100% 100%;\r\n\twidth : 170px;\r\n\tcolor: #FFFFFF;\r\n\ttext-align: center;\r\n\tposition: absolute;\r\n    right: 200px;\r\n    bottom: 20px;\r\n}\r\n#container-services .container-payment{\r\n\tpadding-top: 60px;\r\n}\r\n#container-services .container-payment .title-main{\r\n\tpadding-bottom: 55px;\r\n}\r\n#container-services .container-payment .container-info{\r\n\tmargin-bottom: 30px;\r\n\toverflow: hidden;\r\n}\r\n#btnConfirmarBono{\r\n\theight: 65px;\r\n\tbackground-color: #e04b31;\r\n\tcolor: #FFFFFF;\r\n\ttext-align: center;\r\n}\r\n.text-information{\r\n\tmargin-bottom: 69px;\r\n}\r\n#inputBono{\r\n\ttext-align: center;\r\n}\r\na:hover{\r\n\ttext-decoration: none !important;\r\n\tcolor: #E35021 !important;\r\n\tcursor: pointer;\r\n}\r\n#viewContainerSuccessPay{\r\n\tbackground-image: url(" + __webpack_require__(781) + ");\r\n\tbackground-repeat: no-repeat;\r\n\tbackground-size: 100% 100%;\r\n\theight: 628px;\r\n\tcolor: #FFFFFF;\r\n}\r\n.padding-top-50{\r\n\tmargin-top: 50px;\r\n}\r\n.padding-top-28{\r\n\tmargin-top: 28px;\r\n}\r\n#btnPay{\r\n\tmargin-bottom: 50px;\r\n}\r\n.text-white{\r\n\tcolor: #FFFFFF;\r\n}\r\n@media screen and (min-width: 307px) and (max-width: 407px){\r\n\t#container-services #container-img-types-services .col-xs-4{\r\n\t\twidth: 100% !important;\r\n\t}\r\n\t#container-services .container-form{\r\n\t    min-height: 872px !important;\r\n\t}\r\n\t#container-services .container-btn-continue{\r\n\t\ttext-align: center !important;\r\n\t}\r\n\t#container-services .btn-continue{\r\n\t\tposition: relative !important;\r\n\t\tbottom: 0px !important;\r\n\t\tright: 0px !important;\r\n\t}\r\n}\r\n\r\n#container-services #viewDataConfirm{\r\n\r\n}\r\n#container-services #viewDataConfirm #img-driver{\r\n\twidth: 100px;\r\n\theight: 100px;\r\n\tborder-radius: 20px;\r\n}\r\n#container-services #viewDataConfirm .margin-borders{\r\n\tpadding-top: 35px;\r\n\tpadding-bottom: 35px;\r\n}\r\n#container-services #viewDataConfirm .margin-bottom{\r\n\tpadding-bottom: 12px;\r\n}\r\n#container-services #viewDataConfirm #btnCancelService{\r\n\tbackground-color: #3a383b !important;\r\n\twidth: 145px;\r\n\tcolor: white;\r\n}\r\n#modal-login .modal-content .header-transparent{\r\n\tbackground-color: #eeeeee !important;\r\n    padding: 4px !important;\r\n    border-bottom: 0px !important;\r\n}\r\n#modal-login .modal-content .btnOrange{\r\n\tbackground: #e04b31;\r\n\tcolor: white;\r\n}\r\n\r\n.margin-t-15{\r\n\tmargin-top: 15px;\r\n}\r\n\r\n.margin-t-10{\r\n\tmargin-top: 10px;\r\n}\r\n\r\n.margin-t-7{\r\n\tmargin-top: 7px;\r\n}\r\n\r\n\r\n#containerRating img{\r\n\twidth: 24px;\r\n\theight: 24px;\r\n}\r\n\r\n#container-services .style-label-tab{\r\n\tcolor: #3a383b !important;\r\n\tfont-family: \"GothamRnd-Medium\" !important;\r\n\tfont-size: 1.25em !important;\r\n}\r\n\r\n#container-services .style-label-tab li.active a{\r\n\tcolor: #da481f !important;\r\n}\r\n\r\n#container-services .style-label-tab li a {\r\n    text-decoration: none !important;\r\n    color: #3a383b !important;\r\n}\r\n\r\n#container-services .style-text-tab{\r\n\tcolor: #333333 !important;\r\n\tfont-family: \"Varela_Round_Regular\" !important;\r\n\tfont-size: 14px !important;\r\n}\r\n\r\n.containerVehicleSelect{\r\n\tbackground: #eeeeee;\r\n    border: 1px solid #4e4c4e;\r\n    border-radius: 20%;\r\n    padding: 20px;\r\n    height: 112px;\r\n    width: 155px;\r\n    font-weight: bold;\r\n    color: #de5a38;\r\n}\r\n\r\n#containerVehicleVan, #containerVehicleWhite, #containerVehicleElectric{\r\n\tpadding: 20px;\r\n    height: 110px;\r\n    width: 137px;\r\n    cursor: pointer;\r\n}\r\n\r\n\r\n#containerVehicleVan:hover, #containerVehicleWhite:hover, #containerVehicleElectric:hover{\r\n\tbackground: #f8f8f8;\r\n    border: 1px solid #d2d1d2;\r\n    border-radius: 20%;\r\n    padding: 20px;\r\n}\r\n\r\n#viewOpenModalBond{\r\n\ttext-decoration: underline;\r\n\tfont-size: 1.2em;\r\n}\r\n\r\n#viewOpenModalBond:hover{\r\n\tfont-weight: bold;\r\n\ttext-decoration: underline;\r\n}\r\n\r\n.container-values{\r\n\tmargin-top: 114px;\r\n    margin-bottom: 37px;\r\n}\r\n\r\n#viewPayCredit {\r\n\tdisplay: none;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 477:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)();
// imports


// module
exports.push([module.i, "#container-vehicles{\n\t\tmargin-top: 132px;\n\t\tpadding-top: 50px;\n\t\tpadding-bottom: 50px;\n\t\tbackground-image: url(" + __webpack_require__(786) + ");\n\t    background-repeat: no-repeat;\n\t    background-size: 78% 91%;\n\t}\n\n\t#container-vehicles .container-info{\n\t\twidth: 100%;\n\t\ttext-align: right;\n\t\tfont-weight: bold;\n\t}\n\n\t#container-vehicles .container-info .container-info-vehicles{\n\t\tmargin-bottom: 40px;\n\t}\n\n\t#container-vehicles .container-info .container-info-vehicles:last-child{\n\t\tmargin-bottom: 0px;\n\t}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 478:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 142,
	"./af.js": 142,
	"./ar": 148,
	"./ar-dz": 143,
	"./ar-dz.js": 143,
	"./ar-ly": 144,
	"./ar-ly.js": 144,
	"./ar-ma": 145,
	"./ar-ma.js": 145,
	"./ar-sa": 146,
	"./ar-sa.js": 146,
	"./ar-tn": 147,
	"./ar-tn.js": 147,
	"./ar.js": 148,
	"./az": 149,
	"./az.js": 149,
	"./be": 150,
	"./be.js": 150,
	"./bg": 151,
	"./bg.js": 151,
	"./bn": 152,
	"./bn.js": 152,
	"./bo": 153,
	"./bo.js": 153,
	"./br": 154,
	"./br.js": 154,
	"./bs": 155,
	"./bs.js": 155,
	"./ca": 156,
	"./ca.js": 156,
	"./cs": 157,
	"./cs.js": 157,
	"./cv": 158,
	"./cv.js": 158,
	"./cy": 159,
	"./cy.js": 159,
	"./da": 160,
	"./da.js": 160,
	"./de": 162,
	"./de-at": 161,
	"./de-at.js": 161,
	"./de.js": 162,
	"./dv": 163,
	"./dv.js": 163,
	"./el": 164,
	"./el.js": 164,
	"./en-au": 165,
	"./en-au.js": 165,
	"./en-ca": 166,
	"./en-ca.js": 166,
	"./en-gb": 167,
	"./en-gb.js": 167,
	"./en-ie": 168,
	"./en-ie.js": 168,
	"./en-nz": 169,
	"./en-nz.js": 169,
	"./eo": 170,
	"./eo.js": 170,
	"./es": 172,
	"./es-do": 171,
	"./es-do.js": 171,
	"./es.js": 172,
	"./et": 173,
	"./et.js": 173,
	"./eu": 174,
	"./eu.js": 174,
	"./fa": 175,
	"./fa.js": 175,
	"./fi": 176,
	"./fi.js": 176,
	"./fo": 177,
	"./fo.js": 177,
	"./fr": 180,
	"./fr-ca": 178,
	"./fr-ca.js": 178,
	"./fr-ch": 179,
	"./fr-ch.js": 179,
	"./fr.js": 180,
	"./fy": 181,
	"./fy.js": 181,
	"./gd": 182,
	"./gd.js": 182,
	"./gl": 183,
	"./gl.js": 183,
	"./he": 184,
	"./he.js": 184,
	"./hi": 185,
	"./hi.js": 185,
	"./hr": 186,
	"./hr.js": 186,
	"./hu": 187,
	"./hu.js": 187,
	"./hy-am": 188,
	"./hy-am.js": 188,
	"./id": 189,
	"./id.js": 189,
	"./is": 190,
	"./is.js": 190,
	"./it": 191,
	"./it.js": 191,
	"./ja": 192,
	"./ja.js": 192,
	"./jv": 193,
	"./jv.js": 193,
	"./ka": 194,
	"./ka.js": 194,
	"./kk": 195,
	"./kk.js": 195,
	"./km": 196,
	"./km.js": 196,
	"./ko": 197,
	"./ko.js": 197,
	"./ky": 198,
	"./ky.js": 198,
	"./lb": 199,
	"./lb.js": 199,
	"./lo": 200,
	"./lo.js": 200,
	"./lt": 201,
	"./lt.js": 201,
	"./lv": 202,
	"./lv.js": 202,
	"./me": 203,
	"./me.js": 203,
	"./mi": 204,
	"./mi.js": 204,
	"./mk": 205,
	"./mk.js": 205,
	"./ml": 206,
	"./ml.js": 206,
	"./mr": 207,
	"./mr.js": 207,
	"./ms": 209,
	"./ms-my": 208,
	"./ms-my.js": 208,
	"./ms.js": 209,
	"./my": 210,
	"./my.js": 210,
	"./nb": 211,
	"./nb.js": 211,
	"./ne": 212,
	"./ne.js": 212,
	"./nl": 214,
	"./nl-be": 213,
	"./nl-be.js": 213,
	"./nl.js": 214,
	"./nn": 215,
	"./nn.js": 215,
	"./pa-in": 216,
	"./pa-in.js": 216,
	"./pl": 217,
	"./pl.js": 217,
	"./pt": 219,
	"./pt-br": 218,
	"./pt-br.js": 218,
	"./pt.js": 219,
	"./ro": 220,
	"./ro.js": 220,
	"./ru": 221,
	"./ru.js": 221,
	"./se": 222,
	"./se.js": 222,
	"./si": 223,
	"./si.js": 223,
	"./sk": 224,
	"./sk.js": 224,
	"./sl": 225,
	"./sl.js": 225,
	"./sq": 226,
	"./sq.js": 226,
	"./sr": 228,
	"./sr-cyrl": 227,
	"./sr-cyrl.js": 227,
	"./sr.js": 228,
	"./ss": 229,
	"./ss.js": 229,
	"./sv": 230,
	"./sv.js": 230,
	"./sw": 231,
	"./sw.js": 231,
	"./ta": 232,
	"./ta.js": 232,
	"./te": 233,
	"./te.js": 233,
	"./tet": 234,
	"./tet.js": 234,
	"./th": 235,
	"./th.js": 235,
	"./tl-ph": 236,
	"./tl-ph.js": 236,
	"./tlh": 237,
	"./tlh.js": 237,
	"./tr": 238,
	"./tr.js": 238,
	"./tzl": 239,
	"./tzl.js": 239,
	"./tzm": 241,
	"./tzm-latn": 240,
	"./tzm-latn.js": 240,
	"./tzm.js": 241,
	"./uk": 242,
	"./uk.js": 242,
	"./uz": 243,
	"./uz.js": 243,
	"./vi": 244,
	"./vi.js": 244,
	"./x-pseudo": 245,
	"./x-pseudo.js": 245,
	"./yo": 246,
	"./yo.js": 246,
	"./zh-cn": 247,
	"./zh-cn.js": 247,
	"./zh-hk": 248,
	"./zh-hk.js": 248,
	"./zh-tw": 249,
	"./zh-tw.js": 249
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 478;


/***/ }),

/***/ 504:
/***/ (function(module, exports) {

module.exports = "<div id=\"container-agreement\">\n\t<div class=\"container\">\n\n\t\t<label class=\"col-xs-12 col-sm-12 text-orange text-1-25em text-center\">\n\t\t\tCONVENIOS\n\t\t</label>\n\n\t\t<div class=\"col-xs-12 col-sm-12\">\n\t\t\t<div class=\"text-1-25em row font-bold\">\n\t\t\t\tBeep para su trabajo\n\t\t\t</div>\n\t\t\t<div class=\"row\">\n\t\t\t\tMejore el servicio de transporte en su empresa simplificando el papeleo, ahorrando dinero y privilegiando la seguridad para usted y sus colaboradores.\n\t\t\t</div>\n\t\t</div>\n\n\t\t<div class=\"col-xs-12 col-sm-12\" id=\"container-benefits\">\n\t\t\t<div class=\"text-1-25em row font-bold\">\n\t\t\t\tBeneficios\n\t\t\t</div>\n\t\t\t<div class=\"margin-top-20 margin-bottom-20\">\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\t<li>\n\t\t\t\t\t\t<label>Conveniencia</label>\n\t\t\t\t\t</li>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"padding-left-12\">\n\t\t\t\t\tSolicite un servicio donde y cuando lo necesite\n\t\t\t\t</div>\n\t\t\t</div>\t\n\t\t\t<div class=\"margin-top-20 margin-bottom-20\">\n\t\t\t\t<div class=\"row\">\t\t\n\t\t\t\t\t<li>\n\t\t\t\t\t\t<label>Seguridad</label>\n\t\t\t\t\t</li>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"padding-left-12\">\n\t\t\t\t\tReciba un servicio seguro, legal y eficiente\n\t\t\t\t</div>\n\t\t\t</div>\t\n\t\t\t<div class=\"margin-top-20 margin-bottom-20\">\n\t\t\t\t<div class=\"row\">\t\t\n\t\t\t\t\t<li>\n\t\t\t\t\t\t<label>Control</label>\n\t\t\t\t\t</li>\n\t\t\t\t</div>\t\n\t\t\t\t<div class=\"padding-left-12\">\n\t\t\t\t\tLleve control sobre los gastos y recorridos de su equipo de trabajo\n\t\t\t\t</div>\t\n\t\t\t</div>\t\n\t\t\t<div class=\"margin-top-20 margin-bottom-20\">\n\t\t\t\t<div class=\"row\">\t\t\n\t\t\t\t\t<li>\n\t\t\t\t\t\t<label>Ahorro</label>\n\t\t\t\t\t</li>\n\t\t\t\t</div>\t\n\t\t\t\t<div class=\"padding-left-12\">\n\t\t\t\t\tAhorre dinero con nuestros programas de beneficios\n\t\t\t\t</div>\t\n\t\t\t</div>\t\n\t\t\t<div class=\"margin-top-20 margin-bottom-20\">\n\t\t\t\t<div class=\"row\">\t\t\n\t\t\t\t\t<li>\n\t\t\t\t\t\t<label>Eficiencia</label>\n\t\t\t\t\t</li>\n\t\t\t\t</div>\t\n\t\t\t\t<div class=\"padding-left-12\">\n\t\t\t\t\tOptimice los procesos eliminando el papeleo \n\t\t\t\t</div>\t\n\t\t\t</div>\t\n\t\t\t<div class=\"margin-top-20 margin-bottom-20\">\n\t\t\t\t<div class=\"row\">\t\t\n\t\t\t\t\t<li>\n\t\t\t\t\t\t<label>Flexibilidad</label>\n\t\t\t\t\t</li>\n\t\t\t\t</div>\t\n\t\t\t\t<div class=\"padding-left-12\">\n\t\t\t\t\tElija el medio de pago que más le convenga a usted y su equipo\n\t\t\t\t</div>\t\n\t\t\t</div>\t\n\t\t</div>\n\n\t\t<div class=\"col-xs-12 col-sm-12\">\n\t\t\t<label class=\"col-xs-12 col-sm-12 row text-1-25em text-center\">\n\t\t\t\tFormulario de contacto\n\t\t\t</label>\n\t\t\t<form #formAgreement=\"ngForm\" id=\"formAgreement\" class=\"col-xs-12 col-sm-12 row\">\n \t\t\t\t<div class=\"form-group\" style=\"overflow: hidden;\">\n\t\t\t\t\t<div class=\"col-sm-6\">\n\t\t\t\t\t\t<input type=\"text\" name=\"name\" id=\"name\" class=\"form-control\" placeholder=\"Nombre\" required [(ngModel)]=\"dataAgreement.name\" ngControl=\"name\" (keyup)=\"changeValidateInput('name', formAgreement)\">\n\t\t\t\t\t\t<div style=\"display: none\" id=\"status1\" class=\"text-danger\">¡Campo requerido!</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"col-sm-6\">\n\t\t\t\t\t\t<input type=\"text\" name=\"lastname\" id=\"lastname\" class=\"form-control\" placeholder=\"Apellido\" required [(ngModel)]=\"dataAgreement.lastname\" ngControl=\"lastname\" (keyup)=\"changeValidateInput('lastname', formAgreement)\">\n\t\t\t\t\t\t<div style=\"display: none\" id=\"status2\" class=\"text-danger\">¡Campo requerido!</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"form-group\" style=\"overflow: hidden;\">\n\t\t\t\t\t<div class=\"col-sm-6\">\n\t\t\t\t\t\t<input type=\"text\" name=\"email\" id=\"email\" class=\"form-control\" placeholder=\"Correo electrónico\" required [(ngModel)]=\"dataAgreement.email\" ngControl=\"email\" (keyup)=\"changeValidateInput('email', formAgreement)\">\n\t\t\t\t\t\t<div style=\"display: none\" id=\"status3\" class=\"text-danger\">¡Campo requerido!</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"col-sm-6\">\n\t\t\t\t\t\t<input type=\"text\" name=\"company\" id=\"company\" class=\"form-control\" placeholder=\"Empresa\" required [(ngModel)]=\"dataAgreement.company\" ngControl=\"company\" (keyup)=\"changeValidateInput('company', formAgreement)\">\n\t\t\t\t\t\t<div style=\"display: none\" id=\"status4\" class=\"text-danger\">¡Campo requerido!</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"form-group\" style=\"overflow: hidden;\">\n\t\t\t\t\t<div class=\"col-sm-6\">\n\t\t\t\t\t\t<input type=\"text\" name=\"rol\" id=\"rol\" class=\"form-control\" placeholder=\"Cargo\" required [(ngModel)]=\"dataAgreement.rol\" ngControl=\"rol\" (keyup)=\"changeValidateInput('rol', formAgreement)\">\n\t\t\t\t\t\t<div style=\"display: none\" id=\"status5\" class=\"text-danger\">¡Campo requerido!</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"col-sm-6\">\n\t\t\t\t\t\t<input type=\"text\" name=\"companySize\" id=\"companySize\" class=\"form-control\" placeholder=\"Tamaño de la empresa\" required [(ngModel)]=\"dataAgreement.companySize\" ngControl=\"companySize\" (keyup)=\"changeValidateInput('companySize', formAgreement)\">\n\t\t\t\t\t\t<div style=\"display: none\" id=\"status6\" class=\"text-danger\">¡Campo requerido!</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"form-group\" style=\"overflow: hidden;\">\n\t\t\t\t\t<div class=\"col-sm-6\">\n\t\t\t\t\t\t<input type=\"text\" name=\"phone\" id=\"phone\" class=\"form-control\" placeholder=\"Teléfono de contacto\" required [(ngModel)]=\"dataAgreement.phone\" ngControl=\"phone\" (keyup)=\"changeValidateInput('phone', formAgreement)\">\n\t\t\t\t\t\t<div style=\"display: none\" id=\"status7\" class=\"text-danger\">¡Campo requerido!</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"col-sm-6\">\n\t\t\t\t\t\t\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<p class=\"text-center\" style=\"font-weight:bold;\">\n\t\t            {{responseMessage}}\n\t\t        </p>\n\t\t\t\t<div class=\"col-sm-12 text-center\">\n\t\t\t\t\t<button type=\"button\" id=\"btnViewContribution\" class=\"btn btn-contact\" (click)=\"sendEmail(formAgreement)\" [disabled]=\"loadingProcess\">\n\t\t\t\t\t\t<i *ngIf=\"loadingProcess\" class=\"fa fa-spinner fa-pulse fa-1x fa-fw margin-bottom\"></i>\n\t\t\t\t\t\t<span *ngIf=\"!loadingProcess\">Enviar</span>\n\t\t\t\t\t</button>\n\t\t\t\t</div>\n\t\t\t</form>\n\t\t</div>\n\n\t</div>\n</div>"

/***/ }),

/***/ 505:
/***/ (function(module, exports) {

module.exports = "<div id=\"container-agreement\">\n\t<div class=\"container\">\n\n\t\t<label class=\"col-xs-12 col-sm-12 text-orange text-1-25em text-center\">\n\t\t\tALIANZAS\n\t\t</label>\n\n\t\t<div class=\"col-xs-12 col-sm-12\" id=\"container-benefits\">\n\n\t\t\t<div class=\"margin-top-20 margin-bottom-20\">\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\tOfrezca cupones de descuento beep para sus clientes\n\t\t\t\t</div>\n\t\t\t</div>\t\n\t\t\t<div class=\"margin-top-20 margin-bottom-20\">\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\tOptimice su proceso logístico usando nuestra infraestructura\n\t\t\t\t</div>\n\t\t\t</div>\t\n\t\t\t<div class=\"margin-top-20 margin-bottom-20\">\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\tUtilice a beep como una solución de transporte para sus eventos\n\t\t\t\t</div>\t\n\t\t\t</div>\t\n\t\t\t<div class=\"margin-top-20 margin-bottom-20\">\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\tGane dinero extra contribuyendo al crecimiento de Beep\n\t\t\t\t</div>\t\n\t\t\t</div>\t\n\t\t\t<div class=\"margin-top-20 margin-bottom-20\">\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\tUse beep en grupo con sus compañeros de trabajo o universidad\n\t\t\t\t</div>\t\n\t\t\t</div>\n\t\t</div>\n\n\t\t<div class=\"col-xs-12 col-sm-12\">\n\t\t\t<label class=\"col-xs-12 col-sm-12 row text-1-25em text-center\">\n\t\t\t\tFormulario de contacto\n\t\t\t</label>\n\t\t\t<form #formAlliance=\"ngForm\" id=\"formAlliance\" class=\"col-xs-12 col-sm-12 row\">\n \t\t\t\t<div class=\"form-group\" style=\"overflow: hidden;\">\n\t\t\t\t\t<div class=\"col-sm-6\">\n\t\t\t\t\t\t<input type=\"text\" name=\"name\" id=\"name\" class=\"form-control\" placeholder=\"Nombre\" required [(ngModel)]=\"dataAlliance.name\" ngControl=\"name\" (keyup)=\"changeValidateInput('name', formAlliance)\">\n\t\t\t\t\t\t<div style=\"display: none\" id=\"status1\" class=\"text-danger\">¡Campo requerido!</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"col-sm-6\">\n\t\t\t\t\t\t<input type=\"text\" name=\"lastname\" id=\"lastname\" class=\"form-control\" placeholder=\"Apellido\" required [(ngModel)]=\"dataAlliance.lastname\" ngControl=\"lastname\" (keyup)=\"changeValidateInput('lastname', formAlliance)\">\n\t\t\t\t\t\t<div style=\"display: none\" id=\"status2\" class=\"text-danger\">¡Campo requerido!</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"form-group\" style=\"overflow: hidden;\">\n\t\t\t\t\t<div class=\"col-sm-6\">\n\t\t\t\t\t\t<input type=\"text\" name=\"email\" id=\"email\" class=\"form-control\" placeholder=\"Correo electrónico\" required [(ngModel)]=\"dataAlliance.email\" ngControl=\"email\" (keyup)=\"changeValidateInput('email', formAlliance)\">\n\t\t\t\t\t\t<div style=\"display: none\" id=\"status3\" class=\"text-danger\">¡Campo requerido!</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"col-sm-6\">\n\t\t\t\t\t\t<input type=\"text\" name=\"company\" id=\"company\" class=\"form-control\" placeholder=\"Empresa\" required [(ngModel)]=\"dataAlliance.company\" ngControl=\"company\" (keyup)=\"changeValidateInput('company', formAlliance)\">\n\t\t\t\t\t\t<div style=\"display: none\" id=\"status4\" class=\"text-danger\">¡Campo requerido!</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"form-group\" style=\"overflow: hidden;\">\n\t\t\t\t\t<div class=\"col-sm-6\">\n\t\t\t\t\t\t<input type=\"text\" name=\"rol\" id=\"rol\" class=\"form-control\" placeholder=\"Cargo\" required [(ngModel)]=\"dataAlliance.rol\" ngControl=\"rol\" (keyup)=\"changeValidateInput('rol', formAlliance)\">\n\t\t\t\t\t\t<div style=\"display: none\" id=\"status5\" class=\"text-danger\">¡Campo requerido!</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"col-sm-6\">\n\t\t\t\t\t\t<input type=\"text\" name=\"companySize\" id=\"companySize\" class=\"form-control\" placeholder=\"Tamaño de la empresa\" required [(ngModel)]=\"dataAlliance.companySize\" ngControl=\"companySize\" (keyup)=\"changeValidateInput('companySize', formAlliance)\">\n\t\t\t\t\t\t<div style=\"display: none\" id=\"status6\" class=\"text-danger\">¡Campo requerido!</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"form-group\" style=\"overflow: hidden;\">\n\t\t\t\t\t<div class=\"col-sm-6\">\n\t\t\t\t\t\t<input type=\"text\" name=\"phone\" id=\"phone\" class=\"form-control\" placeholder=\"Teléfono de contacto\" required [(ngModel)]=\"dataAlliance.phone\" ngControl=\"phone\" (keyup)=\"changeValidateInput('phone', formAlliance)\">\n\t\t\t\t\t\t<div style=\"display: none\" id=\"status7\" class=\"text-danger\">¡Campo requerido!</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"col-sm-6\">\n\t\t\t\t\t\t\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<p class=\"text-center\" style=\"font-weight:bold;\">\n\t\t            {{responseMessage}}\n\t\t        </p>\n\t\t\t\t<div class=\"col-sm-12 text-center\">\n\t\t\t\t\t<button type=\"button\" id=\"btnViewContribution\" class=\"btn btn-contact\" (click)=\"sendEmail(formAlliance)\" [disabled]=\"loadingProcess\">\n\t\t\t\t\t\t<i *ngIf=\"loadingProcess\" class=\"fa fa-spinner fa-pulse fa-1x fa-fw margin-bottom\"></i>\n\t\t\t\t\t\t<span *ngIf=\"!loadingProcess\">Enviar</span>\n\t\t\t\t\t</button>\n\t\t\t\t</div>\n\t\t\t</form>\n\t\t</div>\n\n\t</div>\n</div>"

/***/ }),

/***/ 506:
/***/ (function(module, exports) {

module.exports = "<header id=\"header\">\n    <div class=\"container\">\n        <figure id=\"logoUbeep\">\n            <a (click)=\"changeRoute('home')\" ><img src=\"./assets/img/Logo Head.png\" alt=\"Logo\" /></a>\n        </figure>\n        <div class=\"navbar-header\">\n            <button type=\"button\" class=\"navbar-toggle\" (click)=\"isCollapsed = !isCollapsed\">\n            <span class=\"icon-bar\"></span>\n            <span class=\"icon-bar\"></span>\n            <span class=\"icon-bar\"></span>\n            </button>\n        </div>\n        <nav class=\"navbar navbar-right\">\n            <div class=\"\">\n                <div class=\"collapse navbar-collapse\" id=\"navBeep\" (collapsed)=\"collapsed($event)\" (expanded)=\"expanded($event)\" [collapse]=\"isCollapsed\">\n                    <ul class=\"nav navbar-nav navbar-right\">\n                        <li class=\"active\"><a (click)=\"changeRoute('services')\">Solicitar servicio</a></li>\n                        <!--<li><a (click)=\"changeRoute('vehicles')\">Vehículos</a></li>-->\n                        <li dropdown (onToggle)=\"toggled($event)\" id=\"li-beep\">\n                            <a id=\"simple-dropdownn\" dropdownToggle id=\"nav-beep\" (mouseover)=\"showDropdownBeep()\">Beep</a>\n                            <ul style=\"width: 240px;\" id=\"dropdown-beep\" class=\"dropdown-menu dropdown-menu-beep\" dropdownMenu aria-labelledby=\"simple-dropdown\">\n                                <li><a class=\"dropdown-item\" (click)=\"changeRoute('agreement')\">Convenios</a></li>\n                                <li role=\"separator\" class=\"divider\"></li>\n                                <li><a class=\"dropdown-item\" (click)=\"changeRoute('alliance')\">Alianzas</a></li>\n                            </ul>\n                        </li>\n                        <li><a (click)=\"changeRoute('company')\" >Conduzca con nosotros</a></li>\n                        <li *ngIf=\"!statusLogin\">\n                            <a (click)=\"staticModal.show() && viewEmail=false\"  id=\"btnIngresar\"> {{stringNavLogin}} </a>\n                        </li>\n                        <li *ngIf=\"statusLogin\" dropdown (onToggle)=\"toggled($event)\">\n                            <img src=\"./assets/img/Usuario.png\" alt=\"\" (mouseover)=\"showDropdownUser()\" style=\"width: 29px;height: 29px;border-radius: 50%;margin-left: 14px;\"> <a href=\"#\" id=\"simple-dropdown\" id=\"strUserName\" dropdownToggle (mouseover)=\"showDropdownUser()\" style=\"display: inline-block;\">{{stringNavLogin}}  <i  class=\"fa fa-chevron-down\" aria-hidden=\"true\" id=\"simple-dropdown2\" dropdownToggle></i> </a>\n                            <ul id=\"dropdown-user-login\" class=\"dropdown-menu dropdown-menu-beep\" dropdownMenu aria-labelledby=\"simple-dropdown\">\n                                <li><a class=\"dropdown-item\" (click)=\"changeRoute('profile')\">Configurar perfil</a></li>\n                                <li role=\"separator\" class=\"divider\"></li>\n                                <li><a class=\"dropdown-item\" (click)=\"changeRoute('history')\">Historial de solicitudes</a></li>\n                                <li role=\"separator\" class=\"divider\"></li>\n                                <li *ngIf=\"viewHistory\"><a class=\"dropdown-item\" (click)=\"changeRoute('corporative')\">Crédito corporativo</a></li>\n                                <li *ngIf=\"viewHistory\" role=\"separator\" class=\"divider\"></li>\n                                <!--<li><a class=\"dropdown-item\" (click)=\"changeRoute('payment')\">Agregar método de pago</a></li>-->\n                                <!--<li role=\"separator\" class=\"divider\"></li>-->\n                                <li><a class=\"dropdown-item\" (click)=\"changeRoute('invite')\">Invitar a un amigo</a></li>\n                                <li role=\"separator\" class=\"divider\"></li>\n                                <li><a class=\"dropdown-item\" (click)=\"changeRoute('register-company')\">Inscribe tu empresa</a></li>\n                                <li role=\"separator\" class=\"divider\"></li>\n                                <li><a class=\"dropdown-item\" (click)=\"changeRoute('contact-admin')\">Contactar a un administrador</a></li>\n                                <li role=\"separator\" class=\"divider\"></li>\n                                <li><a (click)=\"modalLogUot.show()\" class=\"dropdown-item\">Salir</a></li>\n                            </ul>\n                        </li>\n                    </ul>\n                </div>\n            </div>\n        </nav>\n    </div>\n</header>\n<div class=\"modal fade\" bsModal #staticModal=\"bs-modal\" [config]=\"{backdrop: 'static', keyboard: false}\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\" id=\"modal-login\" >\n    <div class=\"modal-dialog modal-md\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <a class=\"close\" (click)=\"hideChildModal()\" aria-label=\"Close\">\n                    <img src=\"./assets/img/Close.png\" alt=\"Cerrar\">\n                </a>\n                <span>{{titleModal}}</span>\n            </div>\n            <div class=\"modal-body\">\n\n                <div *ngIf=\"login\">\n                    <form *ngIf=\"!viewEmail\" #formLogin=\"ngForm\" class=\"form-horizontal\" >\n                        <div class=\"form-group\">\n                            <input type=\"email\" class=\"form-control col-sm-12 input-lg\" id=\"email-login\" placeholder=\"Correo electrónico\" name=\"emailLogin\" required [(ngModel)]=\"dataLogin.email\" ngControl=\"email\">\n                        </div>\n                        <div class=\"form-group\">\n                            <input type=\"password\" class=\"form-control col-sm-12 input-lg\" id=\"password-login\" placeholder=\"Contraseña\" name=\"passwordLogin\" required required [(ngModel)]=\"dataLogin.pass\" ngControl=\"pass\">\n                        </div>\n                        <p>\n                            <a (click)=\"changeContentModal('forgotpassword')\">Recordar Contraseña</a>\n                        </p>\n                        <p class=\"text-center\" style=\"font-weight:bold;\">\n                            {{messageLogin}}\n                        </p>\n                        <div class=\"form-group\">\n                            <button type=\"button\" id=\"btn-login\" class=\"btn btn-lg\" (click)=\"loginUser(formLogin, true, 'normal')\" [disabled]=\"loadingProcess\">\n                            <i *ngIf=\"loadingProcess\" class=\"fa fa-spinner fa-pulse fa-1x fa-fw margin-bottom\"></i>\n                            <span *ngIf=\"!loadingProcess\">Ingresar</span>\n                            </button>\n                        </div>\n                        <button type=\"button\" class=\"btn\" style=\"display: none;\" id=\"btnViewEmail\"></button>\n                        <div class=\"form-group\">\n                            <!--<a target=\"popup\" (click)=\"checkFacebook()\"> testNewWindow </a>-->\n                            <button type=\"button\" id=\"btn-login-facebook\" class=\"btn btn-lg\" (click)=\"onFacebookCheck('login')\" [disabled]=\"loadingFacebookProcess\">\n                                <i *ngIf=\"loadingFacebookProcess\" class=\"fa fa-spinner fa-pulse fa-1x fa-fw margin-bottom\"></i>\n                            </button>\n                            <button type=\"button\" id=\"btn-login-google\" class=\"btn btn-lg\" (click)=\"handleAuthClick($event, 'login')\" [disabled]=\"loadingFacebookGoogle\">\n                            <i *ngIf=\"loadingFacebookGoogle\" class=\"fa fa-spinner fa-pulse fa-1x fa-fw margin-bottom\"></i>\n                            </button>\n                        </div>\n                        <p id=\"link-register\">\n                            <a (click)=\"changeContentModal('register')\">Crear nueva cuenta</a>\n                        </p>\n                    </form>\n\n                    <form *ngIf=\"viewEmail\" #formLoginFacebook=\"ngForm\" class=\"form-horizontal\" >\n                        <p class=\"text-center\" style=\"font-weight:bold;\">\n                            Ingrese la siguiente información\n                        </p>\n                        <div class=\"form-group\" *ngIf=\"typeRegisterSocial=='facebook'\">\n                            <input type=\"email\" class=\"form-control col-sm-12 input-lg\" id=\"email-login\" placeholder=\"Correo electrónico\" name=\"emailLogin\" required [(ngModel)]=\"socialRegister.email\" ngControl=\"email\">\n                        </div>\n                        <div class=\"form-group\">\n                            <input type=\"text\" class=\"form-control col-sm-12 input-lg\" id=\"identify-register\" placeholder=\"Número de identificación\" name=\"identify\" required [(ngModel)]=\"socialRegister.identify\" ngControl=\"identify\">\n                        </div>\n                        <div class=\"form-group\">\n                            <input type=\"text\" class=\"form-control col-sm-12 input-lg\" id=\"cell-phone-register\" placeholder=\"Celular\" name=\"cellPhone\" required [(ngModel)]=\"socialRegister.cellPhone\" ngControl=\"cellPhone\">\n                        </div>\n                        <div class=\"form-group\">\n                            <select class=\"form-control col-sm-12 input-lg\" id=\"id_agreement\" name=\"id_agreement\" required [(ngModel)]=\"socialRegister.id_agreement\" ngControl=\"id_agreement\">\n                                <option *ngFor=\"let initial of objInitialSetup; let i = index\" [selected]=\"initial.id == 0\" [value]=\"initial.id\">\n                                    {{initial.name}}\n                                </option>\n                            </select>\n                        </div>\n                        <div class=\"form-group\">\n                            <input type=\"text\" class=\"form-control col-sm-12 input-lg\" id=\"code_agreement\" placeholder=\"Código de convenio\" name=\"code_agreement\" [(ngModel)]=\"socialRegister.code_agreement\" required ngControl=\"code_agreement\">\n                        </div>\n                        <div class=\"form-group\">\n                            <button type=\"button\" id=\"btn-login\" class=\"btn btn-lg\" (click)=\"loginUserFacebook(formLoginFacebook)\" [disabled]=\"loadingProcess\">\n                            <i *ngIf=\"loadingProcess\" class=\"fa fa-spinner fa-pulse fa-1x fa-fw margin-bottom\"></i>\n                            <span *ngIf=\"!loadingProcess\">Ingresar</span>\n                            </button>\n                        </div>\n                    </form>\n                </div>\n                <div *ngIf=\"register\">\n                    <form *ngIf=\"!viewEmail\" #formRegister=\"ngForm\" class=\"form-horizontal\">\n                        <div class=\"form-group\">\n                            <input type=\"text\" class=\"form-control col-sm-12 input-lg\" id=\"user-name-register\" placeholder=\"Nombre de Usuario\" name=\"userNameRegister\" required [(ngModel)]=\"registerUserData.name\" ngControl=\"name\">\n                        </div>\n                        <div class=\"form-group\">\n                            <input type=\"email\" class=\"form-control col-sm-12 input-lg\" id=\"email-register\" placeholder=\"Correo Electrónico\" name=\"emailRegister\" required [(ngModel)]=\"registerUserData.email\" ngControl=\"email\">\n                        </div>\n                        <div class=\"form-group\">\n                            <input type=\"text\" class=\"form-control col-sm-12 input-lg\" id=\"identify-register\" placeholder=\"Número de identificación\" name=\"identifyRegister\" required [(ngModel)]=\"registerUserData.identify\" ngControl=\"identify\">\n                        </div>\n                        <div class=\"form-group\">\n                            <input type=\"text\" class=\"form-control col-sm-12 input-lg\" id=\"cell-phone-register\" placeholder=\"Celular\" name=\"cellPhoneRegister\" required [(ngModel)]=\"registerUserData.cellphone\" ngControl=\"phone\">\n                        </div>\n                        <div class=\"form-group\">\n                            <input type=\"password\" class=\"form-control col-sm-12 input-lg\" id=\"password-register\" placeholder=\"Contraseña\" name=\"passwordRegister\" required [(ngModel)]=\"registerUserData.pass\" ngControl=\"pass\">\n                        </div>\n                        <div class=\"form-group\">\n                            <select class=\"form-control col-sm-12 input-lg\" id=\"id_agreement\" name=\"id_agreement\" required [(ngModel)]=\"registerUserData.id_agreement\" ngControl=\"id_agreement\">\n                              <option *ngFor=\"let initial of objInitialSetup; let i = index\" [selected]=\"initial.id == 0\" [value]=\"initial.id\">\n                                {{initial.name}}\n                              </option>\n                            </select>\n                        </div>\n                        <div class=\"form-group\">\n                            <input type=\"text\" class=\"form-control col-sm-12 input-lg\" id=\"code_agreement\" placeholder=\"Código de convenio\" name=\"codeAgreement\" required [(ngModel)]=\"registerUserData.code_agreement\" ngControl=\"code_agreement\">\n                        </div>\n                        <div class=\"form-group text-center\" id=\"textSecurity\">\n                            <div>\n                                Al registrarte acepta las <a href=\"#/security-policies\"  target=\"_blank\" class=\"font-bold cursor-pointer\">políticas de privacidad</a> y los <a href=\"#/privacy-policies\"  target=\"_blank\" class=\"font-bold cursor-pointer\">términos y condiciones</a> de Beep\n                            </div>\n                        </div>\n\n                        <p class=\"text-center font-bold\">\n                            {{messageRegister}}\n                        </p>\n\n                        <div class=\"form-group\">\n                            <button type=\"button\" id=\"btn-create\" class=\"btn btn-lg\" (click)=\"registerUser(formRegister)\" [disabled]=\"loadingProcess\">\n                            <i *ngIf=\"loadingProcess\" class=\"fa fa-spinner fa-pulse fa-1x fa-fw margin-bottom\"></i>\n                            <span *ngIf=\"!loadingProcess\">Crear cuenta</span>\n                            </button>\n                        </div>\n                        <div class=\"form-group\">\n                            <button type=\"button\" id=\"btn-login-facebook\" class=\"btn btn-lg\" (click)=\"onFacebookCheck('login')\" [disabled]=\"loadingFacebookProcess\">\n                            <i *ngIf=\"loadingFacebookProcess\" class=\"fa fa-spinner fa-pulse fa-1x fa-fw margin-bottom\"></i>\n                            </button>\n                            <button type=\"button\" id=\"btn-login-google\" class=\"btn btn-lg\" (click)=\"handleAuthClick($event, 'login')\" [disabled]=\"loadingFacebookGoogle\">\n                            <i *ngIf=\"loadingFacebookGoogle\" class=\"fa fa-spinner fa-pulse fa-1x fa-fw margin-bottom\"></i>\n                            </button>\n                        </div>\n                        <p id=\"link-login\">\n                            <a (click)=\"changeContentModal('login')\">Ya tengo cuenta</a>\n                        </p>\n                    </form>\n                    <form *ngIf=\"viewEmail\" #formLoginSocial=\"ngForm\" class=\"form-horizontal\" >\n                        <div class=\"form-group\">\n                            <input type=\"email\" class=\"form-control col-sm-12 input-lg\" id=\"email-login\" placeholder=\"Correo electrónico\" name=\"emailLogin\" required ngControl=\"email\">\n                        </div>\n                        <div class=\"form-group\">\n                            <input type=\"text\" class=\"form-control col-sm-12 input-lg\" id=\"identify-register\" placeholder=\"Número de identificación\" name=\"identify\" required ngControl=\"identify\">\n                        </div>\n                        <div class=\"form-group\">\n                            <input type=\"text\" class=\"form-control col-sm-12 input-lg\" id=\"cell-phone-register\" placeholder=\"Celular\" name=\"cellPhone\" required ngControl=\"phone\">\n                        </div>\n                        <div class=\"form-group\">\n                            <select class=\"form-control col-sm-12 input-lg\" id=\"id_agreement\" name=\"id_agreement\" required ngControl=\"id_agreement\">\n                                <option *ngFor=\"let initial of objInitialSetup; let i = index\" [selected]=\"initial.id == 0\" [value]=\"initial.id\">\n                                    {{initial.name}}\n                                </option>\n                            </select>\n                        </div>\n                        <div class=\"form-group\">\n                            <input type=\"text\" class=\"form-control col-sm-12 input-lg\" id=\"code_agreement\" placeholder=\"Código de convenio\" name=\"code_agreement\" [(ngModel)]=\"code_agreement\" required ngControl=\"code_agreement\">\n                        </div>\n                        <p class=\"text-center\" style=\"font-weight:bold;\">\n                            Ingrese su correo electrónico\n                        </p>\n                        <div class=\"form-group\">\n                            <button type=\"button\" id=\"btn-login\" class=\"btn btn-lg\" (click)=\"loginUserFacebook(formLoginSocial)\" [disabled]=\"loadingProcess\">\n                            <i *ngIf=\"loadingProcess\" class=\"fa fa-spinner fa-pulse fa-1x fa-fw margin-bottom\"></i>\n                            <span *ngIf=\"!loadingProcess\">Ingresar</span>\n                            </button>\n                        </div>\n                    </form>\n                </div>\n                <div *ngIf=\"forgotpassword\">\n                    <form #formForgotPassword=\"ngForm\" class=\"form-horizontal\">\n                        <div class=\"form-group\">\n                            <input type=\"text\" class=\"form-control col-sm-12 input-lg\" id=\"email-forgot\" placeholder=\"Correo electrónico\" name=\"emailForgot\" required [(ngModel)]=\"dataForgot.email\" ngControl=\"email\">\n                            <div class=\"text-center\">\n                                {{messageForgot}}\n                            </div>\n                        </div>\n                        <div class=\"form-group\">\n                            <button type=\"button\" id=\"btn-forgot-password\" class=\"btn btn-lg\" (click)=\"forgotPassword(formForgotPassword)\" [disabled]=\"loadingProcess\">\n                            <i *ngIf=\"loadingProcess\" class=\"fa fa-spinner fa-pulse fa-1x fa-fw margin-bottom\"></i>\n                            <span *ngIf=\"!loadingProcess\">Enviar</span>\n                            </button>\n                        </div>\n                    </form>\n                </div>\n\n            </div>\n        </div>\n    </div>\n</div>\n\n<div class=\"modal fade\" bsModal #modalCompleteDataUserSocial=\"bs-modal\" [config]=\"{backdrop: 'static', keyboard: false}\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\" id=\"modal-login\" >\n    <div class=\"modal-dialog modal-md\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <span>Complete sus datos</span>\n            </div>\n            <div class=\"modal-body\">\n                <form #formCompleteDataUserComplete=\"ngForm\" class=\"form-horizontal\">\n                    <div class=\"form-group\">\n                        <input type=\"text\" class=\"form-control col-sm-12 input-lg\" id=\"identify-register\" placeholder=\"Número de identificación\" name=\"identify\" required [(ngModel)]=\"dataUser.identify\" ngControl=\"identify\">\n                    </div>\n                    <div class=\"form-group\">\n                        <input type=\"text\" class=\"form-control col-sm-12 input-lg\" id=\"cell-phone-register\" placeholder=\"Celular\" name=\"cellPhone\" required [(ngModel)]=\"dataUser.phone\" ngControl=\"phone\">\n                    </div>\n                    <div class=\"form-group\">\n                        <select class=\"form-control col-sm-12 input-lg\" id=\"id_agreement\" name=\"id_agreement\" required [(ngModel)]=\"select_id_agreement\" ngControl=\"id_agreement\">\n                          <option *ngFor=\"let initial of objInitialSetup; let i = index\" [selected]=\"initial.id == 0\" [value]=\"initial.id\">\n                            {{initial.name}}\n                          </option>\n                        </select>\n                    </div>\n                    <div class=\"form-group\">\n                        <input type=\"text\" class=\"form-control col-sm-12 input-lg\" id=\"code_agreement\" placeholder=\"Código de convenio\" name=\"code_agreement\" [(ngModel)]=\"code_agreement\" required ngControl=\"code_agreement\">\n                    </div>\n\n                    <p class=\"text-center\" style=\"font-weight:bold;\">\n                        {{messageCompleteData}}\n                    </p>\n                    <div class=\"form-group\">\n                        <button type=\"button\" id=\"btn-create\" class=\"btn btn-lg\" (click)=\"completeDataSocial(formCompleteDataUserComplete, 'notEmailSave')\" [disabled]=\"loadingProcess\">\n                        <i *ngIf=\"loadingProcess\" class=\"fa fa-spinner fa-pulse fa-1x fa-fw margin-bottom\"></i>\n                        <span *ngIf=\"!loadingProcess\">Completar datos</span>\n                        </button>\n                    </div>\n                </form>\n            </div>\n        </div>\n    </div>\n</div>\n\n<div class=\"modal fade\" bsModal #modalCompleteDataUserSocialEmailSave=\"bs-modal\" [config]=\"{backdrop: 'static', keyboard: false}\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\" id=\"modal-login\" >\n    <div class=\"modal-dialog modal-md\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <span>Complete sus datos</span>\n            </div>\n            <div class=\"modal-body\">\n                <form #formCompleteDataUserComplete=\"ngForm\" class=\"form-horizontal\">\n                    <div class=\"form-group\">\n                        <input type=\"text\" class=\"form-control col-sm-12 input-lg\" id=\"identify-register\" placeholder=\"Número de identificación\" name=\"identify\" required [(ngModel)]=\"dataUser.identify\" ngControl=\"identify\">\n                    </div>\n                    <div class=\"form-group\">\n                        <input type=\"text\" class=\"form-control col-sm-12 input-lg\" id=\"cell-phone-register\" placeholder=\"Celular\" name=\"cellPhone\" required [(ngModel)]=\"dataUser.phone\" ngControl=\"phone\">\n                    </div>\n                    <div class=\"form-group\">\n                        <select class=\"form-control col-sm-12 input-lg\" id=\"id_agreement\" name=\"id_agreement\" required [(ngModel)]=\"select_id_agreement\" ngControl=\"id_agreement\">\n                            <option *ngFor=\"let initial of objInitialSetup; let i = index\" [selected]=\"initial.id == 0\" [value]=\"initial.id\">\n                                {{initial.name}}\n                            </option>\n                        </select>\n                    </div>\n                    <div class=\"form-group\">\n                        <input type=\"text\" class=\"form-control col-sm-12 input-lg\" id=\"code_agreement\" placeholder=\"Código de convenio\" name=\"code_agreement\" [(ngModel)]=\"code_agreement\" required ngControl=\"code_agreement\">\n                    </div>\n\n                    <p class=\"text-center\" style=\"font-weight:bold;\">\n                        {{messageCompleteData}}\n                    </p>\n                    <div class=\"form-group\">\n                        <button type=\"button\" id=\"btn-create\" class=\"btn btn-lg\" (click)=\"completeDataSocial(formCompleteDataUserComplete, 'emailSave')\" [disabled]=\"loadingProcess\">\n                            <i *ngIf=\"loadingProcess\" class=\"fa fa-spinner fa-pulse fa-1x fa-fw margin-bottom\"></i>\n                            <span *ngIf=\"!loadingProcess\">Completar datos</span>\n                        </button>\n                    </div>\n                </form>\n            </div>\n        </div>\n    </div>\n</div>\n\n<div class=\"modal fade\" bsModal #modalCompleteDataUser=\"bs-modal\" [config]=\"{backdrop: 'static', keyboard: false}\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\" id=\"modal-login\" >\n    <div class=\"modal-dialog modal-md\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <span>Complete sus datos</span>\n            </div>\n            <div class=\"modal-body\">\n                <form #formCompleteDataUser=\"ngForm\" class=\"form-horizontal\">\n                    <div class=\"form-group\">\n                        <input type=\"text\" class=\"form-control col-sm-12 input-lg\" id=\"identify-register\" placeholder=\"Número de identificación\" name=\"identify\" required [(ngModel)]=\"dataUser.identify\" ngControl=\"identify\">\n                    </div>\n                    <div class=\"form-group\">\n                        <input type=\"text\" class=\"form-control col-sm-12 input-lg\" id=\"cell-phone-register\" placeholder=\"Celular\" name=\"cellPhone\" required [(ngModel)]=\"dataUser.phone\" ngControl=\"phone\">\n                    </div>\n\n                    <p class=\"text-center\" style=\"font-weight:bold;\">\n                        {{messageCompleteData}}\n                    </p>\n                    <div class=\"form-group\">\n                        <button type=\"button\" id=\"btn-create\" class=\"btn btn-lg\" (click)=\"completeData(formCompleteDataUser)\" [disabled]=\"loadingProcess\">\n                        <i *ngIf=\"loadingProcess\" class=\"fa fa-spinner fa-pulse fa-1x fa-fw margin-bottom\"></i>\n                        <span *ngIf=\"!loadingProcess\">Completar datos</span>\n                        </button>\n                    </div>\n                </form>\n            </div>\n        </div>\n    </div>\n</div>\n\n<div class=\"modal fade\" bsModal #modalLogUot=\"bs-modal\" [config]=\"{backdrop: 'static', keyboard: false}\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\" id=\"modal-login\" >\n    <div class=\"modal-dialog modal-md\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <a class=\"close\" (click)=\"modalLogUot.hide()\" aria-label=\"Close\">\n                    <img src=\"./assets/img/Close.png\" alt=\"Cerrar\">\n                </a>\n                <span>¿Está seguro que desea salir?</span>\n            </div>\n            <div class=\"modal-body\">\n                <div class=\"form-group\">\n                    <button type=\"button\" class=\"btn btn-lg btn-orange\" style=\"background-color: #e04b31;color:#FFFFFF;\" (click)=\"logOutUser('si')\" >Si</button>\n                    <button type=\"button\" class=\"btn btn-lg\" (click)=\"logOutUser('no') && hideChildModal()\" >No</button>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n<div class=\"modal fade\" bsModal #modalConfirmCancelService=\"bs-modal\" [config]=\"{backdrop: 'static', keyboard: false}\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\" id=\"modal-login\" >\n    <div class=\"modal-dialog modal-md\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <a class=\"close\" (click)=\"confirmCancelService('no')\" aria-label=\"Close\">\n                    <img src=\"./assets/img/Close.png\" alt=\"Cerrar\">\n                </a>\n                <span>¿Está seguro que desea salir?</span>\n            </div>\n            <div class=\"modal-body\">\n                <div class=\"form-group text-center\">\n                    <span>Si sale de esta página su solicitud será cancelada</span>\n                    <br>\n                    <br>\n                    <button type=\"button\" class=\"btn btn-lg btn-orange\" style=\"background-color: #e04b31;color:#FFFFFF;\" (click)=\"confirmCancelService('si')\" >Si</button>\n                    <button type=\"button\" class=\"btn btn-lg\" (click)=\"confirmCancelService('no')\" >No</button>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n<div class=\"modal fade\" bsModal #modalExitViewServices=\"bs-modal\" [config]=\"{backdrop: 'static', keyboard: false}\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\" id=\"modal-login\" >\n    <div class=\"modal-dialog modal-md\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <a class=\"close\" (click)=\"exitViewService('no')\" aria-label=\"Close\">\n                    <img src=\"./assets/img/Close.png\" alt=\"Cerrar\">\n                </a>\n                <span>¿Está seguro que desea salir?</span>\n            </div>\n            <div class=\"modal-body\">\n                <div class=\"form-group text-center\">\n                    <span>Si sale de esta página los datos serán eliminados</span>\n                    <br>\n                    <br>\n                    <button type=\"button\" class=\"btn btn-lg btn-orange\" style=\"background-color: #e04b31;color:#FFFFFF;\" (click)=\"exitViewService('si')\" >Si</button>\n                    <button type=\"button\" class=\"btn btn-lg\" (click)=\"exitViewService('no')\" >No</button>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n\n<div class=\"modal fade\" bsModal #timeSocketModal=\"bs-modal\" [config]=\"{backdrop: 'static', keyboard: false}\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\" id=\"modal-login\" >\n    <div class=\"modal-dialog modal-md\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <a class=\"close\" (click)=\"timeSocketModal.hide()\" aria-label=\"Close\">\n                    <img src=\"./assets/img/Close.png\" alt=\"Cerrar\">\n                </a>\n                <span>&nbsp;</span>\n            </div>\n            <div class=\"modal-body\">\n                <div class=\"form-group text-center\">\n                    <span>Lo sentimos, aún no puede solicitar un servicio, debe esperar aproximadamente 1 minuto</span>\n                    <br>\n                    <br>\n                    <button type=\"button\" class=\"btn btn-lg btn-orange\" style=\"background-color: #e04b31;color:#FFFFFF;\" (click)=\"timeSocketModal.hide()\" >Aceptar</button>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n\n\n\n<router-outlet></router-outlet>\n<footer>\n    <div id=\"container-info-footer\">\n        <div class=\"row\">\n            <div class=\"container\">\n                <div class=\"col-xs-12 col-sm-12\">\n                    <div class=\"row\">\n                        <div class=\"col-sm-4\">\n                            <figure>\n                                <img src=\"./assets/img/Logo Head.png\" alt=\"\">\n                            </figure>\n                            <div class=\"font-bold\">\n                                <div>Dirección Bogotá: Carrera 85ª # 52ª-41 - Barrio Los Monjes</div>\n                                <div class=\"font-gothamRnd-medium\">Bogotá - Colombia</div>\n                                <div>Conmutador: 4 171 717</div>\n                                <div class=\"font-gothamRnd-medium\">Correo electrónico: servicioalcliente@ubeep.co</div>\n                            </div>\n                        </div>\n                        <div class=\"col-sm-2 container-info-footer\">\n\n                        </div>\n                        <div class=\"col-sm-2 container-info-footer\">\n                            <div class=\"\">\n                                <div for=\"\" class=\"font-gothamRnd-medium text-1-25em\">Compañía</div>\n                                <p (click)=\"changeRoute('beep')\" class=\"cursor-pointer font-gothamRnd-book\">Acerca de Beep</p>\n                                <p (click)=\"changeRoute('press')\" class=\"cursor-pointer font-gothamRnd-book\">Prensa</p>\n                                <p (click)=\"changeRoute('company')\" class=\"cursor-pointer font-gothamRnd-book\">Trabaje con nosotros</p>\n                            </div>\n                        </div>\n                        <div class=\"col-sm-2 container-info-footer\">\n                            <div class=\"\">\n                                <div for=\"\" class=\"font-gothamRnd-medium text-1-25em\">Beep Corporate</div>\n                                <p (click)=\"changeRoute('agreement')\" class=\"cursor-pointer font-gothamRnd-book\">Convenios</p>\n                                <p (click)=\"changeRoute('alliance')\" class=\"cursor-pointer font-gothamRnd-book\">Alianzas</p>\n                            </div>\n                        </div>\n                        <div class=\"col-sm-2 container-info-footer\">\n                            <div class=\"\">\n                                <div for=\"\" class=\"font-gothamRnd-medium text-1-25em\">Viaje</div>\n                                <p (click)=\"changeRoute('faq')\" class=\"cursor-pointer font-gothamRnd-book\">Preguntas frecuentes</p>\n                                <p (click)=\"changeRoute('security')\" class=\"cursor-pointer font-gothamRnd-book\">Seguridad</p>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n    <div id=\"container-social-network\">\n        <div class=\"container\">\n            <div class=\"col-xs-12 col-sm-12 container-middle\">\n                <div class=\"row col-sm-6 content-middle-left\">\n                    <div class=\"label-social-network\">\n                        <span><a id=\"terminosA\" class=\"font-gothamRnd-medium\" style=\"color: white;text-decoration: none;\" (click)=\"changeRoute('privacy-policies')\" >Términos y condiciones</a></span>\n                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n                        <span><a style=\"color: white;text-decoration: none;\" (click)=\"changeRoute('security-policies')\" class=\"font-gothamRnd-medium\">Políticas de seguridad</a></span>\n                    </div>\n                </div>\n                <div class=\"row col-sm-6 content-middle-left text-right\">\n                    <div class=\"label-social-network font-gothamRnd-medium\">Síguenos en redes:</div>\n                    <div class=\"container-img-social-network\">\n                        <figure>\n                            <a href=\"https://www.facebook.com/Beep-280538782328892/?fref=ts\" target=\"_blank\" tooltip=\"Facebook\"><img id=\"facebookUbeep\"></a>\n                        </figure>\n                        <figure>\n                            <a href=\"https://www.instagram.com/beep_col/\" target=\"_blank\" tooltip=\"Instagram\"><img id=\"instagramUbeep\"></a>\n                        </figure>\n                        <figure>\n                            <a href=\"https://twitter.com\" target=\"_blank\" tooltip=\"Twitter\"><img id=\"twitterUbeep\"></a>\n                        </figure>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</footer>\n"

/***/ }),

/***/ 507:
/***/ (function(module, exports) {

module.exports = "<div id=\"container-beep\">\n\t<div class=\"container\">\n\t\t\n\t\t<div class=\"text-center title font-gothamRnd-bold\">\n\t\t\tACERCA DE BEEP\n\t\t</div>\n\t\t\n\t\t<div>\n\t\t\t<p class=\"content font-gothamRnd-book\">\n\t\t\t\tBeep es una aplicación web y móvil que se integra al esquema <b>NEXT TRANSPORT</b> con el fin de mejorar la experiencia de los usuarios y facilitar el trabajo de los conductores. Con un modelo enfocado en la legalidad como pilar fundamental, Beep aparece como la solución más eficiente y segura del mercado.\n\t\t\t</p>\n\t\t\t<p class=\"content font-gothamRnd-book\">\n\t\t\t\tEsta aplicación estará disponible para todos nuestros clientes de forma gratuita y su flexibilidad le permitirá adaptarse a las necesidades puntuales de cada uno de ellos.\n\t\t\t</p>\n\t\t</div>\n\t\t<div class=\"text-center\">\n\t\t\t<figure>\n\t\t\t\t<img class=\"img-screenshots\" src=\"./assets/img/screenshots.png\" alt=\"¡Beep!\">\n\t\t\t</figure>\n\t\t</div>\n\t\t<div class=\"text-center\">\n\t\t\t<p class=\"content\">\n\t\t\t\t<span class=\"font-gothamRnd-bold\">Descárguela ahora</span>\n\t\t\t</p>\n\t\t\t<p class=\"content font-gothamRnd-book\">\n\t\t\t\tY empiece a disfrutar de la experiencia Beep\n\t\t\t</p>\n\t\t\t<div>\n\t\t\t\t<figure>\n\t\t\t\t\t<img class=\"img-store\" src=\"./assets/img/appStore.png\" alt=\"App Store\">\n\t\t\t\t\t<img class=\"img-store\" src=\"./assets/img/googlePlay.png\" alt=\"Google Play\">\n\t\t\t\t</figure>\n\t\t\t</div>\n\t\t</div>\n\t\t\n\t</div>\n</div>"

/***/ }),

/***/ 508:
/***/ (function(module, exports) {

module.exports = "<div id=\"container-drive\" >\n\t<div *ngIf=\"viewInfo\" style=\"overflow: hidden;\">\n\t\t<div class=\"container-banner \">\n\t\t\t<div class=\"container container-middle\">\n\t\t\t\t<div class=\"col-sm-8 content-middle-right\">\n\t\t\t\t\t<span class=\"text-3em\"  style=\"color:white;\">¿Qué es Beep?</span>\n\t\t\t\t\t<br/>\n\t\t\t\t\t<hr>\n\t\t\t\t\t<p class=\"text-1-5em\" style=\"color:white;\">\n\t\t\t\t\t\tBeep es una aplicación web y móvil que conecta a pasajeros que necesiten viajar con conductores que ofrecen el servicio de transporte especial.\n\t\t\t\t\t</p>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-sm-4 text-center\">\n\t\t\t\t\t<figure id=\"container-img-phone\">\n\t\t\t\t\t\t<img src=\"./assets/img/app.png\" alt=\"\" id=\"img-phone\">\n\t\t\t\t\t</figure>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"container container-info\">\n\t\t\t<div class=\"col-sm-12\">\n\t\t\t\t<div class=\"\">\n\t\t\t\t\t<div class=\"col-sm-4\">\n\t\t\t\t\t\t<div class=\"row text-center\">\n\t\t\t\t\t\t\t<figure id=\"container-img\">\n\t\t\t\t\t\t\t\t<img src=\"./assets/img/legalidad.png\" alt=\"\">\n\t\t\t\t\t\t\t</figure>\n\t\t\t\t\t\t\t<p class=\"text-1-25em text-orange font-bold\">\n\t\t\t\t\t\t\t\tLEGALIDAD\n\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\n\t\t\t\t\t\t<div class=\"padding-border\">\n\t\t\t\t\t\t\t<hr>\n\t\t\t\t\t\t\tBeep únicamente registra a conductores que cumplen con la normativa expedida por el Ministerio de Transporte, por tanto sus pasajeros siempre estarán tranquilos de que el servicio de transporte que les es prestado es totalmente legal. \n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"col-sm-4\">\n\t\t\t\t\t\t<div class=\"row text-center\">\n\t\t\t\t\t\t\t<figure id=\"container-img\">\n\t\t\t\t\t\t\t\t<img src=\"./assets/img/ingresosAdicionales.png\" alt=\"\">\n\t\t\t\t\t\t\t</figure>\n\t\t\t\t\t\t\t<p class=\"text-1-25em text-orange font-bold\">\n\t\t\t\t\t\t\t\tINGRESOS ADICIONALES\n\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\n\t\t\t\t\t\t<div class=\"padding-border\">\n\t\t\t\t\t\t\t<hr>\n\t\t\t\t\t\t\tBeep es una solución integral que pretende no solo resolver los aspectos de legalidad y seguridad asociados al servicio de transporte sino que además busca mejorar la calidad de vida de los conductores ofreciendo opciones de ingreso adicionales.\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"col-sm-4\">\n\t\t\t\t\t\t<div class=\"row text-center\">\n\t\t\t\t\t\t\t<figure id=\"container-img\">\n\t\t\t\t\t\t\t\t<img src=\"./assets/img/cooperativa.png\" alt=\"\">\n\t\t\t\t\t\t\t</figure>\n\t\t\t\t\t\t\t<p class=\"text-1-25em text-orange font-bold\">\n\t\t\t\t\t\t\t\tCOOPERATIVA\n\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\n\t\t\t\t\t\t<div class=\"padding-border\">\n\t\t\t\t\t\t\t<hr>\n\t\t\t\t\t\t\tUsted podrá afiliarse a Beep siguiendo siendo un transportista independiente o afiliandose a la cooperativa de transporte “xxx” la cual tiene convenio exclusivo con Beep.\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"col-sm-12 text-center\">\n\t\t\t<a href=\"#viewForm\">\n\t\t\t\t<button type=\"button\" class=\"btn btn-contact margin-top-100\" (click)=\"nextView('form');\">\n\t\t\t\t\tRegistrarse\n\t\t\t\t</button>\n\t\t\t</a>\n\t\t</div>\n\t</div>\n\t\n\t<div *ngIf=\"viewForm\" class=\"container\" id=\"viewForm\">\n\t\t<form #formContact=\"ngForm\">\n\t\t\t<div class=\"col-sm-12 container-info text-center\">\n\t\t\t\t<p class=\"text-1-8em\">¿Cómo quisiera unirse?</p>\n\t\t\t\t<p>\n\t\t\t\t\tSeleccione la opción como quiere unirse a nuestra compañía\n\t\t\t\t\t<br>\n\t\t\t\t\ty diligencie el formulario\n\t\t\t\t</p>\n\t\t\t</div>\n\t\t\t<div class=\"col-sm-12 container-imgs-driver text-center\">\n\t\t\t\t<input type=\"hidden\" name=\"type\"\n\t\t\t\tid=\"type\"\n\t\t\t\trequired\n\t\t\t\t[(ngModel)]=\"dataContact.type\"\n\t\t\t\tngControl=\"type\">\n\t\t\t\t<div class=\"container-drive\" id=\"btnVehicle \" (click)=\"changeType('Tengo un vehículo')\">\n\t\t\t\t\t<figure class=\"container-img-drive\" id=\"img-vehicle\">\n\t\t\t\t\t</figure>\n\t\t\t\t\t<div class=\"container-text-img\">\n\t\t\t\t\t\tTengo un vehículo\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"container-drive\" id=\"btnVehicles\" (click)=\"changeType('Tengo una flota de vehículos')\">\n\t\t\t\t\t<figure class=\"container-img-drive\" id=\"img-vehicles\">\n\t\t\t\t\t</figure>\n\t\t\t\t\t<div class=\"container-text-img\">\n\t\t\t\t\t\tTengo una flota de vehículos\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"container-drive\" id=\"btnDrive\" (click)=\"changeType('Soy conductor')\">\n\t\t\t\t\t<figure class=\"container-img-drive\" id=\"img-drive\">\n\t\t\t\t\t</figure>\n\t\t\t\t\t<div class=\"container-text-img\">\n\t\t\t\t\t\tSoy conductor\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"col-sm-12\" style=\"margin-bottom: 20px;\">\n\t\t\t\t<div style=\"display: none\" id=\"status1\" class=\"text-center text-danger\">¡Campo requerido!</div>\n\t\t\t</div>\n\t\t\t\n\t\t\t<div class=\"col-sm-8 col-sm-offset-2 text-center\">\n\t\t\t\t<p class=\"text-1-25em\">Formulario de contacto</p>\n\t\t\t\t<br>\n\t\t\t\t<div class=\"form-group\" style=\"overflow: hidden;\">\n\t\t\t\t\t<div class=\"col-sm-6\">\n\t\t\t\t\t\t<input type=\"text\" name=\"name\" id=\"name\" class=\"form-control\" placeholder=\"Nombre\" required [(ngModel)]=\"dataContact.name\" ngControl=\"name\" (keyup)=\"changeValidateInput('name', formContact)\">\n\t\t\t\t\t\t<div style=\"display: none\" id=\"status2\" class=\"text-danger\">¡Campo requerido!</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"col-sm-6\">\n\t\t\t\t\t\t<input type=\"text\" name=\"lastname\" id=\"lastname\" class=\"form-control\" placeholder=\"Apellido\" required [(ngModel)]=\"dataContact.lastname\" ngControl=\"lastname\"  (keyup)=\"changeValidateInput('lastname', formContact)\">\n\t\t\t\t\t\t<div style=\"display: none\" id=\"status3\" class=\"text-danger\">¡Campo requerido!</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"form-group\" style=\"overflow: hidden;\">\n\t\t\t\t\t<div class=\"col-sm-6\">\n\t\t\t\t\t\t<input type=\"text\" name=\"phone\" id=\"phone\" class=\"form-control\" placeholder=\"Teléfono\" required [(ngModel)]=\"dataContact.phone\" ngControl=\"phone\" (keyup)=\"changeValidateInput('phone', formContact)\">\n\t\t\t\t\t\t<div style=\"display: none\" id=\"status4\" class=\"text-danger\">¡Campo requerido!</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"col-sm-6\">\n\t\t\t\t\t\t<input type=\"text\" name=\"address\" id=\"address\" class=\"form-control\" placeholder=\"Dirección\" required [(ngModel)]=\"dataContact.address\" ngControl=\"address\" (keyup)=\"changeValidateInput('address', formContact)\">\n\t\t\t\t\t\t<div style=\"display: none\" id=\"status5\" class=\"text-danger\">¡Campo requerido!</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"form-group\" style=\"overflow: hidden;\">\n\t\t\t\t\t<div class=\"col-sm-6\">\n\t\t\t\t\t\t<input type=\"text\" name=\"email\" id=\"email\" class=\"form-control\" placeholder=\"Correo electrónico\" required [(ngModel)]=\"dataContact.email\" ngControl=\"email\" (keyup)=\"changeValidateInput('email', formContact)\">\n\t\t\t\t\t\t<div style=\"display: none\" id=\"status6\" class=\"text-danger\">¡Campo requerido!</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"col-sm-6\">\n\t\t\t\t\t\t<select class=\"form-control\" name=\"reason\" id=\"reason\" required [(ngModel)]=\"dataContact.reason\" ngControl=\"reason\" (mouseup)=\"changeValidateInput('reason', formContact)\">\n\t\t\t\t\t\t\t<option value=\"\">¿Cómo se enteró?</option>\n\t\t\t\t\t\t\t<option value=\"Redes Sociales\">Redes Sociales</option>\n\t\t\t\t\t\t\t<option value=\"Medio de comunicación\">Medio de comunicación</option>\n\t\t\t\t\t\t\t<option value=\"Un amigo\">Un amigo</option>\n\t\t\t\t\t\t\t<option value=\"Otro\">Otro</option>\n\t\t\t\t\t\t</select>\n\t\t\t\t\t\t<div style=\"display: none\" id=\"status7\" class=\"text-danger\">¡Campo requerido!</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"text-center\" style=\"font-weight:bold;\">\n\t\t\t\t\t{{responseMessage}}\n\t\t\t\t</div>\n\t\t\t\t<div class=\"form-group text-center\">\n\t\t\t\t\t<button type=\"button\" (click)=\"submitEmailContacto(formContact)\" class=\"btn btn-contact\" [disabled]=\"loadingProcess\">\n\t\t\t\t\t\t<i *ngIf=\"loadingProcess\" class=\"fa fa-spinner fa-pulse fa-1x fa-fw margin-bottom\"></i>\n\t\t\t\t\t\t<span *ngIf=\"!loadingProcess\">Aplicar ahora</span>\n\t\t\t\t\t</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</form>\n\t</div>\n</div>"

/***/ }),

/***/ 509:
/***/ (function(module, exports) {

module.exports = "<div id=\"container-contact-admin\">\n\t<div class=\"container\">\n\t\t<div class=\"text-center title\">\n\t\t\t<label>\n\t\t\t\tContactar a administrador\n\t\t\t</label>\n\t\t</div>\n\t\t<div class=\"col-xs-12 col-sm-12\">\n\t\t\t<form #formContact=\"ngForm\">\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\t<div class=\"col-sm-6\">\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<input type=\"text\" name=\"name\" id=\"name\" class=\"form-control\" placeholder=\"Nombre\" required [(ngModel)]=\"dataContact.name\" ngControl=\"name\" (keyup)=\"changeValidateInput('name', formContact)\">\n\t\t\t\t\t\t\t<div style=\"display: none\" id=\"status1\" class=\"text-danger\">¡Campo requerido!</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"col-sm-6\">\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<input type=\"text\" name=\"lastname\" id=\"lastname\" class=\"form-control\" placeholder=\"Apellido\" required [(ngModel)]=\"dataContact.lastname\" ngControl=\"lastname\" (keyup)=\"changeValidateInput('lastname', formContact)\">\n\t\t\t\t\t\t\t<div style=\"display: none\" id=\"status2\" class=\"text-danger\">¡Campo requerido!</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\t<div class=\"col-sm-6\">\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<input type=\"text\" name=\"email\" id=\"email\" class=\"form-control\" placeholder=\"Correo electrónico\" required [(ngModel)]=\"dataContact.email\" ngControl=\"email\" (keyup)=\"changeValidateInput('email', formContact)\">\n\t\t\t\t\t\t\t<div style=\"display: none\" id=\"status3\" class=\"text-danger\">¡Campo requerido!</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"col-sm-6\">\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<input type=\"text\" name=\"phone\" id=\"phone\" class=\"form-control\" placeholder=\"Teléfono\" required [(ngModel)]=\"dataContact.phone\" ngControl=\"phone\" (keyup)=\"changeValidateInput('phone', formContact)\">\n\t\t\t\t\t\t\t<div style=\"display: none\" id=\"status4\" class=\"text-danger\">¡Campo requerido!</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\t<div class=\"col-sm-6\">\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<textarea name=\"reason\" id=\"reason\" class=\"form-control\" placeholder=\"Comentarios\" required [(ngModel)]=\"dataContact.reason\" ngControl=\"phone\" rows=\"5\" (keyup)=\"changeValidateInput('reason', formContact)\"></textarea>\n\t\t\t\t\t\t\t<div style=\"display: none\" id=\"status5\" class=\"text-danger\">¡Campo requerido!</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"row text-center font-bold\">\n\t\t\t\t\t{{responseMessage}}\n\t\t\t\t</div>\n\t\t\t\t<div class=\"row container-button text-center\">\n\t\t\t\t\t<button type=\"button\" (click)=\"submitEmailContact(formContact)\" class=\"btn btn-orange\" [disabled]=\"loadingProcess\">\n\t\t\t\t\t\t<i *ngIf=\"loadingProcess\" class=\"fa fa-spinner fa-pulse fa-1x fa-fw margin-bottom\"></i>\n\t\t\t\t\t\t<span *ngIf=\"!loadingProcess\" class=\"\">Enviar</span>\n\t\t\t\t\t</button>\n\t\t\t\t</div>\n\t\t\t</form>\n\t\t</div>\n\t</div>\n</div>"

/***/ }),

/***/ 510:
/***/ (function(module, exports) {

module.exports = "<div id=\"container-history\">\n\t\n\t<div *ngIf=\"!load && objHistory.length > 0\" class=\"container \">\n\n\t\t<div class=\"col-xs-12 col-sm-4\">\n\t\t\t<div class=\"row container-services\">\n\t\t\t\t<div>\n\t\t\t\t\t<div *ngFor=\"let history of objHistory; let i = index\" class=\"content-history padding-borders\" [ngClass]=\"{selected: activeHistory==i}\" (click)=\"getHistoryData(history.id, i)\">\n\t\t\t\t\t\t<span class=\"container-arrow-right\" *ngIf=\"activeHistory!=i\">\n\t\t\t\t\t\t\t<img src=\"./assets/img/arrowRight.png\" class=\"arrow-right\" alt=\"Ver información\">\n\t\t\t\t\t\t</span>\n\t\t\t\t\t\t<span class=\"container-arrow-right\" *ngIf=\"activeHistory==i\">\n\t\t\t\t\t\t\t<img src=\"./assets/img/arrowRightSelected.png\" class=\"arrow-right\" alt=\"\">\n\t\t\t\t\t\t</span>\n\t\t\t\t\t\t<div class=\"col-sm-8 row\">\n\t\t\t\t\t\t\t<label class=\"col-xs-12 col-sm-12\">{{history.serviceName}}</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-12 col-sm-12\">\n\t\t\t\t\t\t\t\t{{history.date}}\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"col-sm-4 row\">\n\t\t\t\t\t\t\t<label class=\"col-xs-12 col-sm-12\">&nbsp;</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-12 col-sm-12\">\n\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\n\t\t<div class=\"col-xs-12 col-sm-8\">\n\t\t\t\n\t\t\t<div *ngIf=\"dataService.origin\" class=\"row col-xs-12 col-sm-12\">\n\t\t\t\t<div class=\"col-xs-12  col-sm-12m margin-bottom-25\">\n\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t<label class=\"col-xs-12 col-sm-6 text-1-25em\">Detalles del servicio:</label>\n\t\t\t\t\t\t<label class=\"col-xs-12 col-sm-6 text-1-25em text-right\">Crédito corporativo: ${{valueCredit}} COP</label>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-sm-6 border-right\">\n\t\t\t\t\t<label class=\"col-xs-12 col-sm-12\">\n\t\t\t\t\t\t<div class=\"container-circle\">\n\t\t\t\t\t\t\t<div class=\"icon-circle-orange\">\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\tOrigen:\n\t\t\t\t\t</label>\n\t\t\t\t\t<div class=\"col-xs-12 col-sm-12\">\n\t\t\t\t\t\t{{dataService.origin}}\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"col-sm-6 padding-left-75\">\n\t\t\t\t\t<label class=\"col-xs-12 col-sm-12\">\n\t\t\t\t\t\t<div class=\"container-circle\">\n\t\t\t\t\t\t\t<div class=\"icon-circle-black\">\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\tDestino:\n\t\t\t\t\t</label>\n\t\t\t\t\t<div class=\"col-xs-12 col-sm-12\">\n\t\t\t\t\t\t{{dataService.destination}}\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\t<figure class=\"col-xs-12 col-sm-12\" id=\"containerPolyline\">\n\t\t\t\t\t\t<img src=\"{{dataService.image}}\" alt=\"\">\n\t\t\t\t\t</figure>\n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"col-xs-12 col-sm-12\">\n\t\t\t\t\t<div class=\"col-sm-2 text-center\">\n\t\t\t\t\t\t<figure id=\"containerImgDriver\">\n\t\t\t\t\t\t\t<img src={{dataService.photo}}  id=\"img-driver\" alt=\"Conductor\">\n\t\t\t\t\t\t</figure>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"col-sm-5\">\n\t\t\t\t\t\t<div class=\"containerData\">\n\t\t\t\t\t\t\t<label class=\"margin-bottom-25\">{{dataService.employeeName}} {{dataService.employeeLast}}</label>\n\t\t\t\t\t\t\t<div class=\"\">\n\t\t\t\t\t\t\t\t{{dataService.brand}} {{dataService.model}}\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"margin-bottom-60\">\n\t\t\t\t\t\t\t\t{{dataService.plaque}}\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"\">\n\t\t\t\t\t\t\t\t<label class=\"\">Distancia: </label> {{dataService.distance}}\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"\">\n\t\t\t\t\t\t\t\t<label class=\"\">Duración: </label> {{dataService.time}}\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"col-sm-5\">\n\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t<div class=\"col-sm-6\">\n\t\t\t\t\t\t\t\t<label class=\"margin-bottom-25\">CALIFICACION</label>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-sm-6\">\n\t\t\t\t\t\t\t\t<figure *ngIf=\"dataService.ratingImg==1\" id=\"containerRating\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStar.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStar.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStar.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStar.png\" alt=\"\">\n\t\t\t\t\t\t\t\t</figure>\n\t\t\t\t\t\t\t\t<figure *ngIf=\"dataService.ratingImg==2\" id=\"containerRating\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStar.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStar.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStar.png\" alt=\"\">\n\t\t\t\t\t\t\t\t</figure>\n\t\t\t\t\t\t\t\t<figure *ngIf=\"dataService.ratingImg==3\" id=\"containerRating\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStar.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStar.png\" alt=\"\">\n\t\t\t\t\t\t\t\t</figure>\n\t\t\t\t\t\t\t\t<figure *ngIf=\"dataService.ratingImg==4\" id=\"containerRating\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStar.png\" alt=\"\">\n\t\t\t\t\t\t\t\t</figure>\n\t\t\t\t\t\t\t\t<figure *ngIf=\"dataService.ratingImg==5\" id=\"containerRating\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n\t\t\t\t\t\t\t\t</figure>\n\t\t\t\t\t\t\t\t<figure *ngIf=\"!dataService.ratingImg==1 && !dataService.ratingImg==2 && !dataService.ratingImg==3 && !dataService.ratingImg==4 && !dataService.ratingImg==5\" id=\"containerRating\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStar.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStar.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStar.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStar.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStar.png\" alt=\"\">\n\t\t\t\t\t\t\t\t</figure>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t<div class=\"col-xs-12 col-sm-8 text-right\">\n\t\t\t\t\t\t\t\tValor del servicio:\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-12 col-sm-4 text-right\">\n\t\t\t\t\t\t\t\t<label class=\"\">{{dataService.price}}</label>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t<div class=\"col-xs-12 col-sm-8 text-right\">\n\t\t\t\t\t\t\t\tPropina: \n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-12 col-sm-4 text-right\">\n\t\t\t\t\t\t\t\t<label class=\"\">{{dataService.tip}}</label>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t<div class=\"col-xs-12 col-sm-8 text-right\">\n\t\t\t\t\t\t\t\t<label class=\"margin-bottom-40\">Cobro total: </label>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-12 col-sm-4 text-right\">\n\t\t\t\t\t\t\t\t<label class=\"\">{{dataService.total}}</label>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t<div class=\"col-xs-12 col-sm-8 text-right\">\n\t\t\t\t\t\t\t\tTipo de pago:\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-12 col-sm-4 text-right\">\n\t\t\t\t\t\t\t\t<label class=\"\">{{dataService.pay}}</label>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\n\t\t\t</div>\n\t\t</div>\n\n\t</div>\n\n\t<div *ngIf=\"load\" class=\"container text-center\" style=\"height: 800px; width: 100%;\">\n\t\t<figure class=\"text-center\">\n\t\t\t<img src=\"./assets/img/cargando.gif\" alt=\"\">\n\t\t</figure>\n\t\t<br>\n\t\t<br>\n\t\t<br>\n\t\t<br>\n\t\t<div class=\"text-center\">\n\t\t\t<b>Cargando historial</b>\n\t\t</div>\n\t</div>\n\n\t<div *ngIf=\"!load && objHistory.length < 1\" class=\"container text-center\" style=\"height: 800px; width: 100%;\">\n\t\t<br>\n\t\t<br>\n\t\t<br>\n\t\t<br>\n\t\t<br>\n\t\t<br>\n\t\t<br>\n\t\t<div class=\"text-center\">\n\t\t\t<figure class=\"text-center margin-bottom-25\">\n\t\t\t\t<img src=\"./assets/img/empty.png\" alt=\"Aún no has empezado a viajar con Beep\">\n\t\t\t</figure>\n\t\t\t<span class=\"font-bold text-1-3em\">\n\t\t\t\tAún no has empezado a\n\t\t\t\t<br>\n\t\t\t\tviajar con Beep\n\t\t\t</span>\n\t\t</div>\n\t</div>\n\t\n</div>"

/***/ }),

/***/ 511:
/***/ (function(module, exports) {

module.exports = "<div id=\"container-faq\">\n\t<div class=\"container\">\n\t\t\n\t\t<div class=\"col-sm-10 row\">\n\t\t\t<p class=\"container-title\">\n\t\t\t\t<span class=\"font-gothamRnd-medium\">PREGUNTAS FRECUENTES</span>\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<span class=\"text-orange text-1-5em font-gothamRnd-book\">¿El servicio que voy a prestar es legal?</span>\n\t\t\t\t<br><br>\n\t\t\t\t<span class=\"font-gothamRnd-book text-1-1-em\">\n\t\t\t\t\tSi. Está supervisado por un departamento altamente calificado y que está actualizándose según las normas del servicio de transporte de pasajeros bajo la modalidad Servicio Especial.\n\t\t\t\t</span>\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<span class=\"text-orange text-1-5em font-gothamRnd-book\">¿Porque Beep  y no otras aplicaciones?</span>\n\t\t\t\t<br><br>\n\t\t\t\t<span class=\"font-gothamRnd-book text-1-1-em\">\n\t\t\t\t\tPorque además de recibir el pago correspondiente a su labor como conductor dentro de la aplicación, tendrá la posibilidad de aumentar sus ingresos con los demás servicios que ofrece Beep dentro del carro. Recuerde que Beep es un centro de experiencia y que usted como conductor es nuestro principal embajador. Por esta razón y pensando en usted, creamos un sistema de beneficios adicionales que podrá conocer cuando nos contacte para recibir más información.\n\t\t\t\t</span>\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<span class=\"text-orange text-1-5em font-gothamRnd-book\">¿Qué tipo de teléfono debo tener para esta aplicación?</span>\n\t\t\t\t<br><br>\n\t\t\t\t<span class=\"font-gothamRnd-book text-1-1-em\">\n\t\t\t\t\tUn smartphone que en caso de que no lo tenga, podrá financiar a través de nosotros con una de nuestras empresas aliadas.\n\t\t\t\t</span>\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<span class=\"text-orange text-1-5em font-gothamRnd-book\">¿Tendré apoyo por la aplicación?</span>\n\t\t\t\t<br><br>\n\t\t\t\t<span class=\"font-gothamRnd-book text-1-1-em\">\n\t\t\t\t\tSi, estaremos las 24 horas del día y los 365 días del año a tu disposición en asuntos de plataforma y servicios.\n\t\t\t\t</span>\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<span class=\"text-orange text-1-5em font-gothamRnd-book\">¿Como conductor que debo aportar a Beep?</span>\n\t\t\t\t<br><br>\n\t\t\t\t<span class=\"font-gothamRnd-book text-1-1-em\">\n\t\t\t\t\tSu trabajo, atención y buen comportamiento con nuestros clientes. Su buen desempeño según las calificaciones de nuestros usuarios representará beneficios adicionales para usted.\n\t\t\t\t</span>\n\t\t\t</p>\n\t\t\t<p class=\"font-gothamRnd-book text-1-1-em\">\n\t\t\t\tSi tiene inquietudes adicionales escríbanos a <span class=\"font-gothamRnd-bold\">servicioalcliente@ubeep.co</span>\n\t\t\t</p>\n\t\t</div>\n\t</div>\n</div>"

/***/ }),

/***/ 512:
/***/ (function(module, exports) {

module.exports = "<div id=\"container-history\">\n\n\t<div *ngIf=\"!load && objHistory.length > 0\" class=\"container \">\n\n\t\t<div class=\"col-xs-12 col-sm-4\">\n\t\t\t<div class=\"row container-services\">\n\t\t\t\t<div>\n\t\t\t\t\t<div *ngFor=\"let history of objHistory; let i = index\" class=\"content-history padding-borders\" [ngClass]=\"{selected: activeHistory==i}\" (click)=\"getHistoryData(history.id, i)\">\n\t\t\t\t\t\t<span class=\"container-arrow-right\" *ngIf=\"activeHistory!=i\">\n\t\t\t\t\t\t\t<img src=\"./assets/img/arrowRight.png\" class=\"arrow-right\" alt=\"Ver información\">\n\t\t\t\t\t\t</span>\n\t\t\t\t\t\t<span class=\"container-arrow-right\" *ngIf=\"activeHistory==i\">\n\t\t\t\t\t\t\t<img src=\"./assets/img/arrowRightSelected.png\" class=\"arrow-right\" alt=\"\">\n\t\t\t\t\t\t</span>\n\t\t\t\t\t\t<div class=\"col-sm-8 row\">\n\t\t\t\t\t\t\t<label class=\"col-xs-12 col-sm-12\">{{history.serviceName}}</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-12 col-sm-12\">\n\t\t\t\t\t\t\t\t{{history.date}}\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"col-sm-4 row\">\n\t\t\t\t\t\t\t<label class=\"col-xs-12 col-sm-12\">&nbsp;</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-12 col-sm-12\">\n\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\n\t\t<div class=\"col-xs-12 col-sm-8\">\n\t\t\n\t\t\t<div *ngIf=\"dataService.origin\" class=\"row col-xs-12 col-sm-12\">\n\t\t\t\t<div class=\"col-xs-12  col-sm-12m margin-bottom-25\">\n\t\t\t\t\t<label class=\"col-xs-12 col-sm-12 text-1-25em\">Detalles del servicio:</label>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-sm-6 border-right\">\n\t\t\t\t\t<label class=\"col-xs-12 col-sm-12\">\n\t\t\t\t\t\t<div class=\"container-circle\">\n\t\t\t\t\t\t\t<div class=\"icon-circle-orange\">\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\tOrigen:\n\t\t\t\t\t</label>\n\t\t\t\t\t<div class=\"col-xs-12 col-sm-12\">\n\t\t\t\t\t\t{{dataService.origin}}\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"col-sm-6 padding-left-75\">\n\t\t\t\t\t<label class=\"col-xs-12 col-sm-12\">\n\t\t\t\t\t\t<div class=\"container-circle\">\n\t\t\t\t\t\t\t<div class=\"icon-circle-black\">\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\tDestino:\n\t\t\t\t\t</label>\n\t\t\t\t\t<div class=\"col-xs-12 col-sm-12\">\n\t\t\t\t\t\t{{dataService.destination}}\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\t<figure class=\"col-xs-12 col-sm-12\" id=\"containerPolyline\">\n\t\t\t\t\t\t<img src=\"{{dataService.image}}\" alt=\"\">\n\t\t\t\t\t</figure>\n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"col-xs-12 col-sm-12\">\n\t\t\t\t\t<div class=\"col-sm-2 text-center\">\n\t\t\t\t\t\t<figure id=\"containerImgDriver\">\n\t\t\t\t\t\t\t<img src={{dataService.photo}}  id=\"img-driver\" alt=\"Conductor\">\n\t\t\t\t\t\t</figure>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"col-sm-5\">\n\t\t\t\t\t\t<div class=\"containerData\">\n\t\t\t\t\t\t\t<label class=\"margin-bottom-25\">{{dataService.employeeName}} {{dataService.employeeLast}}</label>\n\t\t\t\t\t\t\t<div class=\"\">\n\t\t\t\t\t\t\t\t{{dataService.brand}} {{dataService.model}}\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"margin-bottom-60\">\n\t\t\t\t\t\t\t\t{{dataService.plaque}}\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"\">\n\t\t\t\t\t\t\t\t<label class=\"\">Distancia: </label> {{dataService.distance}}\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"\">\n\t\t\t\t\t\t\t\t<label class=\"\">Duración: </label> {{dataService.time}}\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"col-sm-5\">\n\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t<div class=\"col-sm-6\">\n\t\t\t\t\t\t\t\t<label class=\"margin-bottom-25\">CALIFICACION</label>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-sm-6\">\n\t\t\t\t\t\t\t\t<figure *ngIf=\"dataService.ratingImg==1\" id=\"containerRating\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStar.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStar.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStar.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStar.png\" alt=\"\">\n\t\t\t\t\t\t\t\t</figure>\n\t\t\t\t\t\t\t\t<figure *ngIf=\"dataService.ratingImg==2\" id=\"containerRating\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStar.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStar.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStar.png\" alt=\"\">\n\t\t\t\t\t\t\t\t</figure>\n\t\t\t\t\t\t\t\t<figure *ngIf=\"dataService.ratingImg==3\" id=\"containerRating\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStar.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStar.png\" alt=\"\">\n\t\t\t\t\t\t\t\t</figure>\n\t\t\t\t\t\t\t\t<figure *ngIf=\"dataService.ratingImg==4\" id=\"containerRating\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStar.png\" alt=\"\">\n\t\t\t\t\t\t\t\t</figure>\n\t\t\t\t\t\t\t\t<figure *ngIf=\"dataService.ratingImg==5\" id=\"containerRating\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n\t\t\t\t\t\t\t\t</figure>\n\t\t\t\t\t\t\t\t<figure *ngIf=\"!dataService.ratingImg==1 && !dataService.ratingImg==2 && !dataService.ratingImg==3 && !dataService.ratingImg==4 && !dataService.ratingImg==5\" id=\"containerRating\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStar.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStar.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStar.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStar.png\" alt=\"\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/iconStar.png\" alt=\"\">\n\t\t\t\t\t\t\t\t</figure>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t<div class=\"col-xs-12 col-sm-8 text-right\">\n\t\t\t\t\t\t\t\tValor del servicio:\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-12 col-sm-4 text-right\">\n\t\t\t\t\t\t\t\t<label class=\"\">{{dataService.price}}</label>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t<div class=\"col-xs-12 col-sm-8 text-right\">\n\t\t\t\t\t\t\t\tPropina: \n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-12 col-sm-4 text-right\">\n\t\t\t\t\t\t\t\t<label class=\"\">{{dataService.tip}}</label>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t<div class=\"col-xs-12 col-sm-8 text-right\">\n\t\t\t\t\t\t\t\t<label class=\"margin-bottom-40\">Cobro total: </label>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-12 col-sm-4 text-right\">\n\t\t\t\t\t\t\t\t<label class=\"\">{{dataService.total}}</label>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t<div class=\"col-xs-12 col-sm-8 text-right\">\n\t\t\t\t\t\t\t\tTipo de pago:\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-12 col-sm-4 text-right\">\n\t\t\t\t\t\t\t\t<label class=\"\">{{dataService.pay}}</label>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\n\t\t\t</div>\n\t\t</div>\n\n\t</div>\n\n\t<div *ngIf=\"load\" class=\"container text-center\" style=\"height: 800px; width: 100%;\">\n\t\t<figure class=\"text-center\">\n\t\t\t<img src=\"./assets/img/cargando.gif\" alt=\"\">\n\t\t</figure>\n\t\t<br>\n\t\t<br>\n\t\t<br>\n\t\t<br>\n\t\t<div class=\"text-center\">\n\t\t\t<b>Cargando historial</b>\n\t\t</div>\n\t</div>\n\n\t<div *ngIf=\"!load && objHistory.length < 1\" class=\"container text-center\" style=\"height: 800px; width: 100%;\">\n\t\t<br>\n\t\t<br>\n\t\t<br>\n\t\t<br>\n\t\t<br>\n\t\t<br>\n\t\t<br>\n\t\t<div class=\"text-center\">\n\t\t\t<figure class=\"text-center margin-bottom-25\">\n\t\t\t\t<img src=\"./assets/img/empty.png\" alt=\"Aún no has empezado a viajar con Beep\">\n\t\t\t</figure>\n\t\t\t<span class=\"font-bold text-1-3em\">\n\t\t\t\tAún no has empezado a\n\t\t\t\t<br>\n\t\t\t\tviajar con Beep\n\t\t\t</span>\n\t\t</div>\n\t</div>\n\t\n</div>\n\n\n"

/***/ }),

/***/ 513:
/***/ (function(module, exports) {

module.exports = "\t<carousel [interval]=\"myInterval\" [noWrap]=\"noWrapSlides\">\n\t\t<slide *ngFor=\"let slidez of slides; let index=index\"[active]=\"slidez.active\">\n\t\t\t<img [src]=\"slidez.image\">\n\t\t\t<div class=\"carousel-caption container-middle\">\n\t\t\t\t<div class=\"content-middle-left width-100 text-center\" *ngIf=\"index == 0\">\n\t\t\t\t\t<div class=\"text-5em text-shadow-black\">\n\t\t\t\t\t\tLa forma más sencilla\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"text-orange text-5em container-text-slider\">\n\t\t\t\t\t\tDE VIAJAR POR LA CIUDAD\n\t\t\t\t\t</div>\n\t\t\t\t\t<a href=\"#container-download\">\n\t\t\t\t\t\t<button class=\"btn bnt-lg\" id=\"btn-scroll-download\">\n\t\t\t\t\t\t\tDescargue nuestra App\n\t\t\t\t\t\t</button>\n\t\t\t\t\t</a>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"content-middle-left text-left\" *ngIf=\"index == 1\">\n\t\t\t\t\t<div class=\"text-5em text-shadow-black\">\n\t\t\t\t\t\tUn viaje seguro\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"text-orange text-5em text-shadow-black container-text-slider\">\n\t\t\t\t\t\tUN DESTINO FELIZ\n\t\t\t\t\t</div>\n\t\t\t\t\t<button class=\"btn bnt-lg\" (click)=\"clickBtnBanner('ingresar')\">\n\t\t\t\t\t\tIngrese con su cuenta\n\t\t\t\t\t</button>\n\t\t\t\t</div>\n\t\t\t\t<div  class=\"content-middle-left width-100 text-center\" *ngIf=\"index == 2\">\n\t\t\t\t\t<div class=\"text-5em text-shadow-black\">\n\t\t\t\t\t\tDisfrute de nuestros beneficios\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"text-orange text-5em container-text-slider\">\n\t\t\t\t\t\t¡ÚNASE A BEEP!\n\t\t\t\t\t</div>\n\t\t\t\t\t<button class=\"btn bnt-lg\" (click)=\"clickBtnBanner('formularioContacto')\">\n\t\t\t\t\t\tConduzca con nosotros\n\t\t\t\t\t</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</slide>\n\t</carousel>\n\n\t<div id=\"container-info\" class=\"container\">\n\t\t<div class=\"col-sm-12 col-md-12 container-middle\">\n\t\t\t<div class=\"col-sm-7 content-middle-right\">\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\t<span class=\"text-3em text-orange\">Beep para todos</span>\n\t\t\t\t\t<br/>\n\t\t\t\t\t<p class=\"text-2em\">Servicios a la medida de sus necesidades</p>\n\t\t\t\t\t<p class=\"text-1-25em font-gothamRnd-light font-3d3d3d font-bold\">\n\t\t\t\t\t\tEn Beep ofrecemos soluciones integrales para empresas y personas. Viajes diseñados para garantizar una experiencia única, segura y eficiente.\n\t\t\t\t\t</p>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"col-sm-5 text-center\">\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\t<figure id=\"manInfo\">\n\t\t\t\t\t\t<img src=\"./assets/img/ImagenInfo.png\" alt=\"Man\">\n\t\t\t\t\t</figure>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\n\t<div id=\"container-contact\">\n\t\t<div class=\"container\">\n\t\t\t<div class=\"col-sm-12 col-md-12 container-middle\">\n\t\t\t\t<div id=\"titile-contact\" class=\"col-sm-6\">\n\t\t\t\t\t<span class=\"text-2em\">¿Quiere ser parte de BEEP?</span>\n\t\t\t\t\t<br/>\n\t\t\t\t\t<span class=\"text-3em\">Conduzca con nosotros</span>\n\t\t\t\t</div>\n\t\t\t\t<div *ngIf=\"!formContactContainer\" class=\"col-sm-6 text-center content-middle-right\">\n\t\t\t\t\t<button type=\"button\" class=\"btn btn-lg btn-contact\" (click)=\"showFormContact('show')\">CONTÁCTENOS</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"col-sm-2\"></div>\n\t\t\t<div *ngIf=\"formContactContainer\" id=\"container-form-contact\" class=\"col-sm-8\">\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\t<form #formContact=\"ngForm\" class=\"form-horizontal\">\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<!--<label for=\"contacto-enterar\" class=\"col-sm-2 control-label\">Como te enteraste</label>-->\n\t\t\t\t\t\t\t<div class=\"col-sm-12\">\n\t\t\t\t\t\t\t\t<select class=\"form-control\" name=\"type\" id=\"type\" required [(ngModel)]=\"dataContact.type\" ngControl=\"type\" (mouseup)=\"changeValidateInput('type', formContact)\">\n\t\t\t\t\t\t\t\t\t<option value=\"\">¿Cómo quiere unirse a nuestra compañía?</option>\n\t\t\t\t\t\t\t\t\t<option value=\"Tengo un vehículo\">Tengo un vehículo</option>\n\t\t\t\t\t\t\t\t\t<option value=\"Tengo una flota de vehículos\">Tengo una flota de vehículos</option>\n\t\t\t\t\t\t\t\t\t<option value=\"Soy conductor\">Soy conductor</option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t\t<div style=\"display: none\" id=\"status1\" class=\"text-danger\">¡Campo requerido!</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<!--<label for=\"contacto-nombre\" class=\"col-sm-2 control-label\">Nombre</label>-->\n\t\t\t\t\t\t\t<div class=\"col-sm-12\">\n\t\t\t\t\t\t\t\t<input type=\"text\" name=\"name\" id=\"name-contact\" class=\"form-control\" placeholder=\"Nombre\" required [(ngModel)]=\"dataContact.name\" ngControl=\"name\" (keyup)=\"changeValidateInput('name', formContact)\">\n\t\t\t\t\t\t\t\t<div style=\"display: none\" id=\"status2\" class=\"text-danger\">¡Campo requerido!</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<!--<label for=\"contacto-apellido\" class=\"col-sm-2 control-label\">Apellido</label>-->\n\t\t\t\t\t\t\t<div class=\"col-sm-12\">\n\t\t\t\t\t\t\t\t<input type=\"text\" name=\"lastname\" id=\"lastname\" class=\"form-control\" placeholder=\"Apellido\" required [(ngModel)]=\"dataContact.lastname\" ngControl=\"lastname\" (keyup)=\"changeValidateInput('lastname', formContact)\">\n\t\t\t\t\t\t\t\t<div style=\"display: none\" id=\"status3\" class=\"text-danger\">¡Campo requerido!</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<!--<label for=\"contacto-telefono\" class=\"col-sm-2 control-label\">Teléfono</label>-->\n\t\t\t\t\t\t\t<div class=\"col-sm-12\">\n\t\t\t\t\t\t\t\t<input type=\"text\" name=\"phone\" id=\"phone\" class=\"form-control\" placeholder=\"Teléfono\" required [(ngModel)]=\"dataContact.phone\" ngControl=\"phone\" (keyup)=\"changeValidateInput('phone', formContact)\">\n\t\t\t\t\t\t\t\t<div style=\"display: none\" id=\"status4\" class=\"text-danger\">¡Campo requerido!</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<!--<label for=\"contacto-ciudad\" class=\"col-sm-2 control-label\">Ciudad</label>-->\n\t\t\t\t\t\t\t<div class=\"col-sm-12\">\n\t\t\t\t\t\t\t\t<input type=\"text\" name=\"address\" id=\"address\" class=\"form-control\" placeholder=\"Dirección\" required [(ngModel)]=\"dataContact.address\" ngControl=\"address\" (keyup)=\"changeValidateInput('address', formContact)\">\n\t\t\t\t\t\t\t\t<div style=\"display: none\" id=\"status5\" class=\"text-danger\">¡Campo requerido!</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<!--<label for=\"contacto-email\" class=\"col-sm-2 control-label\">Correo</label>-->\n\t\t\t\t\t\t\t<div class=\"col-sm-12\">\n\t\t\t\t\t\t\t\t<input type=\"text\" name=\"email\" id=\"email\" class=\"form-control\" placeholder=\"Correo electrónico\" required [(ngModel)]=\"dataContact.email\" ngControl=\"email\" (keyup)=\"changeValidateInput('email', formContact)\">\n\t\t\t\t\t\t\t\t<div style=\"display: none\" id=\"status6\" class=\"text-danger\">¡Campo requerido!</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<!--<label for=\"contacto-enterar\" class=\"col-sm-2 control-label\">Como te enteraste</label>-->\n\t\t\t\t\t\t\t<div class=\"col-sm-12\">\n\t\t\t\t\t\t\t\t<select class=\"form-control\" name=\"reason\" id=\"reason\" required [(ngModel)]=\"dataContact.reason\" ngControl=\"reason\" (mouseup)=\"changeValidateInput('reason', formContact)\">\n\t\t\t\t\t\t\t\t\t<option value=\"\">¿Cómo se entero?</option>\n\t\t\t\t\t\t\t\t\t<option value=\"Redes Sociales\">Redes Sociales</option>\n\t\t\t\t\t\t\t\t\t<option value=\"Medio de comunicación\">Medio de comunicación</option>\n\t\t\t\t\t\t\t\t\t<option value=\"Un amigo\">Un amigo</option>\n\t\t\t\t\t\t\t\t\t<option value=\"Otro\">Otro</option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t\t<div style=\"display: none\" id=\"status7\" class=\"text-danger\">¡Campo requerido!</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"text-center col-sm-12\" style=\"font-weight:bold;\">\n\t\t\t\t\t\t\t{{responseMessage}}\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-sm-12 text-right\">\n\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-gray\" style=\"width:187px;\" (click)=\"showFormContact('hide')\">\n\t\t\t\t\t\t\t\t\tCancelar\n\t\t\t\t\t\t\t\t</button>\n\t\t\t\t\t\t\t\t<button type=\"button\" (click)=\"submitEmailContacto(formContact)\" class=\"btn btn-contact\" style=\"width:187px;\" [disabled]=\"loadingProcess\">\n\t\t\t\t\t\t\t\t\t<i *ngIf=\"loadingProcess\" class=\"fa fa-spinner fa-pulse fa-1x fa-fw margin-bottom\"></i>\n\t\t\t\t\t\t\t\t\t<span *ngIf=\"!loadingProcess\">Aplicar ahora</span>\n\t\t\t\t\t\t\t\t</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</form>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"col-sm-2\"></div>\n\t\t</div>\n\t</div>\n\n\t<div id=\"container-download\">\n\t\t<div class=\"container\">\n\t\t\t<div class=\"col-sm-12 col-md-12 container-middle\">\n\t\t\t\t<div class=\"row container-social-network col-sm-6\">\n\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t<figure id=\"img-download-app\">\n\t\t\t\t\t\t\t<img src=\"./assets/img/DescargaApp.png\" alt=\"descargar Aplicación\">\n\t\t\t\t\t\t</figure>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"row content-middle-right col-sm-6\">\n\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t<div class=\"text-2em\">No importa donde se encuentre</div>\n\t\t\t\t\t\t<br>\n\t\t\t\t\t\t<div class=\"text-1-25em font-gothamRnd-light font-3d3d3d\">\n\t\t\t\t\t\t\tBeep está listo para acompañarlo en cualquier lugar. Comience a disfrutar de nuestra amplia gama de soluciones pensadas a la medida de sus necesidades.\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"container-img-download\">\n\t\t\t\t\t\t\t<a href=\"https://play.google.com/store/apps/details?id=com.logisticapp.beepclient&hl=es\" target=\"_blank\">\n\t\t\t\t\t\t\t\t<figure id=\"downloadGooglePlay\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/Download_GooglePlay.png\" alt=\"\">\n\t\t\t\t\t\t\t\t</figure>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t<a href=\"https://appsto.re/co/5DVAib.i\" target=\"_blank\">\n\t\t\t\t\t\t\t\t<figure id=\"downloadAppStore\">\n\t\t\t\t\t\t\t\t\t<img src=\"./assets/img/Download_AppStore.png\" alt=\"\">\n\t\t\t\t\t\t\t\t</figure>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\n\n\n\n\t<div class=\"modal fade\" bsModal #modalNewPassword=\"bs-modal\" [config]=\"{backdrop: 'static', keyboard: false}\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"modalNewPassLabel\" aria-hidden=\"true\" id=\"modal-login\" >\n\t    <div class=\"modal-dialog modal-md\">\n\t        <div class=\"modal-content\">\n\t            <div class=\"modal-header\">\n\t                <span>Nueva contraseña</span>\n\t            </div>\n\t            <div class=\"modal-body\">\n\t            \t<form #formNewPassword=\"ngForm\" class=\"form-horizontal\" >\n\t                    <div class=\"form-group\">\n\t                        <input type=\"password\" class=\"form-control col-sm-12 input-lg\" id=\"password-new\" placeholder=\"Nueva contraseña\" name=\"passwordNew\" required [(ngModel)]=\"dataNewPassword.passwordNew\" ngControl=\"passwordNew\" (keyup)=\"checkPassword(formNewPassword)\">\n\t                    </div>\n\t                    <div class=\"form-group\">\n\t                        <input type=\"password\" class=\"form-control col-sm-12 input-lg\" id=\"password-confirm\" placeholder=\"Confirmar contraseña\" name=\"passwordConfirm\" required required [(ngModel)]=\"dataNewPassword.passwordConfirm\" ngControl=\"passwordConfirm\" (keyup)=\"checkPassword(formNewPassword)\">\n\t                    </div>\n\t\t\t\t\t\t<p class=\"text-center\">\n\t                        <span class=\"font-bold\" *ngIf=\"messageErrorCheckPassword\">{{messageErrorCheckPassword}}</span>\n\t                        <span class=\"font-bold text-orange\" *ngIf=\"messageSuccessCheckPassword\">{{messageSuccessCheckPassword}}</span>\n\t                        <span *ngIf=\"!messageErrorCheckPassword && !messageSuccessCheckPassword\">{{messageCheckPassword}}</span>\n\t                    </p>\n\t\t\t\t        <div class=\"form-group\">\n\t\t\t\t            <button type=\"button\" class=\"btn btn-lg btn-orange col-sm-12\" (click)=\"changeNewPassword(formNewPassword)\" [disabled]=\"loadingProcessPassword\">\n\t\t\t\t            \t<i *ngIf=\"loadingProcessPassword\" class=\"fa fa-spinner fa-pulse fa-1x fa-fw margin-bottom\"></i>\n\t\t\t\t            \t<span *ngIf=\"!loadingProcessPassword\" class=\"font-bold\">Guardar contraseña</span>\n\t\t\t\t            </button>\n\t\t\t\t        </div>\n\t\t\t\t    </form>\n\t            </div>\n\t        </div>\n\t    </div>\n\t</div>"

/***/ }),

/***/ 514:
/***/ (function(module, exports) {

module.exports = "<div id=\"container-invite\">\n\t<div class=\"container-background\">\n\t\t\n\t\t<div class=\"container\">\n\t\t\t<div class=\"col-xs-12 col-sm-12\">\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\t<div class=\"col-xs-12 col-sm-6\">\n\t\t\t\t\t\t\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"col-xs-12 col-sm-6\">\n\t\t\t\t\t\t<div class=\"panel panel-default\">\n\t\t\t\t\t\t\t<div class=\"panel-heading text-center text-1-8em\">INVITAR A UN AMIGO</div>\n\t\t\t\t\t\t\t<div class=\"panel-body text-center\">\n\n\t\t\t\t\t\t\t\t<form #formInvite=\"ngForm\" id=\"formInvite\" class=\"col-xs-12 col-sm-12 row\">\n\n\t\t\t\t\t\t\t\t\t<div class=\"text-1-5em margin-bottom-50\">\n\t\t\t\t\t\t\t\t\t\tIngresa el correo electrónico de tus amigos <br> e invítalos a vivir la experiencia Beep\n\t\t\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t\t\t\t<div class=\"margin-bottom-50\">\n\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control col-sm-12 input-lg\" id=\"email\" placeholder=\"Correo electrónico\" name=\"email\" required [(ngModel)]=\"dataInvite.email\" ngControl=\"email\" (keyup)=\"changeValidateInput('email', formInvite)\">\n\t\t\t\t\t\t\t\t\t\t<div style=\"display: none\" id=\"status1\" class=\"text-danger\">¡Campo requerido!</div>\n\t\t\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t\t\t\t<p class=\"text-center\" style=\"font-weight:bold;\">\n\t\t\t\t\t\t\t            {{responseMessage}}\n\t\t\t\t\t\t\t        </p>\n\n\t\t\t\t\t\t\t\t\t<div class=\"\">\n\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-lg col-xs-12 col-sm-12 btn-orange\" (click)=\"sendEmail(formInvite)\" [disabled]=\"loadingProcess\">\n\t\t\t\t\t\t\t\t\t\t\t<i *ngIf=\"loadingProcess\" class=\"fa fa-spinner fa-pulse fa-1x fa-fw margin-bottom\"></i>\n\t\t\t\t\t\t\t\t\t\t\t<span *ngIf=\"!loadingProcess\" class=\"\">Enviar invitación</span>\n\t\t\t\t\t\t\t            </button>\n\t\t\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t\t\t</form>\n\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t\n\t</div>\n</div>"

/***/ }),

/***/ 515:
/***/ (function(module, exports) {

module.exports = "<div id=\"container-payment\">\n\n</div>"

/***/ }),

/***/ 516:
/***/ (function(module, exports) {

module.exports = "<div id=\"container-press\">\n\t<div class=\"container\">\n\t\t<div class=\"content-img text-center\">\n\t\t\t<figure>\n\t\t\t\t<img class=\"img-screenshots\" src=\"./assets/img/screenshots.png\" alt=\"¡Beep!\">\n\t\t\t</figure>\n\t\t</div>\n\t</div>\n\t<div class=\"content-download\">\n\t\t<div class=\"container\">\n\t\t\t<div class=\"col-sm-12 col-md-12 container-middle\">\n\t\t\t\t<div class=\"col-sm-6 text-center content-middle-right\">\n\t\t\t\t\t<figure>\n\t\t\t\t\t\t<img class=\"img-beep\" src=\"./assets/img/logo.png\" alt=\"¡Beep!\">\n\t\t\t\t\t</figure>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-sm-6\">\n\t\t\t\t\t<div class=\"text-1-3em font-gothamRnd-bold\">Descarga nuestro Kit de Prensa</div>\n\t\t\t\t\t<p class=\"text-1-3em font-gothamRnd-book\">\n\t\t\t\t\t\tIncluye nuestro logo y manual de\n\t\t\t\t\t\t<br>\n\t\t\t\t\t\tidentidad de marca\n\t\t\t\t\t</p>\n\t\t\t\t\t<a href=\"{{urlDownloadKit}}\"><button type=\"button\" class=\"btn btn-orange\">Descargar</button></a>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t<div class=\"container text-center padding-40\">\n\t\t<div class=\"col-sm-12 col-xs-12 text-1-3em font-gothamRnd-bold\">Contáctenos para prensa</div>\n\t\t<br>\n\t\t<span class=\"text-1-3em font-gothamRnd-book\">\n\t\t\tCorreo: <span class=\"text-orange font-gothamRnd-bold\">plugg@ubeep.co</span>\n\t\t</span>\n\t</div>\n</div>"

/***/ }),

/***/ 517:
/***/ (function(module, exports) {

module.exports = "<div id=\"container-privacy-policy\">\n\t<div class=\"container\">\n\t\t<figure id=\"logo\">\n\t\t\t<img src=\"./assets/img/logo.png\" alt=\"UBeep\">\n\t\t</figure>\n\t\t<p class=\"text-center\">\n\t\t\t<label>Términos y Condiciones del uso de Ubeep</label>\n\t\t</p>\n\n\t\t<div class=\"container-policy\">\n\t\t\t<label>\n\t\t\t\t1. Objeto de la Relación Contractual\n\t\t\t</label>\n\t\t\t<br>\n\t\t\t<p>\n\t\t\t\tLos presentes términos y condiciones de uso (en adelante “Términos”) regulan los términos y condiciones de acceso y uso de aplicaciones, páginas web, contenido, productos y servicios (los “Servicios”)  provistos por Unext SAS (en adelante “Unext”) y su marca comercial ubeep (en adelante “Ubeep o Aplicación”) para usted (en adelante “Usuario”). Unext es una sociedad comercial válidamente constituida bajo las leyes de colombia identificada con número de identificación tributaria No. 901003017-7 con domicilio en la ciudad de Bogotá e inscrita en la Cámara de Comercio de Bogotá. Al acceder a la Aplicación y usar los Servicios de Ubeep usted expresa su consentimiento inequívoco de aceptar los Términos y reconoce haberlos leído en su totalidad. De no aceptar los Términos aca contenidos usted no podrá acceder a la Aplicación o a los Servicios. Estos Términos sustituyen cualquier acuerdo previo que haya existido entre usted y Ubeep. Ubeep podrá sustituir, modificar, eliminar o adicionar parcial o totalmente los Términos cuando lo considere conveniente. Para el evento anterior Ubeep lo notificará  a través de un medio idóneo para que usted acepte los nuevos Términos. Usted siempre podrá verificar los Términos de uso de Ubeep a accediendo a ubeep.com/legal. Ubeep se reserva el derecho de dejar de operar la Aplicación y/o dejar de ofrecer los Servicios en cualquier momento sin mediar notificación alguna.\n\t\t\t</p>\n\t\t</div>\n\n\t\t<div class=\"container-policy\">\n\t\t\t<label>\n\t\t\t\t2. Servicios\n\t\t\t</label>\n\t\t\t<br>\n\t\t\t<p>\n\t\t\t\tLos Servicios son el conjunto de soluciones tecnológicas (Aplicaciones móviles, páginas web, productos, y servicios) que Ubeep pone a su disposición que le permiten realizar comunicación con empresas transportadoras, conductores, empresas logísticas y personas naturales o jurídicas (TERCEROS TRANSPORTISTAS) para realizar reservas y/o organizar transporte de pasajeros y/o cosas.\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\tEn ningún caso Ubeep debe considerarse como una empresa que ofrece servicios de transporte y/o logística. Por lo tanto la responsabilidad por el servicio de transporte es única y exclusivamente del  tercero transportador, empresa transportista y/o de logística. Aceptando los Términos usted entiende y acepta que que los Servicios que ofrece Ubeep son única y exclusivamente de comunicación entre terceros transportistas, empresas transportistas y/o de logística y usted, por lo que mantendrá indemne a Ubeep de cualquier perjuicio o daño que se le cause por el servicio de transporte prestado.\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\tBajo el estricto cumplimiento de los Términos, Ubeep le otorga una licencia restringida, no exclusiva, no cedible, no sublicenciable, revocable, no transferible, no comercial para:  el acceso y uso exclusivo desde su dispositivo personal a los Servicios y de los Contenidos dispuestos para usted a través de los medios autorizados por Ubeep.\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\tAceptando los Términos usted declara que cuenta con la edad suficiente para poder acceder a los Servicios provistos por Ubeep.\n\t\t\t</p>\n\t\t</div>\n\n\t\t<div class=\"container-policy\">\n\t\t\t<label>\n\t\t\t\t3. Política de seguridad y tratamiento de datos personales\n\t\t\t</label>\n\t\t\t<br>\n\t\t\t<p>\n\t\t\t\tLos datos recolectados por Ubeep a través de los medios autorizados serán tratados conforme a la ley y a la política de seguridad y tratamiento de datos la cual podra ser consultada pos usted en ubeep.com/legal\n\t\t\t</p>\n\t\t</div>\n\n\t\t<div class=\"container-policy\">\n\t\t\t<label>\n\t\t\t\t4. Acceso y uso de los Servicios\n\t\t\t</label>\n\t\t\t<br>\n\t\t\t<p>\n\t\t\t\tPara poder acceder a los Servicios usted deberá descargar correctamente en su dispositivo personal la Aplicación solo y únicamente en tiendas virtuales autorizadas. La descarga de la Aplicación no tiene ningún costo. \n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\tUna vez descargada la Aplicación usted deberá suministrar con total veracidad la información personal requerida en el formulario de registro. Dicha información será tratada conforme a la política de seguridad y tratamiento de datos y la ley aplicable. \n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\tEl Usuario deberá mantener la información suministrada actualizada so pena de la imposibilidad del uso de los Servicios. \n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\tEl Usuario se compromete a usar los Servicios siempre con fines legítimos y legales; actuar de buena fe; no acceder a funciones no autorizadas de la Aplicación; no acceder o manipular datos registrados por otros Usuarios en la Aplicación y en general no realizar ningún tipo de conducta que resulte en el perjuicio o daño de un tercero o de Ubeep o en la infracción de una norma legal. \n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\tEl Usuario no podrá transferir o permitir el uso de terceros de su cuenta. \n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\tEl Usuario autoriza a Ubeep el envío de mensajes de texto y/o correos electrónicos con información de su cuenta o de los Servicios.\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\tEl Usuario podrá acceder a su cuenta y disfrutar de los Servicios siempre y cuando suministre la contraseña que informo al momento del registro de su Usuario. El Usuario será en todo momento el único responsable de la custodia de la contraseña por lo que responderá por cualquier daño o perjuicio que cause por el extravío, cesión, o mala custodia de la contraseña.\n\t\t\t</p>\n\t\t</div>\n\n\t\t<div class=\"container-policy\">\n\t\t\t<label>\n\t\t\t\t5. Pago y Medios de Pago\n\t\t\t</label>\n\t\t\t<br>\n\t\t\t<p>\n\t\t\t\tLa contraprestación por el uso de los Servicios será definida e informada por Ubeep al usuario a través de los medios que Ubeep disponga. \n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\tUna vez recibidos los Servicios, Ubeep facturará al Usuario de acuerdo con la tarifa aplicable por el trayecto realizado desde un punto fijo a otro.\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\tUbeep cobrará los cargos correspondientes al servicio prestado al medio de pago informado por el Usuario en el formulario de inscripción inicial. \n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\tUbeep será considerado como un facilitador de pago al tercero transportista que le prestó el servicio y que fue contactado a través de la plataforma de Ubeep. \n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\tLos pagos pagados por usted no serán reembolsables a menos que Ubeep le retorne parte o todo el valor pagado por los servicios suministrados siempre y únicamente cuando Ubeep lo considere.  \n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\tUbeep podrá en cualquier momento modificar los cargos por los servicios ofrecidos, así mismo podrá modificar, eliminar o adicionar servicios a su plataforma.\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\tUbeep en cualquier momento podrá disponer de Servicios promocionales a usuarios determinados o a la totalidad de ellos, a los cuales se les cobrará cargos promocionales. Estos servicios promocionales y cargos promocionales no podrán ser considerados por el Usuario como los servicios y cargos que normalmente presta y cobra Ubeep. \n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\tEl Usuario podrá cancelar el servicio solicitado al tercero transportista en cualquier momento antes de que el tercer transportista llegue al destino informado. Ubeep podrá cobrar al Usuario una tarifa de cancelación. \n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\tEl Usuario podrá en todo momento conocer las facturas generadas por los servicios que se le hayan prestado a través de la página web de ubeep (www.ubeep.co)\n\t\t\t</p>\n\t\t</div>\n\n\t\t<div class=\"container-policy\">\n\t\t\t<label>\n\t\t\t\t6. Política de Seguridad y Tratamiento de Datos\n\t\t\t</label>\n\t\t\t<br>\n\t\t\t<p>\n\t\t\t\tLos datos de los usuarios serán tratados conforme a la Política de Seguridad y Tratamiento de Datos publicado en la página web de Ubeep (www.ubeep.co). Para ejercer los derechos contenidos en la Política de Seguridad y Tratamiento de Datos el Usuario podrá enviar un correo electrónico a servicioalcliente@ubeep.co\n\t\t\t</p>\n\t\t</div>\n\n\t\t<div class=\"container-policy\">\n\t\t\t<label>\n\t\t\t\t7. Propiedad Industrial e Intelectual\n\t\t\t</label>\n\t\t\t<br>\n\t\t\t<p>\n\t\t\t\tEl Usuario reconoce y acepta que toda la propiedad industrial e intelectual sobre los contenidos, Servicios, marcas, logos, software y cualesquiera que sean integrados a la plataforma de Ubeep o cualquier medio filial de Ubeep le pertenece exclusivamente a Ubeep. \n\t\t\t</p>\n\t\t</div>\n\n\t\t<div class=\"container-policy\">\n\t\t\t<label>\n\t\t\t\t8. Renuncia expresa a garantías, responsabilidad y compromiso de indemnidad \n\t\t\t</label>\n\t\t\t<br>\n\t\t\t<p>\n\t\t\t\tUbeep renuncia a cualquier tipo de garantía legal, comercial, de costumbre, expresa, implícita o de cualquier índole que no esté contenida en los presentes Términos hacia el Usuario o a los terceros transportistas. \n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\tUbeep al ser una plataforma de comunicación entre terceros transportistas y usuarios que desean reservar, planificar, u organizar viajes no es responsable en ningún caso por la prestación del servicio de transporte prestado por los terceros transportistas. Usted acepta que Ubeep no será responsable en ningún caso y bajo ningún concepto de cualquier responsabilidad, daño o perjuicio que derive del servicio prestado por el tercero transportista hacia su integridad, propiedad, datos, o cualquier bien que le pertenezca.\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\tUbeep realizará todas las acciones posibles para que sus derechos como consumidor no se vean violados y propenderá por ofrecer las herramientas necesarias para garantizar el mejor servicio de la plataforma de comunicación. \n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\tUsted acuerda mantener indemnes a Ubeep sus filiales, empleados, o cualquier persona natural o jurídica relacionada con Ubeep por cualquier reclamación, demanda, pérdida, responsabilidad y gasto que deriven del mal uso de los Servicios, la infracción de los Términos y de cualquier tipo de conducta que afecte o dañe a Ubeep. \n\t\t\t</p>\n\t\t</div>\n\n\t\t<div class=\"container-policy\">\n\t\t\t<label>\n\t\t\t\t9. Legislación aplicable\n\t\t\t</label>\n\t\t\t<br>\n\t\t\t<p>\n\t\t\t\tCualquier conflicto o disputa que derive de la prestación de los Servicios por parte de Ubeep se deberá someter a la justicia ordinaria y se aplicarán las normas legales vigentes de la República de Colombia así como las costumbres comerciales debidamente certificadas. \n\t\t\t</p>\n\t\t</div>\n\n\t\t<div class=\"container-policy\">\n\t\t\t<label>\n\t\t\t\t10. Notificaciones\n\t\t\t</label>\n\t\t\t<br>\n\t\t\t<p>\n\t\t\t\tUsted autoriza para que Ubeep pueda enviarle notificaciones de los Servicios o de cualquier tema relacionado con UBeep al correo electrónico registrado en el formulario de registro, mediante un aviso en la aplicación dirigido a su cuenta personal, o a través de un mensaje de texto al número de celular que usted registro en el formulario de registro.\n\t\t\t</p>\n\t\t</div>\n\n\t\t<div class=\"container-policy\">\n\t\t\t<label>\n\t\t\t\t11. Cesión\n\t\t\t</label>\n\t\t\t<br>\n\t\t\t<p>\n\t\t\t\tUsted no podrá ceder estos Términos, en parte o en totalidad ni podrá transferir su cuenta o ceder sus derechos que le conceden estos Términos por ningún motivo y bajo ninguna excepción.  \n\t\t\t</p>\n\t\t</div>\n\n\t</div>\n</div>"

/***/ }),

/***/ 518:
/***/ (function(module, exports) {

module.exports = "<div id=\"containerProfile\" class=\"container\">\n\t<div class=\"col-sm-12\">\n\t\t<div class=\"row\">\n\t\t\t<form #formProfile=\"ngForm\">\n\t\t\t\t<div class=\"row container-title\">\n\t\t\t\t\t<div class=\"col-sm-4 text-right\">\n\t\t\t\t\t\t<div class=\"row text-1-8em\">\n\t\t\t\t\t\t\tConfigurar datos de tu perfil\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-sm-4 container-picture\">\n\t\t\t\t\t<figure id=\"container-img-profile\">\n\t\t\t\t\t\t<img src=\"./assets/img/pic.png\" alt=\"...\" class=\"img-circle\">\n\t\t\t\t\t</figure>\n\t\t\t\t\t<p>\n\t\t\t\t\t\t<span>\n\t\t\t\t\t\t\t<!--Foto de perfil-->\n\t\t\t\t\t\t</span>\n\t\t\t\t\t</p>\n\t\t\t\t\t<p>\n\t\t\t\t\t\t<!--<a href=\"\">\n\t\t\t\t\t\t\t<span>\n\t\t\t\t\t\t\t\tCambiar foto de perfil\n\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t</a>-->\n\t\t\t\t\t</p>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-sm-8\">\n\t\t\t\t\t<input type=\"hidden\" name=\"client_id\" id=\"client_id\" class=\"form-control\" placeholder=\"\" required [(ngModel)]=\"dataProfile.client_id\" ngControl=\"client_id\">\n\t\t\t\t\t<div class=\"container-form\">\n\t\t\t\t\t\t<div class=\"form-group\" style=\"overflow: hidden;\">\n\t\t\t\t\t\t\t<label class=\"col-sm-12\" for=\"name\">Nombre de usuario</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-12 row\">\n\t\t\t\t\t\t\t\t<input type=\"text\" name=\"name\" id=\"name\" class=\"form-control\" placeholder=\"\" required [(ngModel)]=\"dataProfile.name\" ngControl=\"name\">\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\" style=\"overflow: hidden;\">\n\t\t\t\t\t\t\t<label class=\"col-sm-12\" for=\"identify\">Cédula de ciudadania</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-12 row\">\n\t\t\t\t\t\t\t\t<input type=\"text\" name=\"identify\" id=\"identify\" class=\"form-control\" placeholder=\"\" required [(ngModel)]=\"dataProfile.identify\" ngControl=\"identify\">\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\" style=\"overflow: hidden;\">\n\t\t\t\t\t\t\t<label class=\"col-sm-12\" for=\"email\">Correo electrónico</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-12 row\">\n\t\t\t\t\t\t\t\t<input type=\"text\" name=\"email\" id=\"email\" class=\"form-control\" placeholder=\"\" required [(ngModel)]=\"dataProfile.email\" ngControl=\"email\" readonly>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\" style=\"overflow: hidden;\">\n\t\t\t\t\t\t\t<label class=\"col-sm-12\" for=\"movil_phone\">Celular</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-12 row\">\n\t\t\t\t\t\t\t\t<input type=\"text\" name=\"movil_phone\" id=\"movil_phone\" class=\"form-control\" placeholder=\"\" required [(ngModel)]=\"dataProfile.movil_phone\" ngControl=\"movil_phone\">\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\n\t\t\t\t\t<div class=\"col-sm-12 text-right row container-buttons\">\n\t\t\t\t\t\t<p class=\"font-bold\">\n\t\t\t\t\t\t\t{{responseMessage}}\n\t\t\t\t\t\t</p>\n\t\t\t\t\t\t<!--<button type<=\"button\" class=\"btn\" style=\"background-color:#3a383b;color:white;\" [disabled]=\"loadingProcess\">\n\t\t\t\t\t\t\t<span >Cancelar</span>\n\t\t\t\t\t\t</button>-->\n\t\t\t\t\t\t<button type=\"button\" (click)=\"submitProfile(formProfile)\" class=\"btn btn-contact\" [disabled]=\"loadingProcess\">\n\t\t\t\t\t\t\t<i *ngIf=\"loadingProcess\" class=\"fa fa-spinner fa-pulse fa-1x fa-fw margin-bottom\"></i>\n\t\t\t\t\t\t\t<span *ngIf=\"!loadingProcess\">Guardar cambios</span>\n\t\t\t\t\t\t</button>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</form>\n\t\t</div>\n\t</div>\n</div>"

/***/ }),

/***/ 519:
/***/ (function(module, exports) {

module.exports = "<div id=\"container-register-company\">\n\t<div class=\"container-background\">\n\t\t\n\t\t<div class=\"container\">\n\t\t\t<div class=\"col-xs-12 col-sm-12\">\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\t<div class=\"col-xs-12 col-sm-6\">\n\t\t\t\t\t\t\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"col-xs-12 col-sm-6\">\n\t\t\t\t\t\t<div class=\"panel panel-default\">\n\t\t\t\t\t\t\t<div class=\"panel-heading text-center text-1-8em\">INSCRIBE TU EMPRESA</div>\n\t\t\t\t\t\t\t<div class=\"panel-body text-center\">\n\t\t\t\t\t\t\t\t<form #formRegister=\"ngForm\" id=\"formInvite\" class=\"col-xs-12 col-sm-12 row\">\n\t\t\t\t\t\t\t\t\t<div class=\"text-1-5em margin-bottom-50\">\n\t\t\t\t\t\t\t\t\t\tEnvíanos tus datos y nuestro equipo se pondrá en contacto contigo\n\t\t\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t\t\t\t<div class=\"margin-bottom-50\">\n\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control col-sm-12 input-lg\" id=\"name\" placeholder=\"Nombre\" name=\"name\" ngModel required [(ngModel)]=\"dataRegister.name\" ngControl=\"name\" (keyup)=\"changeValidateInput('name', formRegister)\">\n\t\t\t\t\t\t\t\t\t\t\t<div style=\"display: none\" id=\"status2\" class=\"text-danger\">¡Campo requerido!</div>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control col-sm-12 input-lg\" id=\"phone\" placeholder=\"Teléfono\" name=\"phone\" ngModel required [(ngModel)]=\"dataRegister.phone\" ngControl=\"phone\" (keyup)=\"changeValidateInput('phone', formRegister)\">\n\t\t\t\t\t\t\t\t\t\t\t<div style=\"display: none\" id=\"status3\" class=\"text-danger\">¡Campo requerido!</div>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control col-sm-12 input-lg\" id=\"email\" placeholder=\"Correo electrónico\" name=\"email\" ngModel required [(ngModel)]=\"dataRegister.email\" ngControl=\"email\" (keyup)=\"changeValidateInput('email', formRegister)\">\n\t\t\t\t\t\t\t\t\t\t\t<div style=\"display: none\" id=\"status1\" class=\"text-danger\">¡Campo requerido!</div>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t\t\t\t<p class=\"text-center\" style=\"font-weight:bold;\">\n\t\t\t\t\t\t\t            {{responseMessage}}\n\t\t\t\t\t\t\t        </p>\n\n\t\t\t\t\t\t\t\t\t<div class=\"\">\n\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-lg col-xs-12 col-sm-12 btn-orange\" (click)=\"sendEmail(formRegister)\" [disabled]=\"loadingProcess\">\n\t\t\t\t\t\t\t\t\t\t\t<i *ngIf=\"loadingProcess\" class=\"fa fa-spinner fa-pulse fa-1x fa-fw margin-bottom\"></i>\n\t\t\t\t\t\t\t\t\t\t\t<span *ngIf=\"!loadingProcess\" class=\"\">Enviar datos</span>\n\t\t\t\t\t\t\t            </button>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</form>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t\n\t</div>\n</div>"

/***/ }),

/***/ 520:
/***/ (function(module, exports) {

module.exports = "<div id=\"container-security-policies\">\n\t<div class=\"container\">\n\t\t<figure id=\"logo\">\n\t\t\t<img src=\"./assets/img/logo.png\" alt=\"UBeep\">\n\t\t</figure>\n\t\t<p class=\"text-center\">\n\t\t\t<label>Política de Seguridad y Tratamiento de Datos<br>UNEXT SAS</label>\n\t\t</p>\n\n\t\t<div class=\"container-policy\">\n\t\t\t<p>\n\t\t\tEn cumplimiento a lo dispuesto en la Ley estatutaria 1581 de 2012 y a su Decreto Reglamentario 1377 de 2013, UNEXT S.A.S (en adelante “Ubeep”) informa la política de seguridad y tratamiento de datos (en adelante la <b>“Política”</b>) \n\t\t\t</p>\n\t\t</div>\n\n\t\t<div class=\"container-policy\">\n\t\t\t<label>\n\t\t\t\t1. Identificación\n\t\t\t</label>\n\t\t\t<br>\n\t\t\t<p>\n\t\t\t\tNombre: Unext S.A.S sociedad comercial válidamente constituida bajo las leyes de Colombia e identificada con número de identificación tributaria No. 901003017-7\n\t\t\t\t<br>\n\t\t\t\tDomicilio: Bogotá\n\t\t\t\t<br>\n\t\t\t\tDirección: Carrera 85a No. 52a-42, Bogotá D.C.\n\t\t\t\t<br>\n\t\t\t\tTeléfono: (+57 1) 4786962\n\t\t\t\t<br>\n\t\t\t\tCorreo Electrónico: servicioalcliente@ubeep.co\n\t\t\t</p>\n\t\t</div>\n\n\t\t<div class=\"container-policy\">\n\t\t\t<label>\n\t\t\t\t2. Marco Legal\n\t\t\t</label>\n\t\t\t<br>\n\t\t\t<p>\n\t\t\t\tConstitución Política, artículo 15.\n\t\t\t\t<br>\n\t\t\t\tLey 1266 de 2008\n\t\t\t\t<br>\n\t\t\t\tLey 1581 de 2012\n\t\t\t\t<br>\n\t\t\t\tDecretos Reglamentarios 1727 de 2009 y 2952 de 2010,\n\t\t\t\t<br>\n\t\t\t\tDecreto Reglamentario parcial 1377 de 2013\n\t\t\t</p>\n\t\t</div>\n\n\t\t<div class=\"container-policy\">\n\t\t\t<label>\n\t\t\t\t3. Definiciones\n\t\t\t</label>\n\t\t\t<br>\n\t\t\t<p>\n\t\t\t\t<label>AUTORIZACIÓN:</label> Consentimiento previo, expreso e informado del titular para llevar a cabo el tratamiento de datos personales.\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>AVISO DE PRIVACIDAD:</label> Comunicación verbal o escrita generada por el responsable dirigida al titular para el tratamiento de sus datos personales, mediante la cual se le informa acerca de la existencia de las políticas de tratamiento de información que le serán aplicables, la forma de acceder a las mismas y las finalidades del tratamiento que se pretende dar a los datos personales.\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>BASE DE DATOS:</label> Conjunto organizado de datos personales que sea objeto de tratamiento.\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>CAUSAHABIENTE:</label> Persona que ha sucedido a otra por causa del fallecimiento de ésta (heredero).\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>DATO PERSONAL:</label> Cualquier pieza de información vinculada a una o varias personas determinadas o determinables o que puedan asociarse a una persona natural o jurídica.\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>DATO PÚBLICO:</label> Es el dato que no sea semiprivado, privado o sensible. Son considerados datos públicos, entre otros, los datos relativos al estado civil de las personas, a su profesión u oficio y a su calidad de comerciante o de servidor público. Por su naturaleza, los datos públicos pueden estar contenidos, entre otros, en registros públicos, documentos públicos, gacetas y boletines oficiales y sentencias judiciales debidamente ejecutoriadas que no estén sometidas a reserva.\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>DATOS SENSIBLES:</label> Se entiende por datos sensibles aquellos que afectan la intimidad del titular o cuyo uso indebido puede generar su discriminación, tales como aquellos que revelen el origen racial o étnico, la orientación política, las convicciones religiosas o filosóficas, la pertenencia a sindicatos, organizaciones sociales, de derechos humanos o que promueva intereses de cualquier partido político o que garanticen los derechos y garantías de partidos políticos de oposición, así como los datos relativos a la salud, a la vida sexual, y los datos biométricos.\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>DATOS INDISPENSABLES:</label> Se entienden como aquellos datos personales de los titulares imprescindibles para llevar a cabo la actividad de Ubeep. Los datos de naturaleza indispensable deberán ser proporcionados por los titulares de los mismos o los legitimados para el ejercicio de estos derechos.\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>DATOS OPCIONALES:</label> Son aquellos datos que UBeep  requiere para ofrecer servicios adicionales.\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>ENCARGADO DEL TRATAMIENTO:</label> Persona natural o jurídica, pública o privada que por sí misma o en asocio con otros, realice el Tratamiento de datos personales por cuenta del Responsable del Tratamiento.\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>LEY DE PROTECCIÓN DE DATOS:</label> Es la Ley 1581 de 2012 y sus Decretos reglamentarios o las normas que los modifiquen, complementen o sustituyan.\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>HABEAS DATA:</label> Derecho de cualquier persona a conocer, actualizar y rectificar las informaciones que se hayan recogido sobre ellas en el banco de datos y en archivos de entidades públicas y privadas.\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>REUbeep ONSABLE DEL TRATAMIENTO:</label> Persona natural o jurídica, pública o privada que por sí misma o en asocio con otros, decida sobre la base de datos y/o Tratamiento de los datos.\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>TITULAR:</label> Persona natural cuyos datos personales sean objeto de Tratamiento.\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>TRATAMIENTO:</label> Cualquier operación o conjunto de operaciones sobre datos personales, tales como la recolección, almacenamiento, uso, circulación o supresión.\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>TRANSFERENCIA:</label> La transferencia de datos tiene lugar cuando el responsable y/o encargado del tratamiento de datos personales, ubicado en Colombia, envía la información o los datos personales a un receptor, que a su vez es responsable del tratamiento y se encuentra dentro o fuera del país.\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>TRANSMISIÓN:</label> Tratamiento de datos personales que implica la comunicación de los mismos dentro o fuera del territorio de la República de Colombia cuando tenga por objeto la realización de un tratamiento por el encargado por cuenta del responsable.\n\t\t\t</p>\n\t\t</div>\n\n\t\t<div class=\"container-policy\">\n\t\t\t<label>\n\t\t\t\t4. Principios\n\t\t\t</label>\n\t\t\t<br>\n\t\t\t<p>\n\t\t\t\t<label>a) Principio de legalidad en materia de Tratamiento de datos:</label> El Tratamiento a que se refiere la Política es una actividad reglada que debe sujetarse a lo establecido en la ley y en las demás disposiciones que la desarrollen:\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>b) Principio de finalidad:</label> El Tratamiento debe obedecer a una finalidad legítima de acuerdo con la Constitución y la Ley, la cual debe ser informada al Titular;\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>c) Principio de libertad:</label> El Tratamiento sólo puede ejercerse con el consentimiento, previo, expreso e informado del Titular. Los datos personales no podrán ser obtenidos o divulgados sin previa autorización, o en ausencia de mandato legal o judicial que releve el consentimiento;\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>d) Principio de veracidad o calidad:</label> La información sujeta a Tratamiento debe ser veraz, completa, exacta, actualizada, comprobable y comprensible. Se prohíbe el Tratamiento de datos parciales, incompletos, fraccionados o que induzcan a error;\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>e) Principio de transparencia:</label> En el Tratamiento debe garantizarse el derecho del Titular a obtener del Responsable del Tratamiento o del Encargado del Tratamiento, en cualquier momento y sin restricciones, información acerca de la existencia de datos que le concierne;\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>f) Principio de acceso y circulación restringida:</label> El Tratamiento se sujeta a los límites que se derivan de la naturaleza de los datos personales, de las disposiciones de la ley y la Constitución. En este sentido, el Tratamiento sólo podrá hacerse por personas autorizadas por el Titular y/o por las personas previstas en la ley; Los datos personales, salvo la información pública, no podrán estar disponibles en Internet u otros medios de divulgación o comunicación masiva, salvo que el acceso sea técnicamente controlable para brindar un conocimiento restringido sólo a los Titulares o terceros autorizados conforme a la ley;\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>g) Principio de seguridad:</label> La información sujeta a Tratamiento por el Responsable del Tratamiento o Encargado del Tratamiento a que se refiere la presente ley, se deberá manejar con las medidas técnicas, humanas y administrativas que sean necesarias para otorgar seguridad a los registros evitando su adulteración, pérdida, consulta, uso o acceso no autorizado o fraudulento;\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>h) Principio de confidencialidad:</label> Todas las personas que intervengan en el Tratamiento de datos personales que no tengan la naturaleza de públicos están obligadas a garantizar la reserva de la información, inclusive después de finalizada su relación con alguna de las labores que comprende el Tratamiento, pudiendo sólo realizar suministro o comunicación de datos personales cuando ello corresponda al desarrollo de las actividades autorizadas en la ley y en los términos de la misma.\n\t\t\t</p>\n\t\t</div>\n\n\t\t<div class=\"container-policy\">\n\t\t\t<label>\n\t\t\t\t5. Derechos que asisten al Titular \n\t\t\t</label>\n\t\t\t<br>\n\t\t\t<p>\n\t\t\t\tEl Titular de los datos personales tendrá los siguientes derechos:\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>a)</label> Conocer, actualizar y rectificar sus datos personales frente Ubeep. Este derecho se podrá ejercer, entre otros frente a datos parciales, inexactos, incompletos, fraccionados, que induzcan a error, o aquellos cuyo Tratamiento esté expresamente prohibido o no haya sido autorizado;\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>b)</label> Solicitar prueba de la autorización otorgada  a UBeep  salvo cuando expresamente se exceptúe como requisito para el Tratamiento, de conformidad con lo previsto en el artículo 10 de la Ley de Protección de Datos;\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>c)</label> Ser informado por UBeep  previa solicitud, respecto del uso que le ha dado a sus datos personales;\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>d)</label> Presentar ante la Superintendencia de Industria y Comercio quejas por infracciones a lo dispuesto en la ley y las demás normas que la modifiquen, adicionen o complementen;\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>e)</label> Revocar la autorización y/o solicitar la supresión del dato cuando en el Tratamiento no se respeten los principios, derechos y garantías constitucionales y legales. La revocatoria y/o supresión procederá cuando la Superintendencia de Industria y Comercio haya determinado que en el Tratamiento, UBeep  ha incurrido en conductas contrarias a esta ley y a la Constitución;\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>f)</label> Acceder en forma gratuita a sus datos personales que hayan sido objeto de Tratamiento.\n\t\t\t</p>\n\t\t</div>\n\n\t\t<div class=\"container-policy\">\n\t\t\t<label>\n\t\t\t\t6. Deberes de UBeep  en el Tratamiento de datos del titular\n\t\t\t</label>\n\t\t\t<br>\n\t\t\t<p>\n\t\t\t\tEn virtud de la presente Política y la Ley de Protección de Datos son deberes de Ubeep,  los siguientes: \n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>a)</label> Garantizar al Titular, en todo tiempo, el pleno y efectivo ejercicio del derecho de hábeas data;\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>b)</label> Solicitar y conservar, en las condiciones previstas en la presente ley, copia de la respectiva autorización otorgada por el Titular;\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>c)</label> Informar debidamente al Titular sobre la finalidad de la recolección y los derechos que le asisten por virtud de la autorización otorgada;\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>d)</label> Conservar la información bajo las condiciones de seguridad necesarias para impedir su adulteración, pérdida, consulta, uso o acceso no autorizado o fraudulento;\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>e)</label> Garantizar que la información que se suministre al Encargado del Tratamiento sea veraz, completa, exacta, actualizada, comprobable y comprensible;\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>f)</label> Actualizar la información, comunicando de forma oportuna al Encargado del Tratamiento, todas las novedades respecto de los datos que previamente le haya suministrado y adoptar las demás medidas necesarias para que la información suministrada a este se mantenga actualizada;\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>g)</label> Rectificar la información cuando sea incorrecta y comunicar lo pertinente al Encargado del Tratamiento;\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>h)</label> Suministrar al Encargado del Tratamiento, según el caso, únicamente datos cuyo Tratamiento esté previamente autorizado de conformidad con lo previsto en la presente ley;\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>i)</label> Exigir al Encargado del Tratamiento en todo momento, el respeto a las condiciones de seguridad y privacidad de la información del Titular;\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>j)</label> Tramitar las consultas y reclamos formulados en los términos señalados en la presente ley;\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>k)</label> Adoptar un manual interno de políticas y procedimientos para garantizar el adecuado cumplimiento de la presente ley y en especial, para la atención de consultas y reclamos;\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>l)</label> Informar al Encargado del Tratamiento cuando determinada información se encuentra en discusión por parte del Titular, una vez se haya presentado la reclamación y no haya finalizado el trámite respectivo;\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>m)</label> Informar a solicitud del Titular sobre el uso dado a sus datos;\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>n)</label> Informar a la autoridad de protección de datos cuando se presenten violaciones a los códigos de seguridad y existan riesgos en la administración de la información de los Titulares.\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>o)</label> Cumplir las instrucciones y requerimientos que imparta la Superintendencia de Industria y Comercio\n\t\t\t</p>\n\t\t</div>\n\n\t\t<div class=\"container-policy\">\n\t\t\t<label>\n\t\t\t\t6.1  Deberes del Encargado del Tratamiento\n\t\t\t</label>\n\t\t\t<br>\n\t\t\t<p>\n\t\t\t\t<label>a)</label> Garantizar al Titular, en todo tiempo, el pleno y efectivo ejercicio del derecho de hábeas data;\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>b)</label> Conservar la información bajo las condiciones de seguridad necesarias para impedir su adulteración, pérdida, consulta, uso o acceso no autorizado o fraudulento;\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>c)</label> Realizar oportunamente la actualización, rectificación o supresión de los datos en los términos de la Política y la ley;\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>d)</label> Actualizar la información reportada por los Responsables del Tratamiento dentro de los cinco (5) días hábiles contados a partir de su recibo;\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>e)</label> Tramitar las consultas y los reclamos formulados por los Titulares en los términos señalados en la ley;\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>f)</label> Registrar en la base de datos las leyenda \"reclamo en trámite\" en la forma en que se regula en la Política y en la ley;\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>g)</label> Insertar en la base de datos la leyenda \"información en discusión judicial\" una vez notificado por parte de la autoridad competente sobre procesos judiciales relacionados con la calidad del dato personal;\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>h)</label> Abstenerse de circular información que esté siendo controvertida por el Titular y cuyo bloqueo haya sido ordenado por la Superintendencia de Industria y Comercio;\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>i)</label> Permitir el acceso a la información únicamente a las personas que pueden tener acceso a ella;\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>j)</label> Informar a la Superintendencia de Industria y Comercio cuando se presenten violaciones a los códigos de seguridad y existan riesgos en la administración de la información de los Titulares;\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>k)</label> Cumplir las instrucciones y requerimientos que imparta la Superintendencia de Industria y Comercio.\n\t\t\t</p>\n\t\t</div>\n\n\t\t<div class=\"container-policy\">\n\t\t\t<label>\n\t\t\t\t7. Registro Nacional de Bases de Datos\n\t\t\t</label>\n\t\t\t<br>\n\t\t\t<p>\n\t\t\t\tEl Registro Nacional de Bases de Datos (RNBD), es el directorio público de las bases de datos sujetas a Tratamiento que operan en el país y será administrado por la Superintendencia de Industria y Comercio y será de libre consulta para los ciudadanos.\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\tUBeep  informa a los Titulares que en su deber legal aportá a la Superintendencia de Industria y Comercio las bases de datos sujetas a tratamiento.\n\t\t\t</p>\n\t\t</div>\n\n\t\t<div class=\"container-policy\">\n\t\t\t<label>\n\t\t\t\t8. Autorizaciones y Consentimiento del Titular\n\t\t\t</label>\n\t\t\t<br>\n\t\t\t<p>\n\t\t\t\tSin perjuicio de las excepciones previstas en la ley, en el Tratamiento se requiere la autorización previa e informada del Titular, la cual deberá ser obtenida por cualquier medio que pueda ser objeto de consulta posterior.\n\t\t\t</p>\n\t\t</div>\n\n\t\t<div class=\"container-policy\">\n\t\t\t<label>\n\t\t\t\t9. Medio y Manifestación para Otorgar la Autorización del Titular para el Tratamiento de sus Datos\n\t\t\t</label>\n\t\t\t<br>\n\t\t\t<p>\n\t\t\t\tUBeep  generó en los términos de la ley un Aviso de Privacidad bajo el cual el Titular expresa su Autorización para el tratamientos de sus datos de acuerdo en lo contenido en la Política y la ley. De igual manera el Aviso de Privacidad informa al Titular que para ejercer su derecho de tratamiento de datos personales este podrá acceder a la página web de UBeep  www.ubeep.co  o enviar un correo a servicioalcliente@ubeep.co  solicitando el ejercicio de los derechos contenidos en la presente Política y/o en la ley.\n\t\t\t</p>\n\t\t</div>\n\n\t\t<div class=\"container-policy\">\n\t\t\t<label>\n\t\t\t\t10. Eventos en los cuales no es necesaria la Autorizaciòn del Titular de los Datos Personales\n\t\t\t</label>\n\t\t\t<br>\n\t\t\t<p>\n\t\t\t\tLa autorización del Titular no será necesaria cuando se trate de:\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>a)</label> Información requerida por una entidad pública o administrativa en ejercicio de sus funciones legales o por orden judicial;\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>b)</label> Datos de naturaleza pública;\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>c)</label> Casos de urgencia médica o sanitaria;\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>d)</label> Tratamiento de información autorizado por la ley para fines históricos, estadísticos o científicos;\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>e)</label> Datos relacionados con el Registro Civil de las Personas\n\t\t\t</p>\n\t\t</div>\n\n\t\t<div class=\"container-policy\">\n\t\t\t<label>\n\t\t\t\t11. Legitimación para el ejercicio del derecho del Titular\n\t\t\t</label>\n\t\t\t<br>\n\t\t\t<p>\n\t\t\t\tLos derechos de los titulares establecidos en la Ley podrán ejercerse por las siguientes personas:\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>a)</label> Por el titular, quien deberá acreditar su identidad en forma suficiente por los distintos medios que le ponga a disposición Ubeep\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>b)</label> Por los causahabientes del titular, quienes deberán acreditar tal calidad.\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>c)</label> Por el representante y/o apoderado del titular, previa acreditación de la representación o apoderamiento.\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>d)</label> Por estipulación a favor de otro o para otro.\n\t\t\t</p>\n\t\t</div>\n\n\t\t<div class=\"container-policy\">\n\t\t\t<label>\n\t\t\t\t12. Tratamiento al cual serán sometidos los Datos y Finalidad del mismo.\n\t\t\t</label>\n\t\t\t<br>\n\t\t\t<p>\n\t\t\t\t<label>a)</label> La principal finalidad del Tratamiento de los datos del Titular es proporcionar una experiencia segura, eficiente, personalizada y eficaz, de los servicios de UBeep  al Titular.\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>b)</label> UBeep  recauda la información personal del Titular, o delega a proveedores de servicios la función de recopilar y usar dichos datos en nombre de Ubeep,  atendiendo para ello lo establecido en esta Política y la ley según esté permitido o sea necesario para\n\t\t\t</p>\n\t\t\t<div>\n\t\t\t\t<p>\n\t\t\t\t\t<label>b.1)</label> Procesar las transacciones y enviar notificaciones a los portales de comercio que se integran son Ubeep,  a las instituciones que validan y procesan las transacciones y a los pagadores. Para el procesamiento de las transacciones UBeep  podrá validar y compartir la información personal del Titular con terceros, tales como instituciones financieras emisoras de los medios de pago, franquicias de adquirencia como Visa y MasterCard, entre otras. <br> Aceptando esta Política de Privacidad, el Titular autoriza a compartir su información personal con tales instituciones.\n\t\t\t\t</p>\n\t\t\t\t<p>\n\t\t\t\t\t<label>b.2)</label> Utilizar los datos del Titular en los filtros internos o provistos por terceros a Ubeep, a fin de validar las transacciones y mitigar el riesgo de suplantación de identidad de los tarjetahabientes.\n\t\t\t\t</p>\n\t\t\t\t<p>\n\t\t\t\t\t<label>b.3)</label> Brindarle al Titular un servicio de asistencia y solución de problemas.\n\t\t\t\t</p>\n\t\t\t\t<p>\n\t\t\t\t\t<label>b.4)</label> Prevenir que el sistema de UBeep  sea utilizado para realizar actividades ilegales y con el fin de hacer cumplir los Términos y Condiciones Generales de uso del sistema de Ubeep.\n\t\t\t\t</p>\n\t\t\t\t<p>\n\t\t\t\t\t<label>b.5)</label> Personalizar, medir y mejorar los servicios prestados por Ubeep,  las páginas web y accesos a los servicios de Ubeep.\n\t\t\t\t</p>\n\t\t\t\t<p>\n\t\t\t\t\t<label>b.6)</label> Comparar información para verificar su precisión.\n\t\t\t\t</p>\n\t\t\t\t<p>\n\t\t\t\t\t<label>b.7)</label> Informar al Titular sobre fallos y actualizaciones de los servicios.\n\t\t\t\t</p>\n\t\t\t\t<p>\n\t\t\t\t\t<label>b.8)</label> Verificar la identidad del Titular.\n\t\t\t\t</p>\n\t\t\t\t<p>\n\t\t\t\t\t<label>b.9)</label> Contactar o enviar notificaciones al Titular en caso de ser necesario por las vías de comunicación que autorice el Titular.\n\t\t\t\t</p>\n\t\t\t\t<p>\n\t\t\t\t\t<label>b.10)</label> Resolver disputas.\n\t\t\t\t</p>\n\t\t\t\t<p>\n\t\t\t\t\t<label>b.11)</label> Facturar el uso de los servicios Ubeep.\n\t\t\t\t</p>\n\t\t\t\t<p>\n\t\t\t\t\t<label>b.12)</label> Usar la información personal del Titular para fines internos, como auditorías, análisis de datos e investigaciones para mejorar los productos, servicios y comunicaciones con los usuarios de los servicios de Ubeep.\n\t\t\t\t</p>\n\t\t\t\t<p>\n\t\t\t\t\t<label>b.13)</label> Llevar a cabo el Tratamiento de los datos personales del Titular en los servidores de Ubeep,  en servidores nacionales e internacionales asegurando siempre que esos países tengan niveles de protección de datos personales acordes a los establecidos en esta Política y en la ley. UBeep  se reserva el derecho de transferir la información personal del Titular fuera del país de su residencia y domicilio, asegurando siempre que esos países tengan niveles de protección de datos personales acordes a los establecidos en esta Política de Privacidad y a la legislación aplicable y siempre que dicha transferencia se realice para cumplir con la prestación de nuestros Servicios o se dé en desarrollo de alguna de las finalidades establecidas en esta Política. Al usar los servicios Ubeep,  El Titular autoriza dicha transferencia.\n\t\t\t\t</p>\n\t\t\t\t<p>\n\t\t\t\t\t<label>b.14)</label> Almacenar los datos personales del Titular por el tiempo que sea requerido para el cumplimiento de los fines para los que fueron recolectados y conforme a las leyes comerciales o de privacidad y protección de la información.\n\t\t\t\t</p>\n\t\t\t\t<p>\n\t\t\t\t\t<label>b.15)</label> Si el Titular es un pagador este tendrá a su disposición, para cuando lo requiera los pagos realizados, la información relacionada con sus equipos electrónicos, así como de su medios de pago, tales como números de tarjetas, códigos de seguridad, siempre y cuando decida hacer uso de las funcionalidades de almacenamiento de datos o similares. Los datos de tarjetas de crédito serán guardados por Ubeep,  teniendo en cuenta los estándares de seguridad y protección de la información establecidos por la normatividad de aplicable.\n\t\t\t\t</p>\n\t\t\t\t<p>\n\t\t\t\t\t<label>b.16)</label> Solicitar la opinión del Titular o su participación en encuestas electrónicas. \n\t\t\t\t</p>\n\t\t\t\t<p>\n\t\t\t\t\t<label>b.17)</label> Enviar al Titular comunicaciones e información comercial y publicitaria de las compañías UBeep  de sus comercios registrados en UBeep  y de terceros que incluyan promociones para la compra de bienes o servicios a través de Ubeep.\n\t\t\t\t</p>\n\t\t\t\t<p>\n\t\t\t\t\t<label>b.18)</label> Organizar y llevar a cabo concursos, juegos, ofertas u operaciones de promoción o marketing, y eventos similares de UBeep  y/o los comercios vinculados o no a Ubeep.\n\t\t\t\t</p>\n\t\t\t\t<p>\n\t\t\t\t\t<label>b.19)</label> Enviar al Titular cupones de descuento u otorgar beneficios para futuras compras a través de Ubeep.\n\t\t\t\t</p>\n\t\t\t\t<p>\n\t\t\t\t\t<label>b.20)</label> Proponer la afiliación del Titular a programas de lealtad, crear perfiles de consumo de usuarios, y enviarle materiales promocionales y publicitarios que puedan llegar a ser de su interés.\n\t\t\t\t</p>\n\t\t\t</div>\n\t\t\t<p>\n\t\t\t\t<label>c)</label> UBeep  solicitará al Titular su consentimiento antes de utilizar tus datos personales, para cualquier fin distinto de los establecidos en su Política.\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>d)</label> UBeep  recopilará los datos personales del Titular en sus correspondientes bases de datos o bancos de datos personales y les dará el tratamiento establecido en esta Política. Cuando quiera que la ley lo exija, dichas bases de datos o bancos de datos serán registradas ante las autoridades competentes.\n\t\t\t</p>\n\t\t</div>\n\n\t\t<div class=\"container-policy\">\n\t\t\t<label>\n\t\t\t\t13. Personas a quienes se les puede suministrar la información tratada\n\t\t\t</label>\n\t\t\t<br>\n\t\t\t<p>\n\t\t\t\tLa información que reúna las condiciones establecidas en la ley podrá suministrarse a las siguientes personas:\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>a)</label> A los titulares, sus causahabientes (cuando aquellos falten) o sus representantes legales.\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>b)</label> A las entidades públicas o administrativas en ejercicio de sus funciones legales o por orden judicial.\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>c)</label> A los terceros autorizados por el titular o por la ley.\n\t\t\t</p>\n\t\t</div>\n\n\t\t<div class=\"container-policy\">\n\t\t\t<label>\n\t\t\t\t14. Persona o dirección responsable de la atención de peticiones, consultas y reclamos\n\t\t\t</label>\n\t\t\t<br>\n\t\t\t<p>\n\t\t\t\tUBeep  ha designado a la dirección de atención al usuario como el ente encargado de recibir peticiones, consultas y reclamos, y de darle su correcto direccionamiento y solución.\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>a) Consultas</label> \n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\tLos Titulares o sus causahabientes podrán consultar la información personal del Titular que repose en las bases de UBeep  quien suministrará toda la información contenida en el registro individual o que esté vinculada con la identificación del Titular. La consulta se formulará a través del correo servicioalcliente@ubeep.co . La consulta será atendida en un término máximo de diez (10) días hábiles contados a partir de la fecha de recibo de la misma. Cuando no fuere posible atender la consulta dentro de dicho término, se informará al interesado, expresando los motivos de la demora y señalando la fecha en que se atenderá su consulta, la cual en ningún caso podrá superar los cinco (5) días hábiles siguientes al vencimiento del primer término.\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>b) Reclamos</label> \n\t\t\t</p>\n\t\t\t<div>\n\t\t\t\tEl Titular o sus causahabientes que consideren que la información contenida en una base de datos debe ser objeto de corrección, actualización o supresión, o cuando adviertan el presunto incumplimiento de cualquiera de los deberes contenidos en la ley, podrán presentar un reclamo ante UBeep  el cual será tramitado bajo las siguientes reglas:\n\t\t\t\t<div>\n\t\t\t\t\t<p>\n\t\t\t\t\t\t<label>1. </label> El reclamo del Titular se formulará mediante solicitud dirigida a UBeep  por el correo electrónico servicioalcliente@ubeep.co  con la identificación del Titular, la descripción de los hechos que dan lugar al reclamo, la dirección, y acompañando los documentos que se quiera hacer valer. Si el reclamo resulta incompleto, se requerirá al interesado dentro de los cinco (5) días siguientes a la recepción del reclamo para que subsane las fallas. Transcurridos dos (2) meses desde la fecha del requerimiento, sin que el solicitante presente la información requerida, se entenderá que ha desistido del reclamo. En caso de que quien reciba el reclamo no sea competente para resolverlo, dará traslado a quien corresponda en un término máximo de dos (2) días hábiles e informará de la situación al interesado.\n\t\t\t\t\t</p>\n\t\t\t\t\t<p>\n\t\t\t\t\t\t<label>2. </label> Una vez recibido el correo servicioalcliente@ubeep.co  con el reclamo completo, éste se catalogará con la etiqueta \"reclamo en trámite\" y el motivo del mismo en un término no mayor a dos (2) días hábiles. Dicha etiqueta se mantendrá hasta que el reclamo sea decidido.\n\t\t\t\t\t</p>\n\t\t\t\t\t<p>\n\t\t\t\t\t\t<label>3. </label> El término máximo para atender el reclamo será de quince (15) días hábiles contados a partir del día siguiente a la fecha de su recibo. Cuando no fuere posible atender el reclamo dentro de dicho término, se informará al interesado los motivos de la demora y la fecha en que se atenderá su reclamo, la cual en ningún caso podrá superar los ocho (8) días hábiles siguientes al vencimiento del primer término.\n\t\t\t\t\t</p>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<p>\n\t\t\t\t<label>c) Petición de actualización, rectificación y supresión de datos</label> \n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\tUBeep  rectificará y actualizará, a solicitud del Titular, la información de éste que resulte ser incompleta o inexacta, de conformidad con el procedimiento y los términos antes señalados, para lo cual el titular allegará la solicitud al correo electrónico servicioalcliente@ubeep.co indicando la actualización, rectificación y supresión del dato y aportará la documentación que soporte su petición.\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t<label>d) Revocatoria de la autorización y/o supresión de los datos</label> \n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\tLos titulares de los datos personales pueden revocar el consentimiento al tratamiento de sus datos personales en cualquier momento, siempre y cuando no lo impida una disposición legal o contractual, para ello UBeep  pondrá a disposición del Titular el correo electrónico servicioalcliente@ubeep.com. Si vencido el término legal respectivo, Ubeep,  según fuera el caso, no hubieran eliminado los datos personales, el Titular tendrá derecho a solicitar a la Superintendencia de Industria y Comercio que ordene la revocatoria de la autorización y/o la supresión de los datos personales. Para estos efectos se aplicará el procedimiento descrito en el artículo 22 de la Ley 1581 de 2012.\n\t\t\t</p>\n\t\t</div>\n\n\t\t<div class=\"container-policy\">\n\t\t\t<label>\n\t\t\t\t15. Transferencia y Transmisión Internacional de Datos Personales\n\t\t\t</label>\n\t\t\t<br>\n\t\t\t<p>\n\t\t\t\tUBeep  en cumplimiento de la finalidad dispuesta en la presente Política para el tratamiento de datos de los Titulares podrá realizar la transferencia internacionales de datos personales de los titulares.\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\tUBeep tomará las medidas necesarias para que los terceros conozcan y se comprometan a observar esta Política, bajo el entendido que la información personal que reciban, únicamente podrá ser utilizada para asuntos  y finalidades directamente relacionados con Ubeep.\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\tPara la transferencia internacional de datos personales se observará lo previsto en el artículo 26 de la Ley 1581 de 2012.\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\tLas transmisiones internacionales de datos personales que efectúe la Ubeep,  no requerirán ser informadas al Titular ni contar con su consentimiento cuando medie un contrato de transmisión de datos personales de conformidad al artículo 25 del Decreto 1377 de 2013.\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\tCon la aceptación de la presente política, el Titular autoriza expresamente para transferir y transmitir su Información Personal.\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\tLa información será transferida y transmitida, para todas las relaciones que puedan establecerse con Ubeep. \n\t\t\t</p>\n\t\t</div>\n\n\t</div>\n</div>"

/***/ }),

/***/ 521:
/***/ (function(module, exports) {

module.exports = "<div id=\"container-security\">\n\t<div id=\"banner\">\n\t\t<div class=\"text-4-5em text-white text-shadow-black text-center\">\n\t\t\tViajes más seguros\n\t\t</div>\n\t\t<div class=\"text-4-5em text-orange text-center\">\n\t\t\tPARA TODOS\n\t\t</div>\n\t</div>\n\t<div class=\"container container-info\">\n\t\t<div class=\"col-xs-12 col-sm-12\">\n\t\t\t\n\t\t\t<div class=\"row\">\n\t\t\t\t<div class=\"col-sm-6 padding-r-40\">\n\t\t\t\t\t<div class=\"\">\n\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\tLínea de respuesta inmediata\n\t\t\t\t\t\t</label>\n\t\t\t\t\t\t<hr>\n\t\t\t\t\t\t<div>\n\t\t\t\t\t\t\tContamos con una línea de atención continua para atender cualquier tipo de incidentes que pueda presentarse en la prestación de nuestros servicios. Si requiere de nuestra colaboración inmediata comuniquese al: 9999999\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-sm-6 padding-l-40\">\n\t\t\t\t\t<div class=\"\">\n\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\tNuestros carros\n\t\t\t\t\t\t</label>\n\t\t\t\t\t\t<hr>\n\t\t\t\t\t\t<div>\n\t\t\t\t\t\t\tContamos únicamente con vehículos modelo 2012 los cuales son revisados periodicamente en talleres autorizados y vigilados por nuestros expertos en seguridad.\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\n\t\t\t<div class=\"row\">\n\t\t\t\t<div class=\"col-sm-6 padding-r-40\">\n\t\t\t\t\t<div class=\"\">\n\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\tBotón de alerta\n\t\t\t\t\t\t</label>\n\t\t\t\t\t\t<hr>\n\t\t\t\t\t\t<div>\n\t\t\t\t\t\t\tDentro de nuestra aplicación y mientras usted se encuntre en trayecto, podrá encontrar un botón de alerta para reportar emergencias. Al activarlo se enviará un mensaje inmediato a nuestra central de seguridad como a las autoridades pertinentes.\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-sm-6 padding-l-40\">\n\t\t\t\t\t<div class=\"\">\n\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\tEvaluación en dos vías\n\t\t\t\t\t\t</label>\n\t\t\t\t\t\t<hr>\n\t\t\t\t\t\t<div>\n\t\t\t\t\t\t\tNuestra aplicación permite tanto a usuarios como conductores calificar y comentar la experiencia al final de cada uno de sus viajes. Si se presentan calificaciones por debajo de 3 estrellas el usuario y el conductor no volverán a ser conectados en nuestra aplicación para futuros servicios.\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\n\t\t\t<div class=\"row\">\n\t\t\t\t<div class=\"col-sm-6 padding-r-40\">\n\t\t\t\t\t<div class=\"\">\n\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\tSelección de conductores\n\t\t\t\t\t\t</label>\n\t\t\t\t\t\t<hr>\n\t\t\t\t\t\t<div>\n\t\t\t\t\t\t\tTodos nuestros conductores han sido sometidos a un estudio de seguridad y antecedentes para garantizar la seguridad integral de nuestro servicio.\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\n\t\t</div>\n\t</div>\n</div>"

/***/ }),

/***/ 522:
/***/ (function(module, exports) {

module.exports = "<div id=\"container-services\">\n    <div *ngIf=\"viewFromService\" id=\"viewContainerService\" class=\"col-sm-12\">\n        <div class=\"row\">\n            <div class=\"col-sm-4 container-form\">\n                <div class=\"row saldo-disponible\">\n                    <input type=\"text\" class=\"inputTextLabel text-center text-white\" id=\"creditInput\" disabled>\n                </div>\n                <div class=\"row style-label-tab\">\n                    <div (click)=\"$event.preventDefault()\">\n                        <tabset [justified]=\"true\">\n                            <tab heading=\"TRANSPORTE\" [active]=\"tab.active\">\n                                <form #formServiceTransport=\"ngForm\">\n                                    <div class=\"col-sm-12 content-tab-transport style-text-tab\">\n                                        <div *ngIf=\"viewOriginTransport\" id=\"viewOriginTransport\">\n                                            <div class=\"form-group\" style=\"overflow: hidden;\">\n                                                <label for=\"servicio-origen\"\n                                                       class=\"col-xs-12 col-sm-12 row\">Origen</label>\n                                                <input type=\"text\"\n                                                       tabindex=\"1\"\n                                                       class=\"form-control col-sm-12\"\n                                                       placeholder=\"Dirección de origen\"\n                                                       name=\"origin_address\"\n                                                       id=\"autocompleteMapaOrigin\"\n                                                       required\n                                                       [(ngModel)]=\"dataServiceTransport.origin_address\"\n                                                       ngControl=\"origin_address\"\n                                                       autocomplete=\"on\"\n                                                       runat=\"server\"\n                                                       autofocus\n                                                       (focus)=\"detectInputMarker('originTransport')\">\n                                                <input type=\"hidden\" name=\"origin_latitude\"\n                                                       id=\"origin_latitude\"\n                                                       required\n                                                       [(ngModel)]=\"dataServiceTransport.origin_latitude\"\n                                                       ngControl=\"origin_latitude\">\n                                                <input type=\"hidden\" name=\"origin_longitude\"\n                                                       id=\"origin_longitude\"\n                                                       required\n                                                       [(ngModel)]=\"dataServiceTransport.origin_longitude\"\n                                                       ngControl=\"origin_longitude\">\n                                                <div style=\"display: none\" id=\"status1\" class=\"text-danger\">¡Campo\n                                                    requerido!\n                                                </div>\n                                            </div>\n                                            <br>\n                                            <div class=\"form-group\" style=\"overflow: hidden;\">\n                                                <label for=\"servicio-origen\"\n                                                       class=\"col-xs-12 col-sm-12 row\">Destino</label>\n                                                <input type=\"text\"\n                                                       tabindex=\"2\"\n                                                       class=\"form-control col-sm-12\"\n                                                       placeholder=\"Dirección de destino\"\n                                                       name=\"destiny_address\"\n                                                       id=\"autocompleteMapaDestination\"\n                                                       required\n                                                       [(ngModel)]=\"dataServiceTransport.destiny_address\"\n                                                       ngControl=\"destiny_address\"\n                                                       autocomplete=\"on\"\n                                                       runat=\"server\"\n                                                       (focus)=\"detectInputMarker('destinationTransport')\">\n                                                <input type=\"hidden\" name=\"destiny_latitude\"\n                                                       id=\"destiny_latitude\"\n                                                       required\n                                                       [(ngModel)]=\"dataServiceTransport.destiny_latitude\"\n                                                       ngControl=\"destiny_latitude\">\n                                                <input type=\"hidden\" name=\"destiny_longitude\"\n                                                       id=\"destiny_longitude\"\n                                                       required\n                                                       [(ngModel)]=\"dataServiceTransport.destiny_longitude\"\n                                                       ngControl=\"destiny_longitude\">\n                                                <div style=\"display: none\" id=\"status2\" class=\"text-danger\">¡Campo\n                                                    requerido!\n                                                </div>\n                                            </div>\n                                            <br>\n                                            <div class=\"form-group\" id=\"container-img-types-services\"\n                                                 style=\"overflow: hidden;\">\n                                                <label for=\"servicio-propina\" class=\"col-xs-12 col-sm-12 row\">Tipo de\n                                                    vehículo</label>\n                                                <div class=\"col-xs-4 col-sm-4 text-center\">\n                                                    <div id=\"containerVehicleVan\">\n                                                        <figure class=\"\" (click)=\"selectTypeVehicle(0)\">\n                                                            <img src=\"./assets/img/transporteVan.png\" id=\"van\" alt=\"\">\n                                                        </figure>\n                                                        <br>\n                                                        <label id=\"input-radio-van\" (click)=\"selectTypeVehicle(0)\">\n                                                            <input type=\"radio\" tabindex=\"3\" id=\"vehiculoVan\"\n                                                                   name=\"bag_id\" value=\"16\"\n                                                                   [(ngModel)]=\"dataServiceTransport.bag_id\"\n                                                                   ngControl=\"bag_id\"> <span\n                                                                style=\"display: none;\"></span> Van\n                                                        </label>\n                                                    </div>\n                                                </div>\n                                                <div class=\"col-xs-4 col-sm-4 text-center\">\n                                                    <div id=\"containerVehicleWhite\">\n                                                        <figure class=\"\" (click)=\"selectTypeVehicle(1)\">\n                                                            <img src=\"./assets/img/transporteBlanco.png\" id=\"blanco\"\n                                                                 alt=\"\">\n                                                        </figure>\n                                                        <br>\n                                                        <label id=\"input-radio-blanco\" (click)=\"selectTypeVehicle(1)\">\n                                                            <input type=\"radio\" tabindex=\"4\" id=\"vehiculoBlanco\"\n                                                                   name=\"bag_id\" value=\"25\"\n                                                                   [(ngModel)]=\"dataServiceTransport.bag_id\"\n                                                                   ngControl=\"bag_id\"> <span\n                                                                style=\"display: none;\"></span> Blanco\n                                                        </label>\n                                                    </div>\n                                                </div>\n                                                <div class=\" col-xs-4 col-sm-4 text-center\">\n                                                    <div id=\"containerVehicleElectric\">\n                                                        <figure class=\"\" (click)=\"selectTypeVehicle(2)\">\n                                                            <img src=\"./assets/img/transporteElectrico.png\"\n                                                                 id=\"electrico\" alt=\"\">\n                                                        </figure>\n                                                        <br>\n                                                        <label id=\"input-radio-electrico\"\n                                                               (click)=\"selectTypeVehicle(2)\">\n                                                            <input type=\"radio\" tabindex=\"5\" id=\"vehiculoElectrico\"\n                                                                   name=\"bag_id\" value=\"26\"\n                                                                   [(ngModel)]=\"dataServiceTransport.bag_id\"\n                                                                   ngControl=\"bag_id\"> <span\n                                                                style=\"display: none;\"></span> Eléctrico\n                                                        </label>\n                                                    </div>\n                                                </div>\n                                                <div style=\"display: none\" id=\"status3\" class=\"text-danger\">¡Campo\n                                                    requerido!\n                                                </div>\n                                                <input type=\"hidden\" name=\"type_service\"\n                                                       id=\"type_service\"\n                                                       required\n                                                       [(ngModel)]=\"dataServiceTransport.type_service\"\n                                                       ngControl=\"type_service\">\n                                            </div>\n                                            <br>\n                                            <div class=\"form-group\" style=\"overflow: hidden;\">\n                                                <label for=\"propinaTransporte\"\n                                                       class=\"col-xs-12 col-sm-12 row\">Propina</label>\n                                                <input type=\"text\"\n                                                       tabindex=\"6\"\n                                                       class=\"form-control col-sm-12\"\n                                                       placeholder=\"Opcional\"\n                                                       name=\"tip\"\n                                                       id=\"tip\"\n                                                       [(ngModel)]=\"dataServiceTransport.tip\"\n                                                       ngControl=\"tip\">\n                                            </div>\n                                            <div class=\"container-btn-continue text-right\">\n                                                <button type=button style=\"display:none\" id=\"cancelProcessLoading\"\n                                                        (click)=\"cancelProcessLoading()\"></button>\n                                                <button type=\"button\" id=\"btnViewContribution\"\n                                                        class=\"btn btn-lg btn-continue\"\n                                                        (click)=\"onSubmitFormTransport(formServiceTransport.value)\"\n                                                        [disabled]=\"loadingProcess\">\n                                                    <i *ngIf=\"loadingProcess\"\n                                                       class=\"fa fa-spinner fa-pulse fa-1x fa-fw margin-bottom\"></i>\n                                                    <span *ngIf=\"!loadingProcess\">Continuar</span>\n                                                </button>\n                                            </div>\n                                        </div>\n                                    </div>\n                                </form>\n                            </tab>\n                            <tab heading=\"ENVÍOS\" [active]=\"!tab.active\" (select)=\"loadContentTabSend(1)\">\n                                <form #formServiceSend=\"ngForm\">\n                                    <div class=\"col-sm-12 content-tab-transport style-text-tab\">\n                                        <div id=\"viewOriginSend\">\n                                            <div class=\"form-group\" style=\"overflow: hidden;    margin-bottom: 27px;\">\n                                                <label for=\"autocompleteMapaOriginSend\" class=\"col-xs-12 col-sm-12 row\">Origen</label>\n                                                <input type=\"text\"\n                                                       tabindex=\"1\"\n                                                       class=\"form-control col-sm-12\"\n                                                       placeholder=\"Dirección de origen\"\n                                                       name=\"origin_address\"\n                                                       id=\"autocompleteMapaOriginSend\"\n                                                       required\n                                                       [(ngModel)]=\"dataServiceSend.origin_address\"\n                                                       ngControl=\"origin_address\"\n                                                       autocomplete=\"on\"\n                                                       runat=\"server\"\n                                                       autofocus\n                                                       (focus)=\"detectInputMarker('originSend')\">\n                                                <input type=\"hidden\" name=\"origin_latitude\"\n                                                       id=\"origin_latitude\"\n                                                       required\n                                                       [(ngModel)]=\"dataServiceSend.origin_latitude\"\n                                                       ngControl=\"origin_latitude\">\n                                                <input type=\"hidden\" name=\"origin_longitude\"\n                                                       id=\"origin_longitude\"\n                                                       required\n                                                       [(ngModel)]=\"dataServiceSend.origin_longitude\"\n                                                       ngControl=\"origin_longitude\">\n                                            </div>\n                                            <div style=\"display: none\" id=\"status4\" class=\"text-danger\">¡Campo\n                                                requerido!\n                                            </div>\n                                            <br>\n                                            <div class=\"form-group\" style=\"overflow: hidden;    margin-bottom: 27px;\">\n                                                <input type=\"text\"\n                                                       tabindex=\"2\"\n                                                       class=\"form-control col-sm-12\"\n                                                       placeholder=\"Piso / Apartamento / Oficina\"\n                                                       name=\"origin_detail\"\n                                                       id=\"origin_detail\"\n                                                       [(ngModel)]=\"dataServiceSend.origin_detail\"\n                                                       ngControl=\"origin_detail\">\n                                            </div>\n                                            <br>\n                                            <br>\n                                            <div class=\"form-group\" style=\"overflow: hidden;    margin-bottom: 27px;\">\n                                                <label for=\"autocompleteMapaDestinationSend\"\n                                                       class=\"col-xs-12 col-sm-12 row\">Destino</label>\n                                                <input type=\"text\"\n                                                       tabindex=\"3\"\n                                                       class=\"form-control col-sm-12\"\n                                                       placeholder=\"Dirección de destino\"\n                                                       name=\"destiny_address\"\n                                                       id=\"autocompleteMapaDestinationSend\"\n                                                       required\n                                                       [(ngModel)]=\"dataServiceSend.destiny_address\"\n                                                       ngControl=\"destiny_address\"\n                                                       autocomplete=\"on\"\n                                                       runat=\"server\"\n                                                       (focus)=\"detectInputMarker('destinationSend')\">\n                                                <input type=\"hidden\" name=\"destiny_latitude\"\n                                                       id=\"destiny_latitude\"\n                                                       required\n                                                       [(ngModel)]=\"dataServiceSend.destiny_latitude\"\n                                                       ngControl=\"destiny_latitude\">\n                                                <input type=\"hidden\" name=\"destiny_longitude\"\n                                                       id=\"destiny_longitude\"\n                                                       required\n                                                       [(ngModel)]=\"dataServiceSend.destiny_longitude\"\n                                                       ngControl=\"destiny_longitude\">\n                                            </div>\n                                            <div style=\"display: none\" id=\"status5\" class=\"text-danger\">¡Campo\n                                                requerido!\n                                            </div>\n                                            <br>\n                                            <div class=\"form-group\" style=\"overflow: hidden;    margin-bottom: 27px;\">\n                                                <input type=\"text\"\n                                                       tabindex=\"4\"\n                                                       class=\"form-control col-sm-12\"\n                                                       placeholder=\"Piso / Apartamento / Oficina\"\n                                                       name=\"destiny_detail\"\n                                                       id=\"destiny_detail\"\n                                                       [(ngModel)]=\"dataServiceSend.destiny_detail\"\n                                                       ngControl=\"destiny_detail\">\n                                            </div>\n                                            <button type=\"button\" id=\"changeProcessView\" class=\"btn btn-lg btn-continue\"\n                                                    (click)=\"nextProcess('viewFromService4',formServiceSend, 2)\"\n                                                    [disabled]=\"loadingProcess\">Continuar\n                                            </button>\n                                        </div>\n                                        <div id=\"viewDestinationSend\">\n                                            <div class=\"form-group\" style=\"overflow: hidden;    margin-bottom: 27px;\">\n                                                <label for=\"descripcionSend\"\n                                                       class=\"col-xs-12 col-sm-12 row\">Descripción</label>\n                                                <input type=\"text\"\n                                                       tabindex=\"1\"\n                                                       class=\"form-control col-sm-12\"\n                                                       placeholder=\"Breve descripción de tu envío\"\n                                                       name=\"description_text\"\n                                                       id=\"description_text\"\n                                                       [(ngModel)]=\"dataServiceSend.description_text\"\n                                                       ngControl=\"description_text\"\n                                                       (ngModelChange)=\"changeValidateInput('description_text')\">\n                                                <div style=\"display: none\" id=\"status6\" class=\"text-danger\">¡Campo\n                                                    requerido!\n                                                </div>\n                                            </div>\n                                            <br>\n                                            <div class=\"form-group\" style=\"overflow: hidden;    margin-bottom: 27px;\">\n                                                <label for=\"valorDeclaradoSend\" class=\"col-xs-12 col-sm-12 row\">Valor\n                                                    declarado</label>\n                                                <input type=\"text\"\n                                                       tabindex=\"2\"\n                                                       class=\"form-control col-sm-12\"\n                                                       placeholder=\"En pesos colombianos\"\n                                                       name=\"declared_value\"\n                                                       id=\"declared_value\"\n                                                       [(ngModel)]=\"dataServiceSend.declared_value\"\n                                                       ngControl=\"declared_value\"\n                                                       (ngModelChange)=\"changeValidateInput('declared_value')\">\n                                                <div style=\"display: none\" id=\"status7\" class=\"text-danger\">¡Campo\n                                                    requerido!\n                                                </div>\n                                            </div>\n                                            <br>\n                                            <div class=\"form-group\" style=\"overflow: hidden;    margin-bottom: 27px;\">\n                                                <label for=\"destinatarioSend\" class=\"col-xs-12 col-sm-12 row\">Destinatario</label>\n                                                <input type=\"text\"\n                                                       tabindex=\"3\"\n                                                       class=\"form-control col-sm-12\"\n                                                       placeholder=\"Nombre y apellido del destinatario\"\n                                                       name=\"destiny_name\"\n                                                       id=\"destiny_name\"\n                                                       [(ngModel)]=\"dataServiceSend.destiny_name\"\n                                                       ngControl=\"destiny_name\"\n                                                       (ngModelChange)=\"changeValidateInput('destiny_name')\">\n                                                <div style=\"display: none\" id=\"status8\" class=\"text-danger\">¡Campo\n                                                    requerido!\n                                                </div>\n                                            </div>\n                                            <br>\n                                            <div class=\"form-group\" style=\"overflow: hidden;    margin-bottom: 27px;\">\n                                                <label for=\"propinaEnviar\"\n                                                       class=\"col-xs-12 col-sm-12 row\">Propina</label>\n                                                <input type=\"text\"\n                                                       tabindex=\"4\"\n                                                       class=\"form-control col-sm-12\"\n                                                       placeholder=\"Opcional\"\n                                                       name=\"tip\"\n                                                       id=\"tip\"\n                                                       [(ngModel)]=\"dataServiceSend.tip\"\n                                                       ngControl=\"tip\">\n                                            </div>\n                                            <div class=\"container-btn-continue text-right\">\n                                                <button type=button style=\"display:none\" id=\"cancelProcessLoading\"\n                                                        (click)=\"cancelProcessLoading()\"></button>\n                                                <button type=\"button\" class=\"btn btn-lg btn-back\"\n                                                        (click)=\"nextProcess('viewFromService3',formServiceSend, 2)\">\n                                                    Atrás\n                                                </button>\n                                                <button type=\"button\" id=\"btnViewContributionSend\"\n                                                        class=\"btn btn-lg btn-continue\"\n                                                        (click)=\"onSubmitFormSend(formServiceSend.value)\"\n                                                        [disabled]=\"loadingProcess\">\n                                                    <i *ngIf=\"loadingProcess\"\n                                                       class=\"fa fa-spinner fa-pulse fa-1x fa-fw margin-bottom\"></i>\n                                                    <span *ngIf=\"!loadingProcess\">Continuar</span>\n                                                </button>\n                                            </div>\n                                        </div>\n                                    </div>\n                                </form>\n                            </tab>\n                        </tabset>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-sm-8\">\n                <div class=\"row\">\n                    <div id=\"googleMap\"></div>\n                </div>\n            </div>\n        </div>\n    </div>\n    <div *ngIf=\"viewContribution\" id=\"viewContainerPayment\" class=\"container\">\n        <div class=\"container-payment\">\n            <div class=\"row\">\n                <div class=\"col-sm-offset-2 col-sm-4\">\n                    <div class=\"text-center title-main\">\n                        <span for=\"\" class=\"text-1-4em font-gothamRnd-medium\">RESUMEN DE TU SERVICIO</span>\n                    </div>\n                    <div class=\"row\">\n                        <div class=\"container-info\">\n                            <span for=\"\" class=\"col-sm-12 font-gothamRnd-medium text-1-2em\">ORIGEN</span>\n                            <div class=\"col-sm-10 text-1-1em\">\n                                {{origin_address}}\n                                <br>\n                                {{origin_detail}}\n                            </div>\n                            <div class=\"col-sm-2 text-right row\">\n                                <a class=\"font-bold text-orange text-1-2em\" (click)=\"editInfo('origin')\">\n                                    Editar\n                                </a>\n                            </div>\n                        </div>\n                        <div *ngIf=\"containerTypeVehicle\" class=\"container-info\">\n                            <span for=\"\" class=\"col-sm-12 font-gothamRnd-medium text-1-2em\">TIPO DE VEHÍCULO</span>\n                            <div class=\"col-sm-10 text-1-1em\">\n                                {{type_service_string}}\n                            </div>\n                            <div class=\"col-sm-2 text-right row text-1-2em\">\n                                <a class=\"font-bold text-orange\" (click)=\"editInfo('type_vehicle')\">\n                                    Editar\n                                </a>\n                            </div>\n                        </div>\n                        <div class=\"container-info\">\n                            <span for=\"\" class=\"col-sm-12 font-gothamRnd-medium text-1-2em\">DESTINO</span>\n                            <div class=\"col-sm-10 text-1-1em\">\n                                {{destiny_address}}\n                                <br>\n                                {{destiny_detail}}\n                            </div>\n                            <div class=\"col-sm-2 text-right row\">\n                                <a class=\"font-bold text-orange text-1-2em\" (click)=\"editInfo('destination')\">\n                                    Editar\n                                </a>\n                            </div>\n                        </div>\n                        <div *ngIf=\"containerDestinyName\" class=\"container-info\">\n                            <span for=\"\" class=\"col-sm-12 font-gothamRnd-medium text-1-2em\">DESTINATARIO</span>\n                            <div class=\"col-sm-10 text-1-1em\">\n                                {{destiny_name}}\n                            </div>\n                            <div class=\"col-sm-2 text-right row\">\n                                <a class=\"font-bold text-orange text-1-2em\" (click)=\"editInfo('addressee')\">\n                                    Editar\n                                </a>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"col-sm-offset-1 col-sm-4\">\n                    <div class=\"text-center title-main\">\n                        <span for=\"\" class=\"text-1-4em font-gothamRnd-medium\">MEDIO DE PAGO</span>\n                    </div>\n                    <div class=\"row\">\n                        <div class=\"container-info\">\n                            <!--<label for=\"pay-card\" class=\"col-sm-12 row\">&nbsp;</label>-->\n                            <label id=\"pay-card\" (click)=\"selectTypePay('0')\">\n                                <input type=\"radio\" name=\"testRadio\" value=\"3\"> <span></span>Tarjeta de crédito\n                            </label>\n                            <span class=\"pull-right btn-cancel-type-pay\" *ngIf=\"viewFormTarget\"\n                                  (click)=\"selectTypePay('cancel')\">Cancelar</span>\n                            <br>\n                            <div class=\"col-sm-12\" *ngIf=\"viewFormTarget\">\n                                <br>\n                                <div class=\"row\">\n                                    <span>Número de tarjeta</span>\n                                    <input type=\"text\" class=\"form-control col-sm-12 col-xs-12\" [(ngModel)]=\"paymentGateWay.x_card_num\" maxlength=\"16\">\n                                </div>\n                                <br>\n                                <div class=\"row\">\n                                    <div class=\"col-sm-5\">\n                                        <div class=\"row\">\n                                            <span>Fecha de vencimiento</span>\n                                            <br>\n                                            <input type=\"text\" class=\"form-control col-sm-12 col-xs-12\" [(ngModel)]=\"paymentGateWay.x_exp_date\" maxlength=\"5\">\n                                        </div>\n                                    </div>\n                                    <div class=\"col-sm-6 col-sm-offset-1\">\n                                        <div class=\"row\">\n                                            <span>Código de seguridad</span>\n                                            <br>\n                                            <input type=\"text\" class=\"form-control col-sm-12 col-xs-12\" [(ngModel)]=\"paymentGateWay.x_card_code\" maxlength=\"5\">\n                                        </div>\n                                    </div>\n                                </div>\n                                <br>\n                                <div class=\"row\">\n                                    <div class=\"col-sm-12 center\">\n                                        <div class=\"row\">\n                                            <p>\n                                                <b>{{messagePaymentGateWayTokenize}}</b>\n                                            </p>\n                                            <!--Guardar tarjeta de crédito para próximos servicios-->\n                                            <!--<div class=\"checkbox pull-right\" style=\"margin-top: 0px;\">-->\n                                                <!--<label>-->\n                                                    <!--&nbsp;&nbsp;<input type=\"checkbox\">-->\n                                                <!--</label>-->\n                                            <!--</div>-->\n                                            <button type=\"button\" class=\"btn btn-lg btn-orange\" (click)=\"paymentGateWayTokenize()\"\n                                                    [disabled]=\"loadingProcessPaymentGateWayPost\">\n                                                <i *ngIf=\"loadingProcessPaymentGateWayPost\" class=\"fa fa-spinner fa-pulse fa-1x fa-fw margin-bottom\"></i>\n                                                <span *ngIf=\"!loadingProcessPaymentGateWayPost\">Agregar</span>\n                                            </button>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                        <div *ngIf=\"!viewFormTarget\" class=\"container-info\">\n                            <label id=\"pay-cash\" (click)=\"selectTypePay('1')\">\n                                <input type=\"radio\" name=\"testRadio\" value=\"2\"> <span></span>Pago en efectivo\n                            </label>\n                        </div>\n                        <div *ngIf=\"!viewFormTarget\" class=\"container-info\">\n                            <div id=\"viewPayCredit\">\n                                <label id=\"pay-corporate\" (click)=\"selectTypePay('2')\">\n                                    <input type=\"radio\" name=\"testRadio\" value=\"3\"> <span></span>Crédito corporativo\n                                </label>\n                            </div>\n                        </div>\n                        <div class=\"container-info text-center\" id=\"viewOpenModalBond\">\n                            <a class=\"text-orange text-1-1em\" (click)=\"showChildModal()\">Ingresar código de bono</a>\n                        </div>\n                        <div class=\"container-info text-center\" id=\"viewCodeSuccess\" style=\"display: none;\">\n                            <span class=\"font-bold text-1-2em\">Tu código {{code_confirm}} <br> ha sido registrado exitosamente <br> por un valor de\n                                <span class=\"text-orange\" id=\"code_value\"> </span>\n                            </span>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"row container-values\">\n                <div class=\"col-xs-12 col-sm-12\">\n                    <div class=\"row\">\n                        <div class=\"col-sm-6 text-right\">\n                            <span class=\"font-gothamRnd-medium text-1-2em\">VALOR APROXIMADO</span>\n                        </div>\n                        <div class=\"col-sm-6 text-left\">\n                            <i id=\"spinnerIconReal\" class=\"fa fa-spinner fa-pulse fa-1x fa-fw margin-bottom\"></i>\n                            <span id=\"price_service_real\" class=\"font-bold\">\n\t\t\t\t\t\t\t</span>\n                        </div>\n                    </div>\n                    <div id=\"containerBond\" class=\"row\" style=\"display:none;\">\n                        <div class=\"col-sm-6 text-right\">\n                            <span class=\"font-gothamRnd-medium text-1-2em\">DESCUENTO BONO</span>\n                        </div>\n                        <div class=\"col-sm-6\">\n\t\t\t\t\t\t\t<span id=\"bond_service\" class=\"font-bold\">\n\t\t\t\t\t\t\t</span>\n                        </div>\n                    </div>\n                    <div class=\"row margin-t-15\">\n                        <div class=\"col-sm-6 text-right text-orange text-1-3em\">\n                            <label>TOTAL APROXIMADO</label>\n                        </div>\n                        <div class=\"col-sm-6\">\n                            <i id=\"spinnerIcon\" class=\"fa fa-spinner fa-pulse fa-2x fa-fw margin-bottom\"></i>\n                            <span id=\"price_service\" class=\"text-orange text-1-3em font-bold\">\n\t\t\t\t\t\t\t</span>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"row\">\n                <div class=\"col-sm-12 text-center\">\n                    <div style=\"margin-bottom: 50px;\">\n                        <div class=\"text-danger text-center font-bold\" id=\"status9\" style=\"display: none !important;\">\n                            ¡Seleccione un tipo de pago!\n                        </div>\n                    </div>\n                    <div style=\"margin-bottom: 50px;\">\n                        <div class=\"text-center font-bold\">\n                            {{messagePaymentGateWayPost}}\n                        </div>\n                    </div>\n                    <div class=\"text-center\" id=\"responsePayService\" style=\"font-weight:bold;\">\n\n                    </div>\n                    <button type=button style=\"display:none\" id=\"cancelProcessLoading\"\n                            (click)=\"cancelProcessLoading()\"></button>\n                    <button type=\"button\" id=\"btnPay\" class=\"btn btn-lg btn-orange\" (click)=\"payService()\"\n                            [disabled]=\"loadingProcess\">\n                        <i *ngIf=\"loadingProcess\" class=\"fa fa-spinner fa-pulse fa-1x fa-fw margin-bottom\"></i>\n                        <span *ngIf=\"!loadingProcess\">Realiza tu pago</span>\n                    </button>\n                    <button type=\"button\" id=\"nextProcessViewSuccess\" style=\"display:none\"\n                            (click)=\"nextProcessViewSuccess()\" class=\"btn\"></button>\n                </div>\n            </div>\n        </div>\n    </div>\n    <div *ngIf=\"viewSuccess\" id=\"viewContainerSuccessPay\">\n        <div class=\"container text-right\" style=\"padding-top: 68px;\">\n            <div class=\"text-4em font-bold\">\n                ¡TU SOLICITUD <br>\n                HA SIDO REALIZADA!\n            </div>\n            <div class=\"text-3-5em padding-top-28 font-gothamRnd-book\">\n                Su Beep va en camino\n            </div>\n            <div class=\"text-2em font-gothamRnd-book\">\n                Le notificaremos cuando su conductor <br> haya llegado a su ubicación\n            </div>\n            <button type=\"button\" class=\"btn\" style=\"display:none;\" id=\"btnPushWeb\" (click)=\"pushWeb()\">2</button>\n            <button type=\"button\" class=\"btn\" style=\"display:none;\" id=\"btnViewConfirmService\"\n                    (click)=\"btnViewConfirmService()\">ok\n            </button>\n            <div class=\"text-2em padding-top-50\">\n                <div class=\"\">\n                    <button type=\"button\" (click)=\"cancelService()\" class=\"btn btn-cancel\">\n                        <span class=\"\">Cancelar servicio</span>\n                    </button>\n                </div>\n            </div>\n        </div>\n    </div>\n    <div *ngIf=\"viewDataConfirm\" id=\"viewDataConfirm\" class=\"col-sm-12 row\">\n        <div class=\"col-sm-4 container-form\">\n            <label for=\"servicio-origen\" class=\"col-xs-12 col-sm-12 text-orange text-1-25em text-center margin-borders\">¡TU\n                SOLICITUD HA SIDO REALIZADA!</label>\n            <div class=\"col-xs-12 col-sm-12\">\n                <figure class=\"margin-borders text-center\">\n                    <img src={{picture_employee}} id=\"img-driver\" alt=\"...\">\n                </figure>\n                <figure *ngIf=\"rating_employee==1\" id=\"containerRating\" class=\"text-center\">\n                    <img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n                    <img src=\"./assets/img/iconStar.png\" alt=\"\">\n                    <img src=\"./assets/img/iconStar.png\" alt=\"\">\n                    <img src=\"./assets/img/iconStar.png\" alt=\"\">\n                    <img src=\"./assets/img/iconStar.png\" alt=\"\">\n                </figure>\n                <figure *ngIf=\"rating_employee==2\" id=\"containerRating\" class=\"text-center\">\n                    <img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n                    <img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n                    <img src=\"./assets/img/iconStar.png\" alt=\"\">\n                    <img src=\"./assets/img/iconStar.png\" alt=\"\">\n                    <img src=\"./assets/img/iconStar.png\" alt=\"\">\n                </figure>\n                <figure *ngIf=\"rating_employee==3\" id=\"containerRating\" class=\"text-center\">\n                    <img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n                    <img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n                    <img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n                    <img src=\"./assets/img/iconStar.png\" alt=\"\">\n                    <img src=\"./assets/img/iconStar.png\" alt=\"\">\n                </figure>\n                <figure *ngIf=\"rating_employee==4\" id=\"containerRating\" class=\"text-center\">\n                    <img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n                    <img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n                    <img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n                    <img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n                    <img src=\"./assets/img/iconStar.png\" alt=\"\">\n                </figure>\n                <figure *ngIf=\"rating_employee==5\" id=\"containerRating\" class=\"text-center\">\n                    <img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n                    <img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n                    <img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n                    <img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n                    <img src=\"./assets/img/iconStarSelected.png\" alt=\"\">\n                </figure>\n                <figure *ngIf=\"!rating_employee==1 && !rating_employee==2 && !rating_employee==3 && !rating_employee==4 && !rating_employee==5\"\n                        id=\"containerRating\" class=\"text-center\">\n                    <img src=\"./assets/img/iconStar.png\" alt=\"\">\n                    <img src=\"./assets/img/iconStar.png\" alt=\"\">\n                    <img src=\"./assets/img/iconStar.png\" alt=\"\">\n                    <img src=\"./assets/img/iconStar.png\" alt=\"\">\n                    <img src=\"./assets/img/iconStar.png\" alt=\"\">\n                </figure>\n                <div class=\"margin-bottom\">\n\n                </div>\n                <div class=\"margin-bottom\">\n                    <div class=\"text-1-25em text-center\">\n                        {{name_employee}} {{lastName_employee}}\n                    </div>\n                </div>\n                <div class=\"margin-bottom\">\n                    <div class=\"text-1-25em text-center font-bold\">\n                        {{vehicle_brand}} {{vehicle_model}}\n                    </div>\n                    <div>\n                        {{license_plate}}\n                    </div>\n                </div>\n                <div class=\"margin-borders row\">\n                    <div class=\"col-sm-6 row\">\n                        <div class=\"text-1-25em text-center font-bold\">\n                            Tiempo de llegada:\n                        </div>\n                        <div class=\"text-center\">\n                            {{arrival_time}}\n                        </div>\n                    </div>\n                    <div class=\"col-sm-6 row\">\n                        <div class=\"text-1-25em text-center font-bold\">\n                            Código de seguridad:\n                        </div>\n                        <div class=\"text-center\">\n                            {{security_code}}\n                        </div>\n                    </div>\n                </div>\n                <div class=\"margin-borders text-center\">\n                    <button type=\"button\" class=\"btn\" style=\"display: none;\" id=\"btnShowModalConfirmBeep\"\n                            (click)=\"showModalConfirmBeep()\">modal\n                    </button>\n                    <button type=\"button\" id=\"btnCancelService\" class=\"btn\" (click)=\"cancelService()\">Cancelar</button>\n                </div>\n            </div>\n        </div>\n        <div class=\"col-sm-8\">\n            <div class=\"row\">\n                <div id=\"googleMap\"></div>\n            </div>\n        </div>\n    </div>\n</div>\n\n<div class=\"modal fade\" bsModal #modalBono=\"bs-modal\" [config]=\"{backdrop: 'static'}\" tabindex=\"-1\" role=\"dialog\"\n     aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\" id=\"modal-login\">\n    <div class=\"modal-dialog modal-md\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <a class=\"close\" (click)=\"modalBono.hide()\" aria-label=\"Close\">\n                    <img src=\"./assets/img/Close.png\" alt=\"Cerrar\">\n                </a>\n                <span>Bono</span>\n            </div>\n            <div class=\"modal-body\">\n                <form #formCode=\"ngForm\">\n                    <div class=\"form-group\">\n                        <input type=\"text\" class=\"form-control col-sm-12 input-lg\" id=\"inputBono\" name=\"inputBono\"\n                               ngModel required>\n                    </div>\n                    <div class=\"form-group text-center text-information\">\n                        <p class=\"inputTextLabel text-center\">\n                            <span class=\"text-1-5em\">{{message_bond}}</span>\n                        </p>\n                    </div>\n                    <div class=\"form-group text-center\">\n                        <button style=\"display:none\" type=\"button\" id=\"btnConfirmarBonod\" class=\"btn btn-lg col-sm-12\">\n\n                        </button>\n                        <button type=\"button\" id=\"btnConfirmarBono\" class=\"btn btn-lg col-sm-12\"\n                                (click)=\"confirmCode(formCode)\" [disabled]=\"loadingBoundProcess\">\n                            <i *ngIf=\"loadingBoundProcess\" class=\"fa fa-spinner fa-pulse fa-1x fa-fw margin-bottom\"></i>\n                            <span *ngIf=\"!loadingBoundProcess\">Confirmar</span>\n                        </button>\n                    </div>\n                </form>\n            </div>\n        </div>\n    </div>\n</div>\n<div class=\"modal fade\" bsModal #modalLoading=\"bs-modal\" [config]=\"{backdrop: 'static'}\" tabindex=\"-1\" role=\"dialog\"\n     aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\" id=\"modal-login\">\n    <div class=\"modal-dialog modal-md\">\n        <!--<img src=\"./assets/img/\" alt=\"\">-->\n    </div>\n</div>\n<div class=\"modal fade\" bsModal #modalConfirmBeep=\"bs-modal\" [config]=\"{backdrop: 'static'}\" tabindex=\"-1\" role=\"dialog\"\n     aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\" id=\"modal-login\">\n    <div class=\"modal-dialog modal-md\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header header-transparent\">\n                <a class=\"close\" (click)=\"modalConfirmBeep.hide()\" aria-label=\"Close\">\n                    <img src=\"./assets/img/Close.png\" alt=\"Cerrar\">\n                </a>\n            </div>\n            <div class=\"modal-body\">\n\n                <div class=\"form-group text-center text-information\">\n                    <p class=\"inputTextLabel text-center\">\n                        <span class=\"text-1-5em font-bold\">¡TU BEEP HA LLEGADO!</span>\n                    </p>\n                </div>\n                <div class=\"form-group text-center\">\n                    <button type=\"button\" id=\"btnConfirmarBono\" class=\"btn btn-lg col-sm-12 btnOrange\"\n                            (click)=\"hideModalConfirmBeep()\">\n                        <span class=\"\">Aceptar</span>\n                    </button>\n                </div>\n\n            </div>\n        </div>\n    </div>\n</div>\n<div class=\"modal fade\" bsModal #modalCancelBeep=\"bs-modal\" [config]=\"{backdrop: 'static'}\" tabindex=\"-1\" role=\"dialog\"\n     aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\" id=\"modal-login\">\n    <div class=\"modal-dialog modal-md\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header header-transparent\">\n                <a class=\"close\" (click)=\"hideModalCancelBeep()\" aria-label=\"Close\">\n                    <img src=\"./assets/img/Close.png\" alt=\"Cerrar\">\n                </a>\n            </div>\n            <div class=\"modal-body\">\n\n                <div class=\"form-group text-center text-information\">\n                    <p class=\"inputTextLabel text-center\">\n                        <span class=\"text-1-5em font-bold\">¡TU SOLICITUD SE HA CANCELADO!</span>\n                    </p>\n                </div>\n                <div class=\"form-group text-center\">\n                    <button type=\"button\" id=\"btnHideModalCancelBeep\" class=\"btn btn-lg col-sm-12 btnOrange\"\n                            (click)=\"hideModalCancelBeep()\">\n                        <span class=\"\">Aceptar</span>\n                    </button>\n                </div>\n\n            </div>\n        </div>\n    </div>\n</div>\n<div class=\"modal fade\" bsModal #modalCancelDriveBeep=\"bs-modal\" [config]=\"{backdrop: 'static'}\" tabindex=\"-1\"\n     role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\" id=\"modal-login\">\n    <div class=\"modal-dialog modal-md\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header header-transparent\">\n                <a class=\"close\" (click)=\"modalCancelDriveBeep.hide()\" aria-label=\"Close\">\n                    <img src=\"./assets/img/Close.png\" alt=\"Cerrar\">\n                </a>\n            </div>\n            <div class=\"modal-body\">\n\n                <div class=\"form-group text-center text-information\">\n                    <p class=\"inputTextLabel text-center\">\n                        <span class=\"text-1-5em font-bold\">¡TU SOLICITUD SE HA CANCELADO!</span>\n                        <br><br>\n                        <span class=\"text-1-5em font-bold\">Estamos buscando un nuevo conductor</span>\n                    </p>\n                </div>\n                <div class=\"form-group text-center\">\n                    <button type=\"button\" id=\"btnHideModalCancelBeep\" class=\"btn btn-lg col-sm-12 btnOrange\"\n                            (click)=\"modalCancelDriveBeep.hide()\">\n                        <span class=\"\">Aceptar</span>\n                    </button>\n                </div>\n\n            </div>\n        </div>\n    </div>\n</div>\n<div class=\"modal fade\" bsModal #modalConfirmCancelBeep=\"bs-modal\" [config]=\"{backdrop: 'static', keyboard: false}\"\n     tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\" id=\"modal-login\">\n    <div class=\"modal-dialog modal-md\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <a class=\"close\" (click)=\"modalConfirmCancelBeep.hide()\" aria-label=\"Close\">\n                    <img src=\"./assets/img/Close.png\" alt=\"Cerrar\">\n                </a>\n                <span>¿Está seguro que desea cancelar el servicio?</span>\n            </div>\n            <div class=\"modal-body\">\n                <div class=\"form-group\">\n                    <button type=\"button\" class=\"btn btn-lg btn-orange\" style=\"background-color: #e04b31;color:#FFFFFF;\"\n                            (click)=\"confirmCancelBeep('si')\">Si\n                    </button>\n                    <button type=\"button\" class=\"btn btn-lg\" (click)=\"confirmCancelBeep('no') && hideChildModal()\">No\n                    </button>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n"

/***/ }),

/***/ 523:
/***/ (function(module, exports) {

module.exports = "<div id=\"container-vehicles\" class=\"container\">\n\t<div class=\"col-sm-12\">\n\t\t<div class=\"container-info row\">\n\t\t\t<div class=\"container-info-vehicles\">\n\t\t\t\t<p class=\"text-1-5em text-orange\">Station Wagon</p>\n\t\t\t\t<p>\n\t\t\t\t\t<span>4x4</span>\n\t\t\t\t\t<br>\n\t\t\t\t\t<span>4x2</span>\n\t\t\t\t</p>\n\t\t\t</div>\n\t\t\t<div class=\"container-info-vehicles\">\n\t\t\t\t<p class=\"text-1-5em text-orange\">Pickups</p>\n\t\t\t\t<p>\n\t\t\t\t\t<span>4x4</span>\n\t\t\t\t\t<br>\n\t\t\t\t\t<span>4x2</span>\n\t\t\t\t</p>\n\t\t\t</div>\n\t\t\t<div class=\"container-info-vehicles\">\n\t\t\t\t<p class=\"text-1-5em text-orange\">Vans y Buses</p>\n\t\t\t\t<p>\n\t\t\t\t\t<span>11 pasajeros</span>\n\t\t\t\t\t<br>\n\t\t\t\t\t<span>18 pasajeros</span>\n\t\t\t\t\t<br>\n\t\t\t\t\t<span>22 pasajeros</span>\n\t\t\t\t</p>\n\t\t\t</div>\n\t\t\t<div class=\"container-info-vehicles\">\n\t\t\t\t<p class=\"text-1-5em text-orange\">Carga</p>\n\t\t\t\t<p>\n\t\t\t\t\t<span>Pequeña</span>\n\t\t\t\t\t<br>\n\t\t\t\t\t<span>Mediana</span>\n\t\t\t\t\t<br>\n\t\t\t\t\t<span>Grande</span>\n\t\t\t\t</p>\n\t\t\t</div>\n\n\t\t</div>\n\t</div>\n</div>"

/***/ }),

/***/ 55:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angular2_cookie_services_cookies_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angular2_cookie_services_cookies_service___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_angular2_cookie_services_cookies_service__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Rx__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__assets_services_auth_forgot_forgot_component__ = __webpack_require__(400);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__assets_services_auth_register_register_component__ = __webpack_require__(402);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__assets_services_auth_login_login_component__ = __webpack_require__(401);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_services_component__ = __webpack_require__(129);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__services_http_service__ = __webpack_require__(393);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__profile_profile_service__ = __webpack_require__(128);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__login__ = __webpack_require__(380);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__register__ = __webpack_require__(390);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__forgot__ = __webpack_require__(369);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__data_user__ = __webpack_require__(367);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__constantsUbeep__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__socialRegister__ = __webpack_require__(397);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__userRegister__ = __webpack_require__(398);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


















var AppComponent = (function () {
    function AppComponent(_viewContainerRef, _cookieService, _routerModule, _compiler, _httpForgotService, _httpRegisterService, _LoginComponent, _HttpService, activatedRoute, _ConstantUbeep, _HttpProfile) {
        this._viewContainerRef = _viewContainerRef;
        this._cookieService = _cookieService;
        this._routerModule = _routerModule;
        this._compiler = _compiler;
        this._httpForgotService = _httpForgotService;
        this._httpRegisterService = _httpRegisterService;
        this._LoginComponent = _LoginComponent;
        this._HttpService = _HttpService;
        this.activatedRoute = activatedRoute;
        this._ConstantUbeep = _ConstantUbeep;
        this._HttpProfile = _HttpProfile;
        this.stringNavLogin = 'Ingresar';
        this.statusLogin = false;
        this.statusInputUserNameRegister = true;
        this.statusInputPasswordRegister = true;
        this.statusInputEmailRegister = true;
        this.statusInputCellPhoneRegister = true;
        this.disabled = false;
        this.status = { isopen: false };
        this.items = ['The first choice!', 'And another choice for you.', 'but wait! A third!'];
        this.isCollapsed = true;
        this.titleModal = 'Iniciar Sesión';
        this.login = true;
        this.register = false;
        this.forgotpassword = false;
        this.messageRegister = '';
        this.messageLogin = '';
        this.messageForgot = 'Escribe tu dirección de correo electrónico para reestablecer tu contraseña';
        this.loadingProcess = false;
        this.loadingFacebookProcess = false;
        this.loadingFacebookGoogle = false;
        this.dataLogin = new __WEBPACK_IMPORTED_MODULE_11__login__["a" /* Login */]('', '');
        this.dataRegister = new __WEBPACK_IMPORTED_MODULE_12__register__["a" /* Register */]('', '', '', '', '');
        this.dataForgot = new __WEBPACK_IMPORTED_MODULE_13__forgot__["a" /* Forgot */]('');
        this.dataUser = new __WEBPACK_IMPORTED_MODULE_14__data_user__["a" /* DataUser */]('', '');
        this.socialRegister = new __WEBPACK_IMPORTED_MODULE_16__socialRegister__["a" /* SocialRegister */]('', '', '', '', '');
        this.objInitialSetup = [];
        this.select_id_agreement = 0;
        this.completeDataUser = false;
        this.accessToken = null;
        this.viewEmail = false;
        this.viewHistory = false;
        this.typeRegisterSocial = '';
        this.registerUserData = new __WEBPACK_IMPORTED_MODULE_17__userRegister__["a" /* RegisterUser */]();
        this.dataUserLogin = { token: null, email: null };
        this.messageCompleteData = "";
        //router
        this.routeChange = null;
        this._viewContainerRef = _viewContainerRef;
        this._compiler.clearCache();
        var thisClass = this;
        setTimeout(function () {
            //---Carga Facebook
            FB.init({
                appId: '555091548022084',
                cookie: false,
                xfbml: true,
                version: 'v2.8'
            });
            //---Carga Google
            gapi.load('client:auth2', thisClass.initClient());
            if (thisClass._cookieService.get('name') != undefined && thisClass._cookieService.get('id') != undefined) {
                thisClass.statusLogin = true;
                var nameUser = (thisClass._cookieService.get('name')).split(" ");
                thisClass.stringNavLogin = nameUser[0];
            }
        }, 600);
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this._cookieService.get('credit') !== undefined) {
            this.viewHistory = true;
        }
        else {
            this.viewHistory = false;
        }
        this._cookieService.put('validRequiestService', 'true');
        var thisClass = this;
        this.subscription = this.activatedRoute.queryParams.subscribe(function (param) {
            // console.log(param);
            if (param['r'] === '1' || param['r'] === 1) {
                setTimeout(function () {
                    thisClass.showChildModalRegisterInvite();
                }, 10);
            }
        });
        window.addEventListener('scroll', function (event) {
            var sizePageY = window.scrollY || document.body.scrollTop;
            if (sizePageY > 49) {
                document.getElementById('header').style.minHeight = '10px';
                document.getElementById('header').style.height = '92px';
                document.getElementById('header').style.paddingTop = '13px';
                document.getElementById('logoUbeep').style.width = '208px';
            }
            else {
                document.getElementById('header').style.minHeight = '133px';
                document.getElementById('header').style.height = '133px';
                document.getElementById('header').style.paddingTop = '30px';
                document.getElementById('logoUbeep').style.width = '240px';
            }
        });
        document.getElementById('bodyUbeep').onclick = function () {
            if (document.getElementById('dropdown-beep') != null) {
                document.getElementById('dropdown-beep').style.display = 'none';
            }
            if (document.getElementById('dropdown-user-login') != null) {
                document.getElementById('dropdown-user-login').style.display = 'none';
            }
        };
        this._httpRegisterService.initialSetup(this._ConstantUbeep.urlServer).subscribe(function (data) { return _this.postInitialSetup = JSON.stringify(data); }, function (error) { return console.log(''); }, function () { return _this.initialSetup(_this.postInitialSetup); });
    };
    AppComponent.prototype.initialSetup = function (data) {
        var dataResponse = JSON.parse(data);
        // console.log(dataResponse);
        if (dataResponse.return) {
            this.objInitialSetup = dataResponse.data;
            this.objInitialSetup.push({ 'name': 'Seleccione un convenio', 'id': 0 });
        }
        else {
            // console.log(dataResponse.message);
            // alert("no entra!");
        }
    };
    AppComponent.prototype.showDropdownBeep = function () {
        document.getElementById('dropdown-beep').style.display = 'block';
        if (document.getElementById('dropdown-user-login') != null) {
            document.getElementById('dropdown-user-login').style.display = 'none';
        }
    };
    AppComponent.prototype.showDropdownUser = function () {
        document.getElementById('dropdown-user-login').style.display = 'block';
        if (document.getElementById('dropdown-beep') != null) {
            document.getElementById('dropdown-beep').style.display = 'none';
        }
    };
    // Registro de usuario
    AppComponent.prototype.registerUser = function (f) {
        var _this = this;
        this.loadingProcess = true;
        console.log(f.value, this.registerUserData);
        this._httpRegisterService.registerAcount(this.registerUserData.name, SHA256(this.registerUserData.pass), this.registerUserData.email, this.registerUserData.cellphone, this.registerUserData.identify, this._ConstantUbeep.urlServer, this.registerUserData.id_agreement, this.registerUserData.code_agreement).subscribe(function (data) { return _this.postMyCarToServer = JSON.stringify(data); }, function (error) { return console.log(''); }, function () { return _this.registerSuccess(_this.postMyCarToServer, f); });
    };
    AppComponent.prototype.registerSuccess = function (data, f) {
        this.loadingProcess = false;
        var dataResponse = JSON.parse(data);
        // console.log(dataResponse);
        if (dataResponse.return && dataResponse.message === 'Usuario logueado exitosamente') {
            // alert("logeado");
            this._cookieService.put('statusServices', 'false');
            this._cookieService.put('id', dataResponse.data.user.id);
            this._cookieService.put('name', dataResponse.data.user.name);
            this._cookieService.put('movil_phone', dataResponse.data.user.phone);
            this._cookieService.put('social_id', dataResponse.data.user.social_id);
            this._cookieService.put('identify', dataResponse.data.user.identify);
            this._cookieService.put('email', dataResponse.data.user.email);
            // console.log(dataResponse.data.user.socket);
            var sockets = (dataResponse.data.user.socket).split(',');
            // console.log(sockets);
            this._cookieService.put('socket1', sockets[0]);
            this._cookieService.put('socket2', sockets[1]);
            this._cookieService.put('socket3', sockets[2]);
            this._cookieService.put('service_provider_id_1', dataResponse.data.menu[0].service_provider_id);
            this._cookieService.put('type_service_1', dataResponse.data.menu[0].type_service);
            this._cookieService.put('title_1', dataResponse.data.menu[0].title);
            this._cookieService.put('text_description_1', dataResponse.data.menu[0].text_description);
            this._cookieService.put('bag_services_1', dataResponse.data.menu[0].bag_services.length);
            this._cookieService.put('shipping_bag_id_1', dataResponse.data.menu[0].bag_services[0].shipping_bag_id);
            this._cookieService.put('title_shipping_bag_id_1', dataResponse.data.menu[0].bag_services[0].title);
            this._cookieService.put('subtitle_shipping_bag_id_1', dataResponse.data.menu[0].bag_services[0].subtitle);
            this._cookieService.put('shipping_bag_id_2', dataResponse.data.menu[0].bag_services[1].shipping_bag_id);
            this._cookieService.put('title_shipping_bag_id_2', dataResponse.data.menu[0].bag_services[1].title);
            this._cookieService.put('subtitle_shipping_bag_id_2', dataResponse.data.menu[0].bag_services[1].subtitle);
            this._cookieService.put('shipping_bag_id_3', dataResponse.data.menu[0].bag_services[2].shipping_bag_id);
            this._cookieService.put('title_shipping_bag_id_3', dataResponse.data.menu[0].bag_services[2].title);
            this._cookieService.put('subtitle_shipping_bag_id_3', dataResponse.data.menu[0].bag_services[2].subtitle);
            this._cookieService.put('service_provider_id_2', dataResponse.data.menu[1].service_provider_id);
            this._cookieService.put('type_service_2', dataResponse.data.menu[1].type_service);
            this._cookieService.put('title_2', dataResponse.data.menu[1].title);
            this._cookieService.put('text_description_2', dataResponse.data.menu[1].text_description);
            this._cookieService.put('bag_services_2', dataResponse.data.menu[1].bag_services.length);
            this._cookieService.put('shipping_bag_id_4', dataResponse.data.menu[1].bag_services[0].shipping_bag_id);
            this._cookieService.put('title_shipping_bag_id_4', dataResponse.data.menu[1].bag_services[0].title);
            this._cookieService.put('subtitle_shipping_bag_id_4', dataResponse.data.menu[1].bag_services[0].subtitle);
            if (dataResponse.data.user.credit_card[0]) {
                this._cookieService.put('credit_default', dataResponse.data.user.credit_card[0].default);
                this._cookieService.put('credit_franchise', dataResponse.data.user.credit_card[0].franchise);
                this._cookieService.put('credit_last_digits', dataResponse.data.user.credit_card[0].last_digits);
            }
            this._cookieService.put('pay_type', dataResponse.data.user.pay);
            if (dataResponse.data.user.credit !== undefined) {
                this._cookieService.put('credit_default', dataResponse.data.user.credit);
            }
            if (this._cookieService.get('name') !== undefined && this._cookieService.get('id') !== undefined) {
                var nameUser = (this._cookieService.get('name')).split(' ');
                this.statusLogin = true;
                this.stringNavLogin = nameUser[0];
            }
            this.dataLogin = new __WEBPACK_IMPORTED_MODULE_11__login__["a" /* Login */]('', '');
            this.dataRegister = new __WEBPACK_IMPORTED_MODULE_12__register__["a" /* Register */]('', '', '', '', '');
            this.dataForgot = new __WEBPACK_IMPORTED_MODULE_13__forgot__["a" /* Forgot */]('');
            this.socialRegister = new __WEBPACK_IMPORTED_MODULE_16__socialRegister__["a" /* SocialRegister */]('', '', '', '', '');
            this.registerUserData = new __WEBPACK_IMPORTED_MODULE_17__userRegister__["a" /* RegisterUser */]();
            this.staticModal.hide();
            if (dataResponse.data.user.phone.trim().length < 1 || dataResponse.data.user.identify.trim().length < 1) {
                this.completeDataUser = true;
                this.showChildModalCompleteData();
            }
            // console.log("hola yo soy el 2");
            // location.reload();
            // f.reset();
            // Redirecciona al home de usuario
            // this._routerModule.navigateByUrl('solicitar');
        }
        else if ((dataResponse.return === false && dataResponse.message === 'El correo ya se encuentra registrado , por favor vaya a la sección de logueo')) {
            alert("El correo ya se encuentra registrado , por favor vaya a la sección de logueo");
        }
        else if ((dataResponse.return === false && dataResponse.message === 'El correo ya se encuentra registrado con la red social Facebook, por favor vaya a la sección de logueo')) {
            this.messageRegister = dataResponse.message;
        }
        else {
            this.messageRegister = dataResponse.message;
            // alert("no entra!");
        }
    };
    // Login de usuario normal
    AppComponent.prototype.loginUser = function (f, type, typeSocial) {
        var _this = this;
        // console.log(this);
        // console.log("hara loginnnn", f, type);
        if (type) {
            this.loadingProcess = true;
            // Normal
            this._LoginComponent.loginUser(f.value.emailLogin, SHA256(f.value.passwordLogin), this._ConstantUbeep.urlServer).subscribe(function (data) { return _this.postMyCarToServer = data; }, function (error) { return _this.loginError(_this.postMyCarToServer); }, function () { return _this.loginSuccess(_this.postMyCarToServer, f); });
        }
        else {
            // Facebook y Google
            this.typeRegisterSocial = typeSocial;
            this.accessToken = f;
            // this.showChildModalCompleteDataSocial();
            this._LoginComponent.loginCheckUserSocial(f, this._ConstantUbeep.urlServer).subscribe(function (data) { return _this.postMyCarToServer = data; }, function (error) { return _this.loginError(_this.postMyCarToServer); }, function () { return _this.loginSuccess(_this.postMyCarToServer, f); });
        }
    };
    AppComponent.prototype.loginSuccess = function (data, f) {
        var dataResponse = data;
        // let dataResponse = JSON.parse(data);
        // console.log("LOGIN SUCCESS", data, f);
        // console.log(dataResponse);
        this.loadingProcess = false;
        this.loadingFacebookProcess = false;
        this.loadingFacebookGoogle = false;
        if (dataResponse.return && dataResponse.message === 'Usuario logueado exitosamente') {
            this.hideChildModalCompleteDataSocial();
            if (this._cookieService.get('name') !== undefined && this._cookieService.get('id') !== undefined) {
                var nameUser = (this._cookieService.get('name')).split(" ");
                this.statusLogin = true;
                this.stringNavLogin = nameUser[0];
            }
            if (this._cookieService.get('credit') !== undefined) {
                this.viewHistory = true;
            }
            else {
                this.viewHistory = false;
            }
            // f.reset();
            this.titleModal = 'Iniciar Sesión';
            this.messageForgot = 'Escribe tu dirección de correo electrónico para reestablecer tu contraseña';
            this.messageRegister = '';
            this.login = true;
            this.register = false;
            this.forgotpassword = false;
            this.staticModal.hide();
            this.loadingFacebookProcess = false;
            this.loadingFacebookGoogle = false;
            this.loadingProcess = false;
            this.viewEmail = false;
            this.staticModal.hide();
            this.registerUserData = new __WEBPACK_IMPORTED_MODULE_17__userRegister__["a" /* RegisterUser */]();
            // console.log(window.location.hash);
            this.dataLogin = new __WEBPACK_IMPORTED_MODULE_11__login__["a" /* Login */]('', '');
            this.dataRegister = new __WEBPACK_IMPORTED_MODULE_12__register__["a" /* Register */]('', '', '', '', '');
            this.dataForgot = new __WEBPACK_IMPORTED_MODULE_13__forgot__["a" /* Forgot */]('');
            this.socialRegister = new __WEBPACK_IMPORTED_MODULE_16__socialRegister__["a" /* SocialRegister */]('', '', '', '', '');
            if (dataResponse.data.user.credit_card[0]) {
                this._cookieService.put('credit_default', dataResponse.data.user.credit_card[0].default);
                this._cookieService.put('credit_franchise', dataResponse.data.user.credit_card[0].franchise);
                this._cookieService.put('credit_last_digits', dataResponse.data.user.credit_card[0].last_digits);
            }
            this._cookieService.put('pay_type', dataResponse.data.user.pay);
            if (window.location.hash === '#/services') {
                // console.log(this);
                __WEBPACK_IMPORTED_MODULE_8__services_services_component__["a" /* ServicesComponent */].prototype.loginSuccess();
                // console.log(ServiceComponent.prototype);
                // this._ServiceComponent.loginSuccess();
            }
            if (dataResponse.data.user.phone.trim().length < 1 || dataResponse.data.user.identify.trim().length < 1) {
                this.completeDataUser = true;
                this.showChildModalCompleteData();
            }
            // location.reload();
        }
        else if (!dataResponse.return && dataResponse.message === 'Email requerido') {
            // console.log("Email requeridoooooooo");
            // if ( this.dataUserLogin.email == null ) {
            document.getElementById('btnViewEmail').click();
            this.viewEmail = true;
            // } else {
            //   this.showChildModalCompleteDataUserSocialEmailSave();
            // }
        }
        else {
            this.messageLogin = dataResponse.message;
        }
    };
    AppComponent.prototype.loginUserFacebook = function (f) {
        var _this = this;
        this.loadingProcess = true;
        if (this.typeRegisterSocial === 'google') {
            this._LoginComponent.loginUserSocial(this.accessToken, this._ConstantUbeep.urlServer, f.value.id_agreement, f.value.code_agreement, f.value.cellPhone, f.value.identify).subscribe(function (data) { return _this.postMyCarToServer = data; }, function (error) { return _this.loginError(_this.postMyCarToServer); }, function () { return _this.loginSuccess(_this.postMyCarToServer, f); });
        }
        else {
            this._LoginComponent.loginUserFacebook(f.value.emailLogin, this.accessToken, this._ConstantUbeep.urlServer, f.value.id_agreement, f.value.code_agreement, f.value.cellPhone, f.value.identify).subscribe(function (data) { return _this.postMyCarToServer = data; }, function (error) { return _this.loginError(_this.postMyCarToServer); }, function () { return _this.loginSuccess(_this.postMyCarToServer, f); });
        }
    };
    AppComponent.prototype.loginError = function (data) {
        // console.log("LOGIN RROR", data);
        this.loadingProcess = false;
        this.messageLogin = "Credenciales incorrectas";
        this.loadingFacebookProcess = false;
        this.loadingFacebookProcess = false;
        this.loadingFacebookGoogle = false;
        this.loadingProcess = false;
    };
    // Cerrar sesion
    AppComponent.prototype.logOutUser = function (type) {
        if (type === 'si') {
            this._cookieService.removeAll();
            this.stringNavLogin = 'Ingresar';
            this.statusLogin = false;
            this.titleModal = 'Iniciar Sesión';
            this.login = true;
            this.register = false;
            this.forgotpassword = false;
            this.modalLogUot.hide();
            this.registerUserData = new __WEBPACK_IMPORTED_MODULE_17__userRegister__["a" /* RegisterUser */]();
            this._routerModule.navigateByUrl('home');
            // location.reload();
        }
        else {
            this.modalLogUot.hide();
        }
    };
    // Recordar contraseña
    AppComponent.prototype.forgotPassword = function (f) {
        var _this = this;
        this.loadingProcess = true;
        this._httpForgotService.forgotAcount(f.value.emailForgot, this._ConstantUbeep.urlServer).subscribe(function (data) { return _this.postMyCarToServer = JSON.stringify(data); }, function (error) { return console.log(""); }, function () { return _this.forgotSuccess(_this.postMyCarToServer, f, _this.staticModal, _this.messageForgot, _this.titleModal, _this.login, _this.register, _this.forgotpassword); });
    };
    AppComponent.prototype.forgotSuccess = function (data, f, modal, messageForgot, titleModal, login, register, forgotpassword) {
        this.loadingProcess = false;
        var dataResponse = JSON.parse(data);
        if (dataResponse.return) {
            // alert("logeado");
            this.messageForgot = dataResponse.message;
            // setTimeout(function() {
            // setTimeout( function() {
            f.reset();
            titleModal = 'Iniciar Sesión';
            login = true;
            register = false;
            forgotpassword = false;
            messageForgot = 'Escribe tu dirección de correo electrónico para reestablecer tu contraseña';
            this.dataLogin = new __WEBPACK_IMPORTED_MODULE_11__login__["a" /* Login */]('', '');
            this.dataRegister = new __WEBPACK_IMPORTED_MODULE_12__register__["a" /* Register */]('', '', '', '', '');
            this.dataForgot = new __WEBPACK_IMPORTED_MODULE_13__forgot__["a" /* Forgot */]('');
            // }, 0);
            // modal.hide();
            // }, 4000);
            // Redirecciona al home de usuario
            // this._routerModule.navigateByUrl('solicitar');
        }
        else {
            if (dataResponse.message === 'Trying to get property of non-object') {
                this.messageForgot = 'El usuario no aparece en nuestro sistema';
            }
            else {
                this.messageForgot = dataResponse.message;
            }
        }
    };
    // Google Login y Logout
    AppComponent.prototype.initClient = function () {
        var thisClass = this;
        gapi.client.init({
            apiKey: 'AIzaSyAMGlA0FkhU_w_l-C7aBM7HPPmtTPT2F_w',
            discoveryDocs: ['https://people.googleapis.com/$discovery/rest?version=v1'],
            clientId: '633339172985-i1h8150vqaavsnbeldhefu63h2h4ntgn.apps.googleusercontent.com',
            scope: 'profile'
        }).then(function () {
            // console.log("jkadsjkaku");
            // Listen for sign-in state changes.
            gapi.auth2.getAuthInstance().isSignedIn.listen(thisClass.updateSigninStatus);
            // Handle the initial sign-in state.
            thisClass.updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
        });
    };
    AppComponent.prototype.updateSigninStatus = function (isSignedIn) {
        var thisClass = this;
        if (isSignedIn) {
            thisClass.makeApiCall();
        }
    };
    AppComponent.prototype.makeApiCall = function () {
        var thisClass = this;
        gapi.client.people.people.get({
            resourceName: 'people/me'
        }).then(function (resp) {
            // console.log('Hello, ' + resp.result.names[0].givenName);
        }, function (reason) {
            // console.log('Error: ' + reason.result.error.message);
        });
    };
    AppComponent.prototype.handleAuthClick = function (event, type) {
        var thisClass = this;
        this.loadingFacebookGoogle = true;
        gapi.auth2.getAuthInstance().signIn().then(function (data) {
            // data.Zi[0].id_token
            // console.log(data);
            if (data.Zi.id_token !== undefined) {
                var dataLogin = {
                    value: {
                        emailLogin: '',
                        passwordLogin: '',
                        google_id: '',
                        name: ''
                    }
                };
                dataLogin.value.emailLogin = data.w3.U3;
                dataLogin.value.passwordLogin = SHA256(data.Zi.id_token);
                dataLogin.value.google_id = data.w3.Eea;
                dataLogin.value.name = data.w3.ig;
                thisClass.loginUser(data.Zi.id_token, false, 'google');
            }
            else {
                thisClass.loadingFacebookGoogle = false;
                // console.log("Error login google");
            }
        });
    };
    AppComponent.prototype.handleSignoutClick = function (event) {
        gapi.auth2.getAuthInstance().signOut();
    };
    //---Facebook Login y Logout
    AppComponent.prototype.onFacebookCheck = function (type) {
        var thisClass = this;
        this.loadingFacebookProcess = true;
        FB.login(function (data) {
            // console.log(data);
            FB.getLoginStatus(function (response) {
                // console.log(response);
                if (response.status == "connected") {
                    thisClass.getDataUserFacebook(thisClass, type, response);
                }
                else if (response.status == "not_authorized") {
                    thisClass.loadingFacebookProcess = false;
                    thisClass.loadingProcess = false;
                    thisClass.loadingFacebookProcess = false;
                    thisClass.loadingFacebookProcess = false;
                    thisClass.loadingFacebookGoogle = false;
                    thisClass.loadingProcess = false;
                }
                else {
                    thisClass.loadingFacebookProcess = false;
                    thisClass.loadingProcess = false;
                    thisClass.loadingFacebookProcess = false;
                    thisClass.loadingFacebookProcess = false;
                    thisClass.loadingFacebookGoogle = false;
                    thisClass.loadingProcess = false;
                }
            });
        }, { scope: 'email' });
    };
    AppComponent.prototype.getDataUserFacebook = function (thisClass, type, responseStatusLogin) {
        // console.log(responseStatusLogin);
        var url = '/me?fields=name,email';
        FB.api(url, function (userInfo) {
            // console.log(userInfo);
            if (type == "login") {
                setTimeout(function () {
                    thisClass.dataUserLogin.token = responseStatusLogin.authResponse.accessToken;
                    thisClass.dataUserLogin.email = userInfo.email;
                    thisClass.socialRegister.email = userInfo.email;
                    thisClass.loginUser(responseStatusLogin.authResponse.accessToken, false, 'facebook');
                }, 3000);
            }
        });
    };
    //---Dropdown
    AppComponent.prototype.toggled = function (open) {
        //console.log('Dropdown is now: ', open);
    };
    AppComponent.prototype.toggleDropdown = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        this.status.isopen = !this.status.isopen;
    };
    //---Collapsed
    AppComponent.prototype.collapsed = function (event) {
        //console.log(event);
    };
    AppComponent.prototype.expanded = function (event) {
        //console.log(event);
    };
    //---Modal
    AppComponent.prototype.changeContentModal = function (type) {
        if (type == 'register') {
            this.titleModal = 'Registrarse';
            this.messageForgot = "Escribe tu dirección de correo electrónico para reestablecer tu contraseña";
            this.login = false;
            this.register = true;
            this.forgotpassword = false;
            this.messageRegister = "";
            this.loadingFacebookProcess = false;
            this.loadingFacebookGoogle = false;
            this.loadingProcess = false;
        }
        else if (type == 'forgotpassword') {
            this.titleModal = 'Recordar contraseña';
            this.messageForgot = "Escribe tu dirección de correo electrónico para reestablecer tu contraseña";
            this.login = false;
            this.register = false;
            this.forgotpassword = true;
            this.messageRegister = "";
            this.loadingFacebookProcess = false;
            this.loadingFacebookGoogle = false;
            this.loadingProcess = false;
        }
        else if (type == 'login') {
            this.titleModal = 'Iniciar Sesión';
            this.messageForgot = "Escribe tu dirección de correo electrónico para reestablecer tu contraseña";
            this.login = true;
            this.register = false;
            this.forgotpassword = false;
            this.messageRegister = "";
            this.loadingFacebookProcess = false;
            this.loadingFacebookGoogle = false;
            this.loadingProcess = false;
        }
    };
    AppComponent.prototype.showChildModal = function () {
        this.loadingFacebookProcess = false;
        this.loadingFacebookGoogle = false;
        this.loadingProcess = false;
        this.staticModal.show();
        this.socialRegister = new __WEBPACK_IMPORTED_MODULE_16__socialRegister__["a" /* SocialRegister */]("", "", "", "", "");
        this.dataLogin = new __WEBPACK_IMPORTED_MODULE_11__login__["a" /* Login */]("", "");
        this.dataRegister = new __WEBPACK_IMPORTED_MODULE_12__register__["a" /* Register */]("", "", "", "", "");
        this.dataForgot = new __WEBPACK_IMPORTED_MODULE_13__forgot__["a" /* Forgot */]("");
    };
    AppComponent.prototype.showChildModalCompleteData = function () {
        this.loadingProcess = false;
        this.dataUser = new __WEBPACK_IMPORTED_MODULE_14__data_user__["a" /* DataUser */]("", "");
        this.modalCompleteDataUser.show();
    };
    AppComponent.prototype.hideChildModalCompleteData = function () {
        this.loadingProcess = false;
        this.registerUserData = new __WEBPACK_IMPORTED_MODULE_17__userRegister__["a" /* RegisterUser */]();
        this.dataUser = new __WEBPACK_IMPORTED_MODULE_14__data_user__["a" /* DataUser */]("", "");
        this.modalCompleteDataUser.hide();
    };
    AppComponent.prototype.showChildModalCompleteDataSocial = function () {
        this.loadingProcess = false;
        this.dataUser = new __WEBPACK_IMPORTED_MODULE_14__data_user__["a" /* DataUser */]("", "");
        this.modalCompleteDataUserSocial.show();
    };
    AppComponent.prototype.hideChildModalCompleteDataSocial = function () {
        this.loadingProcess = false;
        this.dataUser = new __WEBPACK_IMPORTED_MODULE_14__data_user__["a" /* DataUser */]("", "");
        this.registerUserData = new __WEBPACK_IMPORTED_MODULE_17__userRegister__["a" /* RegisterUser */]();
        this.modalCompleteDataUserSocial.hide();
    };
    AppComponent.prototype.showChildModalCompleteDataUserSocialEmailSave = function () {
        this.loadingProcess = false;
        this.dataUser = new __WEBPACK_IMPORTED_MODULE_14__data_user__["a" /* DataUser */]("", "");
        this.modalCompleteDataUserSocialEmailSave.show();
    };
    AppComponent.prototype.hideChildModalCompleteDataUserSocialEmailSave = function () {
        this.loadingProcess = false;
        this.dataUser = new __WEBPACK_IMPORTED_MODULE_14__data_user__["a" /* DataUser */]("", "");
        this.registerUserData = new __WEBPACK_IMPORTED_MODULE_17__userRegister__["a" /* RegisterUser */]();
        this.modalCompleteDataUserSocialEmailSave.hide();
    };
    AppComponent.prototype.completeData = function (f) {
        var _this = this;
        this.loadingProcess = true;
        // console.log(f.value.cellPhone, f.value.identify, f.value.id_agreement, f.value.code_agreement);
        this._HttpProfile.updateService(this._cookieService.get('id'), this._cookieService.get('name'), this._cookieService.get('email'), '', f.value.cellPhone, f.value.identify).subscribe(function (data) { return _this.postServiceCompleteData = JSON.stringify(data); }, function (error) { return _this.responseCompleteData(_this.postServiceCompleteData, f); }, function () { return _this.responseCompleteData(_this.postServiceCompleteData, f); });
    };
    AppComponent.prototype.completeDataSocial = function (f, type) {
        var _this = this;
        this.loadingProcess = true;
        if (type === 'emailSave') {
            this._LoginComponent.loginUserFacebook(this.dataUserLogin.email, this.dataUserLogin.token, this._ConstantUbeep.urlServer, f.value.id_agreement, f.value.code_agreement, f.value.cellPhone, f.value.identify).subscribe(function (data) { return _this.postMyCarToServer = data; }, function (error) { return _this.loginError(_this.postMyCarToServer); }, function () { return _this.loginSuccess(_this.postMyCarToServer, f); });
        }
        else {
            this._LoginComponent.loginUserSocial(this.accessToken, this._ConstantUbeep.urlServer, f.value.id_agreement, f.value.code_agreement, f.value.cellPhone, f.value.identify).subscribe(function (data) { return _this.postMyCarToServer = data; }, function (error) { return _this.loginError(_this.postMyCarToServer); }, function () { return _this.loginSuccess(_this.postMyCarToServer, f); });
        }
    };
    AppComponent.prototype.responseCompleteDataSocial = function (data, f) {
        var dataResponse = JSON.parse(data);
        // console.log(dataResponse);
        var thisClass = this;
        this.loadingProcess = false;
        if (dataResponse.return) {
            this._cookieService.put('movil_phone', f.value.cellPhone);
            this._cookieService.put('identify', f.value.identify);
            this.messageCompleteData = "Los datos se han completado";
            //f.reset();
            setTimeout(function () {
                // console.log(thisClass);
                thisClass.dataUser = new __WEBPACK_IMPORTED_MODULE_14__data_user__["a" /* DataUser */]("", "");
                thisClass.messageCompleteData = "";
                thisClass.hideChildModalCompleteDataSocial();
            }, 4000);
        }
        else {
            // console.log(thisClass);
            thisClass.messageCompleteData = "Verifique los campos e intente nuevamente";
        }
    };
    AppComponent.prototype.responseCompleteData = function (data, f) {
        var dataResponse = JSON.parse(data);
        // console.log(dataResponse);
        var thisClass = this;
        this.loadingProcess = false;
        if (dataResponse.return) {
            this._cookieService.put('movil_phone', f.value.cellPhone);
            this._cookieService.put('identify', f.value.identify);
            this.messageCompleteData = "Los datos se han completado";
            //f.reset();
            setTimeout(function () {
                // console.log(thisClass);
                thisClass.dataUser = new __WEBPACK_IMPORTED_MODULE_14__data_user__["a" /* DataUser */]("", "");
                thisClass.messageCompleteData = "";
                thisClass.hideChildModalCompleteData();
            }, 4000);
        }
        else {
            // console.log(thisClass);
            thisClass.messageCompleteData = "Verifique los campos e intente nuevamente";
        }
    };
    AppComponent.prototype.showChildModalRegisterInvite = function () {
        this.titleModal = 'Registrarse';
        this.messageForgot = "Escribe tu dirección de correo electrónico para reestablecer tu contraseña";
        this.messageRegister = "";
        this.messageLogin = "";
        this.login = false;
        this.register = true;
        this.forgotpassword = false;
        this.viewEmail = false;
        this.loadingFacebookProcess = false;
        this.loadingFacebookGoogle = false;
        this.loadingProcess = false;
        this.staticModal.show();
        this.dataLogin = new __WEBPACK_IMPORTED_MODULE_11__login__["a" /* Login */]("", "");
        this.dataRegister = new __WEBPACK_IMPORTED_MODULE_12__register__["a" /* Register */]("", "", "", "", "");
        this.dataForgot = new __WEBPACK_IMPORTED_MODULE_13__forgot__["a" /* Forgot */]("");
        this.socialRegister = new __WEBPACK_IMPORTED_MODULE_16__socialRegister__["a" /* SocialRegister */]("", "", "", "", "");
    };
    AppComponent.prototype.hideChildModal = function () {
        this.titleModal = 'Iniciar Sesión';
        this.messageForgot = "Escribe tu dirección de correo electrónico para reestablecer tu contraseña";
        this.messageRegister = "";
        this.messageLogin = "";
        this.login = true;
        this.register = false;
        this.forgotpassword = false;
        this.completeDataUser = false;
        this.socialRegister = new __WEBPACK_IMPORTED_MODULE_16__socialRegister__["a" /* SocialRegister */]("", "", "", "", "");
        this.registerUserData = new __WEBPACK_IMPORTED_MODULE_17__userRegister__["a" /* RegisterUser */]();
        this.staticModal.hide();
        this.viewEmail = false;
        this.loadingFacebookProcess = false;
        this.loadingFacebookGoogle = false;
        this.loadingProcess = false;
        this.viewEmail = false;
        this.dataLogin = new __WEBPACK_IMPORTED_MODULE_11__login__["a" /* Login */]("", "");
        this.dataRegister = new __WEBPACK_IMPORTED_MODULE_12__register__["a" /* Register */]("", "", "", "", "");
        this.dataForgot = new __WEBPACK_IMPORTED_MODULE_13__forgot__["a" /* Forgot */]("");
    };
    AppComponent.prototype.changeRoute = function (route) {
        // console.log(route);
        this.routeChange = route;
        if (window.location.hash == "#/services" && this._cookieService.get('orderFormService') == '3') {
            this.modalConfirmCancelService.show();
        }
        else {
            if (window.location.hash == "#/services") {
                this.modalExitViewServices.show();
            }
            else {
                if (route == "services") {
                    if (this._cookieService.get('id') != undefined) {
                        if (this._cookieService.get("validRequiestService") != "false") {
                            this._routerModule.navigateByUrl(this.routeChange);
                        }
                        else {
                            this.showTimeSocketModal();
                        }
                    }
                    else {
                        //
                        this.showChildModal();
                    }
                }
                else {
                    this._routerModule.navigateByUrl(this.routeChange);
                }
            }
        }
    };
    AppComponent.prototype.confirmCancelService = function (resp) {
        // console.log(resp);
        var thisClass = this;
        if (resp == "si") {
            this.routeChange;
            this.cancelService();
        }
        else if (resp == "no") {
            this.modalConfirmCancelService.hide();
        }
        else {
            this.modalConfirmCancelService.hide();
        }
    };
    AppComponent.prototype.exitViewService = function (resp) {
        // console.log(resp);
        var thisClass = this;
        if (resp == "si") {
            this.modalExitViewServices.hide();
            this.routeChange;
            this._routerModule.navigateByUrl(this.routeChange);
        }
        else if (resp == "no") {
            this.modalExitViewServices.hide();
        }
        else {
            this.modalExitViewServices.hide();
        }
    };
    AppComponent.prototype.cancelService = function () {
        var _this = this;
        // console.log("vamo a cancelar el serv");
        var postCancelService = "";
        // console.log("cancelará el servicio");
        this._HttpService.cancelService(this._cookieService.get('shipping_id')).subscribe(function (data) { return postCancelService = JSON.stringify(data); }, function (error) { return _this.responseCancelService(postCancelService); }, function () { return _this.responseCancelService(postCancelService); });
    };
    AppComponent.prototype.responseCancelService = function (data) {
        // console.log(data);
        var dataPostServiceCancel = JSON.parse(data);
        // console.log(dataPostServiceCancel);
        if (dataPostServiceCancel.return) {
            // console.log(dataPostServiceCancel.message);
            this._cookieService.put('shipping_id', '');
            this._cookieService.put('statusServices', 'false');
            this._routerModule.navigateByUrl(this.routeChange);
            this.modalConfirmCancelService.hide();
        }
        else {
            // console.log("problemas al cancelar servicio");
            this._routerModule.navigateByUrl(this.routeChange);
            this.modalConfirmCancelService.hide();
        }
    };
    //test nueva ventana
    AppComponent.prototype.checkFacebook = function () {
        window.open('https://www.facebook.com/login.php?skip_api_login=1&api_key=555091548022084&signed_next=1&next=https%3A%2F%2Fwww.facebook.com%2Fv2.8%2Fdialog%2Foauth%3Fredirect_uri%3Dhttp%253A%252F%252Fstaticxx.facebook.com%252Fconnect%252Fxd_arbiter%252Fr%252FD6ZfFsLEB4F.js%253Fversion%253D42%2523cb%253Df1eb560c3ad9428%2526domain%253Ddevelop.ubeep.co%2526origin%253Dhttp%25253A%25252F%25252Fdevelop.ubeep.co%25252Ff3260b47f6025e8%2526relation%253Dopener%2526frame%253Df54b9b3e521e8%26display%3Dpopup%26scope%3Demail%26response_type%3Dtoken%252Csigned_request%26domain%3Ddevelop.ubeep.co%26origin%3D1%26client_id%3D555091548022084%26ret%3Dlogin%26sdk%3Djoey%26logger_id%3D691cc260-fd5b-4e8f-8037-9eb677d9acf6&cancel_url=http%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2FD6ZfFsLEB4F.js%3Fversion%3D42%23cb%3Df1eb560c3ad9428%26domain%3Ddevelop.ubeep.co%26origin%3Dhttp%253A%252F%252Fdevelop.ubeep.co%252Ff3260b47f6025e8%26relation%3Dopener%26frame%3Df54b9b3e521e8%26error%3Daccess_denied%26error_code%3D200%26error_description%3DPermissions%2Berror%26error_reason%3Duser_denied%26e2e%3D%257B%257D&display=popup&locale=en_US&logger_id=691cc260-fd5b-4e8f-8037-9eb677d9acf6', 'popup', 'width=300,height=400');
    };
    AppComponent.prototype.setTimeoutServiceToken = function () {
        var thisClass = this;
        setTimeout(function () {
            // console.log(thisClass);
            // console.log(CookieService.prototype.get("validRequiestService"));
            __WEBPACK_IMPORTED_MODULE_2_angular2_cookie_services_cookies_service__["CookieService"].prototype.put("validRequiestService", "true");
        }, 180000);
    };
    AppComponent.prototype.showTimeSocketModal = function () {
        this.timeSocketModal.show();
    };
    AppComponent.prototype.hideTimeSocketModal = function () {
        this.timeSocketModal.hide();
    };
    return AppComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('staticModal'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__["ModalDirective"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__["ModalDirective"]) === "function" && _a || Object)
], AppComponent.prototype, "staticModal", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('modalLogUot'),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__["ModalDirective"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__["ModalDirective"]) === "function" && _b || Object)
], AppComponent.prototype, "modalLogUot", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('modalConfirmCancelService'),
    __metadata("design:type", typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__["ModalDirective"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__["ModalDirective"]) === "function" && _c || Object)
], AppComponent.prototype, "modalConfirmCancelService", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('modalExitViewServices'),
    __metadata("design:type", typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__["ModalDirective"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__["ModalDirective"]) === "function" && _d || Object)
], AppComponent.prototype, "modalExitViewServices", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('timeSocketModal'),
    __metadata("design:type", typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__["ModalDirective"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__["ModalDirective"]) === "function" && _e || Object)
], AppComponent.prototype, "timeSocketModal", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('modalCompleteDataUser'),
    __metadata("design:type", typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__["ModalDirective"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__["ModalDirective"]) === "function" && _f || Object)
], AppComponent.prototype, "modalCompleteDataUser", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('modalCompleteDataUserSocial'),
    __metadata("design:type", typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__["ModalDirective"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__["ModalDirective"]) === "function" && _g || Object)
], AppComponent.prototype, "modalCompleteDataUserSocial", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('modalCompleteDataUserSocialEmailSave'),
    __metadata("design:type", typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__["ModalDirective"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__["ModalDirective"]) === "function" && _h || Object)
], AppComponent.prototype, "modalCompleteDataUserSocialEmailSave", void 0);
AppComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-root',
        template: __webpack_require__(506),
        styles: [__webpack_require__(460)],
        providers: [__WEBPACK_IMPORTED_MODULE_5__assets_services_auth_forgot_forgot_component__["a" /* HttpForgotService */], __WEBPACK_IMPORTED_MODULE_6__assets_services_auth_register_register_component__["a" /* HttpRegisterService */], __WEBPACK_IMPORTED_MODULE_7__assets_services_auth_login_login_component__["a" /* LoginComponent */], __WEBPACK_IMPORTED_MODULE_9__services_http_service__["a" /* HttpService */], __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__["TooltipModule"], __WEBPACK_IMPORTED_MODULE_10__profile_profile_service__["a" /* HttpProfile */]]
    }),
    __metadata("design:paramtypes", [typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"]) === "function" && _j || Object, typeof (_k = typeof __WEBPACK_IMPORTED_MODULE_2_angular2_cookie_services_cookies_service__["CookieService"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_angular2_cookie_services_cookies_service__["CookieService"]) === "function" && _k || Object, typeof (_l = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */]) === "function" && _l || Object, typeof (_m = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["Compiler"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["Compiler"]) === "function" && _m || Object, typeof (_o = typeof __WEBPACK_IMPORTED_MODULE_5__assets_services_auth_forgot_forgot_component__["a" /* HttpForgotService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__assets_services_auth_forgot_forgot_component__["a" /* HttpForgotService */]) === "function" && _o || Object, typeof (_p = typeof __WEBPACK_IMPORTED_MODULE_6__assets_services_auth_register_register_component__["a" /* HttpRegisterService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__assets_services_auth_register_register_component__["a" /* HttpRegisterService */]) === "function" && _p || Object, typeof (_q = typeof __WEBPACK_IMPORTED_MODULE_7__assets_services_auth_login_login_component__["a" /* LoginComponent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__assets_services_auth_login_login_component__["a" /* LoginComponent */]) === "function" && _q || Object, typeof (_r = typeof __WEBPACK_IMPORTED_MODULE_9__services_http_service__["a" /* HttpService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_9__services_http_service__["a" /* HttpService */]) === "function" && _r || Object, typeof (_s = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["c" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["c" /* ActivatedRoute */]) === "function" && _s || Object, typeof (_t = typeof __WEBPACK_IMPORTED_MODULE_15__constantsUbeep__["a" /* ConstantUbeep */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_15__constantsUbeep__["a" /* ConstantUbeep */]) === "function" && _t || Object, typeof (_u = typeof __WEBPACK_IMPORTED_MODULE_10__profile_profile_service__["a" /* HttpProfile */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_10__profile_profile_service__["a" /* HttpProfile */]) === "function" && _u || Object])
], AppComponent);

var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p, _q, _r, _s, _t, _u;
//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 76:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "tengoUnVehiculoSelected.b352ea37e70aba42d242.png";

/***/ }),

/***/ 779:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "GothamRnd-Medium.4518b6f067e234d62330.otf";

/***/ }),

/***/ 780:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "background.b810231c10cef4eb226d.png";

/***/ }),

/***/ 781:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "backgroundConfirmacion.3ddfbb5fa7ca948b23c4.png";

/***/ }),

/***/ 782:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "backgroundInvite.54a934f9ad4e6b2aec4a.png";

/***/ }),

/***/ 783:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "backgroundRegisterCompany.c64279925b4ce11ebc6e.png";

/***/ }),

/***/ 784:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "banner.672228e70329f7fbd73e.png";

/***/ }),

/***/ 785:
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAUIAAABJCAYAAABIHS/rAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABZ0RVh0Q3JlYXRpb24gVGltZQAxNC8xMC8xNusgfikAAAAcdEVYdFNvZnR3YXJlAEFkb2JlIEZpcmV3b3JrcyBDUzbovLKMAAACSklEQVR4nO3awW3TYBjH4X+8ANmg3oCkC1DOqdSOwAZlA8IGdAPYIEjNGU/QlA2cDbpBOKQOVaHiRAx9n0fKIf4c6T399DmfJ3lGv5i3SS4fPrMkr567F+AftU3SJena9ebz08XdbpckmTxd6BfzaZJlkqu/OR3AkW2TLB8H8bch7BfzWZJVkpMjDgdwTF+TvGvXm/shhM2w8hDBLiIIvGwX2bfuoEkOj8Or+B8QqOF1v5h/Gr4MO8Jl7ASBWq6256dnSdI8nA47GAEqWib7HeHluHMAjObN9vy0FUKgurMm+5elAapqmzgpBmqbNX++B+BFmwohUJ4QAuUJIVCeEALlCSFQnhAC5QkhUJ4QAuUJIVCeEALlCSFQnhAC5QkhUJ4QAuUJIVCeEALlCSFQnhAC5QkhUJ4QAuUJIVCeEALlCSFQnhAC5QkhUJ4QAuUJIVCeEALlCSFQnhAC5QkhUJ4QAuUJIVCeEALlCSFQnhAC5QkhUJ4QAuUJIVCeEALlCSFQnhAC5QkhUJ4QAuUJIVCeEALlCSFQnhAC5QkhUJ4QAuUJIVDdfZPkfuwpAEZ01yT5PvYUACPqmySrsacAGFEnhEBl3cnNbd+0602f5HrsaQBG8DH5eWq8TNKPNQnACK5Pbm67JJkMV/rFfJbkW5LpSEMBHMtdu97Md7tdkkfvEbbrzV2St7EzBF62VfatO5g8vaNfzKdJPiR5f6ShAI6hT7Js15svw4VhR/hLCA+/WMzbJBdJLpPM4pEZ+P/0Sbok3eMADoYQ/gB4oF3A1uCSoAAAAABJRU5ErkJggg=="

/***/ }),

/***/ 786:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "flotas.d9039a37cf29faaa2f00.png";

/***/ }),

/***/ 789:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(346);


/***/ })

},[789]);
//# sourceMappingURL=main.bundle.js.map