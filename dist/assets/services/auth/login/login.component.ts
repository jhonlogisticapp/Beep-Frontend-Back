import { Component, ViewChild, ViewContainerRef, Injectable, Compiler, OnInit } from '@angular/core';
import { Ng2BootstrapModule, ModalDirective }                                   from 'ng2-bootstrap/ng2-bootstrap';
import { Http, Response, Headers, RequestOptions }                              from '@angular/http'
import { Observable }                                                           from 'rxjs/Rx';
import { Router }                                                               from '@angular/router';
import 'rxjs/Rx';
import { NgModel }                                                              from '@angular/forms';
import { CookieService }                                                        from 'angular2-cookie/services/cookies.service';
//import { HttpLoginService }                                                     from "./services/login";
import { ServicesComponent }                                                     from './../../../../app/services/services.component';

declare const FB:any;
declare const gapi:any;
declare const SHA256:any;


@Component({
	providers: [  ]
})

@Injectable()
export class LoginComponent {

	private viewContainerRef: ViewContainerRef;
	public _cookieService: CookieService;
	private _routerModule: Router;
	public _ServiceComponent: ServicesComponent;
	postServiceLogin : string;
	private _urlLogin:string = "assets/services/auth/login/services/login.service.php"; //ok
	private _urlLoginSocial:string = "assets/services/auth/login/services/login.social.service.php"; //ok
	private _urlLoginFacebook:string = "assets/services/auth/login/services/login.facebook.php"; //ok
	private _urlLoginCheck:string = "assets/services/auth/login/services/login.check.user.php";

	constructor( private _http: Http)  {

	}

	//---Login de usuario normal
	loginUser (email:string, password:string, url: string){
		let data = JSON.stringify({
			key:"3edcdb20e0030daab21d0ba9af4c0dc2",
			email: email,
			pass: password,
			platform: "web",
			url: url
	    });

	    // console.log("Datos del login nuevo!");
	    // console.log(data);

	    let headers = new Headers({ "Access-Control-Allow-Origin": "*" });
	    let options = new RequestOptions({headers: headers, method: "POST" });

	    return this._http.post(this._urlLogin, data, options)
	        .map(this.loginSuccess)
	        .catch(this.handleError);
  	}


  loginUserSocial (password:string, url: string, id_agreement: string, code_agreement: string, cellPhoneRegister: string, identifyRegister: string){
		// console.log(password, url, id_agreement, code_agreement, cellPhoneRegister, identifyRegister);
		let data = JSON.stringify({
			key:"3edcdb20e0030daab21d0ba9af4c0dc2",
			pass: password,
			platform: "web",
			url: url,
			id_agreement: id_agreement,
			code_agreement: code_agreement,
			cellphone: cellPhoneRegister,
			identify: identifyRegister
	    });

	    // console.log("Datos del login nuevo!");
	    // console.log(data);

	    let headers = new Headers({ "Access-Control-Allow-Origin": "*" });
	    let options = new RequestOptions({headers: headers, method: "POST" });

	    return this._http.post(this._urlLoginSocial, data, options)
	        .map(this.loginSuccess)
	        .catch(this.handleError);
  }

	loginCheckUserSocial (password:string, url: string){
		let data = JSON.stringify({
			key:"3edcdb20e0030daab21d0ba9af4c0dc2",
			pass: password,
			platform: "web",
			url: url
		});

		// console.log("Datos del login nuevo!");
		// console.log(data);

		let headers = new Headers({ "Access-Control-Allow-Origin": "*" });
		let options = new RequestOptions({headers: headers, method: "POST" });

		return this._http.post(this._urlLoginCheck, data, options)
            .map(this.loginSuccess)
            .catch(this.handleError);
	}

	loginUserFacebook (email:string, password:string, url: string, id_agreement: string, code_agreement: string, cellPhoneRegister: string, identifyRegister: string){
		let data = JSON.stringify({
			key:"3edcdb20e0030daab21d0ba9af4c0dc2",
			email: email,
			pass: password,
			platform: "web",
			url: url,
			id_agreement: id_agreement,
			code_agreement: code_agreement,
			cellphone: cellPhoneRegister,
			identify: identifyRegister,
	    });

	    // console.log("Datos del login nuevo!");
	    // console.log(data);

	    let headers = new Headers({ "Access-Control-Allow-Origin": "*" });
	    let options = new RequestOptions({headers: headers, method: "POST" });

	    return this._http.post(this._urlLoginFacebook, data, options)
	        .map(this.loginSuccess)
	        .catch(this.handleError);
  	}

	loginSuccess(res: Response){
		// console.log(this);
		var data = res.json();
		// console.log(data);
		//return true;
		//this.loadingProcess = false;
		//let dataResponse = JSON.parse(data);
		let dataResponse = data;
		// console.log(dataResponse);

		if(dataResponse.return&&dataResponse.message==="Usuario logueado exitosamente"){
			//alert("logeado");
			CookieService.prototype.put('statusServices', 'false');
			CookieService.prototype.put('id',dataResponse.data.user.id);
			CookieService.prototype.put('name',dataResponse.data.user.name);
			CookieService.prototype.put('movil_phone',dataResponse.data.user.phone);
			CookieService.prototype.put('social_id',dataResponse.data.user.social_id);
			CookieService.prototype.put('identify',dataResponse.data.user.identify);
			CookieService.prototype.put('email',dataResponse.data.user.email);
			// console.log(dataResponse.data.user.socket);

			if(dataResponse.data.user.socket!=null){

				let sockets = (dataResponse.data.user.socket).split(",");

				// console.log(sockets);
				CookieService.prototype.put('socket1',sockets[0]);
				CookieService.prototype.put('socket2',sockets[1]);
				CookieService.prototype.put('socket3',sockets[2]);
			}


			//CookieService.prototype.put('local_phone', dataResponse.data.user.email);
			CookieService.prototype.put('service_provider_id_1',dataResponse.data.menu[0].service_provider_id);
			CookieService.prototype.put('type_service_1',dataResponse.data.menu[0].type_service);
			CookieService.prototype.put('title_1',dataResponse.data.menu[0].title);
			CookieService.prototype.put('text_description_1',dataResponse.data.menu[0].text_description);
			CookieService.prototype.put('bag_services_1',dataResponse.data.menu[0].bag_services.length);
			CookieService.prototype.put('shipping_bag_id_1',dataResponse.data.menu[0].bag_services[0].shipping_bag_id);
			CookieService.prototype.put('title_shipping_bag_id_1',dataResponse.data.menu[0].bag_services[0].title);
			CookieService.prototype.put('subtitle_shipping_bag_id_1',dataResponse.data.menu[0].bag_services[0].subtitle);
			CookieService.prototype.put('shipping_bag_id_2',dataResponse.data.menu[0].bag_services[1].shipping_bag_id);
			CookieService.prototype.put('title_shipping_bag_id_2',dataResponse.data.menu[0].bag_services[1].title);
			CookieService.prototype.put('subtitle_shipping_bag_id_2',dataResponse.data.menu[0].bag_services[1].subtitle);
			CookieService.prototype.put('shipping_bag_id_3',dataResponse.data.menu[0].bag_services[2].shipping_bag_id);
			CookieService.prototype.put('title_shipping_bag_id_3',dataResponse.data.menu[0].bag_services[2].title);
			CookieService.prototype.put('subtitle_shipping_bag_id_3',dataResponse.data.menu[0].bag_services[2].subtitle);
			CookieService.prototype.put('service_provider_id_2',dataResponse.data.menu[1].service_provider_id);
			CookieService.prototype.put('type_service_2',dataResponse.data.menu[1].type_service);
			CookieService.prototype.put('title_2',dataResponse.data.menu[1].title);
			CookieService.prototype.put('text_description_2',dataResponse.data.menu[1].text_description);
			CookieService.prototype.put('bag_services_2',dataResponse.data.menu[1].bag_services.length);
			CookieService.prototype.put('shipping_bag_id_4',dataResponse.data.menu[1].bag_services[0].shipping_bag_id);
			CookieService.prototype.put('title_shipping_bag_id_4',dataResponse.data.menu[1].bag_services[0].title);
			CookieService.prototype.put('subtitle_shipping_bag_id_4',dataResponse.data.menu[1].bag_services[0].subtitle);
			if(dataResponse.data.user.credit != undefined ){
				CookieService.prototype.put('credit', dataResponse.data.user.credit);
			}
			if(CookieService.prototype.get('name')!=undefined&&CookieService.prototype.get('id')!=undefined){
				let nameUser = (CookieService.prototype.get('name')).split(" ");
			}
			// console.log(res.json());

			//res => res.json();
			return res.json();
	    } else if(!dataResponse.return&&dataResponse.message==="Email requerido"){
	    	return {return: false, message: dataResponse.message};
	    } else{
			return {return: false, message: "Credenciales incorrectas"};
	    }
	}

	private handleError (error: Response) {
		// console.error(error);
		return Observable.throw(error.json().error || ' error');
	}

}
