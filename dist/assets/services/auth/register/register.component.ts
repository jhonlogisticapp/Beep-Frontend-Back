import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Headers, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Injectable()
export class HttpRegisterService {

    private _urlLogin: string = "assets/services/auth/register/services/register.service.php";
    private _urlInitialSetup: string = "assets/services/auth/register/services/initial.service.php";

    constructor(private _http: Http) {
    }

    registerAcount(userNameRegister: string, passwordRegister: string, emailRegister: string, cellPhoneRegister: string, identifyRegister: string, url: string, id_agreement: string, code_agreement: string) {

        let data = JSON.stringify({
            key: "3edcdb20e0030daab21d0ba9af4c0dc2",
            name: userNameRegister,
            email: emailRegister,
            pass: passwordRegister,
            cellphone: cellPhoneRegister,
            identify: identifyRegister,
            platform: "web",
            url: url,
            id_agreement: id_agreement,
            code_agreement: code_agreement
        });

        // console.log("Datos del registrooooooooooooooooooo");
        console.log(data);

        let headers = new Headers({"Access-Control-Allow-Origin": "*"});
        let options = new RequestOptions({headers: headers, method: "POST"});

        return this._http.post(this._urlLogin, data, options)
            .map(res => res.json())
            .catch(this.handleError);
    }

    initialSetup(url: string) {

        let data = JSON.stringify({
            key: "3edcdb20e0030daab21d0ba9af4c0dc2",
            url: url
        });

        // console.log("trae las empresas");
        // console.log(data);

        let headers = new Headers({"Access-Control-Allow-Origin": "*"});
        let options = new RequestOptions({headers: headers, method: "POST"});

        return this._http.post(this._urlInitialSetup, data, options)
            .map(res => res.json())
            .catch(this.handleError);
    }

    private handleError(error: Response) {
        // console.error(error);
        return Observable.throw(error.json().error || ' error');
    }
}
