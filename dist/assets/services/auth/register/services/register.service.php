<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');

  function sendPost ($url, $data) {



    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $serverOutput = curl_exec ($ch);
    curl_close ($ch);
    echo $serverOutput;


  }

  $postdata = file_get_contents('php://input');
  $request = json_decode($postdata);

  $data = array(
    'key' => $request->key,
    'name' => $request->name,
    'email' => $request->email,
    'pass' => $request->pass,
    'platform' => $request->platform,
    'url' => $request->url,
    'cellphone' => $request->cellphone,
    'identify' => $request->identify,
    'id_agreement' => $request->id_agreement,
    'code_agreement' => $request->code_agreement
  );

  $urlServer = $request->url;

  $url = $urlServer.'api_devel/Client/register';

  sendPost($url, $data);

?>
