<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');

  function sendPost ($url, $data) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $serverOutput = curl_exec ($ch);
    curl_close ($ch);
    echo $serverOutput;
  }

  $postdata = file_get_contents('php://input');
  $request = json_decode($postdata);

  $data = array(
      'key' => $request->key,
      'platform' => $request->platform,
      'client_id' => $request->client_id,
      'quotation_id' => $request->quotation_id
  );

  $urlServer = $request->url;

  //$url = $urlServer.'api_devel/Shipping/quotation';
  $url = $urlServer.'api_devel/Paymentgateway/post';

  sendPost($url, $data);

?>
