<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');

  function sendPost ($url, $data) {



    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $serverOutput = curl_exec ($ch);
    curl_close ($ch);
    echo $serverOutput;


  }

  //$url = 'http://52.43.247.174/api_devel/Shipping/quotation';

  $postdata = file_get_contents('php://input');
  $request = json_decode($postdata);



  $data = array(
    'key' => $request->key,
    'origin_latitude' => $request->origin_latitude,
    'origin_longitude' => $request->origin_longitude,
    'destination_latitude' => $request->destiny_latitude,
    'destination_longitude' => $request->destiny_longitude,
    'type_service' => $request->type_service,
    'bag_id' => $request->bag_id,
    'tip' => $request->tip,
    'destination_address' => $request->destination_address,
    'origin_address' => $request->origin_address,
    'origin_client' => $request->origin_client,
    'pay' => $request->pay
  );

  $urlServer = $request->url;

  //$url = $urlServer.'api_devel/Shipping/quotation';
  $url = $urlServer.'api_devel/request/quotation';

  sendPost($url, $data);

?>
