<?php

    class myclass {

        function socket($portUser){

            //$host = "192.168.0.145";  //Local
            $host = "172.31.21.96";
            $port = $portUser;
            // create socket
            $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP) or die("Could not create socket\n");
            //$socket = socket_create(AF_INET, SOCK_STREAM, 0) or die("Could not create socket\n");
            // bind socket to port
            $result = socket_bind($socket, $host, $port) or die("Could not bind to socket\n");
            // start listening for connections
            $result = socket_listen($socket, 3) or die("Could not set up socket listener\n");

            // accept incoming connections
            // spawn another socket to handle communication
            $spawn = socket_accept($socket) or die("Could not accept incoming connection\n");
            // read client input
            $input = socket_read($spawn, 1024) or die("Could not read input\n");

            $output = "Hola mundo";

            socket_write($spawn, $output, strlen ($output)) or die("Could not write output\n");
            // close sockets
            socket_close($spawn);
            socket_close($socket);

            echo $input;

        }
    }

    $class = new myclass();

    $postdata = file_get_contents('php://input');
    $request = json_decode($postdata);
    $portUser = $request->port;

    $data = $class->socket($portUser);
    //echo "clase".$data;

?>