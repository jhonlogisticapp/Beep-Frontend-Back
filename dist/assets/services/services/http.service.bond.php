<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  function sendPost ($url, $data) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $serverOutput = curl_exec ($ch);
    curl_close ($ch);
    echo $serverOutput;
  }
  //$url = 'http://52.43.247.174/api_devel/Discount/post';
  $postdata = file_get_contents('php://input');
  $request = json_decode($postdata);
  $data = array(
    'key' => $request->key,
    'bond' => $request->bond,
    'client_id' => $request->client_id,
    'amount' => $request->amount,
    'platform' => $request->platform
  );
  $urlServer = $request->url;

  $url = $urlServer.'api_devel/Discount/post';
  sendPost($url, $data);
?>