<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');

  function sendPost ($url, $data) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $serverOutput = curl_exec ($ch);
    curl_close ($ch);
    echo $serverOutput;
  }

  //$url = 'http://52.43.247.174/api_devel/Shipping/quotation';

  $postdata = file_get_contents('php://input');
  $request = json_decode($postdata);

  $data = array(
      'key' => $request->key,
      'platform' => $request->platform,
      'x_card_num' => $request->x_card_num,
      'x_exp_date' => $request->x_exp_date,
      'x_card_code' => $request->x_card_code,
      'client_id' => $request->client_id
  );

  $urlServer = $request->url;

  //$url = $urlServer.'api_devel/Shipping/quotation';
  $url = $urlServer.'api_devel/Paymentgateway/tokenize';

  sendPost($url, $data);
?>
